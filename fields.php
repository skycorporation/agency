<?php
	$fields["status"][1]="Active";
	$fields["status"][0]="Inactive";
	
	$fields["age"][1]="20s";
	$fields["age"][2]="30s";
	$fields["age"][3]="40s";
	$fields["age"][4]="50s+";
	
	$fields["ethnicity"][8]="Caucasian / White";
	$fields["ethnicity"][1]="African-American / Black";
	$fields["ethnicity"][2]="Asian";
	$fields["ethnicity"][3]="Hispanic / Latino";
	$fields["ethnicity"][4]="East Indian";
	$fields["ethnicity"][5]="Native American";
	$fields["ethnicity"][6]="Middle-Eastern";
	$fields["ethnicity"][7]="Multi-Ethnic";
	
	$fields["agency"][1]="Abrams";
	$fields["agency"][2]="CESD";
	$fields["agency"][3]="City";
	$fields["agency"][4]="DBA";
	$fields["agency"][5]="FFT";
	$fields["agency"][6]="Generation";
	$fields["agency"][7]="Glitter";
	$fields["agency"][8]="Independent";
	$fields["agency"][9]="MMG";
	$fields["agency"][10]="NYM";
	$fields["agency"][11]="Ohlsson";
	$fields["agency"][12]="Stefanie";
	$fields["agency"][13]="Stellar";
	$fields["agency"][14]="Take 3";
	$fields["agency"][15]="Teri B";
	$fields["agency"][16]="Tribute";
	$fields["agency"][17]="Zuri";
	
	$fields["profileType"][0]="Child";
	$fields["profileType"][1]="Teen";
	$fields["profileType"][2]="Adult";
	
	$fields["shoeSize"][0]="1";
	$fields["shoeSize"][1]="1.5";
	$fields["shoeSize"][2]="2";
	$fields["shoeSize"][3]="2.5";
	$fields["shoeSize"][4]="3";
	$fields["shoeSize"][5]="3.5";
	$fields["shoeSize"][6]="4";
	$fields["shoeSize"][7]="4.5";
	$fields["shoeSize"][8]="5";
	$fields["shoeSize"][9]="5.5";
	$fields["shoeSize"][10]="6";
	$fields["shoeSize"][11]="6.5";
	$fields["shoeSize"][12]="7";
	$fields["shoeSize"][13]="7.5";
	$fields["shoeSize"][14]="8";
	$fields["shoeSize"][15]="8.5";
	$fields["shoeSize"][16]="9";
	$fields["shoeSize"][17]="9.5";
	$fields["shoeSize"][18]="10";
	$fields["shoeSize"][19]="10.5";
	$fields["shoeSize"][20]="11";
	$fields["shoeSize"][21]="11.5";
	$fields["shoeSize"][22]="12";
	$fields["shoeSize"][23]="12.5";
	$fields["shoeSize"][24]="13";
	$fields["shoeSize"][25]="13.5";
	$fields["shoeSize"][26]="14 and over";

	$fields["dressSize"][0]="0";
	$fields["dressSize"][2]="2";
	$fields["dressSize"][4]="4";
	$fields["dressSize"][6]="6";
	$fields["dressSize"][8]="8";
	$fields["dressSize"][10]="10";
	$fields["dressSize"][12]="12";
	$fields["dressSize"][14]="14";
	$fields["dressSize"][16]="16";
	$fields["dressSize"][18]="18";
	$fields["dressSize"][20]="20";
	
	$fields["shirtSize"][2][0]="XS";
	$fields["shirtSize"][2][1]="Small";
	$fields["shirtSize"][2][2]="Medium";
	$fields["shirtSize"][2][3]="Large";
	$fields["shirtSize"][2][4]="XL";
	$fields["shirtSize"][2][5]="XXL";
	$fields["shirtSize"][2][6]="XXXL";
	
	$fields["shirtSize"][1][0]="18-24 months";
	$fields["shirtSize"][1][1]="2T";
	$fields["shirtSize"][1][2]="3T";
	$fields["shirtSize"][1][3]="4T";
	$fields["shirtSize"][1][4]="5T";
	$fields["shirtSize"][1][5]="5/6 ";
	$fields["shirtSize"][1][6]="6";
	$fields["shirtSize"][1][6]="6x";
	$fields["shirtSize"][1][7]="7/8";
	$fields["shirtSize"][1][8]="8/10";
	$fields["shirtSize"][1][9]="10";
	$fields["shirtSize"][1][10]="10/12";
	$fields["shirtSize"][1][11]="12";
	$fields["shirtSize"][1][12]="12/14";
	$fields["shirtSize"][1][13]="14";
	$fields["shirtSize"][1][14]="14/16";
	$fields["shirtSize"][1][15]="16/18";
	
	$fields["shirtSize"][0][0]="Newborn";
	$fields["shirtSize"][0][1]="0-3 months";
	$fields["shirtSize"][0][2]="3-6 months";
	$fields["shirtSize"][0][3]="6-9 months";
	$fields["shirtSize"][0][4]="9-12 months";
	$fields["shirtSize"][0][5]="12-18 months";
	
	$fields["height"][0][0]="3";
	$fields["height"][0][1]="4";
	$fields["height"][0][2]="5";
	$fields["height"][0][3]="6";
	
	$fields["height"][1][0]="0";
	$fields["height"][1][1]="1";
	$fields["height"][1][2]="2";
	$fields["height"][1][3]="3";
	$fields["height"][1][4]="4";
	$fields["height"][1][5]="5";
	$fields["height"][1][6]="6";
	$fields["height"][1][7]="7";
	$fields["height"][1][8]="8";
	$fields["height"][1][9]="9";
	$fields["height"][1][10]="10";
	$fields["height"][1][11]="11";
	
	$fields["hairColor"][8]="Blonde";
	$fields["hairColor"][1]="Dark Blonde";
	$fields["hairColor"][2]="Brown";
	$fields["hairColor"][3]="Light Brown";
	$fields["hairColor"][4]="Black";
	$fields["hairColor"][5]="Auburn/Red";
	$fields["hairColor"][6]="Salt and Pepper";
	$fields["hairColor"][7]="Gray/White";
	
	$fields["babyAbilities"][4]="Sits up";
	$fields["babyAbilities"][1]="Crawls";
	$fields["babyAbilities"][2]="Stands";
	$fields["babyAbilities"][3]="Walks";
	
	$fields["gender"][0]="Male";
	$fields["gender"][1]="Female";
	$fields["gender"][2]="Non-binary";

	$fields["rateType"][0]="hour";
	$fields["rateType"][1]="day";
	$fields["rateType"][2]="project";

?>