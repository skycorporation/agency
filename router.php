<?php

if (!defined('PATH_APPLICATION'))
    exit('hack off');
$router = new Router();
$router->add('^/cron/works', array('model' => 'Cron', 'action' => 'Index'));

$router->add('^/api', array('model' => 'Server', 'action' => 'Index'));

$router->add('^/records', array('model' => 'Records', 'action' => 'List'));


$router->add('^/pdf', array('model' => 'Profile', 'action' => 'Export'));

$router->add('^/dashboard', array('model' => 'Account', 'action' => 'Dashboard'));
$router->add('^/profile/add', array('model' => 'Profile', 'action' => 'Add'));
$router->add('^/profile/edit', array('model' => 'Profile', 'action' => 'Add'));
$router->add('^/profile/view', array('model' => 'Profile', 'action' => 'View'));
$router->add('^/profile/upload', array('model' => 'Profile', 'action' => 'Upload'));
$router->add('^/profile/deletePhoto', array('model' => 'Profile', 'action' => 'DeletePhoto'));
$router->add('^/profile/delete', array('model' => 'Profile', 'action' => 'Delete'));
$router->add('^/profile/family', array('model' => 'Profile', 'action' => 'Family'));
$router->add('^/search', array('model' => 'Profile', 'action' => 'Search'));
$router->add('^/quick', array('model' => 'Profile', 'action' => 'QuickSearch'));
$router->add('^/groups/addTo', array('model' => 'Account', 'action' => 'ProfileGroupsAddTo'));
$router->add('^/groups/toggleFav', array('model' => 'Account', 'action' => 'ProfileGroupsToggleFav'));
$router->add('^/groups/toggleHide', array('model' => 'Account', 'action' => 'ProfileGroupsToggleHide'));
$router->add('^/groups/add', array('model' => 'Account', 'action' => 'ProfileGroupsAdd'));
$router->add('^/groups/deleteFrom', array('model' => 'Account', 'action' => 'ProfileGroupsDeleteFrom'));
$router->add('^/groups/deleteMultipleFrom', array('model' => 'Account', 'action' => 'ProfileGroupsDeleteMultipleFrom'));
$router->add('^/groups/delete', array('model' => 'Account', 'action' => 'ProfileGroupsDelete'));
$router->add('^/groups/view', array('model' => 'Account', 'action' => 'ProfileGroupsView'));
$router->add('^/groups/edit', array('model' => 'Account', 'action' => 'ProfileGroupsEdit'));
$router->add('^/groups/commentSave', array('model' => 'Account', 'action' => 'CommentSave'));
$router->add('^/groups', array('model' => 'Account', 'action' => 'ProfileGroups'));
$router->add('^/castings/addTo', array('model' => 'Casting', 'action' => 'AddTo'));
$router->add('^/castings/addForm', array('model' => 'Casting', 'action' => 'AddForm'));
$router->add('^/castings/add', array('model' => 'Casting', 'action' => 'Add'));
$router->add('^/castings/toggleFav', array('model' => 'Casting', 'action' => 'ToggleFav'));
$router->add('^/castings/toggleHide', array('model' => 'Casting', 'action' => 'ToggleHide'));
$router->add('^/castings/commentSave', array('model' => 'Casting', 'action' => 'CommentSave'));
$router->add('^/castings/view', array('model' => 'Casting', 'action' => 'View'));
$router->add('^/castings/deleteFrom', array('model' => 'Casting', 'action' => 'DeleteFrom'));
$router->add('^/castings/deleteMultipleFrom', array('model' => 'Casting', 'action' => 'deleteMultipleFrom'));
$router->add('^/castings/delete', array('model' => 'Casting', 'action' => 'Delete'));
$router->add('^/castings/edit', array('model' => 'Casting', 'action' => 'Edit'));
$router->add('^/casting/photos', array('model' => 'Casting', 'action' => 'Photos'));
$router->add('^/castings', array('model' => 'Casting', 'action' => 'List'));
$router->add('^/keywords', array('model' => 'Account', 'action' => 'Keywords'));
$router->add('^/c', array('model' => 'Profile', 'action' => 'MobileForm'));

$router->add('^/registration', array('model' => 'Index', 'action' => 'Registration'));
$router->add('^/login', array('model' => 'Index', 'action' => 'Login'));
$router->add('^/forgot', array('model' => 'Index', 'action' => 'Forgot'));
$router->add('^/exit', array('model' => 'Index', 'action' => 'Exit'));
$router->add('^/uploader/crop', array('model' => 'Uploader', 'action' => 'Crop'));
$router->add('^/uploader', array('model' => 'Uploader', 'action' => 'Upload'));


$router->add('^/done', array('model' => 'Profile', 'action' => 'Done'));


$router->add('^/$', array('model' => 'Index', 'action' => 'Index'));






