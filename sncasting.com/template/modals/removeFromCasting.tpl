<div class="modal fade text-xs-left pr-0" id="removeFromCasting" tabindex="-1" role="dialog" aria-labelledby="myModalLabel10" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-danger white">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel10">Remove profile from casting</h4>
      </div>
      <div class="modal-body">
        <h5>Are you sure you want to remove this profile from this casting?<br>
		  <br>
		  The profile will remain in the database.</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="mybtn2 mybtn-grey mr-1" data-dismiss="modal">Cancel</button>
        <a onclick="window.location='/castings/deleteFrom?id={$group->table->id}&profile=' + profileToDelete;" class="mybtn2 mybtn-red">Remove from casting</a></div>
    </div>
  </div>
</div>
