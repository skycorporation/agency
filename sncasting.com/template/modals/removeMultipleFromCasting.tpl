<div class="modal fade text-xs-left pr-0" id="removeMultiple" tabindex="-1" role="dialog" aria-labelledby="myModalLabel10" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-danger white">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel10">Remove profiles from casting</h4>
      </div>
      <div class="modal-body">
        <h5>Are you sure you want to remove these profiles from this casting?<br>
		  <br>
		  The profiles will remain in the database.</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="mybtn2 mybtn-grey mr-1" data-dismiss="modal">Cancel</button>
        <a onclick="removeMultiple('castings', {$group->table->id})" class="mybtn2 mybtn-red">Remove from casting</a></div>
    </div>
  </div>
</div>
