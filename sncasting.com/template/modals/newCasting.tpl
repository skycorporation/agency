<div class="modal fade text-xs-left pr-0" id="newCasting" tabindex="-1" role="dialog" aria-labelledby="myModalLabel10" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-success white">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel10">Create new casting</h4>
      </div>
      <div class="modal-body">
        <form action="/castings/add/" method="get" id="newCastingForm">
			<small class="text-muted">Casting name:</small>
          <input autocomplete="dontsuggest" type="text" placeholder="Casting name" name="name" class="form-control mb-1 w-100 capThis" onkeyup="var newUrl = $(this).val(); newUrl = newUrl.toLowerCase(); newUrl = newUrl.replace(/ /g,''); newUrl = encodeURI(newUrl); $('#newUrl1').val(newUrl);" required>
          <small class="text-muted">Casting location:</small>
          <input autocomplete="dontsuggest" type="text" placeholder="Casting location" name="location" class="form-control mb-1 w-100 checkThis">
          <br>
          <div style="width: 100%; text-align: center" class="mt-1 mb-1 clearfix"><small class="text-muted">Casting sheet URL for models:</small><br><br>
            <div style="width: 45%; float: left; text-align: right; line-height: 2">www.sncasting.com/c/</div>
            <div style="width: 50%; float: left">
              <input autocomplete="dontsuggest" type="text" id="newUrl1" placeholder="url" name="url" style="width: 80%; float: left; padding: 5px; border-width: 0; border-bottom: 1px solid #959595 !important">
            </div>
          </div>
          <small class="text-muted">Casting date:</small>
          <input type='text' placeholder="Select date" class="form-control pickadate-dropdown-all" style="width: 50%" name="date"/>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="mybtn2 mybtn-grey mr-1" data-dismiss="modal">Cancel</button>
        <a onclick="$('#newCastingForm').submit();" class="mybtn2 mybtn-green">Create casting</a></div>
    </div>
  </div>
</div>
