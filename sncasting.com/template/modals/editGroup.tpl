<div class="modal fade text-xs-left pr-0" id="editGroup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel10" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-warning white">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel10">Edit group name</h4>
      </div>
      <div class="modal-body"><small class="text-muted">Group name:</small>
        <input type="text" placeholder="Group name" id="editProfileGroupName" class="form-control capThis mb-1 w-100" required>
      </div>
      <div class="modal-footer">
        <button type="button" class="mybtn2 mybtn-grey mr-1" data-dismiss="modal">Cancel</button>
        <a onclick="window.location='/groups/edit?id=' + profileToEdit + '&name=' + $('#editProfileGroupName').val();" class="mybtn2 mybtn-yellow">Update name</a></div>
    </div>
  </div>
</div>
