<div class="modal fade text-xs-left pr-0" id="deleteCasting" tabindex="-1" role="dialog" aria-labelledby="myModalLabel10" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-danger white">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel10">Delete casting</h4>
      </div>
      <div class="modal-body">
        <h5>Are you sure you want to delete this casting?<br>
          <br>
          All model profiles from this casting will remain in the database.</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="mybtn2 mybtn-grey mr-1" data-dismiss="modal">Cancel</button>
        <a onclick="window.location='/castings/delete?id=' + profileToDelete;" class="mybtn2 mybtn-red">Delete casting</a></div>
    </div>
  </div>
</div>
