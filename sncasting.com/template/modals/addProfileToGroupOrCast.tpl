<div class="modal fade text-xs-left pr-0" id="addProfileToGroupOrCast" tabindex="-1" role="dialog" aria-labelledby="myModalLabel10" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-group white">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
        <h4 class="modal-title" id="myModalLabel10">Add profile to group or casting</h4>
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs nav-top-border no-hover-bg nav-justified">
          <li class="nav-item"> <a class="nav-link active" id="base-tab11" data-toggle="tab" aria-controls="tab11" href="#tab11" aria-expanded="true">Add to group</a> </li>
          <li class="nav-item"> <a class="nav-link" id="base-tab12" data-toggle="tab" aria-controls="tab12" href="#tab12" aria-expanded="false">Add to casting</a> </li>
        </ul>
        <div class="tab-content px-1 pt-1">
          <div role="tabpanel" class="tab-pane active" id="tab11" aria-expanded="true" aria-labelledby="base-tab11">
            <h5>Create a new group or use existing:</h5>
            <br>
            <input type="text" placeholder="New group name" id="newProfileGroupName" class="newGroupInput capThis" onchange="$('#existingGroup').val('');">
            &nbsp; or &nbsp;
            <select id="existingGroup" onchange="$('#newProfileGroupName').val('');" style="height: 2.2em">
              <option disabled selected>Existing groups:</option>
              
					{foreach from=$profileGroups item=t key=k}
					
              <option value="{$t->id}">{$t->name}</option>
              
					{/foreach}
				
            </select>
            <br>
            <br>
            <div class="modal-footer pr-0 pt-2">
              <button type="button" class="mybtn2 mybtn-grey" data-dismiss="modal">Cancel</button>
              &nbsp; <a onclick="createGroup()" class="mybtn2 mybtn-group">Add to group</a> </div>
          </div>
          <div class="tab-pane" id="tab12" aria-labelledby="base-tab12">
            <h5>Add to existing casting:</h5>
            <br>
            <select id="existingCasting"  style="height: 2.2em">
              <option disabled selected>Existing castings:</option>
              
					{foreach from=$castingLists item=t key=k}
					
              <option value="{$t->id}">{$t->name}</option>
              
					{/foreach}
				
            </select>
            <br>
            <br>
            <div class="modal-footer pr-0 pt-2">
              <button type="button" class="mybtn2 mybtn-grey" data-dismiss="modal">Cancel</button>
              &nbsp; <a onclick="addToCasting($('#existingCasting').val())" class="mybtn2 mybtn-group">Add to casting</a> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>