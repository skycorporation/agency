{include file="header.tpl"}
{include file="modals/deleteProfile.tpl"}
{include file="modals/deleteMultipleProfiles.tpl"}
{include file="modals/newGroup.tpl"}
<section id="hover-effects">
  <div class="row">
    <div class="col-12 viewSwitcher mobile-only pb-1">
      <button type="button" id="scrollToBottom"><i class="icon-arrow-down4"></i></button>
    </div>
    <div class="col-12">
      <h1 class="mt-3 mb-2" style="text-align: center">Browse profiles &#40;{$count}&#41;</h1>
    </div>
    <div class="col-12">
      <div class="px-1">
        <ul class="pagination pagination-flat d-flex justify-content-center">
          {if $page>1}
          <li class="page-item{if $page==1} active{/if}"> <a class="page-link" href="?page={$page-1}" aria-label="Previous"> <span aria-hidden="true">«</span> <span class="sr-only">Previous</span> </a> </li>
          {/if}
          
          {for $foo=1 to $count/60}
          <li class="page-item{if $foo==$page} active{/if}"><a class="page-link" href="?page={$foo}">{$foo}</a></li>
          {/for}
          {if $page==$foo}
          <li class="page-item"> <a class="page-link" href="?page={$page+1}" aria-label="Next"> <span aria-hidden="true">»</span> <span class="sr-only">Next</span> </a> </li>
          {/if}
        </ul>
      </div>
      <hr class="mb-2">
    </div>
  </div>
  <div class="row"> {foreach from=$profiles item=t key=k}
    {assign var="size" value=getimagesize($t->thumbnail)}
    <div class="col-md-4 mb-2">
      <div class="thumbnail-container" data-id="{$t->id}">
        <div class="thumb-photo-place"><img class="thumbnail-photo" src="{$t->thumbnail|replace:'/home/aibdh80ehx97/public_html/sncasting.com':''}" alt="Thumbnail" {if $size[0]>$size[1]}style="height: 380px; width: auto;"{/if}onerror="this.src='/template/app-assets/images/noimage.png'"/>
          <div class="thumb-overlay"></div>
          <div class="thumb-content">
            <div class="editPanel desktop-only">
              <button onclick="location='/profile/edit?id={$t->id}'" class="editButton" title="Edit profile"><i class="ft-edit-2"></i></button>
              <button class="deleteProfileButton" data-toggle="modal" data-target="#deleteProfile" onclick="profileToDelete={$t->id}" title="Delete profile"><i class="icon-trash4"></i></button>
            </div>
            <a class="thumb-info" href="/profile/view?id={$t->id}">
            <p> {if $t->basedIn}<i class="ft-map-pin"></i><b>{$t->basedIn}</b><br>
              <br>
              {/if}
              {if $t->occupation}<i class="ft-award"></i><b>{$t->occupation}</b><br>
              <br>
              {/if}
              {if $t->height}<i class="ft-arrow-up"></i><b>{$t->height}</b>{/if} </p>
            </a>
            <button class="selectButton desktop-only" onclick="selectProfile(this)"  title="Select profile">Select</button>
          </div>
        </div>
        <a class="name-place" href="/profile/view?id={$t->id}" target="_blank">{$t->firstName}&nbsp;{$t->lastName}</a>
        <div class="thumbnail-toolbar mobile-only">
          <div class="thumb-tools"></div>
          <div class="thumb-mob-actions">
            <div class="mob-actions-inner">
              <button class="btn-mob-edit" onclick="location='/profile/edit?id={$t->id}'"><i class="ft-edit-2" title="Edit profile"></i></button>
              <button class="btn-mob-delete" data-toggle="modal" data-target="#deleteProfile" onclick="profileToDelete={$t->id}" title="Delete profile"><i class="icon-trash4"></i></button>
              <button class="btn-mob-select" onclick="mobSelectProfile(this)"><i class="ft-plus-circle mob-select-icon" title="Select profile"></i></button>
            </div>
          </div>
          <button class="btn-toggle-mob-actions"><i class="ft-more-horizontal"></i></button>
        </div>
      </div>
    </div>
    {/foreach} </div>
  <div class="row mb-2">
    <div class="col-12">
      <hr>
      <div class="px-1">
        <ul class="pagination pagination-flat d-flex justify-content-center">
          {if $page>1}
          <li class="page-item{if $page==1} active{/if}"> <a class="page-link" href="?page={$page-1}" aria-label="Previous"> <span aria-hidden="true">«</span> <span class="sr-only">Previous</span> </a> </li>
          {/if}
          
          {for $foo=1 to $count/60}
          <li class="page-item{if $foo==$page} active{/if}"><a class="page-link" href="?page={$foo}">{$foo}</a></li>
          {/for}
          {if $page==$foo}
          <li class="page-item"> <a class="page-link" href="?page={$page+1}" aria-label="Next"> <span aria-hidden="true">»</span> <span class="sr-only">Next</span> </a> </li>
          {/if}
        </ul>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-12 viewSwitcher mobile-only">
      <button type="button" id="scrollToTop" class="mt-2 mb-2"><i class="icon-arrow-up4"></i></button>
    </div>
  </div>
</section>
{include file="footer.tpl"}