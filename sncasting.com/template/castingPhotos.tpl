{include file="header.tpl"}
<style>
	.dropzone {
    min-height: 200px;
	}
.dropzone .dz-message {
	left: 200px;
	display: none;
}
</style>

<!-- Hover Effects --> 
<br>
<h2 style="text-align: center">Upload photos to {$group->table->name} casting</h2>
<div class="infoNote"><i class="icon-info2"></i><strong>Drag-and-drop photos into relevant fields for each profile.<br>
  All changes are saved automatically.</strong><br>
  <strong style="color: #000 !important;">Please make sure to use JPGs with sRGB color space.</strong></div>
{foreach from=$profiles item=t key=k}
						{assign var="size" value=getimagesize($t->photo)}
						<div class="card">
  <div class="card-header text-center"> <a class="card-title h4" href="/profile/view?id={$t->id}" style="cursor: pointer" target="_blank">{$t->firstName} {$t->lastName}</a><br>
      <span class="font-weight-bold">&#35;{$t->id}</span><br>
      {$fields['gender'][$t->gender]} / {$t->age}
                           
                          </div>
  <div class="card-block">
                            <div class="container-fluid">
      <div class="row flexbox"> {if $t->photo}
                                <div class="col flexitem text-center mr-1" style="flex-grow: 0;"> <b>Current Profile Photo:</b><br>
                                  <img src="{$t->photo|replace:'/home/aibdh80ehx97/public_html/sncasting.com':''}" alt="Current Profile Photo" title="Current profile photo" style="height: 200px; width: auto;" />
        </div>
                                {/if}
                                <div class="col flexitem text-center mr-1"> <b>{if $t->photo}New Profile Photo:{else}Profile Photo:{/if}</b><br>
                                  <form action="/profile/upload/?id={$t->id}&type=1" class="dropzone dpz-single-file" id="dpz-single-file">
                                  </form>
        </div>
                                <div class="col flexitem text-center" style="flex-grow: 3;"> <b>Additional Photos:</b><br>
                                  <form action="/profile/upload/?id={$t->id}" class="dropzone dpz-multiple-files" id="dpz-multiple-files">
                                  </form>
        </div>
                              </div>
    </div>
                          </div>
</div>
{/foreach}

{include file="footer.tpl"}