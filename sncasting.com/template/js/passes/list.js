$(function () {
    $(document).on('click', 'ul.filter li a', function () {
        $(this).parents('ul').find('li').removeClass('active');
        $(this).parents('li').addClass('active');
        loadList();
    })

    function loadList() {
        form = {};
        form.action = 'getList';
        form.is_payed = $('ul.filter:eq(0) li.active a').data('val');
        form.is_used = $('ul.filter:eq(2) li.active a').data('val');
        form.type = $('ul.filter:eq(1) li.active a').data('val');

        $.get('?', form, function (html) {
            $('#list').html(html);
        })
    }
    loadList();
})