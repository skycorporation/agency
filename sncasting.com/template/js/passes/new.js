
$(function () {

    
    $(document).on('click', 'button.type', function () {
        var b = this;
        $('button.type').removeClass('active');

        $('ul[data-type]').hide();
        $('ul[data-type="' + $(b).data('type') + '"]').slideDown();


        $(this).addClass('active');
        return false;
    })
    $(document).on('click', 'a.buy', function () {
        var passid = $(this).data('passid');
        var is_prepayment = $(this).data('prepayment') == '0';
        $('#buy .modal-title').html(
                $(this).parents('li').find('h3').html()
                );
        Cart.showSubs(passid, is_prepayment, function (html) {
            $('#buy .modal-body').html(html);
            $('#buy').modal('toggle');
            $('#buy').find('tr.active .count').trigger('change');
        })
        return false;

    })
    $(document).on('click', '#cart_reset', function () {
        Cart.reset();
    });
    $(document).on('click', '#cart_submit', function () {
        top.location =
                '/invoices/create?cart=' + Cart.getString();
    });
    $(document).on('click', '#buy .btn-success', function () {

        inp = $('#buy tbody.cart .count:visible');
        if (inp.length == 0) {
            $('#buy').modal('toggle');
            return;
        }
        count = parseInt($(inp).val());
        id = parseInt($(inp).data('sub'));

        $('#buy tbody.cart .count').each(function (i, v) {
            Cart.remove($(v).data('sub'), true);
        });
        Cart.add(id, count);
        $('#buy').modal('toggle');
        return true;


    })
    $(document).on('change', 'tbody.cart [type=radio]', function () {
        $('tbody.cart tr').removeClass('active');
        $(this).parents('tr').addClass('active');
        $(this).parents('tr').find('.count').trigger('change');
    });

    $(document).on('change', 'tbody.cart .count', function () {
        count = parseInt($(this).val());
        if (isNaN(count))
            return;
        price = parseFloat($(this).parents('tr').find('.price').html());
        priceout = parseFloat($(this).parents('tr').find('.priceout').html());
        is_nan = false;
        if (isNaN(priceout)) {
            is_nan = true;
            priceout = price;
        }

        console.log('Count: ' + count);
        console.log('purchased  : ' + purchased);
        console.log('In norm: ' + in_norm);
        if ((count + purchased) > in_norm)
            out = (count + purchased) - in_norm;
        else
            out = count - in_norm;

        if (out > count)
            out = count;
        total = 0;
        norm = count - out;
        if (norm < 0)
            norm = 0;

        //out -= purchased;
        totalNorm = Math.round((norm) * price * 100) / 100;
        totalOut = Math.round((out) * priceout * 100) / 100;

        /*if (is_nan) {
         count = out;
         out = 0;
         totalNorm = totalOut;
         totalOut = 0;
         }*/
        $('.extra_norm').hide();
        if (totalOut > 0) {
            $('.extra_norm').show();
            $('.total_extra_norm').html(out);
            $('.total_extra_price').html(totalOut);
        }
        $('.total_norm').html(norm);
        $('.total_norm_price').html(price);
        $('.total').html(totalNorm + totalOut);


    })
})