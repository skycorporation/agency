var Cart = {
    cart: {},
    get: function () {
        return this.cart;
    },
    reload: function () {
        this.render();
    },
    add: function (subPassId, count) {
        this.cart[subPassId] = count;
        this.render();
    },
    getString: function () {
        return JSON.stringify(this.cart);
    },
    render: function () {


        var cart = [];
        for (var k in this.cart) {

            if (k != undefined)
                cart.push(k);

        }


        $.get('/invoices/render', {
            cart: this.cart
        }, function (html) {
            $('#cart_content').html(html);
            if (cart.length == 0) {
                $('#cart_sidebar').slideUp();
                $('#cart_main').removeClass('col-lg-9').addClass('col-lg-12')
                return;
            }
            $('#cart_main').removeClass('col-lg-12').addClass('col-lg-9');
            $('#cart_sidebar').slideDown();
        });




    },
    remove: function (subPassId, do_not_render) {
        for (var k in this.cart) {
            if (k == subPassId)
            {
                delete this.cart[k];
                break;
            }
        }
        if (!do_not_render)
            this.render();
    },
    reset: function () {
        this.cart = {};
        this.render();
    },
    showSubs: function (passId, is_prepayment, callback) {
        $.get('?', {
            action: 'showSubs',
            passid: passId,
            is_prepayment: is_prepayment,
            cart: this.get()

        }, function (html) {
            callback(html);
        })
    },
    init: function () {

    }
}
$(function () {

})