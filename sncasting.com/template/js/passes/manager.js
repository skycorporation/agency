var handleEmailActionButtonStatus = function () {
    if ($("[data-checked=email-checkbox]:checked").length !== 0) {
        $("[data-email-action]").removeClass('disabled').removeAttr("disabled");
    } else {
        $("[data-email-action]").addClass('disabled').attr("disabled", 'disabled');
    }
};
var handleEmailCheckboxChecked = function () {
    $("[data-checked=email-checkbox]").live("click", function () {

        handleEmailActionButtonStatus()
    })
};
var lastAction = '';
var handleEmailAction = function () {
    $("[data-email-action]").live("click", function () {
        lastAction = $(this).data('email-action');
        if (!$(this).data('confirm')) {
            lastAction = $(this).data('email-action');
            html = '';
            if (lastAction == 'delete')
                html = 'удалить';
            if (lastAction == 'block')
                html = 'заблокировать';
            if (lastAction == 'unblock')
                html = 'разблокировать';
            $('#confirm .modal-body b').html(html);
            $('#confirm').modal('toggle');
            return false;
        }
        $(this).data('confirm', false);
        var e = "[data-checked=email-checkbox]:checked";

        form = {};
        form.action = lastAction;
        form.passes = [];
        $(e).each(function (i, v) {
            form.passes.push($(v).data('id'));
        });
        if ($(e).length !== 0 && lastAction == 'delete') {
            $(e).closest("li").slideToggle(function () {
                $(this).remove();
                handleEmailActionButtonStatus()
            })
        }
        $.post('?', form, function () {
            top.location.reload();
        })
        return false;

    })
};
var loadCompany;
var InboxV2 = function () {
    "use strict";
    return{init: function () {
            handleEmailCheckboxChecked();
            handleEmailAction()
        }}
}()

$(function () {
    $('a[href="#create"]').click(function () {
        var $modal = $('#create');
        $modal.find('[name="data[type]"]').val($(this).data('type'));
        $modal.modal();
        return false;

    })
    $(document).on('submit', '#create form', function () {
        $('#create_error').addClass('hidden');
        $(this).ajaxSubmit({
            dataType: 'json',
            success: function (ret) {
                if (ret && ret.status == 'ok') {
                    $('#create').modal('toggle');

                    top.location.reload();
                    return;
                }
                $('#create_error').removeClass('hidden').html(ret.error);
            }
        })
        return false;
    })
    $(document).on('click', '#confirm .btn-success', function () {
        $('#confirm').modal('toggle');
        $('[data-email-action="' + lastAction + '"]').data('confirm', true).click();
    });

    InboxV2.init();

})
