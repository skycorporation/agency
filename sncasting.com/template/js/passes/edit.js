function changePhoto(el, url, res) {

    $('[name="extra[photoUrl]"').val(res.url);
    $('#photoView img').attr('src', res.url + '?' + Math.random());
    $('#photoEdit').hide();
    $('#photoView').slideDown();
}

$(function () {
    $(document).on('click', '#photoView a', function () {
        $('#photoView').hide();
        $('#photoEdit').slideDown();
    })
});