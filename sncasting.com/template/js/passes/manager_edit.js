$(function () {
    function reloadSubs() {
        $('#subs_list').load('?action=getSubs');

    }
    $(document).on('submit', '#create form', function () {
        $(this).ajaxSubmit({
            dataType: 'json',
            success: function () {
                $('#create').modal('toggle');
                reloadSubs()
            }
        })
        return false;
    })
    $(document).on('click', '[data-sub] .delete', function () {
        if (!confirm('Удалить?'))
            return;
        parent = $(this).parents('[data-sub]');
        $.post('?', {
            action: 'deleteSub',
            id: $(parent).data('sub')
        }, function () {
            reloadSubs()
        });
        return false;
    })
    $(document).on('blur', '[data-name]', function () {
        parent = $(this).parents('[data-sub]');

        req = {};
        req.action = 'update';
        req.name = $(this).data('name');
        req.value = $(this).val();
        req.sid = $(parent).data('sub');
        $.post('?', req, function () {
            reloadSubs();
        });
    })
    reloadSubs();
})