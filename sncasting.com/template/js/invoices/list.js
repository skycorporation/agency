$(function () {
    $(document).on('click', 'input.delete', function () {
        if (!confirm("Удалить?"))
            return false;
        $(this).parents('tr').fadeOut(function () {
            $(this).remove();
        })
        $.post('?', {
            action: 'delete',
            id: $(this).data('id')
        });
    })
    $(document).on('change', 'select.changeStatus', function () {
        $.post('?', {
            action: 'changeStatus',
            id: $(this).data('id'),
            val: $(this).val()

        });
    })
    $(document).on('click', 'a.sort', function () {
        $('a.sort').removeClass('active');
        $(this).addClass('active');
        loadList();
        return false;
    });
    function loadList() {
        $.get('?', {
            action: 'getList',
            type: $('a.sort.active').data('type')
        }, function (html) {
            $('#inv_list').html(html);
        })
    }
    loadList();
})