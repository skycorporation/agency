var Cropper = new Crop();
$(function(){
Cropper.init();
 $('[type="file"].uploader').each(function () {
        if ($(this).data('_init')) {
            return;
        }
        $(this).attr('type', 'hidden');
        var file = $('<input class="uploader" type=file>');
        $(file).data($(this).data());

        $(file).on('filebrowse', function(){
            $(file).fileinput('clear');
        })
        $(file).on('fileimagesloaded', function () {
            $(file).fileinput('upload');
        })
        $(file).on('fileuploaded', function (event, data, msg) {
            if ($(event.target).data('target')) {

                var target = $(event.target).data('target');
                var success = function (element, url, response) {
					//debugger;
                    $(element).val(url);
                    if ($(element).data('onuploaded')) {
                        window['' + $(element).data('onuploaded')](element, url, response);
                    }
                }
                if ($(target).data('ratio')) {
                    // need crop
					
                    Cropper.setRatio($(target).data('ratio'));
                    Cropper.setElement(target);
                    Cropper.showModal(data.response.url, success);
                }
                else {
                    success(target, data.response.url, data.response);
                }
            }

        })
        $(this).after(file);
        $(file).data('target', $(this));

        $(file).fileinput({
            uploadUrl: '/uploader',
            //minFileCount: 1,
            dropZoneTitle: 'Перетащите файл сюда',
            uploadAsync: true,
            validateInitialCount: true,
            browseLabel: 'Выбрать',
            browseClass: 'btn btn-default',
            removeLabel: '',
            uploadLabel: '',
            browseIcon: '<i class="icon-plus22 position-left"></i> ',
            uploadClass: 'btn btn-primary btn-icon hidden',
            uploadIcon: '<i class="icon-file-upload"></i> ',
            removeClass: 'btn btn-danger btn-icon',
            removeIcon: '<i class="icon-cancel-square"></i> ',
            initialCaption: "Файл не выбран",
            layoutTemplates: {
                caption: '<div tabindex="-1" class="form-control file-caption {class}">\n' + '<span class="icon-file-plus kv-caption-icon"></span><div class="file-caption-name"></div>\n' + '</div>',
                main1: "{preview}\n" +
                        "<div class='input-group {class}'>\n" +
                        "   <div class='input-group-btn'>\n" +
                        "       {browse}\n" +
                        "   </div>\n" +
                        "   {caption}\n" +
                        "   <div class='input-group-btn hidden'>\n" +
                        "       {upload}\n" +
                        "       {remove}\n" +
                        "   </div>\n" +
                        "</div>"
            }
        });
    });
    
});