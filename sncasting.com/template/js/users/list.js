var handleEmailActionButtonStatus = function () {
    if ($("[data-checked=email-checkbox]:checked").length !== 0) {
        $("[data-email-action]").removeClass("hide")
    } else {
        $("[data-email-action]").addClass("hide")
    }
};
var handleEmailCheckboxChecked = function () {
    $("[data-checked=email-checkbox]").live("click", function () {
        var e = $(this).closest("label");
        var t = $(this).closest("li");
        if ($(this).prop("checked")) {
            $(e).addClass("active");
            $(t).addClass("selected")
        } else {
            $(e).removeClass("active");
            $(t).removeClass("selected")
        }
        handleEmailActionButtonStatus()
    })
};
var lastAction = '';
var handleEmailAction = function () {
    $("[data-email-action]").live("click", function () {
        lastAction = $(this).data('email-action');
        if (!$(this).data('confirm')) {
            lastAction = $(this).data('email-action');
            html = '';
            if (lastAction == 'delete')
                html = 'удалить';
            if (lastAction == 'block')
                html = 'заблокировать';
            if (lastAction == 'unblock')
                html = 'разблокировать';
            $('#confirm .modal-body b').html(html);
            $('#confirm').modal('toggle');
            return false;
        }
        $(this).data('confirm', false);
        var e = "[data-checked=email-checkbox]:checked";

        form = {};
        form.action = lastAction;
        form.users = [];
        $(e).each(function (i, v) {
            form.users.push($(v).data('id'));
        });
        if ($(e).length !== 0 && lastAction == 'delete') {
            $(e).closest("li").slideToggle(function () {
                $(this).remove();
                handleEmailActionButtonStatus()
            })
        }
        $.post('?', form, function () {
            loadUsers();
        })
        return false;

    })
};
var loadUsers;
var InboxV2 = function () {
    "use strict";
    return{init: function () {
            handleEmailCheckboxChecked();
            handleEmailAction()
        }}
}()

$(function () {
    $(document).on('submit', '#create form', function () {
        $('#create_error').addClass('hidden');
        $(this).ajaxSubmit({
            dataType: 'json',
            success: function (ret) {
                if (ret && ret.status == 'ok') {
                    $('#create').modal('toggle');

                    loadUsers();
                    return;
                }
                $('#create_error').removeClass('hidden').html(ret.error);
            }
        })
        return false;
    })
    $(document).on('click', '#confirm .btn-success', function () {
        $('#confirm').modal('toggle');
        $('[data-email-action="' + lastAction + '"]').data('confirm', true).click();
    });
    $(document).on('click', 'a.company', function () {
        $('.companies li').removeClass('active');
        $(this).parents('li').addClass('active');
        loadUsers($(this).data('id'));
        $('#create select').val($(this).data('id'));
        return false;
    });
    loadUsers = function () {
        form = {};
        form.action = 'getUsers';
        form.cid = $('.companies li.active a').data('id');
        $.post('?', form, function (html) {
            $('#users_list').html(html);
            handleEmailActionButtonStatus();
        })

    }
    InboxV2.init();
    loadUsers(0)
})
