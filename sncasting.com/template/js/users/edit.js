$(function () {
    $('#image_change,#image_src').click(function () {
        if ($('#upload_form').length > 0) {
            $('#upload_form [type="file"]').click();
            return;
        }
        $('<form style="position:absolute;top:-500px;" id=upload_form action=? method=post enctype="multipart/forn-data"><input type=hidden name=userid value='+objectID+'><input type=file name=file></form>').appendTo('body');
        setTimeout(function () {
            $('#upload_form [type="file"]').click();
        }, 0);

    })
    $(document).on('change', '#upload_form [type="file"]', function () {
        $('#upload_form').submit();
    });
    $(document).on('submit', '#upload_form', function () {
        $(this).ajaxSubmit({
            dataType: 'json',
            success: function (res) {
                $('#image_src').attr('src', res + '?' + Math.random());
            }
        })
        return false;
    })
})