var deviceIsMobile = Boolean;
var deviceIsDesktop = Boolean;
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    console.log('You are using a mobile device!');
    deviceIsMobile = true;
    deviceIsDesktop = false;
} else {
    console.log('You are using a desktop device!');
    deviceIsMobile = false;
    deviceIsDesktop = true;
}

var isTouchDevice = 'ontouchstart' in document.documentElement;
var CommentMode = false;

//SwitchCommentMode();

if (CommentMode !== true) {
    //Do something
}

function SwitchCommentMode() {
    CommentMode = !CommentMode;
    return CommentMode;
}

//Thumbnail hover

$(document).on('mouseenter', '.thumb-photo-place', function () {
    var mycomments = $(this).find('.thumb-comments');
    if (mycomments.hasClass('active')) {
        console.log('Comments are showing');
    } else {
        $(this).parent().addClass('hover');
        /*$(this).find('.thumbnail-photo').addClass('active');
        $(this).find('.thumb-overlay').addClass('active');
        $(this).find('.thumb-info').addClass('active');
        $(this).find('.editPanel').addClass('active');
        $(this).find('.selectButton').addClass('active');*/

    }
});
$(document).on('mouseleave', '.thumb-photo-place', function () {
    var myparent = $(this);
    var mycomments = myparent.find('.thumb-comments');
    if (mycomments.hasClass('active')) {} else {
        $(this).parent().removeClass('hover');
        /*myparent.find('.thumbnail-photo').removeClass('active');
        myparent.find('.thumb-overlay').removeClass('active');
        myparent.find('.thumb-info').removeClass('active');
        myparent.find('.editPanel').removeClass('active');
        myparent.find('.selectButton').removeClass('active');*/
    }
});
$('.thumb-photo-place').on('touchstart', function () {

    console.log('Touching start');
    if (CommentMode !== true) {
        $(this).parent().addClass('hover');
        /*$(this).find('.thumbnail-photo').addClass('active');
        $(this).find('.thumb-overlay').addClass('active');
        $(this).find('.thumb-info').addClass('active');
        $(this).find('.editPanel').addClass('active');
        $(this).find('.selectButton').addClass('active');*/
    }

});
$('.thumb-photo-place').on('touchend', function () {

    console.log('Touching end');
    var myparent = $(this);
    if (CommentMode !== true) {
        $(this).parent().removeClass('hover');
        /*myparent.find('.thumbnail-photo').removeClass('active');
        myparent.find('.thumb-overlay').removeClass('active');
        myparent.find('.thumb-info').removeClass('active');
        myparent.find('.editPanel').removeClass('active');
        myparent.find('.selectButton').removeClass('active');*/
    }

});

// End of Thumbnail hover

// Toggle thumbnail comments
$(document).on('click', '.btn-comments', function () {
    $(this).toggleClass('active');

    var myparent = $(this).closest('.thumbnail-container');
    var mycomments = myparent.find('.thumb-comments');
    var myinput = mycomments.find('.comment-input');

    myparent.find('.thumbnail-photo').toggleClass('active');
    myparent.find('.thumb-overlay').toggleClass('active');
    mycomments.toggleClass('active');
    myinput.focus();

});
// End of toggle thumbnail comments


/*// Show/Hide comments
$(document).on('click', '.btn-reveal-comments', function () {
    console.log('Toggle all present comments');
    $(this).toggleClass('active');

    var profilecard = $('body').find('.thumbnail-container');

    profilecard.each(function () {
            if ($(this).hasClass('hasComment')) {
                var mycomments = $(this).find('.thumb-comments');
                mycomments.toggleClass('active');
                $(this).find('.thumbnail-photo').toggleClass('active');
                $(this).find('.thumb-overlay').toggleClass('active');
            } else {
                $(this).parent().fadeOut();
            }
    });

});
// End of Show/Hide comments*/

$(function () {
    $("#quickSearchResult").width($("#quickSearch").width() - 40);
    $("#quickSearchResult").css("margin-left", "-" + (($("#quickSearch").width() / 2) - 5) + "px");
});

function deletePhoto(name, obj) {
    $.ajax({
            url: "/profile/deletePhoto",
            method: "GET",
            data: {
                name: name
            },
            dataType: "html"
        })
        .success(function () {
            $(obj).parent().fadeOut();
        });
}

function changeSearchForm(obj, t) {
    if (obj.checked) {
        $('.pt' + t).show();
    } else {
        $('.pt' + t).hide();
    }
}








function selectProfile(obj) {
    var myparent = $(obj).closest('.thumbnail-container');
    if (myparent.hasClass("selectedProfile")) {
        myparent.removeClass("selectedProfile");
        //$(obj).find('#mobSelectStat').attr('class', 'ft-plus-circle');
        $(obj).html('SELECT');
        $(obj).css('transition', 'all 0.2s ease-in-out');
        $(obj).css('background', '#50B4D1');
        $(obj).css('border', '1px solid #50B4D1');

    } else {
        myparent.addClass("selectedProfile");
        //$(obj).find('#mobSelectStat').attr('class', 'ft-minus-circle');
        $(obj).html('DESELECT');
        $(obj).css('transition', 'all 0.2s ease-in-out');
        $(obj).css('background', '#959595');
        $(obj).css('border', '1px solid #959595');
    }
    if ($("body").find(".selectedProfile").length > 0) {

        $("#mainToolbox").css('opacity', '1');
        $("#mainToolbox").css('transform', 'translate(-50%, 0)');

    } else {

        $("#mainToolbox").css('transform', 'translate(-50%, 80px)');
        $("#mainToolbox").css('opacity', '0');
    }
}

function mobSelectProfile(obj) {
    var myparent = $(obj).closest('.thumbnail-container');
    if (myparent.hasClass("selectedProfile")) {
        myparent.removeClass("selectedProfile");
        $(obj).find('i').attr('class', 'ft-plus-circle');
        myparent.find('.selectButton').html('SELECT');

    } else {
        myparent.addClass("selectedProfile");
        $(obj).find('i').attr('class', 'ft-minus-circle');
        myparent.find('.selectButton').html('DESELECT');
    }
    if ($("body").find(".selectedProfile").length > 0) {
        $("#mainToolbox").css('opacity', '1');
        $("#mainToolbox").css('transform', 'translate(-50%, 0)');


    } else {

        $("#mainToolbox").css('transform', 'translate(-50%, 80px)');
        $("#mainToolbox").css('opacity', '0');
    }
}

function loveProfile(obj, type, list_id, profile) {
    $.ajax({
        url: '/' + type + '/toggleFav',
        data: {
            list_id: list_id,
            profile: profile
        },
        success: function (data) {
            if (data == 'added') {
                $(obj).closest('.thumbnail-container').addClass("likedProfile");
            } else if (data == 'removed') {
                $(obj).closest('.thumbnail-container').removeClass("likedProfile");
            }
        },
        dataType: 'text'
    });
}

function hideProfile(obj, type, list_id, profile) {
    $.ajax({
        url: '/' + type + '/toggleHide',
        data: {
            list_id: list_id,
            profile: profile
        },
        success: function (data) {
            if (data == 'added') {
                $(obj).closest('.thumbnail-container').addClass("hiddenProfile");
            } else if (data == 'removed') {
                $(obj).closest('.thumbnail-container').removeClass("hiddenProfile");
            }
        },
        dataType: 'text'
    });
}

function saveComments(obj, type, list_id, profile) {
    $.ajax({
        url: '/' + type + '/commentSave',
        data: {
            list_id: list_id,
            profile: profile,
            comment: $(obj).parent().find('.comment-input').val()
        },
        success: function (data) {
        },
        dataType: 'text'
    });
}

function clearComments(obj, type, list_id, profile) {
    $.ajax({
        url: '/' + type + '/commentSave',
        data: {
            list_id: list_id,
            profile: profile,
            comment: ''
        },
        success: function (data) {
        	$(obj).parent().find('.comment-input').val("");
        },
        dataType: 'text'
    });
}

$('.btn-hide').click(function () {
    $(this).closest('.thumbnail-container').toggleClass('hiddenProfile');
    if ($(this).find('i').hasClass('ft-eye')) {
        $(this).find('i').removeClass('ft-eye');
        $(this).find('i').addClass('ft-eye-off');
    } else {
        $(this).find('i').removeClass('ft-eye-off');
        $(this).find('i').addClass('ft-eye');
    }
});

function selectFavs() {
    $(".likedProfile").each(function () {
        $(this).addClass("selectedProfile");
        $(this).find(".selectButton").html('DESELECT');
        $(this).find(".selectButton").css('background', '#959595');
        $(this).find(".selectButton").css('border', '1px solid #959595');
    });
    if ($("body").find(".selectedProfile").length > 0) {
        $("#mainToolbox").css('opacity', '1');
        $("#mainToolbox").css('transform', 'translate(-50%, 0)');


    } else {
        $("#mainToolbox").css('transform', 'translate(-50%, 80px)');
        $("#mainToolbox").css('opacity', '0');

    }
}
function selectAll() {
	$("body").find(".thumbnail-container").each(function() {
    $(this).addClass("selectedProfile");
	$(this).find(".selectButton").html('DESELECT');
	$(this).find(".selectButton").css('background', '#959595');
	$(this).find(".selectButton").css('border', '1px solid #959595');
  });
	if ($("body").find(".selectedProfile").length > 0) {
		$("#mainToolbox").css('opacity', '1');
		$("#mainToolbox").css('transform', 'translate(-50%, 0)');
		

	} else {
		$("#mainToolbox").css('transform', 'translate(-50%, 80px)');
		$("#mainToolbox").css('opacity', '0');
		
	}
}

function deselectAll() {
    if ($("body").find(".selectedProfile").length > 0) {
        $(".selectedProfile").each(function () {
            $(this).removeClass("selectedProfile");
            $(this).find(".selectButton").html('SELECT');
            $(this).find(".selectButton").css('background', '#50B4D1');
            $(this).find(".selectButton").css('border', '1px solid #50B4D1');
            $("#mainToolbox").css('transform', 'translate(-50%, 80px)');
            $("#mainToolbox").css('opacity', '0');
        });
    }
}

function clearExistinGroup() {
    $('#existingGroup').val('');
    $('#existingGroup').css('border', '1px solid #959595');
}

function clearNewGroup() {
    $('#newProfileGroupName').val('');
    $('#newProfileGroupName').css('border', '1px solid #959595');
}

if (window.location.href.indexOf("profile") > -1) {
    $('#newGroupModalTitle').text('Add profile to group or casting');
}

var vr;

function quickSearch(evt) {
    evt = evt || window.event;
    if (evt.keyCode == 27) {
        $("#quickSearchResult").fadeOut();
        $('#quickSearch').val('');
        $('#quickSearch').blur();
        return;
    }

    var val = $("#quickSearch").val();
    if (vr) {
        vr.abort();
    }

    var i = 0,
        newVal = "";
    vr = $.getJSON("/quick?query=" + val, function (data) {
        if (data) {
            $.each(data, function (key, profile) {
                i++;
                if (profile['thumbnail'] == '' || profile['thumbnail'] == null) {
                    profile['thumbnail'] = '/template/app-assets/images/noimage.png';
                }
                console.log(profile["thumbnail"]);
                newVal += "<li><a href='/profile/view?id=" + profile["id"] + "' target='_blank'><div class='userQuickImage' style='background: url(" + profile["thumbnail"].replace("/home/aibdh80ehx97/public_html/sncasting.com", "") + ");'></div><h5>" + profile["firstName"] + " " + profile["lastName"] + "</h5><span>" + profile["age"] + "</span></a></li>";
                if (i == 5) {
                    return false;
                }
            });

            $("#quickSearchResult").html("");
            $("#quickSearchResult").append(newVal);
            $("#quickSearchResult").fadeIn();
        } else {
            $("#quickSearchResult").html('<li><p class="text-muted ml-1 mt-2">Hmm... nothing found &#58;&#40;</p></li>');

        }
    });
}
$("#quickSearch").focusout(function () {
    $('#quickSearch').val('');
});

function createGroup() {
    var listProfiles = "";
    $("body").find(".selectedProfile").each(function () {
        if (listProfiles.length > 0) {
            listProfiles += ",";
        }
        listProfiles += $(this).data("id");
    });
    if ($("#existingGroup").val() > 0) {
        window.location = "/groups/addTo?id=" + $("#existingGroup").val() + "&profiles=" + listProfiles;
    } else {
        window.location = "/groups/add?name=" + $("#newProfileGroupName").val() + "&profiles=" + listProfiles;
    }
}


function linkAsFamily() {
    var listProfiles = "";
    $("body").find(".selectedProfile").each(function () {
        if (listProfiles.length > 0) {
            listProfiles += ",";
        }
        listProfiles += $(this).data("id");
    });

    $.ajax("/profile/family?&profiles=" + listProfiles)
        .done(function () {
            toastr.success('Selected profiles have been joined as a family', 'Join as family', {
                "showDuration": 500
            });
        });
}

function addToCasting(casting) {
    var listProfiles = "";
    $("body").find(".selectedProfile").each(function () {
        if (listProfiles.length > 0) {
            listProfiles += ",";
        }
        listProfiles += $(this).data("id");
    });
    window.location = "/castings/addTo?&profiles=" + listProfiles + "&casting=" + casting;
}

function addOneToCasting(casting) {
    var listProfiles = "";
    $("body").find(".selectedProfile").each(function () {
        if (listProfiles.length > 0) {
            listProfiles += ",";
        }
        listProfiles += $(this).data("id");
    });
    window.location = "/castings/addTo?&profiles=" + listProfiles + "&casting=" + casting;
}

function removeMultiple(type, groupId) {
    var listProfiles = "";
    $("body").find(".selectedProfile").each(function () {
        if (listProfiles.length > 0) {
            listProfiles += ",";
        }
        listProfiles += $(this).data("id");
    });

    if (type == 'profiles') {
        window.location = '/profile/delete?id=' + listProfiles;
    } else {
        window.location = '/' + type + '/deleteMultipleFrom?id=' + groupId + '&profiles=' + listProfiles;
    }

}

$("#viewAsList").click(function () {
    $(".thumb-photo-place").css('padding-bottom', '0');
    console.log('Viewing as list');
});


$("#viewAsThumbs").click(function () {
    $(".thumb-photo-place").css('padding-bottom', '100%');
    console.log('Viewing as thumbs');
});


$('#scrollToBottom').click(function () {
    $('html, body').animate({
        scrollTop: $(document).height()
    }, 1200);
    return false;
});
$('#scrollToTop').click(function () {
    $('html, body').animate({
        scrollTop: 0
    }, 1200);
    return false;
});

$(".trimit").focusout(function () {
    if ($(this).val() != '') {
        $(this).val($.trim($(this).val()));
    }
});

// Select dropdowns
if ($('select').length) {
    // Traverse through all dropdowns
    $.each($('select'), function (i, val) {
        var $el = $(val);

        // If there's any dropdown with default option selected
        // give them `not_chosen` class to style appropriately
        // We assume default option to have a value of ''
        if (!$el.val()) {
            $el.addClass('not_chosen');
        }

        // Add change event handler to do the same thing,
        // i.e., adding/removing classes for proper
        // styling. Basically we're emulating placeholder
        // behaviour on select dropdowns.
        $el.on('change', function () {
            if (!$el.val()) {
                $el.addClass('not_chosen');
            } else {
                $el.removeClass('not_chosen');
            }
        });

        // end of each callback
    });
}

$('.btn-toggle-mob-actions').click(function () {
    $(this).parent().find('.thumb-tools').toggleClass('active');
    $(this).parent().find('.thumb-mob-actions').toggleClass('active');
});

$('#keywords').click(function () {
    $('.type-zone').focus();
});

$('.type-zone').on("focus", function () {
    console.log('WTFin');
    $('#keywords').addClass('active');
});

$('.type-zone').on("blur", function () {
    console.log('WTFout');
    $('#keywords').removeClass('active');
});


$('#editGroup').on('shown.bs.modal', function () {
    $('#editProfileGroupName').focus();
});

$('form input').on('keypress', function (e) {
    return e.which !== 13;
});




if ($("#keywords").length > 0) {

    var t, $tag_box;

    t = $("#keywords").tagging({
        "edit-on-delete": false,
    });

    $tag_box = t[0];

    // Execute callback when a tag is added
    $tag_box.on("add:after", function (el, text, tagging) {
        $('input[name="data[keywords]"]').val(JSON.stringify($tag_box.tagging("getTags")));
        //console.log($('input[name="data[keywords]"]').val());
    });

    // Execute callback when a tag is removed
    $tag_box.on("remove:after", function (el, text, tagging) {
        $('input[name="data[keywords]"]').val(JSON.stringify($tag_box.tagging("getTags")));
    });

}

$('.press-repeater').repeater({
    show: function () {
        $(this).slideDown();
    },
    hide: function (remove) {
        //if (confirm('Are you sure you want to remove this item?')) {
        $(this).slideUp(remove);
        setTimeout(updatePress, 1000);
        //}
    },
    isFirstItemUndeletable: true
});

$('.press-repeater').on('blur', '.input-press', updatePress);

function updatePress() {
    var allValues = [];
    $('.press-repeater .input-press').each(function () {
        if ($(this).val() != '') {
            allValues.push($(this).val());
        }
    })
    $("#press").val(JSON.stringify(allValues));
    console.log($("#press").val());
}

$('.instagram-repeater').repeater({
    show: function () {
        $(this).slideDown();
    },
    hide: function (remove) {
        //if (confirm('Are you sure you want to remove this item?')) {
        $(this).slideUp(remove);
        setTimeout(updateInstagram, 1000);
        //}
    },
    isFirstItemUndeletable: true
});

$('.instagram-repeater').on('blur', '.input-instagram', updateInstagram);

function updateInstagram() {
    var allValues = [];
    $('.instagram-repeater .input-instagram').each(function () {
        if ($(this).val() != '') {
            allValues.push($(this).val());
        }
    })
    $("#instagram").val(JSON.stringify(allValues));
    console.log($("#instagram").val());
}

$('.video-repeater').repeater({
    show: function () {
        $(this).slideDown();
    },
    hide: function (remove) {
        //if (confirm('Are you sure you want to remove this item?')) {
        $(this).slideUp(remove);
        setTimeout(updateVideo, 1000);
        //}
    },
    isFirstItemUndeletable: true
});

$('.video-repeater').on('blur', '.input-video', updateVideo);

function updateVideo() {
    var allValues = [];
    $('.video-repeater .input-video').each(function () {
        if ($(this).val() != '') {
            allValues.push($(this).val());
        }
    })
    $("#video").val(JSON.stringify(allValues));
    console.log($("#video").val());
}
$('.music-repeater').repeater({
    show: function () {
        $(this).slideDown();
    },
    hide: function (remove) {
        //if (confirm('Are you sure you want to remove this item?')) {
        $(this).slideUp(remove);
        setTimeout(updateMusic, 1000);
        //}
    },
    isFirstItemUndeletable: true
});

$('.music-repeater').on('blur', '.input-music', updateMusic);

function updateMusic() {
    var allValues = [];
    $('.music-repeater .input-music').each(function () {
        if ($(this).val() != '') {
            allValues.push($(this).val());
        }
    })
    $("#music").val(JSON.stringify(allValues));
    console.log($("#music").val());
}




var profile = $('body').find('.thumbnail-container');

profile.each(function () {
    if ($(this).hasClass('selectedProfile')) {
        $(this).fadeOut();

        $(this).find('.mob-select-icon').removeClass('ft-plus-circle');
        $(this).find('.mob-select-icon').addClass('ft-minus-circle');
    } else {
        $(this).find('.mob-select-icon').removeClass('ft-minus-circle');
        $(this).find('.mob-select-icon').addClass('ft-plus-circle');
    }
});

var profileComment = $('body').find('.comment-input');

profileComment.each(function () {
    if ($.trim($(this).val()).length !== 0) {
        $(this).closest('.thumbnail-container').addClass('hasComment');
    }
});
$('.comment-input').on('keyup keypress', function (e) {
    if ($.trim($(this).val()).length !== 0) {
        $(this).closest('.thumbnail-container').addClass('hasComment');
    }
});
var profileWithComment = $('body').find('.thumbnail-container').hasClass('hasComment');


// Show/Hide comments
$(document).on('click', '.btn-reveal-comments', function () {
    console.log('Toggle all present comments');

    $(this).toggleClass('active');

    if ($('.btn-reveal-comments').hasClass('active')) {
        profile.each(function () {
            if ($(this).hasClass('hasComment')) {
                $(this).find('.thumb-comments').addClass('active');
                $(this).find('.thumbnail-photo').addClass('active');
                $(this).find('.thumb-overlay').addClass('active');
                $(this).find('.btn-comments').addClass('active');
            } else {
                //$(this).parent().fadeOut();
            }
        });
    } else {
        profile.each(function () {
            $(this).find('.thumb-comments').removeClass('active');
            $(this).find('.thumbnail-photo').removeClass('active');
            $(this).find('.thumb-overlay').removeClass('active');
            $(this).find('.btn-comments').removeClass('active');
            //$(this).parent().fadeIn();
        });
    }

});
// End of Show/Hide comments

if (deviceIsMobile == false) {
    $('body').find('.mobile-only').css('display', 'none');
} else {
    $('body').find('.mobile-only').css('display', 'block');
}
if (deviceIsDesktop == false) {
    $('body').find('.desktop-only').css('display', 'none');
} else {
    $('body').find('.desktop-only').css('display', 'block');
}
