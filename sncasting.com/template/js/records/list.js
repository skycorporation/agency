function calculateDivider() {
    var e = 4;
    if ($(this).width() <= 480) {
        e = 1
    } else if ($(this).width() <= 767) {
        e = 2
    } else if ($(this).width() <= 980) {
        e = 3
    }
    return e
}
var handleIsotopesGallery = function () {
    "use strict";
    //$(window).load(function () {
        var e = $("#gallery");
        var t = calculateDivider();
        var n = $(e).width() - 20;
        var r = n / t;
        $(e).isotope({resizable: false, masonry: {columnWidth: r, columnHeight:'400px'}});
        $(window).smartresize(function () {
            var t = calculateDivider();
            var n = $(e).width() - 20;
            var r = n / t;
            $(e).isotope({masonry: {columnWidth: r, columnHeight:'400px'}})
        });

    //})
};
var handleDP = function () {
    $('#advance-daterange span').html(moment().subtract('days', 5).format('D MMMM, YYYY') + ' - ' + moment().format('D MMMM, YYYY'));
    moment().locale(navigator.language);
    $('#advance-daterange').daterangepicker({
        format: 'DD.MM.YYYY',
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '01.01.2012',
        maxDate: '31.12.2016',
        dateLimit: {days: 60},
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
            'Сегодня': [moment(), moment()],
            'Вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Последние 5 дней': [moment().subtract(6, 'days'), moment()],
            'Последние 30 дней': [moment().subtract(29, 'days'), moment()],
            'Этот месяц': [moment().startOf('month'), moment().endOf('month')],
            'Прошлый месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'right',
        drops: 'down',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-primary',
        cancelClass: 'btn-default',
        separator: ' to ',
        locale: {
            applyLabel: 'Применить',
            cancelLabel: 'Отмена',
            fromLabel: 'От',
            toLabel: 'По',
            customRangeLabel: 'Произвольная',
            daysOfWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            firstDay: 1
        }
    }, function (start, end, label) {
        $('#df').val( start.format('YYYY-MM-DD'));
        $('#dt').val( end.format('YYYY-MM-DD'));
        $('#advance-daterange span').html(start.format('D MMMM, YYYY') + ' - ' + end.format('D MMMM, YYYY'));
    });

}
var Gallery = function () {
    "use strict";
    return{init: function () {
            handleIsotopesGallery();
            
        }}
}()
$(function () {
    handleDP();
    //Gallery.init();
    $('#form').submit( function(){
        $(this).ajaxSubmit({
            success: function(html){
                $('#list').html( html );
                Gallery.init();
            }
        })
        return false;
    })
    $('#form').submit();
})