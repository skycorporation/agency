function Crop() {
    this.ratio = 1;
    this.$cropper = false;
    this.element = false;
    this.url = '';
    this.setElement = function (el) {
        this.element = el;
    };
    this.setRatio = function (ratio) {
        var t = ratio.split(':');
        this.ratio = parseFloat(parseInt(t[0]) / parseInt(t[1]));
    };
    this.setImageUrl = function (url) {
        this.url = url;
        this.$cropper.cropper('reset', true);
        this.$cropper.cropper('setImgSrc', url);
        this.$cropper.cropper('setAspectRatio', this.ratio);
    };
    this.init = function () {
        var inst = this;
        $('.crop').on('build.cropper', function () {
            var w = $('.crop').cropper('getImageData')[0].naturalWidth;
            var h = $('.crop').cropper('getImageData')[0].naturalHeight;
            if (w < h) {
                $('.crop').cropper('setAspectRatio', 1 / inst.ratio);
            }
        }).cropper();
        this.$cropper = $(".crop");
    };
    this.getInfo = function () {
        //this.$cropper.cropper("getData");
        return this.$cropper.cropper("getData");
    };
    this.showModal = function (url, success) {
        this.setImageUrl(url);
        var env = this;
        $('#cropManager').find('.save').off('click').on('click', function () {
            var req = env.getInfo();
            req.url = env.url;
            $.post('/uploader/crop', req, function (res) {
                success(env.element, res.url, res);
                $('#cropManager').find('.cancel').click();
            }, 'json');
        });
        $('#cropManager').find('.cancel').off('click').on('click', function () {
            $('#cropManager').hide();
            $('#cropManagerCover').hide();
        });

        $('#cropManagerCover').show();
        var w = $(window).width();
        $('#cropManager').css({
            left: parseInt(w) / 2 - parseInt($('#cropManager').width()) / 2
        }).show();
    }



}