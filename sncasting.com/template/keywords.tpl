{include file="header.tpl"}
{include file="modals/deleteProfile.tpl"}
{include file="modals/newGroup.tpl"}
{include file="modals/deleteMultipleProfiles.tpl"}
<style>
    	.keyword{
	    	font-size: 32px;
	    	display: inline-block;
	    	margin: 10px 20px;
    	}
    	.keyword span{
	    	font-size: 16px;
    	}
    </style>
  <input type="hidden" value="search" name="action">
  <section class="card">
    <div class="card-header">
      <h4 class="card-title">Keywords</h4>
    </div>
    <div class="card-body">
	    <div class="card-block">
	    	<div class="row">
	    	 {foreach from=$keywords item=t key=k}
	    	 	<a href="/search?action=search&data%5Bkeywords%5D=%5B%22{$k}%22%5D" class="keyword">{$k}<span>{$t}</span></a>
	    	 {/foreach}
	    	</div>
	    </div>
    </div>
  </section>


{include file="footer.tpl"}