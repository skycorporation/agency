{include file="header.tpl"}

{if $my->id>0}
{include file="modals/removeFromCasting.tpl"}
{include file="modals/removeMultipleFromCasting.tpl"}
{include file="modals/newGroup.tpl"}
{include file="modals/editCasting.tpl"}
{/if}
<section id="hover-effects">
  <div class="row">
    <div class="col-12 viewSwitcher mobile-only pb-1">
      <button type="button" id="scrollToBottom"><i class="icon-arrow-down4"></i></button>
    </div>
    <div class="col-12">
      <div class="breadcrumbs">
        <div class="breadcrumb-wrapper text-center">
          <ol class="breadcrumb d-inline-block p-0">
            <li class="breadcrumb-item"><a href="/castings">Castings</a></li>
            <li class="breadcrumb-item active">{$group->table->name}</li>
          </ol>
        </div>
      </div>
    </div>
    {if $my->id>0}
    <div class="col-12 text-center">
      <div class="btn-group">
        <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn mybtn-green dropdown-toggle">Actions</button>
        <div class="dropdown-menu arrow">
          <button class="dropdown-item" data-toggle="modal" data-target="#castingEdit" onclick="$('#editCastingForm').find('[name=name]').val($(this).data('name'));
              $('#editCastingForm').find('[name=location]').val($(this).data('location'));
              $('#editCastingForm').find('[name=date]').val($(this).data('date'));
              $('#editCastingForm').find('[name=id]').val($(this).data('id'));
              $('#editCastingForm').find('[name=url]').val($(this).data('url'));" data-name="{$group->table->name}" data-id="{$group->table->id}" data-location="{$group->table->location}" data-date="{$group->table->date}" data-url="{$group->table->url}"><i class="ft-edit-2 mr-1"></i>Edit casting</button>
          <div class="dropdown-divider mobile-only"></div>
          <button class="dropdown-item mobile-only active" id="viewAsThumbs"><i class="icon-grid2 mr-1"></i>View as grid</button>
          <button class="dropdown-item mobile-only" id="viewAsList"><i class="icon-align-justify2 mr-1"></i>View as List</button>
          <div class="dropdown-divider"></div>
          <button onclick="selectAll(); return false;" class="dropdown-item"><i class="ft-plus-circle mr-1"></i>Select all</button>
          <button onclick="selectFavs(); return false;" class="dropdown-item"><i class="ft-heart mr-1"></i>Select favorites</button>
          <button class="dropdown-item btn-reveal-comments"><i class="ft-message-circle mr-1"></i>Reveal comments</button>
          <a href="/casting/photos?id={$group->table->id}" class="dropdown-item"><i class="ft-upload-cloud mr-1"></i>Upload photos</a> </div>
      </div>
    </div>
    {/if}
    <div class="col-12 text-center {if $my->id>0}mt-2{/if}">
      <div class="d-block">{if $group->table->location}{$group->table->location}, {/if}{$group->table->date}</div>
    </div>
    {if $my->id>0}
    <div class="castingURL"><i class="icon-clipboard4 icon"></i><br>
      <a href="/c/{$group->table->url}" class="castingsheeturl" target="_blank" title="Casting sheet URL">www.sncasting.com/c/{$group->table->url}</a> </div>
    {/if} </div>
  <div class="row">
    <div class="grid-hover"> {foreach from=$profiles item=t key=k}
      {assign var="size" value=getimagesize($t->thumbnail)}
      <div class="col-md-4 col-xs-12" >
        <div class="thumbnail-container{if $t->fav} likedProfile{/if} {if $t->hidden} hiddenProfile{/if} {if $t->comment}hasComment{/if}" data-id="{$t->id}">
          <div class="thumb-photo-place"><img class="thumbnail-photo" src="{$t->thumbnail|replace:'/home/aibdh80ehx97/public_html/sncasting.com':''}" alt="Thumbnail" {if $size[0]>$size[1]}style="height: 380px; width: auto;"{/if}onerror="this.src='/template/app-assets/images/noimage.png'"/>
            <div class="thumb-overlay"></div>
            <div class="thumb-content"> {if $my->id>0}
              <div class="editPanel desktop-only">
                <button onclick="location='/profile/edit?id={$t->id}'" class="editButton" title="Edit profile"><i class="ft-edit-2"></i></button>
                <button class="deleteProfileButton" data-toggle="modal" data-target="#removeFromCasting" onclick="profileToDelete={$t->id}" title="Remove from casting"><i class="ft-user-minus"></i></button>
              </div>
              {/if} <a class="thumb-info" href="/profile/view?id={$t->id}" style="{if $my->id==0}top: 0;{/if}">
              <p> {if $t->basedIn}<i class="ft-map-pin"></i><b>{$t->basedIn}</b><br>
                <br>
                {/if}
                {if $t->occupation}<i class="ft-award"></i><b>{$t->occupation}</b><br>
                <br>
                {/if}
                {if $t->height}<i class="ft-arrow-up"></i><b>{$t->height}</b>{/if} </p>
              </a> {if $my->id>0}
              <button class="selectButton desktop-only" onclick="selectProfile(this)">Select</button>
              {/if} </div>
            <div class="thumb-comments">
              <p class="text-center">Type comments here:</p>
              <textarea class="comment-input" spellcheck="false">{$t->comment}</textarea>
              <button class="btn-clear-comments" onclick="clearComments(this, 'castings', {$group->table->id}, {$t->id});">CLEAR COMMENTS</button>
              <button class="btn-clear-comments" onclick="saveComments(this, 'castings', {$group->table->id}, {$t->id});">SAVE COMMENTS</button>
            </div>
          </div>
          <a class="name-place" href="/profile/view?id={$t->id}" target="_blank">{$t->firstName}&nbsp;{$t->lastName}</a>
          <div class="thumbnail-toolbar"> {if $my->id>0} <span class="pnum">&#35;{$t->num}</span> {/if}
            <div class="thumb-tools">
              <button class="btn-comments"><i class="fa fa-comment" title="Profile comments"></i></button>
              <button class="btn-like" onclick="loveProfile(this, 'castings', {$group->table->id}, {$t->id})" title="Mark as favorite"><i class="fa fa-heart"></i></button>
              {if $my->id>0}
              <button class="btn-hide" onclick="hideProfile(this, 'castings', {$group->table->id}, {$t->id})"><i class="{if $t->hidden}ft-eye-off{else}ft-eye{/if}"></i></button>
              {/if} </div>
            {if $my->id>0}
            <div class="thumb-mob-actions">
              <div class="mob-actions-inner">
                <button class="btn-mob-edit" onclick="location='/profile/edit?id={$t->id}'"><i class="ft-edit-2" title="Edit profile"></i></button>
                <button class="btn-mob-remove" data-toggle="modal" data-target="#removeFromCasting" onclick="profileToDelete={$t->id}" title="Remove from casting"><i class="ft-x"></i></button>
                <button class="btn-mob-select" onclick="mobSelectProfile(this)"><i class="ft-plus-circle mob-select-icon" title="Select profile"></i></button>
              </div>
            </div>
            <button class="btn-toggle-mob-actions mobile-only"><i class="ft-more-horizontal"></i></button>
            {/if} </div>
        </div>
      </div>
      {/foreach} </div>
  </div>
  <div class="row">
    <div class="col-12 viewSwitcher mobile-only">
      <button type="button" id="scrollToTop" class="mt-2 mb-2"><i class="icon-arrow-up4"></i></button>
    </div>
  </div>
</section>
{include file="footer.tpl"}