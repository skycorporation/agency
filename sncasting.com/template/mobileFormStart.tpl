<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Present Motion">
    <title>Shay Nielsen Casting</title>
    <link rel="apple-touch-icon" sizes="60x60" href="/template/app-assets/images/ico/apple-icon-60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/template/app-assets/images/ico/apple-icon-76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/template/app-assets/images/ico/apple-icon-120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/template/app-assets/images/ico/apple-icon-152.png">
    <link rel="shortcut icon" type="image/x-icon" href="/template/app-assets/images/ico/favicon.ico">
    <link rel="shortcut icon" type="image/png" href="/template/app-assets/images/ico/favicon-32.png">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="/template/app-assets/css/bootstrap.css">
    <!-- font icons-->
    <link rel="stylesheet" type="text/css" href="/template/app-assets/fonts/icomoon.css">
    <link rel="stylesheet" type="text/css" href="/template/app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/sliders/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/extensions/pace.css">
    <link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/ui/prism.min.css">
    <link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/js/gallery/photo-swipe/photoswipe.css">
    <link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/js/gallery/photo-swipe/default-skin/default-skin.css">
    <link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/pickers/datetime/bootstrap-datetimepicker.css">
    <link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/file-uploaders/dropzone.min.css">
    <link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/extensions/nouislider.min.css">
    <link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/forms/toggle/bootstrap-switch.min.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN ROBUST CSS-->
    <link rel="stylesheet" type="text/css" href="/template/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/template/app-assets/css/app.css">
    <link rel="stylesheet" type="text/css" href="/template/app-assets/css/colors.css">
    <!-- END ROBUST CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="/template/app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="/template/app-assets/css/core/menu/menu-types/vertical-overlay-menu.css">
    <link rel="stylesheet" type="text/css" href="/template/app-assets/css/pages/gallery.css">
    <link rel="stylesheet" type="text/css" href="/template/app-assets/css/plugins/pickers/daterange/daterange.css">
    <link rel="stylesheet" type="text/css" href="/template/app-assets/css/plugins/file-uploaders/dropzone.css">
    <link rel="stylesheet" type="text/css" href="/template/app-assets/css/pages/users.css">
    <link rel="stylesheet" type="text/css" href="/template/app-assets/css/pages/timeline.css">
    <link rel="stylesheet" type="text/css" href="/template/app-assets/css-rtl/plugins/extensions/noui-slider.css">
    <link rel="stylesheet" type="text/css" href="/template/app-assets/css-rtl/core/colors/palette-noui.css">
    <link rel="stylesheet" type="text/css" href="/template/app-assets/css-rtl/plugins/forms/switch.css">
    <link rel="stylesheet" type="text/css" href="/template/app-assets/css-rtl/core/colors/palette-switch.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/template/app-assets/css/style.css">
    <!-- END Custom CSS-->
  </head>
  <body data-open="hover" data-menu="horizontal-menu" data-col="2-columns" class="horizontal-layout horizontal-menu 2-columns ">


    
    <!-- ////////////////////////////////////////////////////////////////////////////-->


	<div class="mobileForm row-flex align-items-center h-100">
	  	<div class="col text-center">
			<h1 class="success">{$castingList->name} casting</h1>
			<h4>Fill out casting sheet for:</h4>
			<strong>Child</strong> - 12 years or less<br>
			<strong>Teen</strong> - 13 to 17 years old<br>
			<strong>Adult</strong> - 18 years and older<br><br>
			
		{foreach from=$fields["profileType"] item=t key=k}
		<a type="button" href="?profileType={$k}" class="mybtn2 mybtn-green">{$t}</a><br>
		{/foreach}
		</div>
	</div>



    <!-- BEGIN VENDOR JS-->
    <script src="/template/app-assets/js/core/libraries/jquery.min.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/ui/tether.min.js" type="text/javascript"></script>
    <script src="/template/app-assets/js/core/libraries/bootstrap.min.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/ui/unison.min.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/ui/blockUI.min.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/ui/jquery.matchHeight-min.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/ui/jquery-sliding-menu.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/sliders/slick/slick.min.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/ui/screenfull.min.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/extensions/pace.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script type="text/javascript" src="/template/app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script type="text/javascript" src="/template/app-assets/vendors/js/ui/prism.min.js"></script>
    <script src="/template/app-assets/vendors/js/gallery/masonry/masonry.pkgd.min.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/pickers/dateTime/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/pickers/daterange/daterangepicker.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/extensions/wNumb.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/forms/toggle/bootstrap-checkbox.js" type="text/javascript"></script>
   
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN ROBUST JS-->
    <script src="/template/app-assets/js/core/app-menu.js" type="text/javascript"></script>
    <script src="/template/app-assets/js/core/app.js" type="text/javascript"></script>
    <script src="/template/app-assets/js/scripts/ui/fullscreenSearch.js" type="text/javascript"></script>
    <!-- END ROBUST JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="/template/js/script.js" type="text/javascript"></script>
    {foreach from=$js item=url}
	<script src="{$url}" type="text/javascript"></script>
	{/foreach}
    <script src="/template/app-assets/js/scripts/grid/grid.js" type="text/javascript"></script>
    <script src="/template/app-assets/js/scripts/grid/gridStart.js" type="text/javascript"></script>
    <script src="/template/app-assets/js/scripts/pickers/dateTime/picker-date-time.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
    <!-- BEGIN USER JS-->
	<script src="/template/app-assets/js/formfeedback.js" type="text/javascript"></script>
	<!-- END USER JS-->
  </body>
</html>