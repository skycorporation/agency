//Basic validation (check if data exists)

$(".checkThis").focusin(function () {
	$(this).css('border', '1px solid #FDC637');

});

$(".checkThis").focusout(function () {
	if ($(this).val() !== "") {
		$(this).css('border', '1px solid #1AB291');

	} else {
		$(this).css('border', '1px solid #959595');

	}
});
//Capitalize first letter

$(".capThis").focusin(function () {
	$(this).css('border', '1px solid #FDC637');
	$(this).keypress(function () {
		var _val = $(this).val();
		var _txt = _val.charAt(0).toUpperCase() + _val.slice(1);
		$(this).val(_txt);
	});
});

$(".capThis").focusout(function () {
	if ($(this).val() !== "") {
		$(this).css('border', '1px solid #1AB291');

	} else {
		$(this).css('border', '1px solid #959595');

	}
});

//Email validation

$(".emailfield").focusin(function () {
	$(this).css('border', '1px solid #FDC637');
});

$(".emailfield").focusout(function () {
	if ($(this).val() !== "") {

		var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		var address = $(this).val();

		if (reg.test(address) === false) {
			$(this).css('border', '2px solid #DC5150');
			$("#formAttention").addClass('active');
		} else {
			$(this).css('border', '1px solid #1AB291');
			$("#formAttention").removeClass('active');
		}
	} else {
		$(this).css('border', '1px solid #959595');
		$("#formAttention").removeClass('active');
	}
});

//State validation

$(".statefield").focusin(function () {
	$(this).css('border', '1px solid #FDC637');

	// Capitalize string - convert textbox user entered text to uppercase
	$(this).keyup(function () {
		$(this).val($(this).val().toUpperCase());
	});

});
$(".statefield").focusout(function () {
	if ($(this).val() !== "") {

		var statereg = /^(A[LKSZRAEP]|C[AOT]|D[EC]|F[LM]|G[ANU]|HI|I[ADLN]|K[SY]|LA|M[ADEHINOPST]|N[CDEHJMVY]|O[HKR]|P[ARW]|RI|S[CD]|T[NX]|UT|V[AIT]|W[AIVY])$/;
		var state = $(this).val();

		if (statereg.test(state) === false) {
			$(this).css('border', '2px solid #DC5150');
			$("#formAttention").css('transition', 'all 0.5s ease-in-out');
			$("#formAttention").css('opacity', '1');

		} else {
			$(this).css('border', '1px solid #1AB291');
			$("#formAttention").css('opacity', '0');
		}
	} else {
		$(this).css('border', '1px solid #959595');
		$("#formAttention").css('opacity', '0');
	}
});

//Zip validation

$(".zipfield").focusin(function () {
	$(this).css('border', '1px solid #FDC637');
	$(this).keypress(function (e) {
		//if the letter is not digit then display error and don't type anything
		if (e.which !== 8 && e.which !== 0 && (e.which < 48 || e.which > 57)) {
			//display error message
			return false;
		}
	});
});

$(".zipfield").focusout(function () {
	if ($(this).val() !== "") {

		var zipreg = /^\d{5}$/;
		var zip = $(this).val();

		if (zipreg.test(zip) === false) {
			$(this).css('border', '2px solid #DC5150');
			$("#formAttention").css('transition', 'all 0.5s ease-in-out');
			$("#formAttention").css('opacity', '1');

		} else {
			$(this).css('border', '1px solid #1AB291');
			$("#formAttention").css('opacity', '0');
		}
	} else {
		$(this).css('border', '1px solid #959595');
		$("#formAttention").css('opacity', '0');

	}
});

//Number validation

$(".numOnly").focusin(function () {
	$(this).css('border', '1px solid #FDC637');
	$(this).keypress(function (e) {
		//if the letter is not digit then display error and don't type anything
		if (e.which !== 8 && e.which !== 0 && (e.which < 48 || e.which > 57)) {
			//display error message
			return false;
		}
	});
});

$(".numOnly").focusout(function () {
	if ($(this).val() !== "") {

		var numreg = /^\d+$/;
		var num = $(this).val();

		if (numreg.test(num) === false) {
			$(this).css('border', '2px solid #DC5150');

		} else {
			$(this).css('border', '1px solid #1AB291');
		}
	} else {
		$(this).css('border', '1px solid #959595');

	}
});

//Instagram validation

$('.form-group').on('focusin', '.input-instagram', function () {
	$(this).css('border', '1px solid #FDC637');
	$(".input-instagram").keypress(function (e) {
		if (e.which === 64) {

			return false;
		}
	});
});

$('.form-group').on('focusout', '.input-instagram', function () {
	var instaval = $(this).val();
	console.log('Instagram value=' + instaval);
	var instareg = /@/;

	if ($(this).val() !== '') {
		if (instareg.test(instaval) === true) {
			$(this).css('border', '2px solid #DC5150');
			$(this).parent().find(".errormsg").css('display', 'block');
			$(this).parent().find(".errormsg").text("*Please don't type the @ symbol");
		} else {
			$(this).css('border', '1px solid #1AB291');
			$(this).parent().find(".errormsg").css('display', 'none');
		}
	} else {
		$(this).css('border', '1px solid #959595');
		$(this).parent().find(".errormsg").css('display', 'none');
	}
});


//URL validation

$(".urlfield").focusin(function () {
	$(this).css('border', '1px solid #FDC637');
	console.log('Focus in');
});

$('.form-group').on('focusout', '.urlfield', function () {

	var newurl = $(this).val();
	var urlreg = /^(https?:)?\/\//i;
	console.log('URL new content: ' + newurl);
	if ($(this).val() !== '') { //Check if url is not blank
		$(this).val($.trim($(this).val())); //Removes blank spaces from start and end
		console.log('URL trimmed');
		if (urlreg.test(newurl) === false) { //Checks for if url doesn't match either of: http://example.com, https://example.com AND //example.com
			console.log('no HTTP or HTTPS detected');
			$(this).val('http://' + newurl); //Prepend http:// to the URL
			$(this).css('border', '1px solid #1AB291');
		} else {
			$(this).css('border', '1px solid #1AB291');
		}
	} else {
		$(this).css('border', '1px solid #959595');
	}
});


/*$(".urlfield").focusin(function () {
		$(this).css('border', '1px solid #FDC637');
	});

	$(".urlfield").focusout(function () {
		if ($(this).val() !== "") {

			var reg = /(\b((?:https?|ftp):\/\/|www\.)([0-9A-Za-z]+\.?)+\b)/;
			var urladdress = $(this).val();

			if (reg.test(urladdress) === false) {
				$(this).css('border', '2px solid #DC5150');
				$("#formAttention").css('transition', 'all 0.5s ease-in-out');
				$("#formAttention").css('opacity', '1');
			} else {
				$(this).css('border', '1px solid #1AB291');
				$("#formAttention").css('opacity', '0');
			}
		} else {
			$(this).css('border', '1px solid #959595');
			$("#formAttention").css('opacity', '0');
		}
	});*/
