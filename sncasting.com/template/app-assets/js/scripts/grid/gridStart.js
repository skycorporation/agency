$(function(){
	$("#grid").gridalicious({
	  gutter: 1,
	  width: 250,
	  animate: true,
	  animationOptions: {
	    queue: true,
	    speed: 200,
	    duration: 300,
	    effect: 'fadeInOnAppear'
	  }
	});
});