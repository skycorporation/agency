{include file="../header.tpl"}
{include file="../left.tpl"}

{if isset($smarty.get.new)}
    <div class="alert alert-warning">
        <h4>Новые пропуска успешно созданы</h4>
        Отредактировать их вы можете на странице <a href="/passes/list">мои пропуска</a>
    </div>
    {/if}
<!-- begin invoice -->
<div class="invoice">
    <div class="invoice-company">
        <span class="pull-right hidden-print">
            <a href="/invoices/view/{$i->id}?mode=pdf" class="btn btn-sm btn-success m-b-10"><i class="fa fa-download m-r-5"></i> Экспорт PDF</a>
            <a href="/invoices/view/{$i->id}?mode=print" class="btn btn-sm btn-success m-b-10"><i class="fa fa-print m-r-5"></i> Печать</a>
            <a href="/invoices/view/{$i->id}?mode=1с" class="btn btn-sm btn-success m-b-10"><i class="fa fa-exchange m-r-5"></i> КлиентБанк</a>
        </span>
        Счет на оплату №{$i->id} от {date("d.m.Y", strtotime( str_replace("-", '/', $i->creationDateTime)) )}
        {if $i->fineID>0}<Br>
            {Fines::getById($i->fineID)}
            {/if}
    </div>
    <div class="invoice-header">
        <div class="invoice-from">
            <small>Покупатель</small>
            <address class="m-t-5 m-b-5">
                <strong>{$buyer.full|default:""}</strong><br />
                {$buyer.address|default:""}<br />

                Телефон: {$buyer.phone|default:""}<br />
                Факс: {$buyer.fax|default:""}
            </address>
        </div>
        <div class="invoice-to">
            <small>Продавец</small>
            <address class="m-t-5 m-b-5">
                <strong>{$seller.full|default:""}</strong><br />
                {$seller.address|default:""}<br />

                Телефон: {$seller.phone|default:""}<br />
                Факс: {$seller.fax|default:""}
            </address>
        </div>
        <div class="invoice-date">
            <small>Счет</small>
            <div class="date m-t-5">{date("d.m.Y", strtotime( str_replace("-", '/', $i->creationDateTime)) )}</div>
            <div class="invoice-detail">
                #{$i->id}<br />

            </div>
        </div>
    </div>
    <div class="invoice-content">
        <div class="table-responsive">
            <table class="table table-invoice">
                <thead>
                    <tr>
                        <th>Наименование</th>
                        <th>Количество</th>
                        <th>Стоимость</th>
                        <th>Итого</th>
                    </tr>
                </thead>
                <tbody>
                    {foreach from=$i->getCart() key=subId item=count}
                        <tr>
                            <td>
                                {Passes_Sub::getById( $subId )->getPassType()}<br />
                                <small>{Passes_Sub::getById( $subId )}</small>
                            </td>
                            <td>{$count}</td>
                            <td>{($i->getAmount( $subId, $count)/$count)|number_format:"2":".":","} руб.</td>

                            <td>{$i->getAmount( $subId, $count)|number_format:"2":".":","} руб.</td>
                        </tr>
                    {foreachelse}
                        <tr>
                            <td>Оплата штрафа №{$i->fineID}</td>
                            <td> 1 </tD>
                            <td> {Fines::getById($i->fineID)->amount} </td>
                            <td> {Fines::getById($i->fineID)->amount} </td>
                        </tr>
                    {/foreach}

                </tbody>
            </table>
        </div>
        <div class="invoice-price">

            <div class="invoice-price-left">
                {*
                <div class="invoice-price-row">
                <div class="sub-price">
                <small>SUBTOTAL</small>
                $4,500.00
                </div>
                <div class="sub-price">
                <i class="fa fa-plus"></i>
                </div>
                <div class="sub-price">
                <small>PAYPAL FEE (5.4%)</small>
                $108.00
                </div>
                </div>
                *}
            </div>
            <div class="invoice-price-right">
                <small>ИТОГО</small> {$i->total()|number_format:"2":".":","} руб.
            </div>
        </div>
    </div>
    <div class="invoice-note">
        {*  * Make all cheques payable to [Your Company Name]<br />
        * Payment is due within 30 days<br />
        * If you have any questions concerning this invoice, contact  [Name, Phone Number, Email]*}
    </div>
    <div class="invoice-footer text-muted">
        {*
        <p class="text-center m-b-5">
        THANK YOU FOR YOUR BUSINESS
        </p>
        <p class="text-center">
        <span class="m-r-10"><i class="fa fa-globe"></i> matiasgallipoli.com</span>
        <span class="m-r-10"><i class="fa fa-phone"></i> T:016-18192302</span>
        <span class="m-r-10"><i class="fa fa-envelope"></i> rtiemps@gmail.com</span>
        </p>
        *}
    </div>
</div>
<!-- end invoice -->

{include file="../footer.tpl"}