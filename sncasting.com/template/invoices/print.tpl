
<!doctype html>
<html>
    <head>
        <title>Счет на оплату</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <style>
            body { width: 210mm; margin-left: auto; margin-right: auto; border: 1px #efefef solid; font-size: 11pt;}
            table.invoice_bank_rekv { {*border-collapse: collapse; *}border: 1px solid; }
            {*table.invoice_bank_rekv > tbody > tr > td, table.invoice_bank_rekv > tr > td { border: 1px solid; }*}
            table.invoice_items { border: 1px solid; border-collapse: collapse;}
            table.invoice_items td, table.invoice_items th { border: 1px solid;}
        </style>
    </head>
    <body>
        <table width="100%">
            <tr>
                <td>&nbsp;</td>
                <td style="width: 155mm;">
                    <div style="width:155mm; ">Внимание! Оплата данного счета означает согласие с условиями поставки товара.</div>
                </td>
            </tr>

        </table>


        <table width="100%" cellpadding="2" cellspacing="2" class="invoice_bank_rekv">
            <tr>
                <td colspan="2" rowspan="2" style="min-height:13mm; width: 105mm;">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="height: 13mm;">
                        <tr>
                            <td valign="top">
                                <div>{$seller.bank|default:""}</div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="bottom" style="height: 3mm;">
                                <div style="font-size:10pt;">Банк получателя        </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="min-height:7mm;height:auto; width: 25mm;">
                    <div>БИK</div>
                </td>
                <td rowspan="2" style="vertical-align: top; width: 60mm;">
                    <div style=" height: 7mm; line-height: 7mm; vertical-align: middle;">{$seller.bik|default:""}</div>
                    <div>{$seller.kor|default:""}</div>
                </td>
            </tr>
            <tr>
                <td style="width: 25mm;">
                    <div>Сч. №</div>
                </td>
            </tr>
            <tr>
                <td style="min-height:6mm; height:auto; width: 50mm;">
                    <div>ИНН {$seller.inn|default:""}</div>
                </td>
                <td style="min-height:6mm; height:auto; width: 55mm;">
                    <div>КПП {$seller.kpp|default:""}</div>
                </td>
                <td rowspan="2" style="min-height:19mm; height:auto; vertical-align: top; width: 25mm;">
                    <div>Сч. №</div>
                </td>
                <td rowspan="2" style="min-height:19mm; height:auto; vertical-align: top; width: 60mm;">
                    <div>{$seller.ras|default:""}</div>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="min-height:13mm; height:auto;">

                    <table border="0" cellpadding="0" cellspacing="0" style="height: 13mm; width: 105mm;">
                        <tr>
                            <td valign="top">
                                <div>{$seller.full|default:""}</div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="bottom" style="height: 3mm;">
                                <div style="font-size: 10pt;">Получатель</div>
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>
        </table>
        <br/>

        <div style="font-weight: bold; font-size: 16pt; padding-left:5px;">
            Счет № {$i->id} от {date("d.m.Y", strtotime($i->creationDateTime))}</div>
        <br/>

        <div style="background-color:#000000; width:100%; font-size:1px; height:2px;">&nbsp;</div>

        <table width="100%">
            <tr>
                <td style="width: 30mm;">
                    <div style=" padding-left:2px;">Поставщик:    </div>
                </td>
                <td>
                    <div style="font-weight:bold;  padding-left:2px;">
                        {$seller.full|default:""}, {$seller.address|default:""}           </div>
                </td>
            </tr>
            <tr>
                <td style="width: 30mm;">
                    <div style=" padding-left:2px;">Покупатель:    </div>
                </td>
                <td>
                    <div style="font-weight:bold;  padding-left:2px;">
                        {$buyer.full|default:""}, {$buyer.address|default:""}            </div>
                </td>
            </tr>
        </table>


        <table class="invoice_items" width="100%" cellpadding="2" cellspacing="2">
            <thead>
                <tr>
                    <th style="width:13mm;">№</th>
                    <th style="width:20mm;">Код</th>
                    <th>Товар</th>
                    <th style="width:20mm;">Кол-во</th>
                    <th style="width:17mm;">Ед.</th>
                    <th style="width:27mm;">Цена</th>
                    <th style="width:27mm;">Сумма</th>
                </tr>
            </thead>
            <tbody >
                {foreach from=$i->getCart() key=subId item=count name=i}
                    <tr>
                        <td>{$smarty.foreach.i.iteration}</td>
                        <td></td>

                        <td>
                            {Passes_Sub::getById( $subId )->getPassType()}<br />
                            <small>{Passes_Sub::getById( $subId )}</small>
                        </td>
                        <td>{$count}</td>
                        <td>усл.</td>
                        <td> {round( $i->getAmount( $subId, $count)/$count, 2) } </td>
                        <td>{$i->getAmount( $subId, $count)|number_format:"2":".":","} руб.</td>
                    </tr>
                {foreachelse}
                    <tr>
                        <td>1</td>
                        <td></td>
                        <td>Оплата штрафа №{$i->fineID}</td>
                        <td> 1 </tD>
                        <td> усл. </tD>
                        <td> {Fines::getById($i->fineID)->amount} </td>
                        <td> {Fines::getById($i->fineID)->amount} </td>
                        
                    </tr>
                {/foreach}
            </tbody>
        </table>

        <table border="0" width="100%" cellpadding="1" cellspacing="1">


            <tr>
                <td></td>
                <td style="width:27mm; font-weight:bold;  text-align:right;">Итого:</td>
                <td style="width:27mm; font-weight:bold;  text-align:right;">{$i->total()|number_format:"2":".":","} руб.</td>
            </tr>
        </table>

        <br />
        <div>
            Всего наименований {count($i->getCart())} на сумму {$i->total()|number_format:"2":".":","} рублей.<br />
            {Utils::num2str( $i->total() )}</div>
        <br /><br />
        <div style="background-color:#000000; width:100%; font-size:1px; height:2px;">&nbsp;</div>
        <br/>

        <div>Руководитель ______________________ (Фамилия И.О.)</div>
        <br/>

        <div>Главный бухгалтер ______________________ (Фамилия И.О.)</div>
        <br/>

        <div style="width: 85mm;text-align:center;">М.П.</div>
        <br/>


        <div style="width:800px;text-align:left;font-size:10pt;">Счет действителен к оплате в течении трех дней.</div>

    </body>
    <script>window.print()</script>
</html>
