{include file="../header.tpl"}
{include file="../left.tpl"}


<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Счета</h1>
<!-- end page-header -->

<!-- begin panel -->
<div class="panel panel-inverse">
    <div class="panel-heading">
        
        <h4 class="panel-title">Список счетов</h4>
    </div>
    <div class="panel-body">
        <p>
            <span class="m-r-5">Показать:</span> 
            <a href="#" data-type="all" class="btn sort btn-primary btn-xs active" >Все</a>
            <a href="#" data-type="1" class="btn sort btn-primary btn-xs" >Оплаченные</a>
            <a href="#" data-type="0" class="btn sort btn-primary btn-xs" >Без оплаты</a>
        </p>
        <div class="table-responsive" id="inv_list">
            Загрузка...
        </div>
        
    </div>
</div>
<!-- end panel -->
{include file="../footer.tpl"}

