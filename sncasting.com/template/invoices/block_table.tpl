<table id="user" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>№ счета</th>
            <th>Компания покупатель</th>
            <th class="width-200">Сумма</th>
            <th class="width-200">Статус</th>
            <th class="width-200">Действие</th>
            <th>-</th>
        </tr>
    </thead>
    <tbody>
        {foreach from=$list item=i}
            <tr>
                <td>№ {$i->id} от {date("d.m.Y", strtotime( str_replace("-","/", $i->creationDateTime)))} 
                    {if $i->fineID>0}
                        <br><br>
                        {Fines::getById($i->fineID)}
                    {/if}</td>
                <td>{if $is_admin}<a href="/company/edit?companyid={$i->company->getId()}">{$i->company}</a>{else}{$i->company}{/if}</td>
                <td>{$i->total()} руб.</td>
                <td>
                    {if !$is_admin}
                        {if $i->is_payed}Оплачен{else}Не оплачен{/if}
                    {else}
                        <select class="form-control changeStatus" data-id="{$i->id}">
                            <option {if $i->is_payed==0}selected{/if} value="0">Не оплачен</option>
                            <option {if $i->is_payed==1}selected{/if} value="1">Оплачен</option>
                        </select>
                    {/if}
                </td>
                <td class="text-center">
                    <a title="Посмотреть" href="/invoices/view/{$i->id}">
                        <i class="fa fa-2x fa-link"></i>
                    </a>
                    &nbsp;
                    <a title="Печать" target=_blank href="/invoices/view/{$i->id}?mode=print">
                        <i class="fa fa-2x fa-print"></i>
                    </a>
                    &nbsp;
                    <a title="PDF" href="/invoices/view/{$i->id}?mode=pdf">
                        <i class="fa fa-2x fa-file-pdf-o"></i>
                    </a>
                </td>
                <td>

                    {if ($is_admin || $my->getCompanyID()==$i->company->getId()) && $i->isCanDelete() }<input type="button" class="btn btn-danger btn-sm delete" data-id={$i->id} value="Удалить" />{/if}
                </td>
            </tr>
        {/foreach}

    </tbody>
</table>