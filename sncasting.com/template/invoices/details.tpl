{include file="../header.tpl"}
{include file="../left.tpl"}
{if !$is_admin}
    {assign var=data value=$company->getDetails()}
{else}
    {assign var=data value=$data}
{/if}


<div class="col-md-12">
    {if $success}
        <div class="alert alert-success">
            Информация сохранена
        </div>
    {/if}
    <form action="?" method="post"> 
        <input type="hidden" name="action" value="save" />
        <input type="hidden" name="id" value="{if $is_admin}root{/if}" />
        <div class="panel panel-inverse">
            <div class="panel-heading">

                <h4 class="panel-title">Список пользователей компании</h4>
            </div>
            <div class="panel-body">
                <ul class="registered-users-list clearfix">

                    {foreach from=$company->getUsers() item=row}
                        {assign var=u value=User::getById($row->id)}
                        <li style="width:20%">
                            <div style="width:100px;height:200px">
                                <div style="height:100px">
                                    <a href="javascript:;"><img style="max-height:100%;max-width:100%" width="150" src="{$u->getPhotoUrl()}" alt="" /></a>
                                </div>
                                <h4 class="username text-ellipsis">
                                    {$u->getNamePart('firstname')} {$u->getNamePart('lastname')}
                                    <small>{$u->getField('position')}</small>
                                </h4>
                            </div>
                        </li>
                    {/foreach}

                </ul>

            </div>
        </div>
        <div class="panel panel-inverse" data-sortable-id="form-stuff-2">
            <div class="panel-heading">

                <h4 class="panel-title">Реквизиты {if $is_admin}администратора{else} компании {$company}{/if}</h4>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label>Полное наименование контрагента</label>
                    <input placeholder="Полное наименование организации, ИП или физлица" class="form-control" type="text" name="data[full]" value="{$data.full|default:""|escape}"/>
                </div>
                <div class="form-group">
                    <label>Удостоверение личности физлица</label>
                    <input placeholder="Паспорт № 00 00 123456, кем выдан, дата выдачи" class="form-control" type="text" name="data[passport]" value="{$data.passport|default:""|escape}"/>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label>Телефон</label>
                        <input class="form-control" type="text" name="data[phone]" value="{$data.phone|default:""|escape}"/>
                    </div>
                    <div class="col-md-6">
                        <label>Факс</label>
                        <input class="form-control" type="text" name="data[fax]" value="{$data.fax|default:""|escape}"/>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-3">
                        <label>ИНН</label>
                        <input class="form-control" type="text" name="data[inn]" value="{$data.inn|default:""|escape}"/>
                    </div>
                    <div class="col-md-3">
                        <label>КПП</label>
                        <input class="form-control" type="text" name="data[kpp]" value="{$data.kpp|default:""|escape}"/>
                    </div>
                    <div class="col-md-3">
                        <label>ОГРН</label>
                        <input class="form-control" type="text" name="data[ogrn]" value="{$data.ogrn|default:""|escape}"/>
                    </div>
                    <div class="col-md-3">
                        <label>ОКПО</label>
                        <input class="form-control" type="text" name="data[okpo]" value="{$data.okpo|default:""|escape}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label>Расчетный счет</label>
                    <input class="form-control" type="text" name="data[ras]" value="{$data.ras|default:""|escape}"/>
                </div>
                <div class="form-group">
                    <label>БИК</label>
                    <input class="form-control" type="text" name="data[bik]" value="{$data.bik|default:""|escape}"/>
                </div>
                <div class="form-group">
                    <label>Наименование банка и город</label>
                    <input class="form-control" type="text" name="data[bank]" value="{$data.bank|default:""|escape}"/>
                </div>
                <div class="form-group">
                    <label>Кор. счет</label>
                    <input class="form-control" type="text" name="data[kor]" value="{$data.kor|default:""|escape}"/>
                </div>
                <div class="form-group">
                    <label>Адрес</label>
                    <input class="form-control" type="text" name="data[address]" value="{$data.address|default:""|escape}"/>
                </div>
                <div class="form-group">
                    <input class="btn btn-success" type="submit" value="Сохранить" />
                </div>

            </div>
        </div>
    </form>
</div>
{include file="../footer.tpl"}

