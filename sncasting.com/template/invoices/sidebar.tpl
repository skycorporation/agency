<div class="text-center p-t-30">
    <h4 class="text-white m-b-20">Ваша корзина</h4>
</div>
<ol >
    {foreach from=$i->getCart() item=count key=id}

        <li class="m-b-20"><span class="text-white">{Passes_Sub::getById($id)->getPassType()}, {Passes_Sub::getById($id)}</span><br>
            Количество &mdash; {$count}<br>
            Итого &mdash; {$i->getAmount($id, $count)} руб</li>
        {/foreach}

</ol>
<div class="text-center p-t-25">
    <h5 class="text-white">Итого: {$i->total()} руб.</h5>
</div><br><Br>


<div class="text-center">
    <button id="cart_submit" class="btn btn-success btn-large btn-lg">Сформировать счет</button>
</div>
<br><br>
<div class="text-center">
    <button id="cart_reset" class="btn btn-danger btn-sm ">Сбросить корзину</button>
</div><br><Br>