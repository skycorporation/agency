

<div id="gallery" class="gallery">
    {foreach from=$records item=r}
    <div class="image gallery-group-1">
        <div class="image-inner">
            <a href="{$r->getPhotoUrl()}" data-lightbox="gallery-group-1">
                <img src="{$r->getPhotoUrl()}" alt="" />
            </a>
            <p class="image-caption">
                {$r->carNumber} {$r->carRegion}
            </p>
        </div>
        <div class="image-info">
            <h5 class="title">{if $is_admin}
                {$r->pass->company}<br>
                {/if}{$r->pass->subPass}, {$r->pass->subPass->getPassType()}
              {if $r->fineID>0} 
                        <label class='label label-danger'>Штраф</label>
                {/if}
            </h5>
            <div class="pull-right">
                <small>{if $r->pass->author}
                    {$r->pass->author->getNamePart('firstname')} {$r->pass->author->getNamePart('lastname')}
                    {/if}
              
                </small> 
            </div>
            <div class="rating">
                Заезд: <small>{$r->insideDate}</small>
            </div>
            <div class="desc">
                {if $r->outsideDate!='0000-00-00 00:00:00'}Выезд: {$r->outsideDate}
                    {if $r->fineID>0} <br>
                        <font color=red><br>
                        Выезд со штрафом. <A href='/invoices/view/{Invoice::getByFineId($r->fineID)->id}'>Счет</a>
                        </font>
                        {/if}
                {else}
                На территории
                {/if}
            </div>
        </div>
    </div>
            {foreachelse}
                <br><br>
                <h4>Записей нет</h4>
    {/foreach}

</div>
