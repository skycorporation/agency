{include file="../header.tpl"}
{include file="../left.tpl" }


<a href="#show-all" class="hidden btn btn-default btn-xs active" data-option-value="*">
    Show All
</a>

<h1 class="page-header">Список проехавших машин</h1>
<div id="options" class="m-b-10 ">
</div>
<div class='col-lg-12 row'>
    <div class='col-lg-12 row' >
        <form action='?' method='get' id='form'>
            {if $is_admin}
                <div class='col-lg-2 row'>
                    <select class='form-control' name="filters[company]">
                        <option value=''>Все компании</option>
                        {foreach from=Company::getList() item=c}
                            <option value='{$c->id}'>{$c}</option>
                        {/foreach}
                        <option value='-1'>Без пропусков</option>
                    </select>
                </div>
            {/if}
            
            <div class='col-lg-4 row'>
                <input type='hidden' value="{date("Y-m-d", strtotime("-5 day"))}" name='filters[dateFrom]' id='df'>
                <input type='hidden' value="{date("Y-m-d")}" name='filters[dateTo]' id='dt'>
                <div class="form-group">
                    
                    <div class="col-md-8">
                        <div id="advance-daterange" class="btn btn-white">
                            <span></span>
                            <i class="fa fa-angle-down fa-fw"></i>
                        </div>
                    </div>
                </div>
            </div>
                <div class='col-lg-2'>
                <select class='form-control' name="filters[passID]">
                    <option value=''>Все пропуска</option>
                    {foreach from=$passes item=p}
                        <option value='{$p->id}'>{$p->subPass->getPassType()}, {$p->subPass}, (Автомобиль: {if $p->carNumber!=''}{$p->carNumber} {$p->carRegion}, {$p->author->getNamePart('lastname')}{else}не указан{/if})</option>
                    {/foreach}
                </select>
            </div>
                <div class='col-lg-4'>
                    <input type='checkbox' name='filters[inside]' {if isset($smarty.get.filters.inside)}checked{/if} value='1'> На территории &nbsp; <input type="submit" class="btn btn-success" value="Применить">
                </div>
        </form>
    </div>
    <div class='clearfix'></div>
    <div id='list' class="col-lg-12">
        Загрузка...
    </div>
</div>

{include file="../footer.tpl"}