{include file="../header.tpl"}
{include file="../left.tpl"}
<style>
    .table.table-profile>tbody>tr>td.field{
        width:200px;
    }
</style>
<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li><a href="/">Главная</a></li>

    <li class="active">Редактирование компании</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Редактирование профайла <small></small></h1>
<!-- end page-header -->
<!-- begin profile-container -->
{if $success}
    <div class='alert alert-success'>
        Информация обновлена
    </div>
{/if}
<form action='?' method='post' onsubmit="javascript:
                $('#e').addClass('hidden');
        if ($('#p1').val() != $('#p2').val()) {
            $('#e').removeClass('hidden').html('Пароли не совпадают');
            return false;
        }

        return true;

      ">
    <script> var objectID = '{$company->id}';</script>
    <input type='hidden' name='companyid' value='{$company->id}' />
    <input type='hidden' name='action' value='save' />
    <div class="profile-container">
        <!-- begin profile-section -->
        <div class="profile-section">
            <!-- begin profile-left -->
            <div class="profile-left">
                <!-- begin profile-image -->
                <div class="profile-image">
                    <img id=image_src src="{$company->getPhotoUrl()}" />
                    <i class="fa fa-user hide"></i>
                </div>
                <!-- end profile-image -->
                <div class="m-b-10">
                    <a id=image_change href="#" class="btn btn-warning btn-block btn-sm">Изменить фотографию</a>
                </div>
                <!-- begin profile-highlight -->

                <!-- end profile-highlight -->
            </div>
            <!-- end profile-left -->
            <!-- begin profile-right -->
            <div class="profile-right">
                <!-- begin profile-info -->
                <div class="profile-info">
                    <!-- begin table -->
                    <div class="table-responsive">
                        <table class="table table-profile">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>
                            <h4>{$company}</h4>

                            </th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr >
                                    <td class="field">Тип</td>
                                    <td>
                                        {assign var=list value=Company_Type::getList()}
                                        <select class="form-control inline " name="data[typeID]" required>
                                            <option value="">Выбрать</option>
                                            {foreach from=$list item=t}
                                                <option {if $t->getId()==$company->getType()->getId()}selected{/if} value="{$t->getId()}">{$t}</option>
                                            {/foreach}
                                        </select>
                                    </td>
                                </tr>
                                <tr >
                                    <td class="field">Форма</td>
                                    <td>
                                        {assign var=list value=Company_Class::getList()}
                                        <select class="form-control inline " name="data[classID]" required>
                                            <option value="">Выбрать</option>
                                            {foreach from=$list item=t}
                                                <option {if $t->getId()==$company->getClass()->getId()}selected{/if} value="{$t->getId()}">{$t}</option>
                                            {/foreach}
                                        </select>


                                    </td>
                                </tr>
                                <tr >
                                    <td class="field">Расположение</td>
                                    <td>
                                        <input name=data[place] type="text" class="form-control inline input-xs" value="{$company->getPlace()|escape}"/>



                                    </td>
                                </tr>
                                <tr >
                                    <td class="field">Площадь (м<sup>2</sup>)</td>
                                    <td>
                                        <input name=data[m2] type="text" class="form-control inline input-xs" value="{$company->getM2()|escape}"/>



                                    </td>
                                </tr>
                                <tr >
                                    <td class="field">max. пользователей</td>
                                    <td>
                                        <input name=data[max_users] type="text" class="form-control inline input-xs" value="{$company->getMaxUsers()}"/>



                                    </td>
                                </tr>
                                <tr >
                                    <td class="field">Инвайт код</td>
                                    <td>
                                        <input name=data[code] type="text" class="form-control inline input-xs" value="{$company->getCode()}"/>



                                    </td>
                                </tr>
                                <tr class="divider">
                                    <td colspan="2"></td>
                                </tr>




                                <tr>
                                    <td colspan="2">
                                        <p class="text-danger hidden" id="e"></p>
                                    </td>
                                </tr>


                                <tr>
                                    <td></td>
                                    <td>
                                        <input type='submit' value='Сохранить' class='btn btn-success' />
                                </tr>
                                {if $is_admin}
                                    {assign var=data value=$company->getData()}
                                    <tr>
                                        <td>
                                        </td>
                                        <td><h4>Реквизиты компании</h4> </td>

                                    </tr>
                                    <tr>
                                        <td class="field">
                                            Удостоверение личности физлица
                                        </td>
                                        <td>
                                            {$data.passport|default:""|escape}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="field">
                                            Телефон
                                        </td>
                                        <td>
                                            {$data.phone|default:""|escape}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="field">
                                            Факс
                                        </td>
                                        <td>
                                            {$data.fax|default:""|escape}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="field">
                                            ИНН
                                        </td>
                                        <td>
                                            {$data.inn|default:""|escape}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="field">
                                            КПП
                                        </td>
                                        <td>
                                            {$data.kpp|default:""|escape}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="field">
                                            ОГРН
                                        </td>
                                        <td>
                                            {$data.ogrn|default:""|escape}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="field">
                                            ОКПО
                                        </td>
                                        <td>
                                            {$data.okpo|default:""|escape}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="field">
                                            Расчетный счет
                                        </td>
                                        <td>
                                            {$data.passport|default:""|escape}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="field">
                                            БИК
                                        </td>
                                        <td>
                                            {$data.passport|default:""|escape}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="field">
                                            Банк и город
                                        </td>
                                        <td>
                                            {$data.passport|default:""|escape}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="field">
                                            Кор. счет
                                        </td>
                                        <td>
                                            {$data.kor|default:""|escape}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="field">
                                            Адрес
                                        </td>
                                        <td>
                                            {$data.address|default:""|escape}
                                        </td>
                                    </tr>


                                {/if}

                            </tbody>
                        </table>
                    </div>
                    <!-- end table -->
                </div>
                <!-- end profile-info -->
            </div>
            <!-- end profile-right -->
        </div>
        <div class="profile-section">
            <!-- begin row -->
            <div class="row">
                <!-- begin col-4 -->
                <div class="col-md-6">
                    {assign var=users value=$company->getUsers()}
                    <h4 class="title">Пользователей <small>{if $users}{count($users)}{else}0{/if}</small></h4>
                    <!-- begin scrollbar -->
                    <div data-scrollbar="true" data-height="280px" class="bg-silver">
                        <!-- begin chats -->
                        <ul class="registered-users-list clearfix">
                            {foreach from=$users item=row}
                                {$user->load($row->id)}
                                <li>
                                    <a href="/profile/edit?userid={$user->id}"><img src="{$user->getPhotoUrl()}" alt="" /></a>
                                    <h4 class="username text-ellipsis">
                                        {$user->getNamePart('firstname')} {$user->getNamePart('lastname')}
                                        <small>{$user->getField('position')}</small>
                                    </h4>
                                </li>
                            {/foreach}

                        </ul>
                        <!-- end chats -->
                    </div>
                    <!-- end scrollbar -->
                </div>
                <!-- end col-4 -->
                <!-- begin col-4 -->
                <div class="col-md-6">
                    <h4 class="title">Счета <small>{count($company->getInvoices())}</small></h4>
                    <!-- begin scrollbar -->
                    <div data-scrollbar="true" data-height="280px" class="bg-silver">
                        <!-- begin table -->
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Дата</th>
                                    <th>Сумма</th>
                                    <th>Статус оплаты</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                            {foreach from=$company->getInvoices() item=i}
                                <tr>
                                    <td><a href="/invoices/view/{$i->id}">{$i->id}</a></td>
                                    <td><a href="/invoices/view/{$i->id}">{date("d.m.Y", strtotime(str_replace("-","/", $i->creationDateTime)))}</a></td>
                                    <td><a href="/invoices/view/{$i->id}">{$i->total()} руб</a></td>
                                    <td><a href="/invoices/view/{$i->id}">{if $i->is_payed}Оплачен{else}Неоплачен{/if}</a></td>
                                </tr>
                                {foreachelse}
                                    <Tr>
                                        <td colspan="3">нет счетов</td>
                                    </tr>
                                {/foreach}
                            
                              
                            </tbody>
                        </table>
                        <!-- end table -->
                    </div>
                    <!-- end scrollbar -->
                </div>
                <!-- end col-4 -->
                <!-- begin col-4 -->
{*                <div class="col-md-4">
                    <h4 class="title">Последние проехавшие <small></small></h4>
                    <!-- begin scrollbar -->
                    <div data-scrollbar="true" data-height="280px" class="bg-silver">
                        <!-- begin todolist -->
                        <ul class="todolist">
                            <li class="active">
                                <a href="javascript:;" class="todolist-container active" data-click="todolist">
                                    <div class="todolist-input"><i class="fa fa-square-o"></i></div>
                                    <div class="todolist-title">Donec vehicula pretium nisl, id lacinia nisl tincidunt id.</div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" class="todolist-container" data-click="todolist">
                                    <div class="todolist-input"><i class="fa fa-square-o"></i></div>
                                    <div class="todolist-title">Duis a ullamcorper massa.</div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" class="todolist-container" data-click="todolist">
                                    <div class="todolist-input"><i class="fa fa-square-o"></i></div>
                                    <div class="todolist-title">Phasellus bibendum, odio nec vestibulum ullamcorper.</div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" class="todolist-container" data-click="todolist">
                                    <div class="todolist-input"><i class="fa fa-square-o"></i></div>
                                    <div class="todolist-title">Duis pharetra mi sit amet dictum congue.</div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" class="todolist-container" data-click="todolist">
                                    <div class="todolist-input"><i class="fa fa-square-o"></i></div>
                                    <div class="todolist-title">Duis pharetra mi sit amet dictum congue.</div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" class="todolist-container" data-click="todolist">
                                    <div class="todolist-input"><i class="fa fa-square-o"></i></div>
                                    <div class="todolist-title">Phasellus bibendum, odio nec vestibulum ullamcorper.</div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" class="todolist-container active" data-click="todolist">
                                    <div class="todolist-input"><i class="fa fa-square-o"></i></div>
                                    <div class="todolist-title">Donec vehicula pretium nisl, id lacinia nisl tincidunt id.</div>
                                </a>
                            </li>
                        </ul>
                        <!-- end todolist -->
                    </div>
                    <!-- end scrollbar -->
                </div>
*}
                <!-- end col-4 -->
            </div>
            <!-- end row -->
        </div>
        <!-- end profile-section -->

    </div>
</form>
<!-- end profile-container -->
</div>
<!-- end #content -->



<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->
{include file="../footer.tpl"}