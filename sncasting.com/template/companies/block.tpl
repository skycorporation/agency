
{foreach from=$companies item=row}

    {$tmpcompany->load($row->id)}

    <li class="list-group-item inverse">
        <div class="email-checkbox">
            <label>
                <i class="fa fa-square-o"></i>
                <input data-id='{$tmpcompany->getId()}' type="checkbox" data-checked="email-checkbox" />
            </label>
        </div>
        <a href="/company/edit?companyid={$tmpcompany->getId()}" class="email-user">
            <img src="{$tmpcompany->getPhotoUrl()}" alt="" />
        </a>
        <div class="email-info">
            <span class="email-time">{count($tmpcompany->getUsers())} пользователя(-ей)</span>
            <h5 class="email-title">
                <a href="/company/edit?companyid={$tmpcompany->getId()}">{$tmpcompany}</a>
                
                {if $tmpcompany->isBlocked()}
                    <span class='label label-danger f-s-10'>Заблокирован</span>
                {/if}
            </h5>
            <p>
                {$tmpcompany->getPlace()}
            </p>

        </div>
    </li>
    
    
{/foreach}