{include file="header.tpl"}

<h1 class="mt-3 mb-2" style="text-align: center">New casting</h1>
<form class="form form-horizontal" method="get" action="/castings/add/" enctype="multipart/form-data">
  <div class="form-body">
  <section class="card">
    <div class="card-block">
      <div class="collapse-icon accordion-icon-rotate">
        <div id="headingCollapseGeneral" class="card-header"> <a data-toggle="collapse" href="#collapseGeneral" aria-expanded="true" aria-controls="collapseGeneral" class="card-title lead collapsed" style="display: block; color: #000;">Casting info</a> </div>
        <div id="collapseGeneral" role="tabpanel" aria-labelledby="headingCollapseGeneral" class="collapse in" aria-expanded="true">
          <div class="card-content">
            <div class="card-body pt-3">
              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 label-control" for="projectinput1">Casting name &#42;</label>
                <div class="col-xs-12 col-sm-12 col-md-6">
                  <input autocomplete="dontsuggest" type="text" name="name" class="form-control checkThis capThis trimit" onkeyup="var newUrl = $(this).val(); newUrl = newUrl.toLowerCase(); newUrl = newUrl.replace(/ /g,''); newUrl = encodeURI(newUrl); $('#newUrl1').val(newUrl); $('#urlPreview').html('www.sncasting.com/c/' + newUrl);" required autofocus>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 label-control" for="projectinput1">Casting location</label>
                <div class="col-xs-12 col-sm-12 col-md-6">
                  <input autocomplete="dontsuggest" type="text" name="location" class="form-control checkThis capThis trimit">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 label-control" for="projectinput1">Casting date</label>
                <div class="col-xs-12 col-sm-12 col-md-6 ">
                  <div style="position: relative; padding-bottom: 3px;">
                    <input type='text' placeholder="select date..." class="form-control pickadate-dropdown-all" name="date" required>
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 label-control" for="projectinput1">Casting sheet URL</label>
                <div class="col-xs-12 col-sm-12 col-md-6">
                  <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text">www.sncasting.com/c/</span></div>
                    <input autocomplete="dontsuggest" type="text" id="newUrl1" placeholder="url" class="form-control" name="url">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="card">
    <div class="card-block">
      <div class="collapse-icon accordion-icon-rotate">
        <div id="headingCollapseOptional" class="card-header"> <a data-toggle="collapse" href="#collapseOptional" aria-expanded="true" aria-controls="collapseOptional" class="card-title lead collapsed" style="display: block; color: #000;">Optional fields</a> </div>
        <div id="collapseOptional" role="tabpanel" aria-labelledby="headingCollapseOptional" class="collapse in" aria-expanded="true">
          <div class="card-content">
            <div class="card-body pt-3">
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Field name</th>
                      <th class="text-center">On/Off for models</th>
                      <th class="text-center">Visibility for clients</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td></td>
                      <td class="text-center"><button type="button" class="mybtn2 mybtn-green" onclick="$('.switch_4-models').trigger('click');"><i class="ft-toggle-right"></i> Toggle all</button></td>
                      <td class="text-center"><button type="button" class="mybtn2 mybtn-green" onclick="$('.switch_4-clients').trigger('click');"><i class="ft-toggle-right"></i> Toggle all</button></td>
                    </tr>
                    <tr>
                      <td>Ethnicity</td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" class="switch_4 switch_4-models" id="ethnicity" name="ethnicity">
                        </div></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" class="switch_4 switch_4-clients" id="ethnicityForClient" name="ethnicityForClient">
                        </div></td>
                    </tr>
                    <tr>
                      <td>Based in</td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" class="switch_4 switch_4-models basedInVal" id="basedIn" name="basedIn">
                        </div></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" class="switch_4 switch_4-clients" id="basedInForClient" name="basedInForClient">
                        </div></td>
                    </tr>
                    <tr>
                      <td>Occupation</td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" name="occupation" id="occupation" class="switch_4 switch_4-models">
                        </div></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="occupationForClient" name="occupationForClient" class="switch_4 switch_4-clients">
                        </div></td>
                    </tr>
                    <tr>
                      <td>Representation</td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="agency" name="agency" class="switch_4 switch_4-models">
                        </div></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="agencyForClient" name="agencyForClient" class="switch_4 switch_4-clients">
                        </div></td>
                    </tr>
                    <tr>
                      <td>Instagram</td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" name="instagram" id="instagram" class="switch_4 switch_4-models">
                        </div></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" name="instagramForClient" id="instagramForClient" class="switch_4 switch_4-clients">
                        </div></td>
                    </tr>
                    <tr>
                      <td>Book URL</td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="book" name="book" class="switch_4 switch_4-models">
                        </div></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="bookForClient" name="bookForClient" class="switch_4 switch_4-clients">
                        </div></td>
                    </tr>
                    <tr>
                      <td>Website URL</td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="website" name="website" class="switch_4 switch_4-models">
                        </div></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="websiteForClient" name="websiteForClient" class="switch_4 switch_4-clients">
                        </div></td>
                    </tr>
                    <tr>
                      <td>Rate</td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="rate" name="rate" class="switch_4 switch_4-models">
                        </div></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="rateForClient" name="rateForClient" class="switch_4 switch_4-clients">
                        </div></td>
                    </tr>
                    <tr>
                      <td>Known for</td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="knownFor" name="knownFor" class="switch_4 switch_4-models">
                        </div></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="knownForForClient" name="knownForForClient" class="switch_4 switch_4-clients">
                        </div></td>
                    </tr>
                    <tr>
                      <td>Press links</td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="press" name="press" class="switch_4 switch_4-models">
                        </div></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="pressForClient" name="pressForClient" class="switch_4 switch_4-clients">
                        </div></td>
                    </tr>
                    <tr>
                      <td>Music links</td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="music" name="music" class="switch_4 switch_4-models">
                        </div></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="musicForClient" name="musicForClient" class="switch_4 switch_4-clients">
                        </div></td>
                    </tr>
                    <tr>
                      <td>Video links</td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="video" name="video" class="switch_4 switch_4-models">
                        </div></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="videoForClient" name="videoForClient" class="switch_4 switch_4-clients">
                        </div></td>
                    </tr>
                    <tr>
                      <td>Upcoming projects</td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="upcomingProjects" name="upcomingProjects" class="switch_4 switch_4-models">
                        </div></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="upcomingProjectsForClient" name="upcomingProjectsForClient" class="switch_4 switch_4-clients">
                        </div></td>
                    </tr>
                    <tr>
                      <td>Photo upload</td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="photoUpload" name="photoUpload" class="switch_4 switch_4-models">
                        </div></td>
                      <td></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  {*<!-- @TODO: Куда переносить js, я пока не знаю -->*}
  <script>
    function toggleCustomURL(index) {
      var input = $('#customUrl' + index + 'name');
      var checkbox = $('#customUrl' + index);

      var oldState = checkbox.attr('checked') ? true : false;
      var newState = (input.val().trim().length > 0);

      if (newState == oldState) {
        return;
      }

      if (newState) {
        checkbox.removeAttr('disabled');
        checkbox.attr('checked', true);
      } else {
        checkbox.removeAttr('checked');
        checkbox.attr('disabled', true);
      }
    }
  </script>

  <section class="card">
    <div class="card-block">
      <div class="collapse-icon accordion-icon-rotate">
        <div id="headingCollapseCustom" class="card-header"> <a data-toggle="collapse" href="#collapseCustom" aria-expanded="true" aria-controls="collapseCustom" class="card-title lead collapsed" style="display: block; color: #000;">Custom URLs</a> </div>
        <div id="collapseCustom" role="tabpanel" aria-labelledby="collapseOptional" class="collapse in" aria-expanded="true">
          <div class="card-content">
            <div class="card-body pt-3">
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Field name</th>
                      <th class="text-center">On/Off for models (auto)</th>
                      <th class="text-center">Visibility for clients</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><input autocomplete="dontsuggest" onkeyup="toggleCustomURL(1);" onpaste="toggleCustomURL(1);" type="text" id="customUrl1name" name="customUrl1name" placeholder="Custom URL name" class="form-control checkThis capThis" style="min-width: 200px"></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl1" name="customUrl1" class="switch_4 basedInVal" disabled>
                        </div></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl1ForClient" name="customUrl1ForClient" class="switch_4">
                        </div></td>
                    </tr>
                    <tr>
                      <td><input autocomplete="dontsuggest" onkeyup="toggleCustomURL(2);" onpaste="toggleCustomURL(2);" type="text" id="customUrl2name" name="customUrl2name" placeholder="Custom URL name" class="form-control checkThis capThis" style="min-width: 200px"></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl2" name="customUrl2" class="switch_4 basedInVal" disabled>
                        </div></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl2ForClient" name="customUrl2ForClient" class="switch_4">
                        </div></td>
                    </tr>
                    <tr>
                      <td><input autocomplete="dontsuggest" onkeyup="toggleCustomURL(3);" onpaste="toggleCustomURL(3);" type="text" id="customUrl3name" name="customUrl3name" placeholder="Custom URL name" class="form-control checkThis capThis" style="min-width: 200px"></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl3" name="customUrl3" class="switch_4 basedInVal" disabled>
                        </div></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl3ForClient" name="customUrl3ForClient" class="switch_4">
                        </div></td>
                    </tr>
                    <tr>
                      <td><input autocomplete="dontsuggest" onkeyup="toggleCustomURL(4);" onpaste="toggleCustomURL(4);" type="text" id="customUrl4name" name="customUrl4name" placeholder="Custom URL name" class="form-control checkThis capThis" style="min-width: 200px"></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl4" name="customUrl4" class="switch_4 basedInVal" disabled>
                        </div></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl4ForClient" name="customUrl4ForClient" class="switch_4">
                        </div></td>
                    </tr>
                    <tr>
                      <td><input autocomplete="dontsuggest" onkeyup="toggleCustomURL(5);" onpaste="toggleCustomURL(5);" type="text" id="customUrl5name" name="customUrl5name" placeholder="Custom URL name" class="form-control checkThis capThis" style="min-width: 200px"></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl5" name="customUrl5" class="switch_4 basedInVal" disabled>
                        </div></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl5ForClient" name="customUrl5ForClient" class="switch_4">
                        </div></td>
                    </tr>
                    <tr>
                      <td><input autocomplete="dontsuggest" onkeyup="toggleCustomURL(6);" onpaste="toggleCustomURL(6);" type="text" id="customUrl6name" name="customUrl6name" placeholder="Custom URL name" class="form-control checkThis capThis" style="min-width: 200px"></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl6" name="customUrl6" class="switch_4 basedInVal" disabled>
                        </div></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl6ForClient" name="customUrl5ForClient" class="switch_4">
                        </div></td>
                    </tr>
                    <tr>
                      <td><input autocomplete="dontsuggest" onkeyup="toggleCustomURL(7);" onpaste="toggleCustomURL(7);" type="text" id="customUrl7name" name="customUrl7name" placeholder="Custom URL name" class="form-control checkThis capThis" style="min-width: 200px"></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl7" name="customUrl7" class="switch_4 basedInVal" disabled>
                        </div></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl7ForClient" name="customUrl7ForClient" class="switch_4">
                        </div></td>
                    </tr>
                    <tr>
                      <td><input autocomplete="dontsuggest" onkeyup="toggleCustomURL(8);" onpaste="toggleCustomURL(8);" type="text" id="customUrl8name" name="customUrl8name" placeholder="Custom URL name" class="form-control checkThis capThis" style="min-width: 200px"></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl8" name="customUrl8" class="switch_4 basedInVal" disabled>
                        </div></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl8ForClient" name="customUrl8ForClient" class="switch_4">
                        </div></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="col-12" style="padding: 0 15px 15px 15px;">
    <button type="submit" class="mybtn2 mybtn-green float-right"><i class="ft-plus"></i> Create casting</button>
  </div>
</form>
{include file="footer.tpl"} 