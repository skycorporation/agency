{include file="header.tpl"} 
<script>
    var profileToDelete;
</script> 
{include file="modals/deleteGroup.tpl"}
{include file="modals/editGroup.tpl"}
<h1 class="text-center mt-3 mb-2">Profile groups</h1>
<section id="hover-effects">
  <div class="row">
    <div class="grid-hover"> {foreach from=$profileGroups item=t}
      <div class="col-md-4 col-xs-12 mb-2">
        <div class="thumbnail-container group">
          <div class="thumb-photo-place">
            <div class="imagesGroup imagesGroup{if $t["count"] > 4}4{else}{$t["count"]}{/if}">
              {assign var="c" value=0}
              {foreach from=$t["photos"] item=photo}
              <div class="imagesGroupImg" style="background: url('{$photo|replace:'/home/aibdh80ehx97/public_html/sncasting.com':''}');"/>
            </div>
            {$c = $c+1}
            {if $c>=4} {break} {/if}
            
            {/foreach}
            <div style="clear: both;"></div>
            <div class="thumb-overlay"></div>
          </div>
          <div class="thumb-content">
            <div class="editPanel desktop-only">
              <button class="editButton" data-toggle="modal" data-target="#editGroup" onclick="profileToEdit={$t['id']}; $('#editProfileGroupName').val('{$t['name']}');"><i class="ft-edit-2"></i></button>
              <button class="deleteProfileButton" data-toggle="modal" data-target="#deleteGroup" onclick="profileToDelete={$t['id']}"><i class="icon-trash4"></i></button>
            </div>
            <a class="thumb-info" href="/groups/view?id={$t['id']}">
            <p class="text-center">{if $t["count"]==0}No{/if}{$t["count"]} profiles</p>
            </a> </div>
        </div>
        <a class="name-place" href="/groups/view?id={$t['id']}">{$t["name"]}</a> </div>
    </div>
    {/foreach} </div>
  </div>
</section>
{include file="footer.tpl"}