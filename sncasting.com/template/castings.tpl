{include file="header.tpl"} 
<script>
    var profileToDelete;
</script> 
{include file="modals/editCasting.tpl"}
{include file="modals/deleteCasting.tpl"}
<section id="hover-effects">
  <div class="row">
    <div class="col-12">
      <h1 class="mt-3 mb-2" style="text-align: center">Castings</h1>
    </div>
  </div>
  <div class="row">
    <div class="grid-hover"> {foreach from=$profileGroups item=t}
      <div class="col-md-4 col-xs-12 mb-2">
        <div class="thumbnail-container casting">
          <div class="thumb-photo-place">
            <div class="imagesGroup imagesGroup{if $t["count"] > 4}4{else}{$t["count"]}{/if}">
              {assign var="c" value=0}
              {foreach from=$t["photos"] item=photo}
              <div class="imagesGroupImg" style="background: url('{$photo|replace:'/home/aibdh80ehx97/public_html/sncasting.com':''}');"/>
            </div>
            {$c = $c+1}
            {if $c>=4} {break} {/if}
            
            {/foreach}
            <div style="clear: both;"></div>
            <div class="thumb-overlay"></div>
          </div>
          <div class="thumb-content">
            <div class="editPanel desktop-only">
              <button class="editButton" data-toggle="modal" data-target="#castingEdit" onclick="$('#editCastingForm').find('[name=name]').val($(this).data('name'));
              $('#editCastingForm').find('[name=location]').val($(this).data('location'));
              $('#editCastingForm').find('[name=date]').val($(this).data('date'));
              $('#editCastingForm').find('[name=id]').val($(this).data('id'));
              $('#editCastingForm').find('[name=url]').val($(this).data('url'));" data-name="{$t["name"]}" data-id="{$t["id"]}" data-location="{$t["location"]}" data-date="{$t["date"]}" data-url="{$t["url"]}"><i class="ft-edit-2"></i></button>
              <button class="deleteProfileButton" data-toggle="modal" data-target="#deleteCasting" onclick="profileToDelete={$t['id']}"><i class="icon-trash4"></i></button>
            </div>
            <a class="thumb-info" href="/castings/view?id={$t['id']}">
            <p class="text-center"> {$t["location"]}<br>
              {$t["date"]}<br>
              {if $t["count"]==0}No{/if}{$t["count"]} profiles</p>
            </a> 
            <!--<button class="selectButton" onclick="activateCasting(this)">Deactivate</button>--> 
          </div>
        </div>
        <a class="name-place" href="/castings/view?id={$t['id']}">{$t["name"]}</a><!--<span class="castingStatus"><i class="fa fa-circle"></i></span>--> 
      </div>
    </div>
    {/foreach} </div>
  </div>
</section>
{include file="footer.tpl"}