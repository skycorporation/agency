
{foreach from=$users item=row}

    {$tmpuser->load($row->id)}

    <li class="list-group-item inverse">
        <div class="email-checkbox">
            <label>
                <i class="fa fa-square-o"></i>
                <input data-id='{$tmpuser->id}' type="checkbox" data-checked="email-checkbox" />
            </label>
        </div>
        <a href="/profile/edit?userid={$tmpuser->id}" class="email-user">
            <img src="{$tmpuser->getPhotoUrl()}" alt="" />
        </a>
        <div class="email-info">
            <span class="email-time">{$tmpuser->getField('lastLoginDateTime')|timeago}</span>
            <h5 class="email-title">
                <a href="/profile/edit?userid={$tmpuser->id}">{$tmpuser->getNamePart('firstname')|escape} {$tmpuser->getNamePart('lastname')|escape} </a>
                <span class="label label-inverse f-s-10">{$tmpuser->getCompany()}</span>
                {if $tmpuser->isBlocked()}
                    <span class='label label-danger f-s-10'>Заблокирован</span>
                {/if}
            </h5>
            <p>
                {$tmpuser->getField('position')|escape}
            </p>

        </div>
    </li>
    
    
{/foreach}