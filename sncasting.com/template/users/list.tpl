{include file="../header.tpl"}
{include file="../left.tpl" _class="content-full-width" _h=1}

<!-- begin vertical-box -->
<div class="vertical-box">
    <!-- begin vertical-box-column -->
    <div class="vertical-box-column width-250">
        <!-- begin wrapper -->

        <!-- end wrapper -->
        <!-- begin wrapper -->
        <div class='vertical-box'>
            <div class="wrapper bg-silver text-center">
                <a href="#create" data-toggle="modal"  class="btn btn-success p-l-40 p-r-40 btn-sm">
                    &plus; Создать пользователя
                </a>
            </div>
            <div class='wrapper text-center'>
                <label><p><b>Пользователи</b></p></label>
            </div>
            <div class="vertical-box-row">
                <div class='vertical-box-cell'>
                    <div class='vertical-box-inner-cell'>
                        <div data-scrollbar="true" data-height="100%" class="wrapper">
                            <ul class="nav nav-pills nav-stacked nav-sm companies">
                                {assign var=list value=Company::getList()}
                                <li class="active"><a href=# class=company data-id=0 >Все компании<span class="badge pull-right">{count($list)}</span></a></li>
                                        {foreach from=$list item=c}
                                    <li><a href=# class="company" data-id="{$c->getId()}">{$c}</a></li>

                                {/foreach}

                            </ul>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!-- end wrapper -->
    </div>
    <!-- end vertical-box-column -->
    <!-- begin vertical-box-column -->
    <div class="vertical-box-column">
        <!-- begin wrapper -->
        <div class='vertical-box'>
            <div class='vertical-box-row'>
                <div class='vertical-box-cell'>
                    <div class='vertical-box-inner-cell'>
                        <!-- begin btn-toolbar -->
                            <div class="wrapper bg-silver-lighter">
                                <div class="btn-toolbar">
                                    <!-- begin btn-group -->
                                    <div class="btn-group pull-right invisible">
                                        <button class="btn btn-white btn-sm">
                                            <i class="fa fa-chevron-left"></i>
                                        </button>
                                        <button class="btn btn-white btn-sm">
                                            <i class="fa fa-chevron-right"></i>
                                        </button>
                                    </div>
                                    <!-- end btn-group -->
                                    <!-- begin btn-group -->
                                    <div class="btn-group">
                                        <button class="btn btn-sm btn-white hide" data-email-action="delete"><i class="fa fa-times m-r-3"></i> <span class="hidden-xs">Удалить</span></button>
                                        <button class="btn btn-sm btn-white hide" data-email-action="block"><i class="fa fa-lock m-r-3"></i> <span class="hidden-xs">Заблокировать</span></button>
                                        <button class="btn btn-sm btn-white hide" data-email-action="unblock"><i class="fa fa-unlock m-r-3"></i> <span class="hidden-xs">Разблокировать</span></button>

                                    </div>
                                    <!-- end btn-group -->
                                </div>
                            </div>
                        <div data-scrollbar="true" data-height="100%" class="wrapper">

                            
                            <!-- end btn-toolbar -->

                            <!-- end wrapper -->
                            <!-- begin list-email -->
                            <ul class="list-group list-group-lg no-radius list-email" id="users_list">
                                <li>Загрузка</li>

                            </ul>
                            <!-- end list-email -->
                            <!-- begin wrapper -->
                           
                        </div>
                        <!-- end wrapper -->
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- end vertical-box-column -->
</div>
<!-- end vertical-box -->
</div>
<!-- end #content -->



<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->
<div class="modal fade" id="confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Подтвердите действие</h4>
            </div>
            <div class="modal-body">
                Подтвердите, что вы хотите <b>удалить</b> выбранных пользователей
            </div>
            <div class="modal-footer">
                <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Закрыть</a>
                <a href="javascript:;" class="btn btn-sm btn-success">Подтвердить</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="create">
    <div class="modal-dialog">
        <form action='?' method="post">
            <input type='hidden' name="action" value="create">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Создание пользователя</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Компания</label>
                        <select name=data[companyID] class='form-control' required=""/>
                        {assign var=list value=Company::getList()}
                        <option value="">Выбрать</option>
                        {foreach from=$list item=c}
                            <option value="{$c->getId()}">{$c}</option>

                        {/foreach}
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Имя</label>
                        <input name="name[firstname]" class='form-control' type='text' required=""/>
                    </div>
                    <div class="form-group">
                        <label>Отчество</label>
                        <input name="name[middlename]" class='form-control' type='text' required=""/>
                    </div>
                    <div class="form-group">
                        <label>Фамилия</label>
                        <input name="name[lastname]" class='form-control' type='text' required=""/>
                    </div>
                    <div class="form-group">
                        <label>Телефон</label>
                        <input name="data[phone]" class='form-control' type='text' required=""/>
                    </div>
                    <div class="form-group">
                        <label>Почта</label>
                        <input name="data[email]" class='form-control' type='email' required="" />
                    </div>
                    <div class="form-group">
                        <label>Пароль</label>
                        <input name="data[pass]" class='form-control' type='text' required=""/>
                    </div>
                    <p id="create_error" class="hidden text-danger"></p>

                </div>
                <div class="modal-footer">
                    <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Закрыть</a>
                    <button type=submit class="btn btn-sm btn-success">Создать</button>
                </div>
            </div>
        </form>
    </div>
</div>
{include file="../footer.tpl" _h=1}