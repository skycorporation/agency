{include file="../header.tpl"}
{include file="../left.tpl"}
<style>
    .table.table-profile>tbody>tr>td.field{
        width:200px;
    }
</style>
<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li><a href="/">Главная</a></li>

    <li class="active">Редактирование профайла</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Редактирование профайла <small></small></h1>
<!-- end page-header -->
<!-- begin profile-container -->
{if $success}
    <div class='alert alert-success'>
        Информация обновлена
    </div>
{/if}
<form action='?' method='post' onsubmit="javascript:
                $('#e').addClass('hidden');
        if ($('#p1').val() != $('#p2').val()) {
            $('#e').removeClass('hidden').html('Пароли не совпадают');
            return false;
        }

        return true;

      ">
    <script> var objectID = '{$user->id}';</script>
    <input type='hidden' name='userid' value='{$user->id}' />
    <input type='hidden' name='action' value='save' />
    <div class="profile-container">
        <!-- begin profile-section -->
        <div class="profile-section">
            <!-- begin profile-left -->
            <div class="profile-left">
                <!-- begin profile-image -->
                <div class="profile-image">
                    <img id=image_src src="{$user->getPhotoUrl()}" />
                    <i class="fa fa-user hide"></i>
                </div>
                <!-- end profile-image -->
                <div class="m-b-10">
                    <a id=image_change href="#" class="btn btn-warning btn-block btn-sm">Изменить фотографию</a>
                </div>
                <!-- begin profile-highlight -->
                {if $is_admin}
                    <div class="profile-highlight">

                        <div class="checkbox m-b-5 m-t-0">
                            <label><input value=1 name="data[is_admin]" {if $user->getField('is_admin')=='1'}checked{/if} type="checkbox" /> Администратор компании</label>
                        </div>
                        <h4><i class="fa fa-cog"></i> Доступ к сервисам </h4>
                        {foreach from=Services::getPrivateServices($user) item=row}
                            <div class="checkbox m-b-0">
                                <label><input name=services[] value='{$row.id}' {if Services::isAvailable($row.id, $user->id )}checked{/if} type="checkbox" /> {$row.name} </label>
                            </div>
                        {/foreach}
                    </div>
                {/if}
                <!-- end profile-highlight -->
            </div>
            <!-- end profile-left -->
            <!-- begin profile-right -->
            <div class="profile-right">
                <!-- begin profile-info -->
                <div class="profile-info">
                    <!-- begin table -->
                    <div class="table-responsive">
                        <table class="table table-profile">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>
                            <h4>{$user->getNamePart('firstname')} {$user->getNamePart('lastname')}

                                </th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr class="highlight">
                                        <td class="field">Компания</td>
                                        <td>{$user->getCompany()}</td>
                                    </tr>
                                    <tr class="divider">
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td class="field">Должность</td>
                                        <td>
                                            <input required="" value="{$user->getField('position')|escape}" type='text' class="form-control input-inline input-xs" name='data[position]' size='35'>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="field">Год работы</td>
                                        <td>

                                            <input required="" value="{$user->getField('startYear')|escape}" type='text' class="form-control input-inline input-xs" name='data[startYear]' size='35'>

                                        </td>
                                    </tr>
                                    <tr class="divider">
                                        <td colspan="2"></td>
                                    </tr>


                                    <tr>
                                        <td class="field">Телефон</td>
                                        <td><input value="{$user->getField('phone')|escape}" type='text' class="form-control input-inline input-xs" name='data[phone]' size='35'></td>
                                    </tr>
                                    <tr class="divider">
                                        <td colspan="2"></td>
                                    </tr>

                                    <tr>
                                        <td class="field">ФИО</td>
                                        <td>

                                            <input required placeholder="Имя" value="{$user->getNamePart('firstname')|escape}" type='text' class="form-control input-inline input-xs" name='name[firstname]' size='15'>
                                            <input required placeholder="Отчество" value="{$user->getNamePart('middlename')|escape}" type='text' class="form-control input-inline input-xs" name='name[middlename]' size='15'>
                                            <input required placeholder="Фамилия" value="{$user->getNamePart('lastname')|escape}" type='text' class="form-control input-inline input-xs" name='name[lastname]' size='15'>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="field">Email</td>
                                        <td>{$user->getField('email')|escape}</td>
                                    </tr>
                                    <tr class="divider">
                                        <td colspan="2"></td>
                                    </tr>

                                    <tr >
                                        <td class="field">Новый пароль</td>
                                        <td><input id=p1 type="password" placeholder="Если не хотите менять - оставьте пустым" class="form-control input-inline input-xs" name="pass[p1]" value="" /></td>
                                    </tr>
                                    <tr >
                                        <td class="field">Новый пароль еще раз</td>
                                        <td><input id=p2 type="password" class="form-control input-inline input-xs" name="pass[p2]" value="" /></td>
                                    </tr>
                                    <tr class="highlight">
                                        <td class="field">Старый пароль</td>
                                        <td><input id=po type="text" class="form-control input-inline input-xs" name="pass[old]" value="" /></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <p class="text-danger hidden" id="e"></p>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td></td>
                                        <td>
                                            <input type='submit' value='Сохранить' class='btn btn-success' />
                                    </tr>

                                </tbody>
                        </table>
                    </div>
                    <!-- end table -->
                </div>
                <!-- end profile-info -->
            </div>
            <!-- end profile-right -->
        </div>
        <!-- end profile-section -->

    </div>
</form>
<!-- end profile-container -->
</div>
<!-- end #content -->



<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->
{include file="../footer.tpl"}