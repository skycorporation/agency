<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<title>Shay Nielsen Casting</title>
<link rel="apple-touch-icon" sizes="60x60" href="/template/app-assets/images/ico/apple-icon-60.png">
<link rel="apple-touch-icon" sizes="76x76" href="/template/app-assets/images/ico/apple-icon-76.png">
<link rel="apple-touch-icon" sizes="120x120" href="/template/app-assets/images/ico/apple-icon-120.png">
<link rel="apple-touch-icon" sizes="152x152" href="/template/app-assets/images/ico/apple-icon-152.png">
<link rel="shortcut icon" type="image/x-icon" href="/template/app-assets/images/ico/favicon.ico">
<link rel="shortcut icon" type="image/png" href="/template/app-assets/images/ico/favicon-32.png">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/bootstrap.css">
<!-- font icons-->
<link rel="stylesheet" type="text/css" href="/template/app-assets/fonts/icomoon.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/sliders/slick/slick.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/extensions/pace.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/forms/icheck/icheck.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/forms/icheck/custom.css">
<!-- END VENDOR CSS-->
<!-- BEGIN ROBUST CSS-->
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/bootstrap-extended.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/app.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/colors.css">
<!-- END ROBUST CSS-->
<!-- BEGIN Page Level CSS-->
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/core/menu/menu-types/vertical-menu.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/core/menu/menu-types/vertical-overlay-menu.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/pages/login-register.css">
<!-- END Page Level CSS-->
<!-- BEGIN Custom CSS-->
<link href="https://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/style.css">
<!-- END Custom CSS-->
<style>
html body {
    background-color: #FFF !important;
}
 #user-name:valid {
 border: 1px solid #1AB291 !important;
}
.mybtn2 {
    display: inline-block;
    font-weight: normal;
    line-height: 1.25;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    cursor: pointer;
    user-select: none;
    padding: 10px 18px;
    font-size: 1rem;
    border-radius: 5px;
    transition: all 0.2s ease-in-out;
    -webkit-appearance: none !important;
}
.mybtn-green, .mybtn-green:hover, .mybtn-green:active, .mybtn-green:visited, .mybtn-green:focus {
    color: #FFF !important;
    border-color: #1AB291 !important;
    background-color: #1AB291 !important;
}
.form-control {
    border: 1px solid #959595 !important;
    border-radius: 5px;
}
.form-control:focus {
    border: 1px solid #FDC637 !important;
}
.card {
    border: none !important;
    box-shadow: none !important;
}
</style>
</head>
<body data-open="click" data-menu="vertical-menu" data-col="1-column" class="vertical-layout vertical-menu 1-column blank-page blank-page">
<!-- ////////////////////////////////////////////////////////////////////////////-->

<div class="modal fade text-xs-left pr-0" id="adminLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel10" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-success white">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title py-1" id="myModalLabel10">Admin login</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" action="#" novalidate method="post">
          <input type="hidden" value="login" name="action">
          <fieldset class="form-group position-relative">
            <input autocomplete="dontsuggest" type="text" class="form-control" id="user-name" placeholder="username" required name="username">
            <div class="form-control-position"> </div>
          </fieldset>
          <fieldset class="form-group position-relative {if $smarty.get.err==1}has-danger{/if}">
            <input autocomplete="dontsuggest" type="password" class="form-control {if $smarty.get.err==1}form-control-danger{/if}" id="user-password" placeholder="password" required name="password">
            <div class="form-control-position"> </div>
          </fieldset>
          <button type="submit" class="mybtn2 mybtn-green btn-block">SIGN IN</button>
          <fieldset class="form-group row" style="margin-top: 20px;">
            <br>
            <div class="col-md-12 col-xs-12 text-xs-center" style="text-align: center !important;"><a href="mailto:oleg@presentmotion.com" class="card-link" style="color: #ccc !important;">Forgot Password?</a></div>
          </fieldset>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="mybtn2 mybtn-grey" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<div class="app-content content container-fluid">
  <div class="content-wrapper">
  <div class="content-header row"> </div>
  <div class="content-body">
  <section class="flexbox-container">
    <div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1 p-0">
      <div class="card px-1 py-1 m-0">
        <div class="card-header no-border">
          <div class="card-title text-center"><img src="/template/app-assets/images/logo-homepage.png" alt="Shay Nielsen Casting logo" style="height: 50px; width: auto;"> </div>
        </div>
        <!--<div class="card-body collapse in">
          <div class="card-block text-center">
			  <br>
            <p class="w-100">Active castings:</p>
            <hr>
			  <a href="#" style="color: black !important;">
            <h5>Casting name</h5>
            <p>Casting location
				<br>
            Casting date</p>
			  </a>
            <hr>
          </div>
        </div>-->
      </div>
    </div>
    </div>
    </div>
  </section>
</div>
</div>
</div>
<footer class="footer fixed-bottom footer-light navbar-border">
  <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 p-1"><span class="float-md-right d-block d-md-inline-blockd-none d-lg-block"><a data-toggle="modal" onclick="" data-target="#adminLogin" title="Admin Login">Admin login<i class="ft-heart pink"></i></a></span></p>
</footer>
<!-- ////////////////////////////////////////////////////////////////////////////--> 

<!-- BEGIN VENDOR JS--> 
<script src="/template/app-assets/js/core/libraries/jquery.min.js" type="text/javascript"></script> 
<script src="/template/app-assets/vendors/js/ui/tether.min.js" type="text/javascript"></script> 
<script src="/template/app-assets/js/core/libraries/bootstrap.min.js" type="text/javascript"></script> 
<script src="/template/app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js" type="text/javascript"></script> 
<script src="/template/app-assets/vendors/js/ui/unison.min.js" type="text/javascript"></script> 
<script src="/template/app-assets/vendors/js/ui/blockUI.min.js" type="text/javascript"></script> 
<script src="/template/app-assets/vendors/js/ui/jquery.matchHeight-min.j') }}s" type="text/javascript"></script> 
<script src="/template/app-assets/vendors/js/ui/jquery-sliding-menu.j') }}s" type="text/javascript"></script> 
<script src="/template/app-assets/vendors/js/sliders/slick/slick.min.js" type="text/javascript"></script> 
<script src="/template/app-assets/vendors/js/ui/screenfull.min.js" type="text/javascript"></script> 
<script src="/template/app-assets/vendors/js/extensions/pace.min.js" type="text/javascript"></script> 
<!-- BEGIN VENDOR JS--> 
<!-- BEGIN PAGE VENDOR JS--> 
<script src="/template/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js" type="text/javascript"></script> 
<script src="/template/app-assets/vendors/js/forms/icheck/icheck.min.js" type="text/javascript"></script> 
<!-- END PAGE VENDOR JS--> 
<!-- BEGIN ROBUST JS--> 
<script src="/template/app-assets/js/core/app-menu.js" type="text/javascript"></script> 
<script src="/template/app-assets/js/core/app.js" type="text/javascript"></script> 
<script src="/template/app-assets/js/scripts/ui/fullscreenSearch.js" type="text/javascript"></script> 
<!-- END ROBUST JS--> 
<!-- BEGIN PAGE LEVEL JS--> 
<script src="/template/app-assets/js/scripts/forms/form-login-register.js" type="text/javascript"></script> 
<!-- END PAGE LEVEL JS-->
</body>
</html>
