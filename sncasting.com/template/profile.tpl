{include file="header.tpl"}
{include file="modals/deleteProfile.tpl"}
{include file="modals/newGroup.tpl"}
<div class="content-body">
  <div class="selectedProfile" data-id="{$profile->id}" style="display: none"></div>
  <div id="user-profile">
    <div class="row">
      <div class="col-xl-6 col-md-6 col-xs-12">
        <div class="card profile-with-cover">
          <div class="media profil-cover-details" style="margin-top: 15px;">
            <div class="media-body media-middle row">
              <div class="col-xl-6 col-md-6 col-xs-12"> 
                <!--<div class="btn-group" role="group" aria-label="Basic example">
                                <a class="btn mybtn-red" data-toggle="modal" data-target="#danger" style="color: #FFF">Delete</a>
                                <a class="btn mybtn-yellow" href="/profile/edit?id={$profile->id}">Edit</a>
                                <button type="button" class="btn mybtn-green">Print</button>
                            </div>--> 
              </div>
              <div class="col-xs-6"> </div>
            </div>
          </div>
          {assign var="size" value=getimagesize($profile->photo)}
          <figure itemprop="associatedMedia" id="firstPhoto" itemscope itemtype="http://schema.org/ImageObject" class="item card-img-top img-fluid"> <a href="{$profile->photo|replace:'/home/aibdh80ehx97/public_html/sncasting.com':''}" itemprop="contentUrl" data-size="{$size[0]}x{$size[1]}"> <img src="{$profile->photo|replace:'/home/aibdh80ehx97/public_html/sncasting.com':''}" itemprop="thumbnail" alt="Image description" style="width: 100%; cursor: pointer;" {if $size[0]>$size[1]}style="height: 380px; width: auto;"{/if}onerror="this.src='/template/app-assets/images/noimage.png'"/> </a> </figure>
        </div>
      </div>
      {if $my->id>0}
      <div class="col-xl-6 col-md-6 col-xs-12">
        <div class="profile-toolset"> <span class="profile-id" title="Profile number"><i class="ft-user"></i>&nbsp;{$profile->id}</span>
          <div style="float: right"> <a class="mybtn mybtn-purple button--moema" data-toggle="modal" data-target="#createGroup" onclick="profileToAddToCast={$profile->id}" id="addToBtn"><i class="ft-copy" style="font-size: 24px; line-height: 1;" title="Add to group or casting"></i></a> <a class="mybtn mybtn-yellow button--moema" href="/profile/edit?id={$profile->id}" title="Edit profile"><i class="ft-edit-2" style="font-size: 24px; line-height: 1;"></i></a> <a class="mybtn mybtn-red button--moema" data-toggle="modal" onclick="profileToDelete={$profile->id}" data-target="#deleteProfile" title="Delete profile"><i class="icon-trash4" style="font-size: 24px; line-height: 1;"></i></a> </div>
        </div>
      </div>
      {/if}
      <div class="col-xl-6 col-md-6 col-xs-12">
        <div class="card">
          <div class="card-body ">
            <div class="card-block">
              <div class="card-text">
                <h3 class="card-title" style="text-align: center">{$profile->firstName} {if $visibleFields['lastName']}{$profile->lastName}{/if}</h3>
                <br>
				<!--Admin view only-->
				{if $my->id>0}
                {if $profile->birthday }Age: <b>{$age}</b>{if $profile->birthday!="0000-00-00"} ({$profile->birthday|date_format}){/if}{/if}<br>
                Gender: <b>{$fields["gender"][$profile->gender]}</b><br>
                {if $ethnicityPrint != ''}Ethnicity: <b>{$ethnicityPrint}</b><br>{/if}
                {if $profile->basedIn}Based in: <b>{$profile->basedIn}</b><br>{/if}
				{if $profile->occupation}Occupation: <b>{$profile->occupation}</b><br>{/if}
				{if $profile->rateAmount != ''}Rate: <b>${$profile->rateAmount}</b> per <b>{if $profile->rateType == 0}hour{/if}{if $profile->rateType == 1}day{/if}{if $profile->rateType == 2}project{/if}</b><br>{/if}
                {if $profile->availability}Availibility: <b>{$profile->availability}</b><br>{/if}
                
                {if $profile->knownFor}Known for: <b>{$profile->knownFor}</b><br>{/if}
                {if $profile->upcomingProjects}Upcoming projects: <b>{$profile->upcomingProjects}</b><br>{/if}
				  
				{if $profileType == 0 && $babyAbilitiesPrint}
                Baby Availability: <b>{$babyAbilitiesPrint}</b><br>{/if}
                
                {if $heightPrint}<br>Height: <b>{$heightPrint}</b><br>{/if}
                {if $profile->weight}Weight: <b>{$profile->weight} lb</b><br>{/if}
                
                
				<!--For adults and teens only-->
				{if $profileType != 0}
				
					<!--For females only-->
					{if $profile->gender==1}
				  		{if $profile->dressSize}Dress size: <b>{$profile->dressSize}</b><br>{/if}
						{if $profile->bust}Bust size: <b>{$profile->bust}</b><br>{/if}
				  		{if $profile->waist}Waist size: <b>{$profile->waist}</b><br>{/if}
						{if $profile->hip}Hip size: <b>{$profile->hip}</b><br>{/if}
						
						
					<!--End of for females only-->
					
				  	<!--For males only-->
					{else}
				  		{if $fields["shirtSize"][$profileType][$profile->shirtSize]}Shirt size: <b>{$fields["shirtSize"][$profileType][$profile->shirtSize]}</b><br>{/if}
						{if $profile->waist}Pant size: <b>{$profile->waist}{if $profile->inseam}x{$profile->inseam}{/if}</b><br>{/if}
					{/if}
                	<!--End of for males only-->
				  
                	
				<!--End of for adults and teens only-->
				
				<!--For children only-->
                {else}
                    {if $fields["shirtSize"][$profileType][$profile->shirtSize]}Clothing size: <b>{$fields["shirtSize"][$profileType][$profile->shirtSize]}</b><br>{/if}
                {/if}
				<!--End of for children only-->
				  
                {if $fields["shoeSize"][$profile->shoeSize]}Shoe size: <b>{$fields["shoeSize"][$profile->shoeSize]}</b><br>{/if}
                
                {if $instagramPrint}<br>Instagram: {$instagramPrint}<br><br>{/if}
				{if $profile->website}Website: <a href="{$profile->website}" target="_blank">External Link</a><br>{/if}
                {if $profile->book}Book: <a href="{$profile->book}" target="_blank">External Link</a><br>{/if}
                {if $pressPrint}<br>Press:<br>{$pressPrint}<br>{/if}
                {if $musicPrint}<br>Music:<br>{$musicPrint}<br>{/if}
                {if $videoPrint}<br>Video:<br>{$videoPrint}<br>{/if}
                
                
                {if $profile->bio}<br><b>Bio:</b><br>{$profile->bio}{/if}
                {if $profile->privateNotes}<br><br><b>Private notes:</b><br>{$profile->privateNotes}{/if}
				{if $keywordsPrint != ''}<br><br>Keywords: <div class="profile-keywords">{$keywordsPrint}</div>{/if}
				{/if}
				<!--End of admin view only-->
				  
				
				<!--Client view only-->
				{if $my->id==0}
				  {if $profile->basedIn && $visibleFields['basedIn']}Based in: <b>{$profile->basedIn}</b><br>{/if}
				  {if $profile->birthday}Age: <b>{$age}</b><br>{/if}
				  {if $profile->occupation && $visibleFields['occupation']}Occupation: <b>{$profile->occupation}</b><br>{/if}
				  {if $profile->knownFor && $visibleFields['knownFor']}Known for: <b>{$profile->knownFor}</b><br>{/if}
				  {if $profile->upcomingProjects && $visibleFields['upcomingProjects']}Upcoming projects: <b>{$profile->upcomingProjects}</b><br>{/if}
				  {if $instagramPrint && $visibleFields['instagram']}<br>Instagram: {$instagramPrint}<br><br>{/if}
				  {if $profile->website && $visibleFields['website']}Website: <a href="{$profile->website}" target="_blank">External Link</a><br>{/if}
				  {if $profile->book && $visibleFields['book']}Book: <a href="{$profile->book}" target="_blank">External Link</a><br>{/if}
				  {if $pressPrint && $visibleFields['press']}<br>Press:<br>{$pressPrint}<br>{/if}
                  {if $musicPrint && $visibleFields['music']}<br>Music:<br>{$musicPrint}<br>{/if}
                  {if $videoPrint && $visibleFields['video']}<br>Video:<br>{$videoPrint}<br>{/if}
				  
				  
				  {if $heightPrint && $visibleFields['height']}<br>Height: <b>{$heightPrint}</b><br>{/if}
				  <!--For adults and teens only-->
				{if $profileType != 0}
				
					<!--For females only-->
					{if $profile->gender==1}
				  		{if $profile->dressSize && $visibleFields['dressSize']}Dress size: <b>{$profile->dressSize}</b><br>{/if}
						{if $profile->bust && $visibleFields['bustSize']}Bust size: <b>{$profile->bust}</b><br>{/if}
				  		{if $profile->waist && $visibleFields['waistSize']}Waist size: <b>{$profile->waist}</b><br>{/if}
						{if $profile->hip && $visibleFields['hipSize']}Hip size: <b>{$profile->hip}</b><br>{/if}
						
						
					<!--End of for females only-->
					
				  	<!--For males only-->
					{else}
				  		{if $fields["shirtSize"][$profileType][$profile->shirtSize] && $visibleFields['shirtSize']}Shirt size: <b>{$fields["shirtSize"][$profileType][$profile->shirtSize]}</b><br>{/if}
						{if $profile->waist && $visibleFields['pantSize']}Pant size: <b>{$profile->waist}{if $profile->inseam}x{$profile->inseam}{/if}</b><br>{/if}
					{/if}
                	<!--End of for males only-->
				  
                	
				<!--End of for adults and teens only-->
				
				<!--For children only-->
                {else}
                    {if $fields["shirtSize"][$profileType][$profile->shirtSize] && $visibleFields['shirtSize']}Clothing size: <b>{$fields["shirtSize"][$profileType][$profile->shirtSize]}</b><br>{/if}
                {/if}
				<!--End of for children only-->
				  
                {if $fields["shoeSize"][$profile->shoeSize] && $visibleFields['shoeSize']}Shoe size: <b>{$fields["shoeSize"][$profile->shoeSize]}</b><br>{/if}
				{if $profile->bio && $visibleFields['bio']}<br><b>Bio:</b><br>{$profile->bio}{/if}
				{if $profile->rateAmount != '' && $visibleFields['rate']}<br><br>Rate: <b>${$profile->rateAmount}</b> per <b>{if $profile->rateType == 0}hour{/if}{if $profile->rateType == 1}day{/if}{if $profile->rateType == 2}project{/if}</b><br>{/if}
				{if $profile->availability && $visibleFields['availability']}<br>Availability: <b>{$profile->availability}</b><br>{/if}
				  
				{/if}
				<!--End of client view only-->
				</div>
            </div>
          </div>
        </div>
      </div>
      {if $profile->agentName or $profile->agency or $profile->agentPhone or $profile->agentEmail}
      <div class="col-xl-6 col-md-6 col-xs-12">
        <div class="section-title">Agency</div>
        <div class="card">
          <div class="card-block" style="padding-bottom: 21px;">
            {if $profile->agentName}{$profile->agentName}{/if}
			  {if $my->id>0}
            	{if $profile->agentPhone}<br><a href="tel:{$profile->agentPhone}">{$profile->agentPhone}</a>{/if}
            	{if $profile->agentEmail}<br><a href="mailto:{$profile->agentEmail}">{$profile->agentEmail}</a>{/if}
			  {/if}
		  </div>
        </div>
      </div>
      {/if}
      
      {if $my->id>0}
      <div class="col-xl-6 col-md-6 col-xs-12">
        <div class="section-title">Contact</div>
        <div class="card">
            <div class="card-block" style="padding-bottom: 21px;">
			  {if $profile->address}{$profile->address}{/if}
              {if $profile->address2}<br>{$profile->address2}{/if}
              {if $profile->city}<br>{$profile->city},{/if}
              {if $profile->state}{$profile->state} {/if}
              {if $profile->ZIPcode}{$profile->ZIPcode}<br><br>{/if}
				
              {if $profile->email}E-mail: <a href="mailto:{$profile->email}">{$profile->email}</a>{/if}
              {if $profile->homePhone}<br>Home phone: <a href="tel:{$profile->homePhone}">{$profile->homePhone}</a>{/if}
              {if $profile->cellPhone}<br>Cell phone: <a href="tel:{$profile->cellPhone}">{$profile->cellPhone}</a>{/if}
			</div>
        </div>
      </div>
      {/if}
      {if $profileType != 2 && $my->id>0 && ($profile->parentName || $profile->parentPhone)}
      <div class="col-xl-6 col-md-6 col-xs-12">
        <div class="section-title">Parent / Guardian</div>
        <div class="card">
          <div class="text-xs-left">
            <div class="card-block" style="padding-bottom: 21px;">
              <h4 class="card-title">{$profile->parentName}</h4>
              {if $profile->parentPhone}Phone: <a href="tel:{$profile->parentPhone}">{$profile->parentPhone}</a><br>
              {/if}
              E-mail: <a href="mailto:{$profile->parentEmail}">{$profile->parentEmail}</a> </div>
          </div>
        </div>
      </div>
      {/if} </div>
    <section class="card">
      <div class="section-title">Gallery</div>
      <div class="card-block p-0">
        <div class="card-body collapse in">
          <div class="card-block my-gallery">
			  {if $photos}{else}<div class="text-muted">No photos here yet...</div>{/if}
			  <!-- Image grid -->  
              <div id="grid">{foreach from=$photos item=p}
              {assign var="size" value=getimagesize($p)}
              <figure class="item"><a href="{$p|replace:'/home/aibdh80ehx97/public_html/sncasting.com':''}" data-size="{$size[0]}x{$size[1]}"><img src="{$p|replace:'/home/aibdh80ehx97/public_html/sncasting.com':''}" alt="" /></a></figure>
              {/foreach} </div>
			  <!--/ Image grid -->  
          </div>
          
          <!--/ Image grid --> 
          <!-- Root element of PhotoSwipe. Must have class pswp. -->
          <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true"> 
            <!-- Background of PhotoSwipe. It's a separate element as animating opacity is faster than rgba(). -->
            <div class="pswp__bg"></div>
            <!-- Slides wrapper with overflow:hidden. -->
            <div class="pswp__scroll-wrap"> 
              <!-- Container that holds slides. PhotoSwipe keeps only 3 of them in the DOM to save memory. Don't modify these 3 pswp__item elements, data is added later on. -->
              <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
              </div>
              <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
              <div class="pswp__ui pswp__ui--hidden">
                <div class="pswp__top-bar"> 
                  <!--  Controls are self-explanatory. Order can be changed. -->
                  <div class="pswp__counter"></div>
                  <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                  <button class="pswp__button pswp__button--share" title="Share"></button>
                  <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                  <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                  <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR --> 
                  <!-- element will get class pswp__preloader-active when preloader is running -->
                  <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                  <div class="pswp__share-tooltip"></div>
                </div>
                <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"> </button>
                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"> </button>
                <div class="pswp__caption">
                  <div class="pswp__caption__center"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--/ PhotoSwipe --> 
      </div>
    </section>
    {if $family} 
    <!-- Hover Effects -->
    <section id="hover-effects" class="card dash" style="margin-top: 20px !important; box-shadow: none; background-color: transparent;">
      <h3 class="mb-2" style="text-align: center">Related profiles:</h3>
      <div class="card-block my-gallery" itemscope itemtype="http://schema.org/ImageGallery">
        <div class="row" style="margin: 0;">
          <div class="grid-hover"> {foreach from=$family item=t key=k}
            {assign var="size" value=getimagesize($t->photo)}
            <div class="col-md-4 col-xs-12" >
              <div class="thumbnail-container" data-id="{$t->id}">
                <figure class="effect-roxy"> <img src="{$t->photo|replace:'/home/aibdh80ehx97/public_html/sncasting.com':''}" alt="img15" {if $size[0]>$size[1]}style="height: 380px; width: auto;"{/if}onerror="this.src='/template/app-assets/images/noimage.png'"/>
                  <figcaption>
                    <div class="thumbInfo" onclick="location='/profile/view?id={$t->id}'">
                      <div class="tableCell align-middle">
                        <p><br>
                          <br>
                          <br>
                          <b>{$fields['profileType'][$t->profileType]}</b><br>
                          Height: <b>{$t->height}</b><br>
                          Weight: <b>{$t->weight}lb</b><br>
                          {if $t->profileType == 2}
                          Shirt: <b>{$fields['shirtSize'][$t->profileType][$t->shirtSize]}</b><br>
                          Pants: <b>{$t->waist}/{$t->inseam}</b><br>
                          {else}
                          Clothes: <b>{$fields['shirtSize'][$t->profileType][$t->shirtSize]}</b><br>
                          {/if}
                          Shoe: <b>{$fields['shoeSize'][$t->shoeSize]}</b></p>
                      </div>
                    </div>
                  </figcaption>
                </figure>
                <a class="name-place" href="/profile/view?id={$t->id}"><span class="pnum">{$t->num}</span>{$t->firstName}&nbsp;{$t->lastName}</a>
                <button class="mobSelectButton" onclick="mobSelectProfile(this)"><i id="mobSelectStat" class="icon-android-radio-button-off"></i></button>
              </div>
            </div>
            {/foreach} </div>
          <div class="row">
            <div class="col-12 hidden-sm-up viewSwitcher">
              <button type="button" id="scrollToTop" class="mt-2"><i class="icon-arrow-up4"></i></button>
            </div>
          </div>
        </div>
      </div>
      <!--/ Image grid --> 
    </section>
    <!--/ Hover Effects --> 
    {/if} </div>
</div>
{include file="footer.tpl"}