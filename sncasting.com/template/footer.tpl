
      </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->
<div class="designedby mb-1"><a href="http://www.presentmotion.com" target="_blank">Designed by Present Motion</a></div>
</div>

    <footer class="footer fixed-bottom footer-light navbar-shadow" style="padding: 0 !important">
		<center>
			<div id="mainToolbox" class="mainToolbox">
      <button type="button" class="deselectAllButton mr-1 button--moema" title="Deselect all" onclick="deselectAll()"><i class="ft-circle mr-0"></i></button><button type="button" class="addToGroupButton mr-1 button--moema" title="Add to group or casting" data-toggle="modal" data-target="#createGroup" onclick="" id="createGroupButton"><i class="ft-copy mr-0"></i></button>
      <!--<button class="linkFamilyButton mr-1" title="Link as family" data-toggle="modal" data-target="" onclick="linkAsFamily()"><i class="icon-ios-people"></i></button>-->
      <button class="deleteMultProfileButton button--moema" title="Delete selected profiles" data-toggle="modal" data-target="#removeMultiple" onclick=""><i class="icon-trash4"></i></button>
    </div>
			<!--<p class="clearfix text-muted text-sm-center mb-0 px-2"><span class="float-md-center d-xs-block d-md-inline-block">Copyright &copy; Shay Nielsen Casting LLC.</span></p>--></center>
    </footer>

    <!-- BEGIN VENDOR JS-->
    <script src="/template/app-assets/js/core/libraries/jquery.min.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/ui/tether.min.js" type="text/javascript"></script>
    <script src="/template/app-assets/js/core/libraries/bootstrap.min.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/ui/unison.min.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/ui/blockUI.min.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/ui/jquery.matchHeight-min.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/ui/jquery-sliding-menu.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/sliders/slick/slick.min.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/ui/screenfull.min.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/extensions/pace.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script type="text/javascript" src="/template/app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script type="text/javascript" src="/template/app-assets/vendors/js/ui/prism.min.js"></script>
    <script src="/template/app-assets/vendors/js/gallery/masonry/masonry.pkgd.min.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/gallery/photo-swipe/photoswipe.min.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/gallery/photo-swipe/photoswipe-ui-default.min.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/pickers/dateTime/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/pickers/daterange/daterangepicker.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/extensions/dropzone.min.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/extensions/wNumb.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/extensions/nouislider.min.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/forms/toggle/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="/template/app-assets/vendors/js/forms/toggle/bootstrap-checkbox.js" type="text/javascript"></script>
	<script src="/template/app-assets/vendors/js/forms/toggle/bootstrap-checkbox.min.js" type="text/javascript"></script>
	<script src="/template/app-assets/vendors/js/forms/toggle/switchery.min.js" type="text/javascript"></script>
	<script src="/template/app-assets/vendors/js/forms/tags/tagging.min.js" type="text/javascript"></script>
	<script src="/template/app-assets/vendors/js/forms/repeater/jquery.repeater.min.js" type="text/javascript"></script>
   
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN ROBUST JS-->
    <script src="/template/app-assets/js/core/app-menu.js" type="text/javascript"></script>
    <script src="/template/app-assets/js/core/app.js" type="text/javascript"></script>
    <script src="/template/app-assets/js/scripts/ui/fullscreenSearch.js" type="text/javascript"></script>
    <!-- END ROBUST JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="/template/app-assets/vendors/js/extensions/toastr.min.js" type="text/javascript"></script>
    <script src="/template/app-assets/js/scripts/forms/tags/tagging.js" type="text/javascript"></script>
    <script src="/template/app-assets/js/formfeedback.js" type="text/javascript"></script>
    <script src="/template/js/script.js" type="text/javascript"></script>
    {foreach from=$js item=url}
	<script src="{$url}" type="text/javascript"></script>
	{/foreach}
    <script src="/template/app-assets/js/scripts/grid/grid.js" type="text/javascript"></script>
    <script src="/template/app-assets/js/scripts/grid/gridStart.js" type="text/javascript"></script>
    <script src="/template/app-assets/js/scripts/pickers/dateTime/picker-date-time.js" type="text/javascript"></script>
    <script src="/template/app-assets/js/scripts/forms/switch.js" type="text/javascript"></script>
    <script src="/template/app-assets/js/scripts/navs/navs.min.js" type="text/javascript"></script>
    <script src="/template/app-assets/js/scripts/forms/form-repeater.js" type="text/javascript"></script>
	<script src="/template/app-assets/js/scripts/extensions/dropzone.js" type="text/javascript"></script>
	<script src="/template/app-assets/vendors/js/forms/extended/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
	<script src="/template/app-assets/js/scripts/forms/extended/form-typeahead.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
    <!-- BEGIN USER JS-->
	
	<script src="/template/app-assets/js/jqBootstrapValidation.js" type="text/javascript"></script>
<script src="/template/app-assets/vendors/js/forms/tags/tagging.min.js" type="text/javascript"></script> 
<script src="/template/app-assets/js/scripts/forms/tags/tagging.js" type="text/javascript"></script> 
<script src="/template/app-assets/vendors/js/forms/icheck/icheck.min.js"></script>
<script src="/template/app-assets/js/scripts/forms/checkbox-radio.min.js"></script>
	<!-- END USER JS-->
  </body>
</html>
