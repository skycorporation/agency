<table width="100%">
	<tbody>
		<tr>
			<td width="50%">
				<h3>{$profile->firstName} {$profile->lastName}</h3>
				<br>
				{$fields["agency"][$profile->agency]}<br>
                {$profile->agentName}<br>
                {$profile->agentPhone}<br>
			</td>
			<td align="right" width="50%">
				Age: <div class="param">&nbsp {$age}</div><br>
				Height: <div class="param">&nbsp {$heightPrint}</div><br>
				Shoe size: <div class="param">&nbsp {$fields["shoeSize"][$profile->shoeSize]}</div><br>
				Weight: <div class="param">&nbsp {$profile->weight}</div><br>
				{if $profileType == 2}
				Shirt size: <div class="param">&nbsp {$fields["shirtSize"][$profileType][$profile->shirtSize]}</div><br>
				Pant size: <div class="param">&nbsp {$profile->waist}/{$profile->inseam}</div><br>
				{else}
				Clothing size: <div class="param">&nbsp {$fields["shirtSize"][$profileType][$profile->shirtSize]}</div><br>
				{/if}
			</td>
		</tr>
		<tr>
			<td width="50%" style="width: 50%;">
				<img src="{$photos[0]|replace:'/home/aibdh80ehx97/public_html/sncasting.com':''}" itemprop="thumbnail" alt="Image description" />
			</td>
			
			<td width="50%" style="width: 50%;">
				<img src="{$photos[1]|replace:'/home/aibdh80ehx97/public_html/sncasting.com':''}" itemprop="thumbnail" alt="Image description" />
			</td>
		</tr>
		<tr>
			<td  colspan="2">
				Comments
			</td>
		</tr>
	</tbody>
</table>



<style>
	.param{
		border: 1px solid #000;
		padding: 10px 20px;
		border-radius: 5px;
		min-height: 19px;
		text-align: center;
		display: inline-block;
		min-width: 50px;
		margin: 5px;
	}
</style>