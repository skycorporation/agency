{include file="header.tpl"}
<style>
	.dropzone {
    min-height: 200px;
	}
	.dropzone .dz-message {
		font-size: 1.5rem;
		height: auto;
		margin-top: 18px;
	}
	.dropzone .dz-default.dz-message:before {
		font-size: 40px;
	}
</style>

{*<!-- Стили и скрипты для переключателей видимости -->*}
<style>
.btn-vis-toggle[data-state="visible"] {
  color: #1AB291 !important;
}
</style>
<script>
  function toggleVisibility(itemName) {
    var btn = $('#' + itemName + 'Visible-btn');
    var checkbox = $('#' + itemName + 'Visible');

    var btnNewState = !(btn.attr('data-state') === 'visible');
    if (btnNewState) {
      checkbox.attr('checked', true);
      btn.attr('data-state', 'visible');
    } else {
      checkbox.attr('checked', false);
      btn.attr('data-state', 'hidden');
    }
  }
</script>

<div class="w-100 text-center mt-3 mb-2">
  <h1>{if $profile->id}Edit{else}Create new{/if} profile</h1>
</div>
<br>
<form class="form form-horizontal" method="post" action="#" enctype="multipart/form-data">
  <div class="form-body">
    <section class="card">
      <div class="card-block">
        <button type="submit" class="btn btn-primary mybtn-green" style="float: right;"> <i class="icon-check2"></i> Save </button>
        <h4 class="form-section"><i class="icon-head"></i>General Info</h4>
        <div class="form-group row">
          <label class="col-md-3 label-control">Profile Type</label>
          <div class="col-md-9">
            <div class="btn-group" role="group" aria-label="Profile Type"> {foreach from=$fields["profileType"] item=t key=k} <a type="button" href="/profile/{if $profile->id}edit?id={$profile->id}&profileType={$k}{else}add?profileType={$k}{/if}" class="mybtn2 mybtn-{if $k==$profileType}green{else}grey{/if}">{$t}</a> {/foreach}
              <input type="hidden" value="{$profileType}" name="data[profileType]">
            </div>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputFirstName">First Name</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="{$profile->firstName}" type="text" id="projectinputFirstName" class="form-control checkThis capThis trimit" name="data[firstName]" required>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputLastName">Last Name</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="{$profile->lastName}" type="text" id="projectinputLastName" class="form-control checkThis capThis trimit" name="data[lastName]" required>
            <div class="field-actions">
            <button title="Visibility for client" type="button" class="btn-vis-toggle" id="lastNameVisible-btn" onclick="toggleVisibility('lastName');" {if $visibleFields['lastName']}data-state="visible"{/if}><i class="ft-eye"></i></button>
          </div>
          <input type="checkbox" style="position: absolute; visibility: hidden;" id="lastNameVisible" name="visible[lastName]" {if $visibleFields['lastName']}checked{/if}/>
          </div>
        </div>
        <div class="form-group row skin skin-flat">
          <label class="col-md-3 col-sm-12 col-xs-12 label-control">Gender</label>
          <div class="col-md-9 float-xs-left">
          	{foreach from=$fields["gender"] item=t key=k}
	          	<fieldset>
	              <input type="radio" name="data[gender]" {if $k==$profile->gender}checked{/if} id="gender-{$k}" value="{$k}">
	              <label for="gender-{$k}">{$t}</label>
	            </fieldset>
          	{/foreach}
          </div>
        </div>
        <div class="form-group row">
          <label class="col-xs-12 col-sm-12 col-md-3 label-control">Birthday</label>
          <div class="col-sm-6 col-md-4 col-lg-3 col-xl-3">
            <div class="input-group">
              <select class="form-control checkThis mr-5px" id="bdMonth" onchange="bdChange()">
                <option value="" disabled selected>month</option>
                <option value="01" {if $profile->birthday|date_format:"%m" == 1}selected{/if}>Jan</option>
                <option value="02" {if $profile->birthday|date_format:"%m" == 2}selected{/if}>Feb</option>
                <option value="03" {if $profile->birthday|date_format:"%m" == 3}selected{/if}>Mar</option>
                <option value="04" {if $profile->birthday|date_format:"%m" == 4}selected{/if}>Apr</option>
                <option value="05" {if $profile->birthday|date_format:"%m" == 5}selected{/if}>May</option>
                <option value="06" {if $profile->birthday|date_format:"%m" == 6}selected{/if}>Jun</option>
                <option value="07" {if $profile->birthday|date_format:"%m" == 7}selected{/if}>Jul</option>
                <option value="08" {if $profile->birthday|date_format:"%m" == 8}selected{/if}>Aug</option>
                <option value="09" {if $profile->birthday|date_format:"%m" == 9}selected{/if}>Sep</option>
                <option value="10" {if $profile->birthday|date_format:"%m" == 10}selected{/if}>Oct</option>
                <option value="11" {if $profile->birthday|date_format:"%m" == 11}selected{/if}>Nov</option>
                <option value="12" {if $profile->birthday|date_format:"%m" == 12}selected{/if}>Dec</option>
              </select>
              <select class="form-control checkThis mr-5px" id="bdDay" onchange="bdChange()">
                <option value="" disabled selected>day</option>
                
		                    {for $k=1 to 31}
                
                <option value="{$k}" {if $profile->birthday|date_format:"%e" == $k}selected{/if}>{$k}</option>
                

		                    {/for}
              
              </select>
              <select class="form-control checkThis" id="bdYear" onchange="bdChange()">
                <option value="" disabled selected>year</option>
                
                
		                    {if $profileType==2}{assign var="d" value=70}{/if}
		                    {if $profileType==1}{assign var="d" value=18}{/if}
		                    {if $profileType==0}{assign var="d" value=3}{/if}
		                    {for $k='Y'|date-$d to 'Y'|date}

                
                <option value="{$k}" {if $profile->birthday|date_format:"%Y" == $k}selected{/if}>{$k}</option>
                
		                    {/for}
              
              </select>
            </div>
            <input value="{$profile->birthday}" type='hidden' name="data[birthday]" id="dbFinal"/>
          </div>
        </div>
        <div class="form-group row typeahead">
          <label class="col-xs-12 col-sm-12 col-md-3 label-control" for="projectinputBased">Based in</label>
          <div class="col-sm-6 col-md-4 col-lg-3 col-xl-3">
              <input autocomplete="" type="text" name="data[basedIш]" class="form-control typeahead-basic typeahead trimit checkThis" value="{$profile->basedIn}" id="projectinputBased">
            <div class="field-actions">
            <button title="Visibility for client" type="button" class="btn-vis-toggle" id="basedInVisible-btn" onclick="toggleVisibility('basedIn');" {if $visibleFields['basedIn']}data-state="visible"{/if}><i class="ft-eye"></i></button>
          </div>
          <input type="checkbox" style="position: absolute; visibility: hidden;" id="basedInVisible" name="visible[basedIn]" {if $visibleFields['basedIn']}checked{/if}/>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control">Keywords</label>
          <div class="col-md-6">
            <fieldset>
              <div class="form-group">
                <div class="form-control" id="keywords" data-tags-input-name="edit-on-delete" style="min-height: 80px">{$keywords}</div>
                <small class="text-muted">Press Enter, Comma or Spacebar to add a new tag, Backspace or Delete to remove.</small></div>
            </fieldset>
            <input type="hidden" name="data[keywords]" value='{$profile->keywords}'>
          </div>
        </div>
          <hr style="margin-bottom: 2.5rem;margin-top: 2.5rem;">
      <div class="form-group row">
          <label class="col-md-3 label-control" for="profilePrivateNotes">Private notes</label>
          <div class="col-md-6">
              <textarea id="profilePrivateNotes" class="form-control checkThis" rows="6" name="data[privateNotes]">{$profile->privateNotes}</textarea>
          </div>
        </div>
      </div>
    </section>
    <section class="card">
      <div class="card-block">
        <h4 class="form-section"><i class="icon-clipboard4"></i>Sizes</h4>
        <div class="form-group row">
          <label class="col-xs-12 col-sm-12 col-md-3 label-control">Height</label>
          <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="input-group"> 
              <!--For children only--> 
              {if $profileType == 0}
              <input autocomplete="dontsuggest" value="{$profile->height}" type="number" id="projectinputHeight" placeholder="inches" class="form-control numOnly checkThis" name="data[height]">

              <!--End of for children only--> 
              
              <!--For teens and adults only--> 
              {else}
              <select id="projectinputHeight1" name="data[height1]" class="form-control checkThis mr-5px">
                <option value="" hidden>feet</option>
                
					  {foreach from=$fields["height"][0] item=t key=k}
					  
                <option value="{$k}" {if $k==$height[0] && $height[0]!=""}selected{/if}>{$t}</option>
                
					  {/foreach}
				  
              </select>
              <select id="projectinputHeight2" name="data[height2]" class="form-control checkThis">
                <option value="" hidden>inches</option>
                
					  {foreach from=$fields["height"][1] item=t key=k}
					  
                <option value="{$k}" {if $k==$height[1] && $height[1]!=""}selected{/if}>{$t}</option>
                
					  {/foreach}
				  
              </select>
              {/if} 
              <!--End of for teens and adults only--> 
            </div>
            <div class="field-actions">
              <button title="Visibility for client" type="button" class="btn-vis-toggle" id="heightVisible-btn" onclick="toggleVisibility('height');" {if $visibleFields['height']}data-state="visible"{/if}><i class="ft-eye"></i></button>
            </div>
            <input type="checkbox" style="position: absolute; visibility: hidden;" id="heightVisible" name="visible[height]" {if $visibleFields['height']}checked{/if}/>
          </div>
        </div>
        <!--<div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputWeight">Weight</label>
          <div class="col-md-3">
            <input autocomplete="dontsuggest" value="{$profile->weight}" type="text" id="projectinputWeight" placeholder="pounds" class="form-control checkThis numOnly" name="data[weight]">
          </div>
        </div>--> 
        
        <!--For children only--> 
        {if $profileType == 0}
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputClothes">Clothing size</label>
          <div class="col-md-3">
            <select id="projectinputClothes" name="data[shirtSize]" class="form-control checkThis">
              <option value="" hidden class="placeholder">select one...</option>
              
				{foreach from=$fields["shirtSize"][$profileType] item=t key=k}
				
              <option value="{$k}" {if $k==$profile->shirtSize && $profile->shirtSize!=""}selected{/if}>{$t}</option>
              
				{/foreach}
			  
            </select>
            <div class="field-actions">
              <button title="Visibility for client" type="button" class="btn-vis-toggle" id="shirtSizeVisible-btn" onclick="toggleVisibility('shirtSize');" {if $visibleFields['shirtSize']}data-state="visible"{/if}><i class="ft-eye"></i></button>
            </div>
            <input type="checkbox" style="position: absolute; visibility: hidden;" id="shirtSizeVisible" name="visible[shirtSize]" {if $visibleFields['shirtSize']}checked{/if}/>
          </div>
        </div>
        {/if} 
        <!--End of for children only--> 
        
        <!--For teens and adults only--> 
        {if $profileType != 0} 
        
        <!--For males only-->
        <div class="form-group row maleField"{if $profile->gender == 0} style="display: block;"{/if}>
          <label class="col-xs-12 col-sm-12 col-md-3 label-control">Shirt size</label>
          <div class="col-sm-6 col-md-3">
            <select id="projectinput7" name="data[shirtSize]" class="form-control checkThis">
              <option value="" hidden>select one...</option>
              

		      {foreach from=$fields["shirtSize"][2] item=t key=k}

              
              <option value="{$k}" {if $k==$profile->shirtSize && $profile->shirtSize!=""}selected{/if}>{$t}</option>
              

		      {/foreach}

            
            </select>
            <div class="field-actions">
              <button title="Visibility for client" type="button" class="btn-vis-toggle" id="shirtSizeVisible-btn" onclick="toggleVisibility('shirtSize');" {if $visibleFields['shirtSize']}data-state="visible"{/if}><i class="ft-eye"></i></button>
            </div>
            <input type="checkbox" style="position: absolute; visibility: hidden;" id="shirtSizeVisible" name="visible[shirtSize]" {if $visibleFields['shirtSize']}checked{/if}/>
          </div>
        </div>
		 <!--End of for males only-->
		  
		  
        <div class="form-group row">
          <label class="col-xs-12 col-sm-12 col-md-3 label-control maleField" {if $profile->gender == 0} style="display: block;"{/if}>Pant size</label>
          <label class="col-xs-12 col-sm-12 col-md-3 label-control femaleField tsField" {if $profile->gender == 1} style="display: block;"{/if}>Waist size</label>
          <div class="col-sm-6 col-md-3">
            <div class="input-group">
              <input autocomplete="dontsuggest" value="{$profile->waist}" type="number" placeholder="waist" class="form-control checkThis numOnly mr-5px" name="data[waist]" {if $profile->gender == 1} style="margin-right: 0 !important; border-bottom-right-radius:5px; border-top-right-radius:5px;"{/if}>
              <input autocomplete="dontsuggest" value="{$profile->inseam}" type="number" placeholder="inseam" class="form-control checkThis numOnly maleField" name="data[inseam]" {if $profile->gender == 1} style="display: none;"{/if}>
            </div>
            <div class="field-actions">
              <button title="Visibility for client" type="button" class="btn-vis-toggle" id="{if $profile->gender == 1}waist{else}pant{/if}SizeVisible-btn" onclick="toggleVisibility('{if $profile->gender == 1}waist{else}pant{/if}Size');" {if $profile->gender == 1 && $visibleFields['waistSize'] || $profile->gender != 1 && $visibleFields['pantSize']}data-state="visible"{/if}><i class="ft-eye"></i></button>
            </div>
            <input type="checkbox" style="position: absolute; visibility: hidden;" id="{if $profile->gender == 1}waist{else}pant{/if}SizeVisible" name="visible[{if $profile->gender == 1}waist{else}pant{/if}Size]" {if $profile->gender == 1 && $visibleFields['waistSize'] || $profile->gender != 1 && $visibleFields['pantSize']}checked{/if}/>
          </div>
        </div>
        
         
        
        <!--For females only-->
        <div class="form-group row femaleField"{if $profile->gender == 1} style="display: block;"{/if}>
          <label class="col-xs-12 col-sm-12 col-md-3 label-control" for="projectinputDress">Dress size</label>
          <div class="col-sm-6 col-md-3">
            <select id="projectinputDress" name="data[dressSize]" class="form-control checkThis">
              <option value="" hidden>select one...</option>
              

		      {foreach from=$fields["dressSize"] item=t key=k}

              
              <option value="{$k}" {if $k==$profile->dressSize && $profile->dressSize!=""}selected{/if}>{$t}</option>
              

		      {/foreach}

            
            </select>
            <div class="field-actions">
              <button title="Visibility for client" type="button" class="btn-vis-toggle" id="dressSizeVisible-btn" onclick="toggleVisibility('dressSize');" {if $visibleFields['dressSize']}data-state="visible"{/if}><i class="ft-eye"></i></button>
            </div>
            <input type="checkbox" style="position: absolute; visibility: hidden;" id="dressSizeVisible" name="visible[dressSize]" {if $visibleFields['dressSize']}checked{/if}/>
          </div>
        </div>
        <div class="form-group row femaleField"{if $profile->gender == 1} style="display: block;"{/if}>
          <label class="col-xs-12 col-sm-12 col-md-3 label-control" for="projectinputBust">Bust size</label>
          <div class="col-sm-6 col-md-3">
            <input id="projectinputBust" autocomplete="dontsuggest" value="{$profile->bust}" type="number" placeholder="bust" class="form-control checkThis mr-5px" name="data[bust]">
            <div class="field-actions">
              <button title="Visibility for client" type="button" class="btn-vis-toggle" id="bustSizeVisible-btn" onclick="toggleVisibility('bustSize');" {if $visibleFields['bustSize']}data-state="visible"{/if}><i class="ft-eye"></i></button>
            </div>
            <input type="checkbox" style="position: absolute; visibility: hidden;" id="bustSizeVisible" name="visible[bustSize]" {if $visibleFields['bustSize']}checked{/if}/>
          </div>
        </div>
        
        <div class="form-group row femaleField"{if $profile->gender == 1} style="display: block;"{/if}>
          <label class="col-xs-12 col-sm-12 col-md-3 label-control" for="projectinputHip">Hip size</label>
          <div class="col-sm-6 col-md-3">
            <input id="projectinputHip" autocomplete="dontsuggest" value="{$profile->hip}" type="number" placeholder="hip" class="form-control checkThis mr-5px" name="data[hip]">
            <div class="field-actions">
              <button title="Visibility for client" type="button" class="btn-vis-toggle" id="hipSizeVisible-btn" onclick="toggleVisibility('hipSize');" {if $visibleFields['hipSize']}data-state="visible"{/if}><i class="ft-eye"></i></button>
            </div>
            <input type="checkbox" style="position: absolute; visibility: hidden;" id="hipSizeVisible" name="visible[hipSize]" {if $visibleFields['hipSize']}checked{/if}/>
          </div>
        </div>
        
        <!--End of for females only--> 
        
        {/if} 
        <!--End of for teens and adults only-->
        
        <div class="form-group row">
          <label class="col-xs-12 col-sm-12 col-md-3 label-control" for="projectinputShoe">Shoe size</label>
          <div class="col-sm-6 col-md-3">
            <select id="projectinputShoe" name="data[shoeSize]" class="form-control checkThis">
              <option value="" hidden>select one...</option>
              
				{foreach from=$fields["shoeSize"] item=t key=k}
				
              <option value="{$k}" {if $k==$profile->shoeSize && $profile->shoeSize!=""}selected{/if}>{$t}</option>
              
				{/foreach}
			  
            </select>
            <div class="field-actions">
              <button title="Visibility for client" type="button" class="btn-vis-toggle" id="shoeSizeVisible-btn" onclick="toggleVisibility('shoeSize');" {if $visibleFields['shoeSize']}data-state="visible"{/if}><i class="ft-eye"></i></button>
            </div>
            <input type="checkbox" style="position: absolute; visibility: hidden;" id="shoeSizeVisible" name="visible[shoeSize]" {if $visibleFields['shoeSize']}checked{/if}/>
          </div>
        </div>
      </div>
    </section>

    <section class="card">
      <div class="card-block">
        <h4 class="form-section"><i class="icon-clipboard4"></i>Details</h4>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="profileBio">Bio</label>
          <div class="col-md-6">
            <textarea id="profileBio" class="form-control checkThis" rows="5" name="data[bio]">{$profile->bio}</textarea>
            <div class="field-actions">
              <button title="Visibility for client" type="button" class="btn-vis-toggle" id="bioVisible-btn" onclick="toggleVisibility('bio');" {if $visibleFields['bio']}data-state="visible"{/if}><i class="ft-eye"></i></button>
            </div>
            <input type="checkbox" style="position: absolute; visibility: hidden;" id="bioVisible" name="visible[bio]" {if $visibleFields['bio']}checked{/if}/>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputKnown">Known for</label>
          <div class="col-md-6">
            <textarea id="projectinputKnown" class="form-control checkThis" rows="3" name="data[knownFor]">{$profile->knownFor}</textarea>
            <div class="field-actions">
              <button title="Visibility for client" type="button" class="btn-vis-toggle" id="knownForVisible-btn" onclick="toggleVisibility('knownFor');" {if $visibleFields['knownFor']}data-state="visible"{/if}><i class="ft-eye"></i></button>
            </div>
            <input type="checkbox" style="position: absolute; visibility: hidden;" id="knownForVisible" name="visible[knownFor]" {if $visibleFields['knownFor']}checked{/if}/>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputProjects">Upcoming projects</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="{$profile->upcomingProjects}" type="text" id="projectinputProjects" class="form-control checkThis trimit" name="data[upcomingProjects]">
            <div class="field-actions">
              <button title="Visibility for client" type="button" class="btn-vis-toggle" id="upcomingProjectsVisible-btn" onclick="toggleVisibility('upcomingProjects');" {if $visibleFields['upcomingProjects']}data-state="visible"{/if}><i class="ft-eye"></i></button>
            </div>
            <input type="checkbox" style="position: absolute; visibility: hidden;" id="upcomingProjectsVisible" name="visible[upcomingProjects]" {if $visibleFields['upcomingProjects']}checked{/if}/>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputOccupation">Occupation/Hobbies</label>
          <div class="col-md-4">
            <input autocomplete="dontsuggest" value="{$profile->occupation}" type="text" id="projectinputOccupation" class="form-control checkThis trimit" name="data[occupation]">
            <div class="field-actions">
              <button title="Visibility for client" type="button" class="btn-vis-toggle" id="occupationVisible-btn" onclick="toggleVisibility('occupation');" {if $visibleFields['occupation']}data-state="visible"{/if}><i class="ft-eye"></i></button>
            </div>
            <input type="checkbox" style="position: absolute; visibility: hidden;" id="occupationVisible" name="visible[occupation]" {if $visibleFields['occupation']}checked{/if}/>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-xs-12 col-sm-12 col-md-3 label-control" for="projectinputRate">Rate</label>
          <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="input-group">
              <input autocomplete="dontsuggest" value="{$profile->rateAmount}" type="number" id="projectinputRate" placeholder="$" class="form-control numOnly checkThis trimit mr-10px" name="data[rateAmount]">
              <span style="float: left; margin-top: 8px;">per</span>
              <select id="projectinput9" name="data[rateType]" class="form-control checkThis ml-10px">
                <option value="" hidden>select...</option>
                
						{foreach from=$fields["rateType"] item=t key=k}
						
                <option value="{$k}" {if $k==$profile->rateType && $profile->rateType!=""}selected{/if}>{$t}</option>
                
						{/foreach}
					
              </select>
            </div>
            <div class="field-actions">
              <button title="Visibility for client" type="button" class="btn-vis-toggle" id="rateVisible-btn" onclick="toggleVisibility('rate');" {if $visibleFields['rate']}data-state="visible"{/if}><i class="ft-eye"></i></button>
            </div>
            <input type="checkbox" style="position: absolute; visibility: hidden;" id="rateVisible" name="visible[rate]" {if $visibleFields['rate']}checked{/if}/>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputAvailability">Availability</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="{$profile->availability}" type="text" id="projectinputAvailability" class="form-control checkThis trimit" name="data[availability]">
            <div class="field-actions">
              <button title="Visibility for client" type="button" class="btn-vis-toggle" id="availabilityVisible-btn" onclick="toggleVisibility('availability');" {if $visibleFields['availability']}data-state="visible"{/if}><i class="ft-eye"></i></button>
            </div>
            <input type="checkbox" style="position: absolute; visibility: hidden;" id="availabilityVisible" name="visible[availability]" {if $visibleFields['availability']}checked{/if}/>
          </div>
        </div>
        <br>
        <br>
        <div class="form-group row">
          <label class="col-md-3 label-control">Ethnicity</label>
          <div class="col-md-9"> {foreach from=$fields["ethnicity"] item=t key=k}
            <fieldset>
              <label class="custom-control custom-checkbox">
                <input {if $ethnicity[$k]==1}checked{/if} type="checkbox" class="custom-control-input" name="data[ethnicity{$k}]">
                <span class="custom-control-indicator"></span> <span class="custom-control-description">{$t}</span> </label>
            </fieldset>
            {/foreach}
          </div>

          {*<!--
          <div class="col-md-9">
            <div class="field-actions">
              <button title="Visibility for client" type="button" class="btn-vis-toggle" id="ethnicityVisible-btn" onclick="toggleVisibility('ethnicity');" {if $visibleFields['ethnicity']}data-state="visible"{/if}><i class="ft-eye"></i></button>
            </div>
            <input type="checkbox" style="position: absolute; visibility: hidden;" id="ethnicityVisible" name="visible[ethnicity]" {if $visibleFields['ethnicity']}checked{/if}/>
          </div>
          -->*}
        </div>
        <!--<div class="form-group row">
          <label class="col-md-3 label-control" for="projectinput7">Hair color</label>
          <div class="col-md-9"> {foreach from=$fields["hairColor"] item=t key=k}
            <fieldset>
              <label class="custom-control custom-checkbox">
                <input {if $hairColor[$k]==1}checked{/if} type="checkbox" class="custom-control-input" name="data[hairColor{$k}]">
                <span class="custom-control-indicator"></span> <span class="custom-control-description">{$t}</span> </label>
            </fieldset>
            {/foreach} </div>
        </div>--> 
      </div>
    </section>
    <section class="card">
      <div class="card-block">
        <h4 class="form-section"><i class="icon-clipboard4"></i>URLs</h4>
        <div class="form-group row">
          <label class="col-md-3 label-control">Instagram</label>
          <div class="col-md-6">
            <div class="form-group instagram-repeater">
              <div data-repeater-list="repeater-group"> {if isset($instagram) and $instagram|count>0}
                {foreach from=$instagram item=t key=k}
                <div class="input-group mb-1" data-repeater-item>
                  <div class="input-group-prepend"><span class="input-group-text">@</span></div>
                  <input autocomplete="dontsuggest" type="text" placeholder="username" class="input-instagram form-control instafield checkThis trimit" name="data[instagram][{$k}]" value="{$t}">
                  <span class="input-group-append" id="button-addon2">
                  <button  type="button" data-repeater-delete class="mybtnSide mybtn-clear instagram-delete"><i class="ft-x"></i></button>
                  </span> <small class="errormsg w-100 ml-3 red" style="display: none"></small> </div>
                {/foreach}
                {else}
                <div class="input-group mb-1" data-repeater-item>
                  <div class="input-group-prepend"><span class="input-group-text">@</span></div>
                  <input autocomplete="dontsuggest" type="text" placeholder="username" class="input-instagram form-control instafield checkThis trimit" name="data[instagram]">
                  <span class="input-group-append" id="button-addon2">
                  <button  type="button" data-repeater-delete class="mybtnSide mybtn-clear instagrm-delete"><i class="ft-x"></i></button>
                  </span> <small class="errormsg w-100 ml-3 red" style="display: none"></small> </div>
                {/if} </div>
              <button type="button" data-repeater-create class="btn mybtn-transp btn-sm" style="float: left; margin-top: -8px;">Add another link</button>
              <div style="float: right; margin-top: -12px;">
                <div class="field-actions">
                  <button title="Visibility for client" type="button" class="btn-vis-toggle" id="instagramVisible-btn" onclick="toggleVisibility('instagram');" {if $visibleFields['instagram']}data-state="visible"{/if}><i class="ft-eye"></i></button>
                </div>
                <input type="checkbox" style="position: absolute; visibility: hidden;" id="instagramVisible" name="visible[instagram]" {if $visibleFields['instagram']}checked{/if}/>
              </div>
              <input type="hidden" value='{$profile->instagram}' name="data[instagram]" id="instagram">
            </div>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputWebsite">Website</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="{$profile->website}" type="text" id="projectinputWebsite" placeholder="www..." class="form-control urlfield" name="data[website]">
            <div class="field-actions">
              <button title="Visibility for client" type="button" class="btn-vis-toggle" id="websiteVisible-btn" onclick="toggleVisibility('website');" {if $visibleFields['website']}data-state="visible"{/if}><i class="ft-eye"></i></button>
            </div>
            <input type="checkbox" style="position: absolute; visibility: hidden;" id="websiteVisible" name="visible[website]" {if $visibleFields['website']}checked{/if}/>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputBook">Book</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="{$profile->book}" type="text" id="projectinputBook" placeholder="www..." class="form-control urlfield" name="data[book]">
            <div class="field-actions">
              <button title="Visibility for client" type="button" class="btn-vis-toggle" id="bookVisible-btn" onclick="toggleVisibility('book');" {if $visibleFields['book']}data-state="visible"{/if}><i class="ft-eye"></i></button>
            </div>
            <input type="checkbox" style="position: absolute; visibility: hidden;" id="bookVisible" name="visible[book]" {if $visibleFields['book']}checked{/if}/>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control">Press</label>
          <div class="col-md-6">
            <div class="form-group press-repeater">
              <div data-repeater-list="press"> {if isset($press) and $press|count>0}
                {foreach from=$press item=t key=k}
                <div class="input-group mb-1" data-repeater-item>
                  <input autocomplete="dontsuggest" type="text" placeholder="www..." class="input-press form-control urlfield" name="data[press][{$k}]" value="{$t}">
                  <span class="input-group-append" id="button-addon2">
                  <button  type="button" data-repeater-delete class="mybtnSide mybtn-clear press-delete"><i class="ft-x"></i></button>
                  </span> </div>
                {/foreach}
                {else}
                <div class="input-group mb-1" data-repeater-item>
                  <input autocomplete="dontsuggest" type="text" placeholder="www..." class="input-press form-control urlfield" name="data[press]">
                  <span class="input-group-append" id="button-addon2">
                  <button  type="button" data-repeater-delete class="mybtnSide mybtn-clear press-delete"><i class="ft-x"></i></button>
                  </span> </div>
                {/if} </div>
              <button type="button" data-repeater-create class="btn mybtn-transp btn-sm" style="float: left; margin-top: -8px;">Add another link</button>
              <div style="float: right; margin-top: -12px;">
                <div class="field-actions">
                  <button title="Visibility for client" type="button" class="btn-vis-toggle" id="pressVisible-btn" onclick="toggleVisibility('press');" {if $visibleFields['press']}data-state="visible"{/if}><i class="ft-eye"></i></button>
                </div>
                <input type="checkbox" style="position: absolute; visibility: hidden;" id="pressVisible" name="visible[press]" {if $visibleFields['press']}checked{/if}/>
              </div>
              <input type="hidden" value='{$profile->press}' name="data[press]" id="press">
            </div>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control">Music</label>
          <div class="col-md-6">
            <div class="form-group music-repeater">
              <div data-repeater-list="music"> {if isset($music) and $music|count>0}
                {foreach from=$music item=t key=k}
                <div class="input-group mb-1" data-repeater-item>
                  <input autocomplete="dontsuggest" type="text" placeholder="www..." class="input-music form-control urlfield" name="data[music][{$k}]" value="{$t}">
                  <span class="input-group-append" id="button-addon2">
                  <button  type="button" data-repeater-delete class="mybtnSide mybtn-clear music-delete"><i class="ft-x"></i></button>
                  </span> </div>
                {/foreach}
                {else}
                <div class="input-group mb-1" data-repeater-item>
                  <input autocomplete="dontsuggest" type="text" placeholder="www..." class="input-music form-control urlfield" name="data[music]">
                  <span class="input-group-append" id="button-addon2">
                  <button  type="button" data-repeater-delete class="mybtnSide mybtn-clear music-delete"><i class="ft-x"></i></button>
                  </span> </div>
                {/if} </div>
              <button type="button" data-repeater-create class="btn mybtn-transp btn-sm" style="float: left; margin-top: -8px;">Add another link</button>
              <div style="float: right; margin-top: -12px;">
                <div class="field-actions">
                  <button title="Visibility for client" type="button" class="btn-vis-toggle" id="musicVisible-btn" onclick="toggleVisibility('music');" {if $visibleFields['music']}data-state="visible"{/if}><i class="ft-eye"></i></button>
                </div>
                <input type="checkbox" style="position: absolute; visibility: hidden;" id="musicVisible" name="visible[music]" {if $visibleFields['music']}checked{/if}/>
              </div>
              <input type="hidden" value='{$profile->music}' name="data[audio]" id="music">
            </div>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control">Video</label>
          <div class="col-md-6">
            <div class="form-group video-repeater">
              <div data-repeater-list="video"> {if isset($video) and $video|count>0}
                {foreach from=$video item=t key=k}
                <div class="input-group mb-1" data-repeater-item>
                  <input autocomplete="dontsuggest" type="text" placeholder="www..." class="input-video form-control urlfield" name="data[video][{$k}]" value="{$t}">
                  <span class="input-group-append" id="button-addon2">
                  <button  type="button" data-repeater-delete class="mybtnSide mybtn-clear video-delete"><i class="ft-x"></i></button>
                  </span> </div>
                {/foreach}
                {else}
                <div class="input-group mb-1" data-repeater-item>
                  <input autocomplete="dontsuggest" type="text" placeholder="www..." class="input-video form-control urlfield" name="data[video]">
                  <span class="input-group-append" id="button-addon2">
                  <button  type="button" data-repeater-delete class="mybtnSide mybtn-clear video-delete"><i class="ft-x"></i></button>
                  </span> </div>
                {/if} </div>
              <button type="button" data-repeater-create class="btn mybtn-transp btn-sm" style="float: left; margin-top: -8px;">Add another link</button>
              <div style="float: right; margin-top: -12px;">
                <div class="field-actions">
                  <button title="Visibility for client" type="button" class="btn-vis-toggle" id="videoVisible-btn" onclick="toggleVisibility('video');" {if $visibleFields['video']}data-state="visible"{/if}><i class="ft-eye"></i></button>
                </div>
                <input type="checkbox" style="position: absolute; visibility: hidden;" id="videoVisible" name="visible[video]" {if $visibleFields['video']}checked{/if}/>
              </div>
              <input type="hidden" value='{$profile->video}' name="data[video]" id="video">
            </div>
          </div>
        </div>
      </div>
    </section>


  {*<!-- @TODO: Куда переносить js, я пока не знаю -->*}
  <script>
    function toggleCustomURL(index) {
      var input = $('#customUrl' + index + 'name');
      var checkbox = $('#customUrl' + index);

      var oldState = checkbox.attr('checked') ? true : false;
      var newState = (input.val().trim().length > 0);

      if (newState == oldState) {
        return;
      }

      if (newState) {
        checkbox.removeAttr('disabled');
        checkbox.attr('checked', true);
      } else {
        checkbox.removeAttr('checked');
        checkbox.attr('disabled', true);
      }
    }
  </script>

  <section class="card">
    <div class="card-block">
      <div class="collapse-icon accordion-icon-rotate">
        <div id="headingCollapseCustom" class="card-header"> <a data-toggle="collapse" href="#collapseCustom" aria-expanded="true" aria-controls="collapseCustom" class="card-title lead collapsed" style="display: block; color: #000;">Custom URLs</a> </div>
        <div id="collapseCustom" role="tabpanel" aria-labelledby="collapseOptional" class="collapse in" aria-expanded="true">
          <div class="card-content">
            <div class="card-body pt-3">
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Field name</th>
                      <th class="text-center">On/Off for models (auto)</th>
                      <th class="text-center">Visibility for clients</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><input autocomplete="dontsuggest" onkeyup="toggleCustomURL(1);" onpaste="toggleCustomURL(1);" type="text" id="customUrl1name" name="customUrl1name" placeholder="Custom URL name" class="form-control checkThis capThis" style="min-width: 200px"></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl1" name="customUrl1" class="switch_4 basedInVal" disabled>
                        </div></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl1ForClient" name="customUrl1ForClient" class="switch_4">
                        </div></td>
                    </tr>
                    <tr>
                      <td><input autocomplete="dontsuggest" onkeyup="toggleCustomURL(2);" onpaste="toggleCustomURL(2);" type="text" id="customUrl2name" name="customUrl2name" placeholder="Custom URL name" class="form-control checkThis capThis" style="min-width: 200px"></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl2" name="customUrl2" class="switch_4 basedInVal" disabled>
                        </div></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl2ForClient" name="customUrl2ForClient" class="switch_4">
                        </div></td>
                    </tr>
                    <tr>
                      <td><input autocomplete="dontsuggest" onkeyup="toggleCustomURL(3);" onpaste="toggleCustomURL(3);" type="text" id="customUrl3name" name="customUrl3name" placeholder="Custom URL name" class="form-control checkThis capThis" style="min-width: 200px"></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl3" name="customUrl3" class="switch_4 basedInVal" disabled>
                        </div></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl3ForClient" name="customUrl3ForClient" class="switch_4">
                        </div></td>
                    </tr>
                    <tr>
                      <td><input autocomplete="dontsuggest" onkeyup="toggleCustomURL(4);" onpaste="toggleCustomURL(4);" type="text" id="customUrl4name" name="customUrl4name" placeholder="Custom URL name" class="form-control checkThis capThis" style="min-width: 200px"></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl4" name="customUrl4" class="switch_4 basedInVal" disabled>
                        </div></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl4ForClient" name="customUrl4ForClient" class="switch_4">
                        </div></td>
                    </tr>
                    <tr>
                      <td><input autocomplete="dontsuggest" onkeyup="toggleCustomURL(5);" onpaste="toggleCustomURL(5);" type="text" id="customUrl5name" name="customUrl5name" placeholder="Custom URL name" class="form-control checkThis capThis" style="min-width: 200px"></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl5" name="customUrl5" class="switch_4 basedInVal" disabled>
                        </div></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl5ForClient" name="customUrl5ForClient" class="switch_4">
                        </div></td>
                    </tr>
                    <tr>
                      <td><input autocomplete="dontsuggest" onkeyup="toggleCustomURL(6);" onpaste="toggleCustomURL(6);" type="text" id="customUrl6name" name="customUrl6name" placeholder="Custom URL name" class="form-control checkThis capThis" style="min-width: 200px"></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl6" name="customUrl6" class="switch_4 basedInVal" disabled>
                        </div></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl6ForClient" name="customUrl5ForClient" class="switch_4">
                        </div></td>
                    </tr>
                    <tr>
                      <td><input autocomplete="dontsuggest" onkeyup="toggleCustomURL(7);" onpaste="toggleCustomURL(7);" type="text" id="customUrl7name" name="customUrl7name" placeholder="Custom URL name" class="form-control checkThis capThis" style="min-width: 200px"></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl7" name="customUrl7" class="switch_4 basedInVal" disabled>
                        </div></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl7ForClient" name="customUrl7ForClient" class="switch_4">
                        </div></td>
                    </tr>
                    <tr>
                      <td><input autocomplete="dontsuggest" onkeyup="toggleCustomURL(8);" onpaste="toggleCustomURL(8);" type="text" id="customUrl8name" name="customUrl8name" placeholder="Custom URL name" class="form-control checkThis capThis" style="min-width: 200px"></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl8" name="customUrl8" class="switch_4 basedInVal" disabled>
                        </div></td>
                      <td><div class="input_wrapper reverse mx-auto">
                          <input type="checkbox" id="customUrl8ForClient" name="customUrl8ForClient" class="switch_4">
                        </div></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

    <section class="card">
      <div class="card-block">
        <h4 class="form-section"><i class="icon-clipboard4"></i>Contact</h4>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputAddress1">Address</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="{$profile->address}" type="text" id="projectinputAddress1" placeholder="house number, street" class="form-control checkThis trimit" name="data[address]">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputAddress2">Address</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="{$profile->address2}" type="text" id="projectinputAddress2" placeholder="apartment" class="form-control checkThis trimit" name="data[address2]">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputCity">City</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="{$profile->city}" type="text" id="projectinputCity" placeholder="city" class="form-control capThis checkThis trimit" name="data[city]">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputState">State</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="{$profile->state}" type="text" id="projectinputState" placeholder="NY, CA, etc." class="form-control statefield checkThis checkThis trimit" name="data[state]">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputZip">Zip code</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="{$profile->ZIPcode}" type="tel" id="projectinputZip" placeholder="12345" class="form-control zipfield checkThis checkThis trimit" name="data[ZIPcode]">
          </div>
        </div>
        <br>
        <br>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputCellPhone">Cell phone</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="{$profile->cellPhone}" type="tel" id="projectinputCellPhone" class="form-control phonefield numOnly checkThis trimit" name="data[cellPhone]">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputPersEmail">E-mail</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="{$profile->email}" type="text" id="projectinputPersEmail" class="form-control emailfield checkThis trimit" name="data[email]">
          </div>
        </div>
      </div>
    </section>
    <section class="card">
      <div class="card-block">
        <h4 class="form-section"><i class="icon-clipboard4"></i>Agency</h4>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputAgentName">Name</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="{$profile->agentName}" type="text" id="projectinputAgentName" class="form-control capThis checkThis trimit" name="data[agentName]">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputAgentPhone">Phone</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="{$profile->agentPhone}" type="tel" id="projectinputAgentPhone" class="form-control phonefield numOnly checkThis trimit" name="data[agentPhone]">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputAgentEmail">E-mail</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="{$profile->agentEmail}" type="text" id="projectinputAgentEmail" class="form-control emailfield checkThis trimit" name="data[agentEmail]">
          </div>
        </div>
        {if $profileType == 0} </div>
    </section>
    <section class="card">
      <div class="card-block">
        <h4 class="form-section"><i class="icon-clipboard4"></i>Parent/Guardian</h4>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputParent">Name</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="{$profile->parentName}" type="text" id="projectinputParent" class="form-control checkThis capThis checkThis trimit" name="data[parentName]">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputParentPhone">Phone</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="{$profile->parentPhone}" type="tel" id="projectinputParentPhone" class="form-control phonefield numOnly checkThis trimit" name="data[parentPhone]">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputParentEmail">E-mail</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="{$profile->parentEmail}" type="text" id="projectinputParentEmail" class="form-control emailfield checkThis trimit" name="data[parentEmail]">
          </div>
        </div>
        {/if} </div>
    </section>
    <section class="card">
      <div class="card-block">
        <h4 class="form-section"><i class="icon-clipboard4"></i>Photo</h4>
        <div class="form-group row">
          <label class="col-md-3 label-control">Profile photo</label>
          <div class="col-md-6"> {if $profile->photo} <img src="{$profile->photo|replace:'/home/aibdh80ehx97/public_html/sncasting.com':''}" height="150" style="margin-top: 10px;"> {else}
            <input type="file" accept="image/jpeg" id="projectinput832" class="form-control" name="data[photo]" style="height: 150px;">
            {/if} </div>
        </div>
        <div class="form-actions">
          <button type="submit" class="btn btn-primary mybtn-green" style="float: right;"> <i class="icon-check2"></i> Save</button>
        </div>
        <input type="hidden" name="action" value="save">
      </div>
    </section>
  </div>
</form>
{if $profile->id}
<div class="card">
  <div class="card-header">
    <h4 class="card-title">Edit gallery</h4>
  </div>
  <div class="card-block"> {if $photos}{else}
    <div class="text-muted">No photos here yet...</div>
    {/if}
    {foreach from=$photos item=p}
    <div class="div-thumbnail"> <img src="{$p|replace:'/home/aibdh80ehx97/public_html/sncasting.com':''}" alt="" class="img-fluid" />
      <button class="deleteThumb" onclick="deletePhoto('{$p}', this)"><i class="icon-close"></i></button>
    </div>
    {/foreach} </div>
</div>
<div class="card">
  <div class="card-header">
    <h4 class="card-title">Upload more photos</h4>
  </div>
  <div class="card-block">
    <div class="bs-callout-warning callout-border-left mb-1 p-1">
      <p>Please make sure to use <strong>JPGs</strong> with <strong>sRGB</strong> color space.</p>
    </div>
    <form action="/profile/upload/?id={$profile->id}" class="dropzone dpz-multiple-files" id="dpz-multiple-files">
    </form>
  </div>
</div>
{/if} 
<script>
function bdChange(){
	var bd = $("#bdYear").val() + "-" + $("#bdMonth").val() + "-" + $("#bdDay").val();
	$("#dbFinal").val(bd);
};
</script>
<script>
  var states = [{foreach from=$basedIn item=t}"{$t}",{/foreach}];
</script>

{include file="footer.tpl"}