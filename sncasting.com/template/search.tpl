{include file="header.tpl"}
{include file="modals/deleteProfile.tpl"}
{include file="modals/newGroup.tpl"}
{include file="modals/deleteMultipleProfiles.tpl"}
<style>
    	.grid-hover{
	    	padding: 0;
    	}
    	.grid-hover .col-md-4{
	    	padding: 0;
    	}
    	.effect-roxy{
	    	margin: 0 !important;
    	}
    	figure.effect-roxy h2{
	    	padding: 22% 0 10px 0 !important;
    	}
		{foreach from=$fields["profileType"] item=t key=k}
			{if $allget["profileType"|cat:$k]}
		.pt{$k}{
				display: block !important;
		}
			{/if}
		{/foreach}
    </style>
<form class="form form-horizontal" method="get" action="#" enctype="multipart/form-data">
  <input type="hidden" value="search" name="action">
  <section class="card">
    <div class="card-header">
      <h4 class="card-title">Search Parameters</h4>
      <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
      <div class="heading-elements">
        <ul class="list-inline mb-0">
          {if $action==0}
          <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
        </ul>
      </div>
    </div>
    <div class="card-body collapse in" aria-expanded="true" style=""> {else}
      <li><a data-action="collapse"><i class="icon-plus4"></i></a></li>
      </ul>
    </div>
    </div>
    <div class="card-body collapse out" aria-expanded="true" style=""> {/if}
      <div class="card-block">
        <div class="form-body"> <BR>
          <div class="form-group row">
            <label class="col-md-3 label-control" for="projectinput1">Profile Type</label>
            <div class="col-md-9">
              <div class="btn-group" role="group" aria-label="Basic example"> {foreach from=$fields["profileType"] item=t key=k} <a type="button" href="/search?profileType={$k}" class="mybtn2 mybtn-{if $k==$profileType}green{else}grey{/if}">{$t}</a> {/foreach}
                <input type="hidden" value="{$profileType}" name="data[profileType]">
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 label-control" for="projectinput1">First name</label>
            <div class="col-md-6">
              <input autocomplete="dontsuggest" type="text" name="data[firstName]" class="form-control" value="{$allget["firstName"]}">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 label-control" for="projectinput1">Last name</label>
            <div class="col-md-6">
              <input autocomplete="dontsuggest" type="text" name="data[secondName]" class="form-control" value="{$allget["secondName"]}">
            </div>
          </div>
          <div class="form-group row typeahead">
            <label class="col-md-3 label-control" for="projectinput1">Based In</label>
            <div class="col-md-6">
              <input autocomplete="" type="text" name="data[basedIn]" class="form-control typeahead-basic typeahead" value="{$allget["basedIn"]}" id="basedInTypeahead">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 label-control" for="projectinput1">Gender</label>
            <div class="col-md-6"> {foreach from=$fields["gender"] item=t key=k}
              <fieldset>
                <label class="custom-control custom-checkbox">
                  <input type="checkbox" {if $allget["gender"|cat:$k]}checked{/if} class="custom-control-input" name="data[gender{$k}]">
                  <span class="custom-control-indicator"></span> <span class="custom-control-description">{$t}</span> </label>
              </fieldset>
              {/foreach} </div>
          </div>
          <br>
          <div class="form-group row">
            <label class="col-md-3 label-control label-push" for="projectinput1">Age</label>
            <div class="col-md-6">
              <div id="slider-age" class="ml-3 mr-2 slider-info" data-min="{$ageMin}" data-max="{$ageMax}"></div>
              <input type="hidden" id="slider-age-val" name="data[age]" value="{if $ageMin}{$ageMin},{$ageMax}{/if}">
            </div>
          </div>
          <br>
          <div class="form-group row">
              <label class="col-md-3 label-control" for="projectinput7">Keywords</label>
              <div class="col-md-6">
                <fieldset>
                  <div class="form-group">
                    <div class="form-control" id="keywords" data-tags-input-name="edit-on-delete" style="min-height: 80px">{$keywords}</div>
                    <small class="text-muted">Press Enter, Comma or Spacebar to add a new tag, Backspace or Delete to remove.</small> </div>
                </fieldset>
                <input type="hidden" name="data[keywords]" value='{$allget["keywords"]}'>
              </div>
          </div>
          <!--<div class="form-group row">
            <label class="col-md-3 label-control" for="projectinput1">Hair color</label>
            <div class="col-md-9"> {foreach from=$fields["hairColor"] item=t key=k}
              <fieldset>
                <label class="custom-control custom-checkbox">
                  <input type="checkbox" {if $allget["hairColor"|cat:$k]}checked{/if} class="custom-control-input" name="data[hairColor{$k}]">
                  <span class="custom-control-indicator"></span> <span class="custom-control-description">{$t}</span> </label>
              </fieldset>
              {/foreach} </div>
          </div>-->
          <div class="form-group row">
            <label class="col-md-3 label-control" for="projectinput1">Ethnicity</label>
            <div class="col-md-6"> {foreach from=$fields["ethnicity"] item=t key=k}
              <fieldset>
                <label class="custom-control custom-checkbox">
                  <input type="checkbox" {if $allget["ethnicity"|cat:$k]}checked{/if} class="custom-control-input" name="data[ethnicity{$k}]">
                  <span class="custom-control-indicator"></span> <span class="custom-control-description">{$t}</span> </label>
              </fieldset>
              {/foreach} </div>
          </div>
          <br>
          <div class="form-group row">
            <label class="col-md-3 label-control label-push" for="projectinput1">Weight</label>
            <div class="col-md-6">
              <div id="slider-weight" class="ml-3 mr-2 slider-info" data-min="{$weightMin}" data-max="{$weightMax}"></div>
              <input type="hidden" id="slider-weight-val" name="data[weight]" value="{if $weightMin}{$weightMin},{$weightMax}{/if}">
            </div>
          </div>
          <br>
          <br>
          <div class="form-group row">
            <label class="col-md-3 label-control label-push" for="projectinput1">Height</label>
            <div class="col-md-6">
              <div id="slider-height" class="ml-3 mr-2 slider-info" data-min="{$heightMin}" data-max="{$heightMax}"></div>
              <input type="hidden" id="slider-height-val" name="data[height]" value="{if $heightMin}{$heightMin},{$heightMax}{/if}">
            </div>
          </div>
          <br>
          <div class="form-group row ">
            <label class="col-md-3 label-control" for="projectinput1">Shirt size (adults)</label>
            <div class="col-md-6"> {foreach from=$fields["shirtSize"][2] item=t key=k}
              <fieldset>
                <label class="custom-control custom-checkbox">
                  <input type="checkbox" {if $allget["shirtSize2-"|cat:$k]}checked{/if} class="custom-control-input" name="data[shirtSize2-{$k}]">
                  <span class="custom-control-indicator"></span> <span class="custom-control-description">{$t}</span> </label>
              </fieldset>
              {/foreach} </div>
          </div>
          {if $profileType==1}
          <div class="form-group row ">
            <label class="col-md-3 label-control" for="projectinput1">Shirt size (childs)</label>
            <div class="col-md-6"> {foreach from=$fields["shirtSize"][1] item=t key=k}
              <fieldset>
                <label class="custom-control custom-checkbox">
                  <input type="checkbox" {if $allget["shirtSize1-"|cat:$k]}checked{/if} class="custom-control-input" name="data[shirtSize1-{$k}]">
                  <span class="custom-control-indicator"></span> <span class="custom-control-description">{$t}</span> </label>
              </fieldset>
              {/foreach} </div>
          </div>
          {/if}
          
          {if $profileType==0}
          <div class="form-group row ">
            <label class="col-md-3 label-control" for="projectinput1">Shirt size (babies)</label>
            <div class="col-md-6"> {foreach from=$fields["shirtSize"][0] item=t key=k}
              <fieldset>
                <label class="custom-control custom-checkbox">
                  <input type="checkbox" {if $allget["shirtSize0-"|cat:$k]}checked{/if} class="custom-control-input" name="data[shirtSize0-{$k}]">
                  <span class="custom-control-indicator"></span> <span class="custom-control-description">{$t}</span> </label>
              </fieldset>
              {/foreach} </div>
          </div>
          {/if}
          
          
          {if $profileType==2} <br>
          <div class="form-group row ">
            <label class="col-md-3 label-control label-push" for="projectinput1">Pant size - waist (adults)</label>
            <div class="col-md-6">
              <div id="slider-waist" class="ml-3 mr-2 slider-info" data-min="{$waistMin}" data-max="{$waistMax}"></div>
              <input type="hidden" id="slider-waist-val" name="data[waist]" value="{if $waistMin}{$waistMin},{$waistMax}{/if}">
            </div>
          </div>
          <div class="form-group row "> <br>
            <br>
            <label class="col-md-3 label-control label-push" for="projectinput1">Pant size - inseam (adults)</label>
            <div class="col-md-6">
              <div id="slider-inseam" class="ml-3 mr-2 slider-info" data-min="{$inseamMin}" data-max="{$inseamMax}"></div>
              <input type="hidden" id="slider-inseam-val" name="data[inseam]" value="{if $inseamMin}{$inseamMin},{$inseamMax}{/if}">
            </div>
          </div>
          {/if}
          <div class="form-group row"> <br>
            <br>
            <label class="col-md-3 label-control label-push" for="projectinput1">Shoe size</label>
            <div class="col-md-6">
              <div id="slider-shoes" class="ml-3 mr-2 slider-info" data-min="{$shoeSizeMin}" data-max="{$shoeSizeMax}"></div>
              <input type="hidden" id="slider-shoeSize-val" name="data[shoeSize]" value="{if $shoeSizeMin}{$shoeSizeMin},{$shoeSizeMax}{/if}">
            </div>
          </div>
     
          {if $profileType == 0}
          <div class="form-group row">
            <label class="col-md-3 label-control" for="projectinput1">Baby abilities</label>
            <div class="col-md-6"> {foreach from=$fields["babyAbilities"] item=t key=k}
              <fieldset>
                <label class="custom-control custom-checkbox">
                  <input type="checkbox" {if $allget["babyAbilities"|cat:$k]}checked{/if} class="custom-control-input" name="data[babyAbilities{$k}]">
                  <span class="custom-control-indicator"></span> <span class="custom-control-description">{$t}</span> </label>
              </fieldset>
              {/foreach} </div>
          </div>
          {/if} </div>
        <div class="form-actions">
          <div class="col-md-12">
            <button type="submit" class="mybtn2 mybtn-green" style="float: right;"> <i class="icon-search"></i> Search </button>
            <br>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
  </section>
</form>
{if $noResults} <br>
<center>
  <h2>Nothing found</h2>
</center>
<br>
<br>
{else} 
<div class="row"> {foreach from=$profiles item=t key=k}
    {assign var="size" value=getimagesize($t->thumbnail)}
    <div class="col-md-4 mb-2">
      <div class="thumbnail-container" data-id="{$t->id}">
        <div class="thumb-photo-place"><img class="thumbnail-photo" src="{$t->thumbnail|replace:'/home/aibdh80ehx97/public_html/sncasting.com':''}" alt="Thumbnail" {if $size[0]>$size[1]}style="height: 380px; width: auto;"{/if}onerror="this.src='/template/app-assets/images/noimage.png'"/>
          <div class="thumb-overlay"></div>
          <div class="thumb-content">
            <div class="editPanel desktop-only">
              <button onclick="location='/profile/edit?id={$t->id}'" class="editButton" title="Edit profile"><i class="ft-edit-2"></i></button>
              <button class="deleteProfileButton" data-toggle="modal" data-target="#deleteProfile" onclick="profileToDelete={$t->id}" title="Delete profile"><i class="icon-trash4"></i></button>
            </div>
            <a class="thumb-info" href="/profile/view?id={$t->id}">
            <p> {if $t->basedIn}<i class="ft-map-pin"></i><b>{$t->basedIn}</b><br>
              <br>
              {/if}
              {if $t->occupation}<i class="ft-award"></i><b>{$t->occupation}</b><br>
              <br>
              {/if}
              {if $t->height}<i class="ft-arrow-up"></i><b>{$t->printheight}</b>{/if} </p>
            </a>
            <button class="selectButton desktop-only" onclick="selectProfile(this)"  title="Select profile">Select</button>
          </div>
        </div>
        <a class="name-place" href="/profile/view?id={$t->id}" target="_blank">{$t->firstName}&nbsp;{$t->lastName}</a>
        <div class="thumbnail-toolbar mobile-only">
          <div class="thumb-tools"></div>
          <div class="thumb-mob-actions">
            <div class="mob-actions-inner">
              <button class="btn-mob-edit" onclick="location='/profile/edit?id={$t->id}'"><i class="ft-edit-2" title="Edit profile"></i></button>
              <button class="btn-mob-delete" data-toggle="modal" data-target="#deleteProfile" onclick="profileToDelete={$t->id}" title="Delete profile"><i class="icon-trash4"></i></button>
              <button class="btn-mob-select" onclick="mobSelectProfile(this)"><i class="ft-plus-circle mob-select-icon" title="Select profile"></i></button>
            </div>
          </div>
          <button class="btn-toggle-mob-actions"><i class="ft-more-horizontal"></i></button>
        </div>
      </div>
    </div>
    {/foreach} </div>
{/if} 
<!--/ Hover Effects --> 

<script>
  var states = [{foreach from=$basedIn item=t}"{$t}",{/foreach}];
</script>

{include file="footer.tpl"}