<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta name="description" content="Robust admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
<meta name="keywords" content="admin template, robust admin template, dashboard template, flat admin template, responsive admin template, web app">
<meta name="author" content="PIXINVENT">
<title>Shay Nielsen Castings</title>
<link rel="apple-touch-icon" sizes="60x60" href="/template/app-assets/images/ico/apple-icon-60.png">
<link rel="apple-touch-icon" sizes="76x76" href="/template/app-assets/images/ico/apple-icon-76.png">
<link rel="apple-touch-icon" sizes="120x120" href="/template/app-assets/images/ico/apple-icon-120.png">
<link rel="apple-touch-icon" sizes="152x152" href="/template/app-assets/images/ico/apple-icon-152.png">
<link rel="shortcut icon" type="image/x-icon" href="/template/app-assets/images/ico/favicon.ico">
<link rel="shortcut icon" type="image/png" href="/template/app-assets/images/ico/favicon-32.png">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/bootstrap.css">
<!-- font icons-->
<link rel="stylesheet" type="text/css" href="/template/app-assets/fonts/icomoon.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/sliders/slick/slick.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/extensions/pace.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/ui/prism.min.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/js/gallery/photo-swipe/photoswipe.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/js/gallery/photo-swipe/default-skin/default-skin.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/pickers/daterange/daterangepicker.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/pickers/datetime/bootstrap-datetimepicker.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/pickers/pickadate/pickadate.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/file-uploaders/dropzone.min.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/extensions/nouislider.min.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/forms/toggle/bootstrap-switch.min.css">
<!-- END VENDOR CSS-->
<!-- BEGIN ROBUST CSS-->
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/bootstrap-extended.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/app.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/colors.css">
<!-- END ROBUST CSS-->
<!-- BEGIN Page Level CSS-->
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/core/menu/menu-types/horizontal-menu.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/core/menu/menu-types/vertical-overlay-menu.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/pages/gallery.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/plugins/pickers/daterange/daterange.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/plugins/file-uploaders/dropzone.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/pages/users.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/pages/timeline.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css-rtl/plugins/extensions/noui-slider.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css-rtl/core/colors/palette-noui.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css-rtl/plugins/forms/switch.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css-rtl/core/colors/palette-switch.css">
<!-- END Page Level CSS-->
<!-- BEGIN Custom CSS-->
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/style.css">
<!-- END Custom CSS-->
</head>
<body data-open="hover" data-menu="horizontal-menu" data-col="2-columns" class="horizontal-layout horizontal-menu 2-columns ">

<!-- ////////////////////////////////////////////////////////////////////////////-->

<div class="container-fluid h-100">
  <div class="row h-100 m-0" style="display:table;width:100%;text-align:center;">
    <div style="display:table-cell;vertical-align:middle;margin: 0 auto;">
      <div class="card" style="max-width: 500px;margin: 0 auto;">
      	{if $profile->id}
        <div class="card-block" style="padding: 50px;"><span style="font-size: 26px">Success!</span> <br>
          <br>
          Your profile number: <br>
          <br>
          <span class="profile-id" style="float: none; font-size: 35px;">{$profile->id|string_format:"%05d"}</span> <br>
          <br>
			<hr>
          <br>
          <a class="btn btn-primary mybtn-green" href="/c/{$url}">Fill out new casting sheet</a><br></div>
          {else}
        <div class="card-block" style="padding: 50px;"><span style="font-size: 26px">Ooops!</span> <br>
          <br>
          Something went wrong. Please try again!<br>
			<hr>
          <br>
          <a class="btn btn-primary mybtn-green" href="/c/{$url}">Try again</a><br></div>
          
          {/if}
      </div>
    </div>
  </div>
</div>
{include file="footer.tpl"} 
