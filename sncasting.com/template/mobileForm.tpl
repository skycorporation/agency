<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="">
<title>Shay Nielsen Casting</title>
<meta name="robots" content="noindex,nofollow">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/bootstrap.css">
<!-- font icons-->
<link rel="stylesheet" type="text/css" href="/template/app-assets/fonts/icomoon.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/fonts/feather/style.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/sliders/slick/slick.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/extensions/pace.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/ui/prism.min.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/js/gallery/photo-swipe/photoswipe.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/js/gallery/photo-swipe/default-skin/default-skin.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/pickers/daterange/daterangepicker.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/pickers/datetime/bootstrap-datetimepicker.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/pickers/pickadate/pickadate.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/file-uploaders/dropzone.min.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/extensions/nouislider.min.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/forms/toggle/bootstrap-switch.min.css">
<!-- END VENDOR CSS-->
<!-- BEGIN ROBUST CSS-->
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/bootstrap-extended.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/app.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/colors.css">
<!-- END ROBUST CSS-->
<!-- BEGIN Page Level CSS-->
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/core/menu/menu-types/horizontal-menu.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/core/menu/menu-types/vertical-overlay-menu.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/pages/gallery.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/plugins/pickers/daterange/daterange.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/plugins/file-uploaders/dropzone.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/pages/users.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/pages/timeline.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css-rtl/plugins/extensions/noui-slider.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css-rtl/core/colors/palette-noui.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css-rtl/plugins/forms/switch.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css-rtl/core/colors/palette-switch.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/forms/tags/tagging.css">
    <link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/forms/icheck/icheck.css">
    <link rel="stylesheet" type="text/css" href="/template/app-assets/vendors/css/forms/icheck/custom.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/components.min.css">
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/plugins/forms/extended/form-extended.min.css">
    
    
    <link rel="stylesheet" type="text/css" href="/template/app-assets/css/plugins/forms/checkboxes-radios.min.css">
<!-- END Page Level CSS-->
<!-- BEGIN Custom CSS-->
<link rel="stylesheet" type="text/css" href="/template/app-assets/css/style.css">
<style>
.maleField {
    display: none !important;
}
.maleField.active {
    display: block !important;
}
.femaleField {
    display: none !important;
}
.femaleField.active {
    display: block !important;
}
</style>
<!-- END Custom CSS-->
</head>
<body data-open="hover" data-menu="horizontal-menu" data-col="2-columns" class="horizontal-layout horizontal-menu 2-columns">

<!-- ////////////////////////////////////////////////////////////////////////////-->

<div class="app-content container center-layout mt-2">
<div class="content-wrapper">

	<div id="formAttention"><div><i class="icon-android-alert" style="position: relative; top: 2px;"></i> Something is off... Please check all fields with red border.</div></div>
	
<div class="w-100 text-center mt-3 mb-3">
  <h1>Casting sheet for:</h1>
  <h3 class="success">{$castingList->name}</h3>
</div>
<form class="form form-horizontal" id="castingSheetForm" method="post" action="#" enctype="multipart/form-data">
  <div class="form-body">
  <section class="card">
    <div class="card-block">
      <input type="hidden" value="{$profileType}" name="data[profileType]">
      <h4 class="form-section"><i class="icon-head"></i>General Info</h4>
      <div class="form-group row">
        <label class="col-md-3 label-control" for="projectinputFirst">First Name</label>
        <div class="col-md-6">
          <input autocomplete="given-name" value="{$profile->firstName}" type="text" id="projectinputFirst" class="form-control checkThis capThis trimit" name="data[firstName]" required>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-md-3 label-control" for="projectinputLast">Last Name</label>
        <div class="col-md-6">
          <input autocomplete="family-name" value="{$profile->lastName}" type="text" id="projectinputLast" class="form-control checkThis capThis trimit" name="data[lastName]" required>
        </div>
      </div>
      <div class="form-group row skin skin-flat">
        <label class="col-md-3 col-sm-12 col-xs-12 label-control">Gender</label>
        <div class="col-md-9 float-xs-left">
          	{foreach from=$fields["gender"] item=t key=k}
	          	<fieldset>
	              <input type="radio" name="data[gender]" {if $k==$profile->gender}checked{/if} id="gender-{$k}" value="{$k}">
	              <label for="gender-{$k}">{$t}</label>
	            </fieldset>
          	{/foreach}
        </div>
      </div>
      <div class="form-group row">
        <label class="col-xs-12 col-sm-12 col-md-3 label-control">Birthday</label>
        <div class="col-sm-6 col-md-4 col-lg-3 col-xl-3">
          <div class="input-group">
            <select class="form-control checkThis  mr-5px" id="bdMonth" onchange="bdChange()" required>
              <option value="" disabled selected>month</option>
              <option value="01">Jan</option>
              <option value="02">Feb</option>
              <option value="03">Mar</option>
              <option value="04">Apr</option>
              <option value="05">May</option>
              <option value="06">Jun</option>
              <option value="07">Jul</option>
              <option value="08">Aug</option>
              <option value="09">Sep</option>
              <option value="10">Oct</option>
              <option value="11">Nov</option>
              <option value="12">Dec</option>
            </select>
            <select class="form-control checkThis  mr-5px" id="bdDay" onchange="bdChange()" required>
              <option value="" disabled selected>day</option>
              
              

		                    {for $k=1 to 31}

                
              
              <option value="{$k}">{$k}</option>
              
              

		                    {/for}

            
            </select>
            <select class="form-control checkThis" id="bdYear" onchange="bdChange()" required>
              <option value="" disabled selected>year</option>
              

		                    {if $profileType==2}{assign var="d" value=70}{/if}
		                    {if $profileType==1}{assign var="d" value=18}{/if}
		                    {if $profileType==0}{assign var="d" value=3}{/if}
		                    {for $k='Y'|date-$d to 'Y'|date}

              
              <option value="{$k}">{$k}</option>
              
		                    {/for}
            
            </select>
          </div>
          <input value="{$profile->birthday}" type='hidden' name="data[birthday]" id="dbFinal"/>
        </div>
      </div>
      {if in_array('basedIn',$selectedFields)}
          <div class="form-group row typeahead">
			  <label class="col-xs-12 col-sm-12 col-md-3 label-control" for="projectinputBased">Based in</label>
            <div class="col-sm-6 col-md-4 col-lg-3 col-xl-3">
              <input autocomplete="" type="text" name="data[basedin]" class="form-control typeahead-basic typeahead checkThis" value="{$profile->basedin}" id="basedInTypeahead" placeholder="city" id="projectinputBased">
            </div>
          </div>
          
      {/if} </div>
  </section>
  <section class="card">
    <div class="card-block">
      <h4 class="form-section"><i class="icon-clipboard4"></i>Sizes</h4>
      <div class="form-group row">
        <label class="col-xs-12 col-sm-12 col-md-3 label-control">Height</label>
        <div class=" col-xs-12 col-sm-6 col-md-3">
          <div class="input-group"> 
            <!--For children only--> 
            {if $profileType == 0}
            <input autocomplete="dontsuggest" value="{$profile->height}" type="text" id="projectinputHeight" class="form-control checkThis numOnly" name="data[height]" placeholder="inches" required>
            <!--End of for children only--> 
            
            <!--For teens and adults only--> 
            {else}
            <select id="projectinputHeight1" name="data[height1]" class="form-control checkThis mr-5px" required>
              <option value="" hidden>feet</option>
              
              
  
		                    {foreach from=$fields["height"][0] item=t key=k}

                
              
              <option value="{$k}" {if $k==$height[0] && $height[0]!=""}selected{/if}>{$t}</option>
              
              

		                    {/foreach}

              
            
            </select>
            <select id="projectinputHeight2" name="data[height2]" class="form-control checkThis" required>
              <option value="" hidden>inches</option>
              
              
                

		                    {foreach from=$fields["height"][1] item=t key=k}
	
                
              
              <option value="{$k}" {if $k==$height[1] && $height[1]!=""}selected{/if}>{$t}</option>
              
              

		                    {/foreach}
		                    

              
            
            </select>
            {/if} 
            <!--End of for teens and adults only--> 
          </div>
        </div>
      </div>
      <!--<div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputWeight">Weight</label>
          <div class="col-md-3">
            <input autocomplete="dontsuggest" value="{$profile->weight}" type="text" id="projectinputWeight" placeholder="pounds" class="form-control checkThis numOnly" name="data[weight]" required>
          </div>
        </div>--> 
      
      <!--For children only--> 
      {if $profileType == 0}
      <div class="form-group row">
        <label class="col-md-3 label-control" for="projectinputClothes">Clothing size</label>
        <div class="col-md-3">
          <select id="projectinputClothes" name="data[shirtSize]" class="form-control checkThis">
            <option value="" hidden>select one...</option>
            
            

		                    {foreach from=$fields["shirtSize"][$profileType] item=t key=k}

            
            <option value="{$k}" {if $k==$profile->shirtSize && $profile->shirtSize!=""}selected{/if}>{$t}</option>
            

		                    {/foreach}

          
          </select>
        </div>
      </div>
      {/if} 
      <!--End of for children only--> 
      
      <!--For teens and adults only--> 
      {if $profileType != 0} 
      
      <!--For males only-->
      <div class="form-group row maleField active">
        <label class="col-xs-12 col-sm-12 col-md-3 label-control">Shirt size</label>
        <div class="col-sm-6 col-md-3">
          <select id="projectinput7" name="data[shirtSize]" class="form-control checkThis">
            <option value="" hidden>select one...</option>
            
			  {foreach from=$fields["shirtSize"][2] item=t key=k}
			  
            <option value="{$k}" {if $k===$profile->shirtSize && $profile->shirtSize!=""}selected{/if}>{$t}</option>
            
              {/foreach}
          
          </select>
        </div>
      </div>
	  <!--End of for males only-->
      
		
		<div class="form-group row">
          <label class="col-xs-12 col-sm-12 col-md-3 label-control maleField active">Pant size</label>
          <label class="col-xs-12 col-sm-12 col-md-3 label-control femaleField">Waist size</label>
          <div class="col-sm-6 col-md-3">
            <div class="input-group">
              <input autocomplete="dontsuggest" value="{$profile->waist}" type="tel" placeholder="waist" class="form-control checkThis numOnly mr-5px" name="data[waist]">
              <input autocomplete="dontsuggest" value="{$profile->inseam}" type="tel" placeholder="inseam" class="form-control checkThis numOnly maleField active" name="data[inseam]">
            </div>
          </div>
        </div>
       
      
      <!--For females only-->
      <div class="form-group row femaleField">
        <label class="col-xs-12 col-sm-12 col-md-3 label-control" for="projectinputBust">Bust size</label>
        <div class="col-sm-6 col-md-3">
          <input id="projectinputBust" autocomplete="dontsuggest" value="{$profile->bust}" type="number" placeholder="bust" class="form-control checkThis mr-5px" name="data[bust]">
        </div>
      </div>
      <div class="form-group row femaleField">
        <label class="col-xs-12 col-sm-12 col-md-3 label-control" for="projectinputHip">Hip size</label>
        <div class="col-sm-6 col-md-3">
          <input id="projectinputHip" autocomplete="dontsuggest" value="{$profile->hip}" type="number" placeholder="hip" class="form-control checkThis  mr-5px" name="data[hip]">
        </div>
      </div>
      
      <div class="form-group row femaleField">
        <label class="col-xs-12 col-sm-12 col-md-3 label-control" for="projectinputDress">Dress size</label>
        <div class="col-sm-6 col-md-3">
          <select id="projectinputDress" name="data[dressSize]" class="form-control checkThis">
            <option value="" hidden>select one...</option>
            

		      {foreach from=$fields["dressSize"] item=t key=k}

              
            <option value="{$k}" {if $k==$profile->dressSize && $profile->dressSize!=""}selected{/if}>{$t}</option>
            

		      {/foreach}

            
          </select>
        </div>
      </div>
      <!--End of for females only--> 
      {/if} 
      <!--End of for teens and adults only-->
      
      <div class="form-group row">
        <label class="col-xs-12 col-sm-12 col-md-3 label-control" for="projectinputShoe">Shoe size</label>
        <div class="col-sm-6 col-md-3">
          <select id="projectinputShoe" name="data[shoeSize]" class="form-control checkThis">
            <option value="" hidden>select one...</option>
            
            

		                    {foreach from=$fields["shoeSize"] item=t key=k}

              
            
            <option value="{$k}" {if $k==$profile->shoeSize && $profile->shoeSize!=""}selected{/if}>{$t}</option>
            
            

		                    {/foreach}

            
          
          </select>
        </div>
      </div>
    </div>
  </section>
  <section class="card">
    <div class="card-block">
      <h4 class="form-section"><i class="icon-clipboard4"></i>Details</h4>
      <div class="form-group row">
        <label class="col-md-3 label-control" for="profileBio">Bio</label>
        <div class="col-md-6">
          <textarea value="{$profile->bio}" id="profileBio" class="form-control checkThis" rows="5" name="data[bio]"></textarea>
        </div>
      </div>
      {if in_array('knownFor',$selectedFields)}
      <div class="form-group row">
        <label class="col-md-3 label-control" for="projectinputKnown">Known for</label>
        <div class="col-md-6">
          <textarea id="projectinputKnown" class="form-control checkThis" rows="3" name="data[knownFor]">{$profile->knownFor}</textarea>
        </div>
      </div>
      {/if}
      {if in_array('upcomingProjects',$selectedFields)}
      <div class="form-group row">
        <label class="col-md-3 label-control" for="projectinputProjects">Upcoming projects</label>
        <div class="col-md-6">
          <input autocomplete="dontsuggest" value="{$profile->projects}" type="text" id="projectinputProjects" class="form-control checkThis" name="data[upcomingProjects]">
        </div>
      </div>
      {/if}
      
      {if in_array('occupation',$selectedFields)}
      <div class="form-group row">
        <label class="col-md-3 label-control" for="projectinputOccupation">Occupation</label>
        <div class="col-md-6">
          <input autocomplete="dontsuggest" value="{$profile->occupation}" type="text" id="projectinputOccupation" class="form-control checkThis" name="data[occupation]">
        </div>
      </div>
      {/if}
      <div class="form-group row">
        <label class="col-md-3 label-control">Ethnicity</label>
        <div class="col-md-9"> {foreach from=$fields["ethnicity"] item=t key=k}
          <fieldset>
            <label class="custom-control custom-checkbox">
              <input {if $ethnicity[$k]==1}checked{/if} type="checkbox" class="custom-control-input" name="data[ethnicity{$k}]">
              <span class="custom-control-indicator"></span> <span class="custom-control-description">{$t}</span></label>
          </fieldset>
          {/foreach} </div>
      </div>
    </div>
  </section>
  <section class="card">
    <div class="card-block"> {if array_intersect(array('instagram','website','book','press','music','video'), $selectedFields) || count($customUrlFields) > 0}
      <h4 class="form-section"><i class="icon-clipboard4"></i>URLs</h4>
      {/if}
      {if in_array('instagram',$selectedFields)}
      <div class="form-group row">
        <label class="col-md-3 label-control">Instagram</label>
        <div class="col-md-6">
          <div class="form-group instagram-repeater">
            <div data-repeater-list="repeater-group">
              <div class="input-group mb-1" data-repeater-item>
                <div class="input-group-prepend"><span class="input-group-text">@</span></div>
                <input autocomplete="dontsuggest" type="text" placeholder="username" class="input-instagram form-control instafield checkThis trimit" name="data[instagram]">
                <span class="input-group-append" id="button-addon2">
                <button  type="button" data-repeater-delete class="mybtnSide mybtn-clear instagrm-delete"><i class="ft-x"></i></button>
                </span> <small class="errormsg w-100 ml-3 red" style="display: none"></small> </div>
            </div>
            <button type="button" data-repeater-create class="btn mybtn-transp btn-sm" style="float: right; margin-top: -10px;">Add another link</button>
            <input type="hidden" value='{$profile->instagram}' name="data[instagram]" id="instagram">
          </div>
        </div>
      </div>
      {/if}
      {if in_array('website',$selectedFields)}
      <div class="form-group row">
        <label class="col-md-3 label-control" for="projectinputSite">Website</label>
        <div class="col-md-6">
          <input autocomplete="dontsuggest" value="{$profile->websiteurl}" type="url" id="projectinputSite" placeholder="www..." class="form-control urlfield" name="data[website]">
        </div>
      </div>
      {/if}
      {if in_array('book',$selectedFields)}
      <div class="form-group row">
        <label class="col-md-3 label-control" for="projectinputBook">Book</label>
        <div class="col-md-6">
          <input autocomplete="dontsuggest" value="{$profile->bookurl}" type="url" id="projectinputBook" placeholder="www..." class="form-control urlfield" name="data[book]">
        </div>
      </div>
      {/if}
      {if in_array('press',$selectedFields)}
      <div class="form-group row">
        <label class="col-md-3 label-control">Press</label>
        <div class="col-md-6">
          <div class="form-group press-repeater">
            <div data-repeater-list="press">
              <div class="input-group mb-1" data-repeater-item>
                <input autocomplete="dontsuggest" type="url" placeholder="www..." class="input-press form-control urlfield" name="data[press]">
                <span class="input-group-append" id="button-addon2">
                <button  type="button" data-repeater-delete class="mybtnSide mybtn-clear press-delete"><i class="ft-x"></i></button>
                </span> </div>
            </div>
            <button type="button" data-repeater-create class="btn mybtn-transp btn-sm" style="float: right; margin-top: -10px;">Add another link</button>
            <input type="hidden" value='{$profile->press}' name="data[press]" id="press">
          </div>
        </div>
      </div>
      {/if}
      {if in_array('music',$selectedFields)}
      <div class="form-group row">
        <label class="col-md-3 label-control">Audio</label>
        <div class="col-md-6">
          <div class="form-group music-repeater">
            <div data-repeater-list="music">
              <div class="input-group mb-1" data-repeater-item>
                <input autocomplete="dontsuggest" type="url" placeholder="www..." class="input-music form-control urlfield" name="data[music]">
                <span class="input-group-append" id="button-addon2">
                <button  type="button" data-repeater-delete class="mybtnSide mybtn-clear music-delete"><i class="ft-x"></i></button>
                </span> </div>
            </div>
            <button type="button" data-repeater-create class="btn mybtn-transp btn-sm" style="float: right; margin-top: -10px;">Add another link</button>
            <input type="hidden" value='{$profile->music}' name="data[audio]" id="music">
          </div>
        </div>
      </div>
      {/if}
      {if in_array('video',$selectedFields)}
      <div class="form-group row">
        <label class="col-md-3 label-control">Video</label>
        <div class="col-md-6">
          <div class="form-group video-repeater">
            <div data-repeater-list="video">
              <div class="input-group mb-1" data-repeater-item>
                <input autocomplete="dontsuggest" type="url" placeholder="www..." class="input-video form-control urlfield" name="data[video]">
                <span class="input-group-append" id="button-addon2">
                <button  type="button" data-repeater-delete class="mybtnSide mybtn-clear video-delete"><i class="ft-x"></i></button>
                </span> </div>
            </div>
            <button type="button" data-repeater-create class="btn mybtn-transp btn-sm" style="float: right; margin-top: -10px;">Add another link</button>
            <input type="hidden" value='{$profile->video}' name="data[video]" id="video">
          </div>
        </div>
      </div>
      {/if}

      {* Поля кастом-URL'ов *}
      {foreach from=$customUrlFields item=customUrlName key=customUrl}
      <div class="form-group row">
        <label class="col-md-3 label-control" for="projectinputBook">{$customUrlName}</label>
        <div class="col-md-6">
          <input autocomplete="dontsuggest" value="{$profile->$customUrl}" type="url" id="projectinputBook" placeholder="" class="form-control urlfield" name="data[{$customUrl}]">
        </div>
      </div>
      {/foreach}
      </div>
  </section>
  <section class="card">
    <div class="card-block">
      <h4 class="form-section"><i class="icon-clipboard4"></i>Contact</h4>
      <div class="form-group row">
        <label class="col-md-3 label-control" for="projectinputAddress1">Address 1</label>
        <div class="col-md-6">
          <input autocomplete="address-line1" value="{$profile->address}" type="text" id="projectinputAddress1" placeholder="house number and street" class="form-control checkThis trimit" name="data[address]">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-md-3 label-control" for="projectinputAddress2">Address 2</label>
        <div class="col-md-6">
          <input autocomplete="address-line2" value="{$profile->address2}" type="text" id="projectinputAddress2" placeholder="apartment" class="form-control checkThis trimit" name="data[address2]">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-md-3 label-control" for="projectinputCity">City</label>
        <div class="col-md-6">
          <input autocomplete="address-level2" value="{$profile->city}" type="text" id="projectinputCity" placeholder="city" class="form-control checkThis capThis trimit" name="data[city]">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-md-3 label-control" for="projectinputState">State</label>
        <div class="col-md-6">
          <input autocomplete="address-level1" value="{$profile->state}" type="text" id="projectinputState" placeholder="NY, NJ, etc." class="form-control statefield checkThis trimit" name="data[state]">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-md-3 label-control" for="projectinputZip">Zip code</label>
        <div class="col-md-6">
          <input autocomplete="postal-code" value="{$profile->ZIPcode}" type="tel" id="projectinputZip" placeholder="12345" class="form-control checkThis trimit" name="data[ZIPcode]">
        </div>
      </div>
      <br>
      <br>
      <div class="form-group row">
        <label class="col-md-3 label-control" for="projectinputHomePhone">Home phone</label>
        <div class="col-md-6">
          <input autocomplete="dontsuggest" value="{$profile->homePhone}" type="tel" id="projectinputHomePhone" class="form-control numOnly checkThis trimit" name="data[homePhone]">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-md-3 label-control" for="projectinputCellPhone">Cell phone</label>
        <div class="col-md-6">
          <input autocomplete="tel" value="{$profile->cellPhone}" type="tel" id="projectinputCellPhone" class="form-control numOnly checkThis trimit" name="data[cellPhone]">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-md-3 label-control" for="projectinputPersEmail">E-mail</label>
        <div class="col-md-6">
          <input autocomplete="email" value="{$profile->email}" type="email" id="projectinputPersEmail" class="form-control emailfield checkThis trimit" name="data[email]">
        </div>
      </div>
    </div>
  </section>
  {if in_array('agency',$selectedFields)}
  <section class="card">
    <div class="card-block">
      <h4 class="form-section"><i class="icon-clipboard4"></i>Agency</h4>
      <div class="form-group row">
        <label class="col-md-3 label-control" for="projectinputAgent">Agent name</label>
        <div class="col-md-6">
          <input autocomplete="dontsuggest" value="{$profile->agentName}" type="text" id="projectinputAgent" class="form-control checkThis capThis trimit" name="data[agentName]">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-md-3 label-control" for="projectinputAgentPhone">Agent phone</label>
        <div class="col-md-6">
          <input autocomplete="dontsuggest" value="{$profile->agentPhone}" type="tel" id="projectinputAgentPhone" class="form-control numOnly checkThis trimit" name="data[agentPhone]">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-md-3 label-control" for="projectinputAgentEmail">Agent e-mail</label>
        <div class="col-md-6">
          <input autocomplete="dontsuggest" value="{$profile->agentEmail}" type="email" id="projectinputAgentEmail" class="form-control emailfield checkThis trimit" name="data[agentEmail]">
        </div>
      </div>
    </div>
  </section>
  {/if}
  
  
  {if $profileType == 0}
  <section class="card">
    <div class="card-block"> <br>
      <h4 class="form-section"><i class="icon-clipboard4"></i>Parent/Guardian</h4>
      <div class="form-group row">
        <label class="col-md-3 label-control" for="projectinput2">Name</label>
        <div class="col-md-6">
          <input autocomplete="dontsuggest" value="{$profile->parentName}" type="text" id="projectinput2" class="form-control checkThis capThis trimit" name="data[parentName]">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-md-3 label-control" for="projectinput2">Phone</label>
        <div class="col-md-6">
          <input autocomplete="dontsuggest" value="{$profile->parentPhone}" type="tel" id="projectinput2" class="form-control numOnly checkThis trimit" name="data[parentPhone]">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-md-3 label-control" for="projectinput2">E-mail</label>
        <div class="col-md-6">
          <input autocomplete="dontsuggest" value="{$profile->parentEmail}" type="email" id="projectinput2" class="form-control emailfield checkThis trimit" name="data[parentEmail]">
        </div>
      </div>
    </div>
  </section>
  {/if}
  <section class="card">
  <div class="card-block d-flex justify-content-center align-items-center">
    <h3 class="success d-inline-block m-0">Looking good!</h3>
    <button type="submit" id="submitBtn" class="btn btn-primary mybtn-green ml-2"><i class="icon-check2"></i> Submit</button>
    <input type="hidden" name="action" value="save">
    </section>
  </div>
</form>

<script>
  var states = [{foreach from=$basedIn item=t}"{$t}",{/foreach}];
</script>
{include file="footer.tpl"} 
