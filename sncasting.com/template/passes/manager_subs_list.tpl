{assign var=subs value=Passes_Sub::getListByTypeId($passType->id)}

<table class="table">
    <thead>
        <tr>
            <th>Название</th>
            <th>Цена в рамках нормы</th>
            <th>Цена в вне нормы</th>
            <th>Цена</th>
            <th>Статус</th>
                {if $passType->type=='auto'}
                <th>Время на стоянке</th>
                <th>Период на территории</th>
                {else}
                <th>Действие пропуска (дней)</th>
                {/if}
            <th>Штраф за нарушение</th>
            <th>Период штрафа</th>
            <th>-</th>
        </tr>
    </thead>
    {foreach from=$subs item=sub}
        <tr data-sub="{$sub->id}">
            <td><input data-name="name" value="{$sub|escape}" class="form-control"/></td>
            <td>{if $passType->norm==0}&mdash;{else}<input size=6 class="input-inline form-control" data-name="normPrice" value="{$sub->normPrice|escape}" />{/if}</td>
            <td>{if $passType->norm==0}&mdash;{else}<input size=6 class="input-inline form-control" data-name="outOfNormPrice" value="{$sub->outOfNormPrice|escape}" />{/if}</td>
            <td>{if $passType->norm>0}&mdash;{else}<input size=6 class="input-inline form-control" data-name="normPrice" value="{$sub->normPrice|escape}" />{/if}</td>

            <td><select data-name="is_blocked" class="form-control">
                    <option {if $sub->isBlocked()==0}selected{/if} value="0">Активен</option>
                    <option {if $sub->isBlocked()==1}selected{/if} value="1">Заблокирован</option>
                </select></td>
                {if $passType=='auto'}
                <td><input class="form-control" data-name="stayValue" value="{$sub->stayValue|escape}" /></td>
                <td>
                    <select class="form-control" data-name="stayPeriod" >
                        <option {if $sub->stayPeriod==0}selected{/if} value="0">Факт</option>
                        <option {if $sub->stayPeriod==1}selected{/if} value="1">Час</option>
                        <option {if $sub->stayPeriod==2}selected{/if} value="2">День</option>
                        <option {if $sub->stayPeriod==3}selected{/if} value="3">Месяц</option>
                    </select>
                </td>
            {else}
                <td>
                    <input class="form-control" data-name="days" value="{$sub->days|escape}" />
                </td>
            {/if}
            <td><input class="form-control" data-name="fine" value="{$sub->fine|escape}" /></td>
            <td class="width-150">
                <select class="form-control" data-name="finePeriod" >
                    <option {if $sub->finePeriod==0}selected{/if} value="0">Факт</option>
                    <option {if $sub->finePeriod==1}selected{/if} value="1">Час</option>
                    <option {if $sub->finePeriod==2}selected{/if} value="2">День</option>
                    <option {if $sub->finePeriod==3}selected{/if} value="3">Месяц</option>
                </select>
            </td>
            <td>
                <input type="button" class="btn btn-sm btn-danger delete" value="Удалить" />
            </td>
        </tr>
    {/foreach}
</table>