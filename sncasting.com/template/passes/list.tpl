{include file="../header.tpl"}
{include file="../left.tpl" _class="content-full-width" }

<div class="p-20">
    <!-- begin row -->
    <div class="row">
        <!-- begin col-2 -->
        <div class="col-md-2">

            <div class="email-btn-row hidden-xs">
                <a href="/passes/new" data-toggle="modal" class="btn btn-sm btn-inverse"><i class="fa fa-plus m-r-5"></i> Заказать новый пропуск </a>
            </div>
            <ul class="filter nav nav-pills nav-stacked nav-sm types">
                <li class="active"><a data-val="all" href=# class=type data-id=0 >Оплата: Все<span class="badge pull-right">{count(Pass::getList())}</span></a></li>
                <li><a href=# data-val="1" class="type" data-id="2">Оплаченные</a></li>
                <li><a href=# data-val="0" class="type" data-id="1">Неоплаченные</a></li>
            </ul>
            <ul class="filter nav nav-pills nav-stacked nav-sm types">
                <li class="active"><a data-val="all" href=# class=type data-id=0 >Тип: Все<span class="badge pull-right"></span></a></li>
                <li><a href=# data-val="auto" class="type" data-id="3">Автомобили</a></li>
                <li><a href=# data-val="personal" class="type" data-id="2">Сотрудники</a></li>
                <li><a href=# data-val="guest" class="type" data-id="1">Гости</a></li>
            </ul>


            <ul class="filter nav nav-pills nav-stacked nav-sm types nav-inbox">
                <li class="active"><a data-val="all" href=# class=type data-id=0 >Использование: Все</a></li>
                <li><a href=# data-val="1" class="type" data-id="2">Использованные</a></li>
                <li><a href=# data-val="0" class="type" data-id="1">Неиспользованные</a></li>
            </ul>



        </div>
        <!-- end col-2 -->
        <!-- begin col-10 -->
        <div class="col-md-10">
            <div class="email-content" id="list">
                Loading...

            </div>
        </div>
        <!-- end col-10 -->
    </div>
    <!-- end row -->
</div>


<div class="modal fade" id="confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Подтвердите действие</h4>
            </div>
            <div class="modal-body">
                Подтвердите, что вы хотите <b>удалить</b> выбранные группы
            </div>
            <div class="modal-footer">
                <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Закрыть</a>
                <a href="javascript:;" class="btn btn-sm btn-success">Подтвердить</a>
            </div>
        </div>
    </div>
</div>
{include file="../footer.tpl" _h=1}