{include file="../header.tpl"}
{include file="../left.tpl" _class="content-full-width" }
<style>
    .pricing-table{
        display:none;
    }
    tbody.cart tr .count{
        display: none;
    }
    tbody.cart tr.active .count{
        display:block;
    }
    .type.active{
        border-bottom:3px solid orange;
    }
    .type{
        margin-right: 30px;
    }

</style>
<!-- begin #pricing -->
<div id="pricing" data-scrollview="true">
    <!-- begin container -->
    <div id="cart_main" class="container col-lg-12">

        <h2 class="content-title">Заказать новый пропуск</h2>
        <p class="content-desc">
            На этой странице вы можете выбрать и оформить новый пропуск в систему.
        </p>
        <br><br>
        {assign var=auto_min value=99999999}
        {assign var=personal_min value=99999999}
        {assign var=guest_min value=99999999}

        {* Немного говнокода :( *}
        {assign var=passesType value=Passes_Type::getList(false,'auto')}
        {foreach from=$passesType item=pass}
            {assign var=auto_min value=min($auto_min, $pass->getMinPrice()->normPrice)}
        {/foreach}
        {assign var=passesType value=Passes_Type::getList(false,'personal')}
        {foreach from=$passesType item=pass}
            {assign var=personal_min value=min($personal_min, $pass->getMinPrice()->normPrice)}
        {/foreach}
        {assign var=passesType value=Passes_Type::getList(false,'guest')}
        {foreach from=$passesType item=pass}
            {assign var=guest_min value=min($guest_min, $pass->getMinPrice()->normPrice)}
        {/foreach}
        
        <div class="text-left">
            <button class="btn btn-large btn-inverse type" data-type="auto" >
                <div class="pull-left" style="margin:5px"><i class="fa fa-automobile fa-4x"></i>
                </div>
                <div class="pull-left text-white">
                    <h5 class="text-white">Пропуск для Автомобиля</h5>
                    <h6 class="text-white">от {$auto_min} руб.</h6>
                </div>
            </button>
            <button class="btn btn-large btn-inverse type"  data-type="personal">
                <div class="pull-left" style="margin:5px"><i class="fa fa-user fa-4x"></i>
                </div>
                <div class="pull-left text-white">
                    <h5 class="text-white">Пропуск для сотрудника</h5>
                    <h6 class="text-white">от {$personal_min} руб.</h6>
                </div>

            </button>
            <button class="btn btn-large btn-inverse type"  data-type="guest">
                <div class="pull-left" style="margin:5px"><i class="fa fa-building-o fa-4x"></i>
                </div>
                <div class="pull-left text-white">
                    <h5 class="text-white">Пропуск для посетителя</h5>
                    <h6 class="text-white">от {$guest_min} руб.</h6>
                </div>

            </button>
        </div>
        <!-- begin pricing-table -->
        {foreach from=array('auto','personal','guest') item=type}
            <ul class="pricing-table col-3" data-type="{$type}">

                {assign var=passesType value=Passes_Type::getList(false,$type)}
                {foreach from=$passesType item=pass}
                    <li data-animation="true" data-animation-type="fadeInUp">
                        <div class="pricing-container">
                            <h3>{$pass}</h3>
                            <div class="price">
                                <div class="price-figure">
                                    <span class="price-number"> от {$pass->getMinPrice()->normPrice} руб.</span>
                                </div>
                            </div>
                            <ul class="features">
                                {if $pass->norm>0}
                                    <li>1 пропуск на {$pass->norm} м<sup>2</sup></li>
                                    {/if}
                                    {if $pass->hourFrom>0}
                                    <li>Доступен с {$pass->hourFrom} часов до {$pass->hourTo}</li>
                                    {/if}
                                <li>Количество использования: <br> {if $pass->count>0}{$pass->count}{else}без ограничений{/if}</li>
                                {if $pass->fine>0}<li>Штраф за превышение времени: <br> {$pass->fine} руб.</li>{/if}

                            </ul>

                            <div class="footer row" style="margin:0" >


                                {if $pass->prepayment==0}
                                    <div class="col-lg-6 col-sm-12 row">
                                        <a href="#" class="btn btn-inverse btn-b1ock buy" data-passid="{$pass->id}" data-prepayment="1">Приобрести</a>
                                    </div>
                                    <div class="col-lg-6 col-sm-12">
                                        <a href="#" class="btn btn-inverse btn-info buy" data-passid="{$pass->id}" data-prepayment="0">Заказать сейчас</a>
                                    </div>
                                {else}
                                    <div class="col-lg-12">
                                        <a href="#" class="btn btn-inverse btn-bl1ock buy" data-passid="{$pass->id}" data-prepayment="1">Приобрести</a>
                                    </div>
                                {/if}
                                <div class="clearfix"></div>
                                <br>


                            </div>
                        </div>
                    </li>
                {/foreach}

            </ul>
        {/foreach}
    </div>

    <div id="cart_sidebar" class="col-lg-3 bg-black-darker" style="display:none; height:100vh; color: #aaaaaa;font-size:14px;">
        <div id="cart_content">

        </div>
    </div>

    <!-- end container -->
</div>
<div class="modal fade" id="buy">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Подтвердите действие</h4>
            </div>
            <div class="modal-body">
                Loading...
            </div>
            <div class="modal-footer">
                <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Закрыть</a>
                <a href="javascript:;" class="btn btn-sm btn-success">Добавить в счет</a>
            </div>
        </div>
    </div>
</div>
<!-- end #pricing -->
{include file="../footer.tpl"}

