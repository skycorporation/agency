{include file="../header.tpl"}
{include file="../left.tpl"}

{assign var=can_edit value=($p->canEdit&& (count($p->getRecords())==0))}

<style>
    .carPlate{
        height: 100px;
        border:2px solid black;
        border-radius: 5px;
    }
    .carNumber{
        width: 75%;
        float: left;
        height: 96px;
        color: black;
        border-right: 1px solid black;
    }
    .carRegion{
        float: left;
        width: 25%;
    }
    .carRegionNumber{
        height: 65px;
    }
    .carRegionDesc{
        text-align: center;
        font-size: 15pt;

    }
    .carNumber input{
        width: 100%;
        height: 100%;
        font-size: 49pt;
        border: none;
        text-align:center;
        outline: none;
    }
    .carRegionNumber input{
        color: black;
        width: 100%;
        height: 100%;
        font-size: 30pt;
        border: none;
        text-align:center;
        outline: none;
    }
    .carRegi
</style>

<!-- end breadcrumb -->
<!-- begin page-header -->
<a href="/passes/list" class="btn btn-default">&larr; Назад к пропускам</a><br><Br>
<h1 class="page-header">Просмотр пропуска</h1>
<!-- end page-header -->

{if $error}
    <div class="alert alert-danger">
        <h4>Произошла ошибка</h4>
        {$error}
    </div>
{/if}
<!-- begin panel -->
<div class="col-lg-12 row">
    <div class="col-lg-6 row">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Пропуск</h4>
            </div>
            <div class="panel-body">

                <div class="table-responsive" id="inv_list">
                    <form action="?" method="post">
                        <input type="hidden" name="action" value="save">
                        <div class="form-group">
                            <label ><b>Пропуск</b></label><br>
                            {$p->subPass}
                        </div>
                        <div class="form-group">
                            <label ><b>Группа пропуска</b></label><br>
                            {$p->subPass->getPassType()}
                        </div>
                        <div class="form-group">
                            <label ><b>Компания</b></label><br>
                            {if !$can_edit}
                                {$p->company}
                            {else}
                                <select class="form-control" name="data[companyID]">

                                    {if $service_admin}
                                        {assign var=list value=Company::getList()}

                                    {else}
                                        {assign var=list value=array($user->getCompany())}
                                    {/if}
                                    {foreach from=$list item=row}
                                        <option {if $row->id==$p->proxyTable()->companyID}selected{/if} value="{$row->id}">{$row|escape}</option>
                                    {/foreach}
                                </select>
                            {/if}
                        </div>
                        <div class="form-group">
                            <label ><b>Сотрудник (инициатор)</b></label><br>
                            {if !$can_edit}
                                {$p->author->getNamePart('firstname')}
                                {$p->author->getNamePart('lastname')}
                            {else}
                                <select class="form-control" name="data[authorID]">
                                    {foreach from=$company_users item=row}
                                        {assign var=tmp value=User::getById($row->id)}
                                        <option {if $row->id==$user->id}selected{/if} value="{$row->id}">{implode(' ', $tmp->getName())|escape}</option>
                                    {/foreach}
                                </select>
                            {/if}
                        </div>
                        <div class="form-group">
                            <label ><b>Статус оплаты</b></label><br>
                        {if $p->is_payed}Оплачен до {$p->expireDate}{else}Неоплачен. {if $p->subPass->getPassType()->prepayment=='0'}Постоплата{/if}{/if}
                    </div>
                    <div class="form-group">
                        <label ><b>Действителен до</b></label><br>
                        {$p->expireDate}
                    </div>
                    <div class="form-group">
                        <label ><b>Описание пропуска</b></label><br>
                        <input name="data[description]" value="{$p->description|escape}" class="form-control" />    
                    </div>


                    {assign var=type value=$p->subPass->getPassType()->type}
                    <div class="form-group">
                        {include file="template/passes/new/"|cat:$type|cat:'.tpl'}

                    </div>
                    <div class="form-group">
                        <div class='pull-left'>
                            <button type='submit' class="btn btn-success btn-sm" name="btn" value="save">Сохранить</button>
                        </div>
                        {if $can_edit}
                            <div class='pull-right'>
                                <a href="?del&hash={md5(md5($p->id))}" name=btn value=del class="btn btn-sm btn-danger"  >Удалить пропуск</a>
                            </div>
                        {/if}
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<div class="col-lg-6">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">Журнал</h4>
        </div>
        <div class="panel-body">
            <p>
                <span class="m-r-5">Показать: Дата</span> 

            </p>
            <div class="table-responsive" id="inv_list">
                <table class="table">
                    <thead>
                        <tr>
                            
                            <th>Дата и время события</th>
                            <th>Направление</th>
                            <th>Фото(если авто)</th>

                        </tr>
                    </thead>
                    <tbody>
                        {foreach from=$p->getRecords() item=r}
                            <tr>
                                <td rowspan='2'><a href="{$r->getPhotoUrl()}" target="_blank"><img style="max-width:100px" src="{$r->getPhotoUrl()}"></a>
                                    <br>
                                    {$r->carNumber}, {$r->carRegion}
                                </td>

                                <td>Заезд: {$r->insideDate}<br>
                                    <BR>
                                    Выезд: {if $r->outsideDate=='0000-00-00 00:00:00'}на территории{else}{$r->outsideDate}{/if}</td>


                            </tr>
                            <tr>
                                <td>
                                    {if $r->fineID>0}   
                                        {Fines::getById($r->fineID)}
                                    {/if}    
                                </td>
                            </tr>

                        {/foreach}
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
</div>
<!-- end panel -->
{include file="../footer.tpl"}

