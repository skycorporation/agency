{include file="../header.tpl"}
{include file="../left.tpl" _class="content-full-width" }

<div class="p-20">
    <!-- begin row -->
    <div class="row">
        <!-- begin col-2 -->
        <div class="col-md-12">


        </div>
        <!-- end col-2 -->
        <!-- begin col-10 -->
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#auto">Автомобили</a></li>
                <li><a data-toggle="tab" href="#personal">Сотрудники</a></li>
                <li><a data-toggle="tab" href="#guest">Гости</a></li>
            </ul>
            <div class="tab-content">
                <div id="auto" class="tab-pane fade in active">
                    <div class="email-btn-row hidden-xs">
                        <a href="#create" data-type="auto" class="btn btn-sm btn-inverse"><i class="fa fa-plus m-r-5"></i> Новая группа пропусков</a>
                        <a href="#" data-email-action="block" class="btn btn-sm btn-inverse disabled">Заблокировать</a>
                        <a href="#" data-email-action="unblock" class="btn btn-sm btn-inverse disabled">Разблокировтаь</a>
                        <a href="#" data-email-action="delete" class="btn btn-sm btn-inverse disabled">Удалить</a>

                    </div>
                    <div class="email-content">
                        <table class="table table-email">
                            <thead>
                                <tr>
                                    <th class="email-select">
                                    </th>
                                    <th class='width-250'>
                                        Название группы
                                    </th>
                                    <th>
                                        Ограничение по норме
                                    </th>
                                    <th>
                                        Ограничение по времени использования
                                    </th>
                                    <th>
                                        Ограничение по количеству использования
                                    </th>
                                    <th>

                                        Количество в группе

                                    </th>
                                    <th>
                                        Оформленных пропусков
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach from=Passes_Type::getList() item=row}
                                    <tr>
                                        <td >
                                            <div class="email-checkbox">
                                                <label>

                                                    <input data-id='{$row->id}' type="checkbox" data-checked="email-checkbox" />
                                                </label>
                                            </div>
                                        </td>
                                        <td class="email-sender">
                                            <a href="/passes/{$row->id}/manager">{$row}</a>
                                            {if $row->isBlocked()}
                                                <label class="label label-danger">Заблокирован</label>
                                            {/if}
                                        </td>
                                        <td class="email-subject">
                                            {if $row->norm>0}
                                                <i class='fa fa-check'></i>
                                            {else}
                                                <i class='fa fa-close'></i>
                                            {/if}
                                        </td>
                                        <td class="email-subject">
                                            {if $row->hourFrom>0}
                                                <i class='fa fa-check'></i>
                                            {else}
                                                <i class='fa fa-close'></i>
                                            {/if}
                                        </td>
                                        <td class="email-subject">
                                            {if $row->count>0}
                                                <i class='fa fa-check'></i>
                                            {else}
                                                <i class='fa fa-close'></i>
                                            {/if}
                                        </td>

                                        <td class="email-date">{count($row->getSubPasses())}</td>
                                        <td class="email-date">{count($row->getPurchased())}</td>
                                    </tr>
                                {foreachelse}
                                    <tr>
                                        <td colspan='7' class='text-center'><i>Пропусков нет</i></td>
                                    </tr>
                                {/foreach}

                            </tbody>
                        </table>

                    </div>
                </div>
                <div id="personal" class="tab-pane fade">
                    <div class="email-btn-row hidden-xs">
                        <a href="#create" data-type="personal" class="btn btn-sm btn-inverse"><i class="fa fa-plus m-r-5"></i> Новая группа пропусков</a>
                        <a href="#" data-email-action="block" class="btn btn-sm btn-inverse disabled">Заблокировать</a>
                        <a href="#" data-email-action="unblock" class="btn btn-sm btn-inverse disabled">Разблокировтаь</a>
                        <a href="#" data-email-action="delete" class="btn btn-sm btn-inverse disabled">Удалить</a>

                    </div>
                    <div class="email-content">
                        <table class="table table-email">
                            <thead>
                                <tr>
                                    <th class="email-select">
                                    </th>
                                    <th class='width-250'>
                                        Название группы
                                    </th>
                                    <th>
                                        Ограничение по норме
                                    </th>
                                    <th>
                                        Ограничение по времени использования
                                    </th>
                                    <th>
                                        Ограничение по количеству использования
                                    </th>
                                    <th>

                                        Количество в группе

                                    </th>
                                    <th>
                                        Оформленных пропусков
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach from=Passes_Type::getList(-1, 'personal') item=row}
                                    <tr>
                                        <td >
                                            <div class="email-checkbox">
                                                <label>

                                                    <input data-id='{$row->id}' type="checkbox" data-checked="email-checkbox" />
                                                </label>
                                            </div>
                                        </td>
                                        <td class="email-sender">
                                            <a href="/passes/{$row->id}/manager">{$row}</a>
                                            {if $row->isBlocked()}
                                                <label class="label label-danger">Заблокирован</label>
                                            {/if}
                                        </td>
                                        <td class="email-subject">
                                            {if $row->norm>0}
                                                <i class='fa fa-check'></i>
                                            {else}
                                                <i class='fa fa-close'></i>
                                            {/if}
                                        </td>
                                        <td class="email-subject">
                                            {if $row->hourFrom>0}
                                                <i class='fa fa-check'></i>
                                            {else}
                                                <i class='fa fa-close'></i>
                                            {/if}
                                        </td>
                                        <td class="email-subject">
                                            {if $row->count>0}
                                                <i class='fa fa-check'></i>
                                            {else}
                                                <i class='fa fa-close'></i>
                                            {/if}
                                        </td>

                                        <td class="email-date">{count($row->getSubPasses())}</td>
                                        <td class="email-date">{count($row->getPurchased())}</td>
                                    </tr>
                                {foreachelse}
                                    <tr>
                                        <td colspan='7' class='text-center'><i>Пропусков нет</i></td>
                                    </tr>
                                {/foreach}

                            </tbody>
                        </table>

                    </div>
                </div>
                <div id="guest" class="tab-pane fade">
                    <div class="email-btn-row hidden-xs">
                        <a href="#create" data-type="guest" class="btn btn-sm btn-inverse"><i class="fa fa-plus m-r-5"></i> Новая группа пропусков</a>
                        <a href="#" data-email-action="block" class="btn btn-sm btn-inverse disabled">Заблокировать</a>
                        <a href="#" data-email-action="unblock" class="btn btn-sm btn-inverse disabled">Разблокировтаь</a>
                        <a href="#" data-email-action="delete" class="btn btn-sm btn-inverse disabled">Удалить</a>

                    </div>
                    <div class="email-content">
                        <table class="table table-email">
                            <thead>
                                <tr>
                                    <th class="email-select">
                                    </th>
                                    <th class='width-250'>
                                        Название группы
                                    </th>
                                    <th>
                                        Ограничение по норме
                                    </th>
                                    <th>
                                        Ограничение по времени использования
                                    </th>
                                    <th>
                                        Ограничение по количеству использования
                                    </th>
                                    <th>

                                        Количество в группе

                                    </th>
                                    <th>
                                        Оформленных пропусков
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach from=Passes_Type::getList(-1, 'guest') item=row}
                                    <tr>
                                        <td >
                                            <div class="email-checkbox">
                                                <label>

                                                    <input data-id='{$row->id}' type="checkbox" data-checked="email-checkbox" />
                                                </label>
                                            </div>
                                        </td>
                                        <td class="email-sender">
                                            <a href="/passes/{$row->id}/manager">{$row}</a>
                                            {if $row->isBlocked()}
                                                <label class="label label-danger">Заблокирован</label>
                                            {/if}
                                        </td>
                                        <td class="email-subject">
                                            {if $row->norm>0}
                                                <i class='fa fa-check'></i>
                                            {else}
                                                <i class='fa fa-close'></i>
                                            {/if}
                                        </td>
                                        <td class="email-subject">
                                            {if $row->hourFrom>0}
                                                <i class='fa fa-check'></i>
                                            {else}
                                                <i class='fa fa-close'></i>
                                            {/if}
                                        </td>
                                        <td class="email-subject">
                                            {if $row->count>0}
                                                <i class='fa fa-check'></i>
                                            {else}
                                                <i class='fa fa-close'></i>
                                            {/if}
                                        </td>

                                        <td class="email-date">{count($row->getSubPasses())}</td>
                                        <td class="email-date">{count($row->getPurchased())}</td>
                                    </tr>
                                {foreachelse}
                                    <tr>
                                        <td colspan='7' class='text-center'><i>Пропусков нет</i></td>
                                    </tr>
                                {/foreach}

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <!-- end col-10 -->
        </div>
        <!-- end row -->
    </div>

    <div class="modal fade" id="create">
        <div class="modal-dialog">
            <form action='?' method="post">
                <input type='hidden' name="action" value="create">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Новая группа пропусков</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Название</label>
                            <input name=data[name] type='text' class='form-control' required="" placeholder='Название группы пропусков'>

                        </div>
                        <div class="form-group">
                            <label>Тип</label>
                            <select name=data[type] type='text' class='form-control' required="" >
                                <option value="auto">Автомобили</option>
                                <option value="personal">Сотрудники</option>
                                <option value="guest">Гости</option>
                            </select>

                        </div>
                        <div class="form-group">
                            <label>Норма (м<sup>2</sup>)</label>
                            <input name="data[norm]" class='form-control' type='text' placeholder='0 - без ограничений' value='0'/>
                        </div>
                        <div class="form-group">
                            <label>Норма по времени</label>
                            <div class='col-lg-12 row'>
                                <div class='col-lg-6 row' style="margin:0px -12px">
                                    <input name="data[hourFrom]" class='form-control' type='text' placeholder='от (часы) ' />
                                </div>
                                <div class='col-lg-6'>
                                    <input name="data[hourTo]" class='form-control' type='text' placeholder='до (часы) ' />
                                </div>
                            </div>

                        </div>
                        <div class="form-group"></div>
                        <div class="form-group">
                            <label>Количество использования</label>
                            <input name="data[count]" class='form-control' type='text' value='0'/>
                        </div>
                        <div class="form-group">
                            <label>Штраф за превышение времени использования</label>
                            <input name="data[fine]" class='form-control' type='text' />
                        </div>
                        <div class="form-group">
                            <label>Период начислений штрафа </label>
                            <select name="data[finePeriod]" class='form-control' >
                                <option value="0">Факт</option>
                                <option value="1">1 час</option>
                                <option value="2">24 часа</option>
                                <option value="3">Месяц</option>

                            </select>
                        </div>
                        <div class="form-group">
                            <label>Тип оплаты</label>
                            <select name='data[prepayment]' class="form-control">
                                <option value='0'>Предоплатный</option>
                                <option value='1'>Постоплатный</option>
                            </select>

                        </div>
                        <p id="create_error" class="hidden text-danger"></p>

                    </div>
                    <div class="modal-footer">
                        <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Закрыть</a>
                        <button type=submit class="btn btn-sm btn-success">Создать</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="confirm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Подтвердите действие</h4>
                </div>
                <div class="modal-body">
                    Подтвердите, что вы хотите <b>удалить</b> выбранные группы
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Закрыть</a>
                    <a href="javascript:;" class="btn btn-sm btn-success">Подтвердить</a>
                </div>
            </div>
        </div>
    </div>
    {include file="../footer.tpl" _h=1}