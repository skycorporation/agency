{include file="../header.tpl"}
{include file="../left.tpl" _class="content-full-width" }

<div class="p-20">
    <!-- begin row -->
    <div class="row">
        <!-- begin col-2 -->
        <div class="col-md-12">


        </div>
        <!-- end col-2 -->
        <!-- begin col-10 -->
        <div class="col-md-12">
            <div class="email-btn-row hidden-xs row p-l-10">
                <div class="pull-left">
                    <a href="/passes/manager" class="btn btn-sm btn-inverse"><i class="fa fa-backward m-r-5"></i> Назад</a>
                    Пропуск: {$passType}
                </div>
                <div class="pull-right p-r-10">
                    <a href="#create" data-toggle="modal" class="btn btn-success btn-sm">Создать пропуск</a>
                </div>
            </div>
            <div class="email-content">
                <table class="table table-email table-condensed">
                    <form action="?" method="post">
                        <input type="hidden" name="action" value="save" />
                        <tr>
                            <td class="width-250">
                            </td>
                            <td><h4>Редактирование группы пропусков "{$passType}"</h4></td>
                        </tr>
                        <tr>
                            <td class="p-t-15 width-250 text-right">Название пропуска</td>
                            <td class=""><input class="form-control" type="text" name="data[name]" value="{$passType->name|escape}" /></td>
                        </tr>
                        <tr>
                            <td class="p-t-15 width-250 text-right">Норма (м<sup>2</sup>)</td>
                            <td class=""><input class="form-control" type="text" name="data[norm]" value="{$passType->norm|escape}" /></td>
                        </tr>
                        {if $passType=='auto'}
                            <tr>

                                <td class="p-t-15 width-250 text-right">Норма по времени</td>
                                <td class="">
                                    <label>Часы (от)</label>
                                    <br>
                                    <input class="form-control input-inline" size=3 type="text" name="data[hourFrom]" value="{$passType->hourFrom|escape}" />
                                    <br><BR>
                                    <label>Часы (до)</label>
                                    <br>
                                    <input class="form-control input-inline" size=3 type="text" name="data[hourTo]" value="{$passType->hourTo|escape}" />

                                </td>
                            </tr>
                        {/if}
                        <tr>
                            <td class="p-t-15 width-250 text-right">Количество использования</td>
                            <td class=""><input class="form-control" type="text" name="data[count]" value="{$passType->count|escape}" /></td>
                        </tr>
                        <tr>
                            <td class="p-t-15 width-250 text-right">Штраф за превышение времени использования (руб.)</td>
                            <td class=""><input class="form-control" type="text" name="data[fine]" value="{$passType->fine|escape}" /></td>
                        </tr>
                        <tr>
                            <td class="p-t-15 width-250 text-right">Период начисления</td>
                            <td class=""><select class="form-control" type="text" name="data[finePeriod]" >
                                    <option {if 0==$passType->finePeriod}selected{/if} value="0">Факт</option>
                                    <option {if 1==$passType->finePeriod}selected{/if} value="1">1 час</option>
                                    <option {if 2==$passType->finePeriod}selected{/if} value="2">24 часа</option>
                                    <option {if 3==$passType->finePeriod}selected{/if} value="3">Месяц</option>
                                </select>

                        </tr>
                        <tr>
                            <td class="p-t-15 width-250 text-right">Тип оплаты</td>
                            <td class=""><select class="form-control" type="text" name="data[prepayment]" >
                                    <option {if 0==$passType->prepayment}selected{/if} value="0">Постоплатный</option>
                                    <option {if 1==$passType->prepayment}selected{/if} value="1">Предоплатный</option>

                                </select>

                        </tr>
                        <tr>
                            <td class="p-t-15 width-250 text-right"></td>
                            <td class=""><input class="btn  btn-success" type="submit" value="Сохранить" /></td>
                        </tr>
                    </form>
                </table>
            </div>
            <div class="clearfix"></div>
            <br>
            <div class="email-content" id="subs_list">
                Loading...

            </div>
        </div>
        <!-- end col-10 -->
    </div>
    <!-- end row -->
</div>



<div class="modal fade" id="confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Подтвердите действие</h4>
            </div>
            <div class="modal-body">
                Подтвердите, что вы хотите <b>удалить</b> выбранные группы
            </div>
            <div class="modal-footer">
                <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Закрыть</a>
                <a href="javascript:;" class="btn btn-sm btn-success">Подтвердить</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="create">
    <div class="modal-dialog">
        <form action='?' method="post">
            <input type='hidden' name="action" value="create">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Новый пропуск</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Название</label>
                        <input name=data[name] type='text' class='form-control' required="" placeholder='Название пропускa'>

                    </div>

                    <p id="create_error" class="hidden text-danger"></p>

                </div>
                <div class="modal-footer">
                    <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Закрыть</a>
                    <button type=submit class="btn btn-sm btn-success">Создать</button>
                </div>
            </div>
        </form>
    </div>
</div>
{include file="../footer.tpl" _h=1}