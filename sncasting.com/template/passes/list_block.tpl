<table class="table table-email">
    <thead>
        <tr>

            <th class='width-250'>
                Название пропуска
            </th>
            <th>
                Инициатор
            </th>
            <th>
                Дата создания
            </th>
            <th>
                Дата начала действия
            </th>
            <th>
                Дата окончания действия
            </th>
            <th>
                Статус оплаты
            </th>
            <th>
                Использование пропуска
            </th>
        </tr>
    </thead>
    <tbody>
        {foreach from=$list item=p}
            <tr>
                <td><A href="/passes/edit/{$p->id}">{$p->subPass->getPassType()}, {$p->subPass}</a><br>
                    <small>{$p->description}</small></td>
                <td>{if $p->carNumber!=''}<b>{$p->carNumber}, {$p->carRegion}</b><br>{/if}
                    {$p->author->getNamePart('firstname')} {$p->author->getNamePart('lastname')}</td>
                <td>{date("d.m.Y", strtotime(str_replace("-","/", $p->creationDateTime)))}</td>
                <td>{$p->openDate}</td>
                <td>{$p->expireDate}</td>
                <td>{if $p->is_payed}Оплачен{else}не оплачен{/if}</td>
                <td><A href="/passes/edit/{$p->id}">{count($p->getRecords())}</a></td>
            </tr>
        {foreachelse}
            <tr>
                <td colspan='7' class='text-center'><i>Пропусков нет</i></td>
            </tr>
        {/foreach}


    </tbody>
</table>