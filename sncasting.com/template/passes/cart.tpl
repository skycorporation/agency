{assign var=max value=0}
{if $pass->norm>0}
    {assign var=max value=floor( $my->getCompany()->getM2()/$pass->norm )-count($pass->getPurchased( $my->getCompany()->getId()))}
{if $max<0}{assign var=max value=0}{/if}
{/if}
<script>
    var //{if $pass->norm>0}

            in_norm = {floor( $my->getCompany()->getM2()/$pass->norm )};
            purchased = {count($pass->getPurchased( $my->getCompany()->getId()))};
    
    //{else}

    in_norm = purchased = 0;
    purchased = {count($pass->getPurchased( $my->getCompany()->getId()))};
    //  {/if}

</script>
<style>
    .total:after, .total_extra_price:after, .total_norm_price:after{
        content: " руб. "
    }
</style>
<table class="table">
    <thead>
        <tr>
            {if !$is_prepayment}<th></th>{/if}
            <th>Название</th>
                {if $pass->norm>0}
                <th>Цена в норме</th>
                <th>Цена вне нормы</th>
                {else}
                <th>Цена</th>
                {/if}
            <th>Количество</th>
        </tr>
    </thead>
    <tbody class="cart">

        {foreach from=Passes_Sub::getListByTypeId( $pass->id ) item=sub}
            <tr {if in_array($sub->id, array_keys( $smarty.request.cart|default:array()))}class="active"{/if}>
                {if !$is_prepayment}
                    <td width="50"><input {if in_array($sub->id, array_keys( $smarty.request.cart|default:array()))}checked{/if} type="radio" name="sub" value="{$sub->id}" ></td>
                    {/if}
                <td>{$sub}</td>

                <td class="price">{$sub->normPrice} руб. </td>
                {if $pass->norm>0}
                    <td class="priceout">{$sub->outOfNormPrice} руб.</td>
                {/if}
                {if !$is_prepayment}
                    <td class="width-100">
                        <input data-sub="{$sub->id}" class="form-control input-inline width-50 count" size=4 type="number" value="{if in_array($sub->id, array_keys( $smarty.request.cart|default:array()))}{$smarty.request.cart[$sub->id]}{else}1{/if}" min="1" step="1" />
                    </td>
                {else}
                    <td>
                        <a href="/passes/create/{$sub->id}" class="btn btn-success btn-sm" >Заказать </a>
                    </td>
                {/if}
            </tr>
        {/foreach}

    </tbody>
</table>
{if $pass->norm>0}
    В рамках нормы 1 пропуск на {$pass->norm} м<sup>2</sup>. Вы можете приобрести еще {$max} пропусков.
{/if}
<br>
<br>
{if !$is_prepayment}
    <h5 class="norm">
        В рамках нормы: <span class="total_norm">0</span> х <span class="total_norm_price">0</span>
        <span class="extra_norm" style="display:none"><br><bR>
            Вне нормы: <span class="total_extra_norm">0</span> x <span class="total_extra_price">0</span>
        </span>
    </h5>
    <div class="pull-right">
        <h5>Итого: <span class=total>0</span></h5>
    </div>
{/if}
<br><br>