<label ><b>Анкетные данные сотрудника</b></label><br>
{assign var=extra value=$p->getExtra()}

{*

<input value="{$p->carNumber|escape}" name='data[carNumber]' size='6' type='text' class='form-control input-xs input-inline' placeholder="Номер" />
<input value="{$p->carRegion|escape}" name='data[carRegion]' size='6' type="number" class='form-control input-xs input-inline' placeholder="Регион" />
*}
<div class='col-lg-6 text-center'>
    <input type="hidden" name="extra[photoUrl]" value="{$extra->photoUrl|escape}" />
    <div id="photoEdit" style="{if $extra->photoUrl!=''}display:none{/if}">
        <div class='col-lg-12'>
            <input data-ratio="1:1" data-onuploaded="changePhoto" name="file" type="file" class="uploader">

        </div>
    </div>
    <div id="photoView" style="{if $extra->photoUrl==''}display:none{/if}" class="text-center">
        <center><img src='{$extra->photoUrl|escape}' class='img-responsive text-center'>
        </center>
        <a href="#">Изменить фото</a>
    </div>


</div>
<div class='col-lg-6'>
    <div class="form-group"><input required="" {if !$can_edit}readonly{/if} type='text' class='form-control' name='extra[firstName]' value="{$extra->firstName|escape}" placeholder='Фамилия' /></div>
    <div class="form-group"><input required="" {if !$can_edit}readonly{/if} type='text' class='form-control' name='extra[secondName]' value="{$extra->secondName|escape}" placeholder='Имя' /></div>
    <div class="form-group"><input required="" {if !$can_edit}readonly{/if} type='text' class='form-control' name='extra[thirdName]' value="{$extra->thirdName|escape}" placeholder='Отчество' /></div>
    <div class="form-group"><input required="" {if !$can_edit}readonly{/if} type='text' class='form-control' name='extra[position]' value="{$extra->position|escape}" placeholder='Должность' /></div>
    <div class="form-group"><input required="" {if !$can_edit}readonly{/if} type='text' class='form-control' name='extra[docType]' value="{$extra->docType|escape}" placeholder='Тип документа (паспорт, права)' /></div>
    <div class="form-group"><input required="" {if !$can_edit}readonly{/if} type='text' class='form-control' name='extra[docNumber]' value="{$extra->docNumber|escape}" placeholder='Серия и номер документа' /></div>
    <div class="form-group"><input required="" {if !$can_edit}readonly{/if} type='text' class='form-control' name='extra[docDate]' value="{$extra->docDate|escape}" placeholder='Дата выдачи документа' /></div>
</div>
<br><br>
