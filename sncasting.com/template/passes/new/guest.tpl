<label ><b>Анкетные данные посетителя</b></label><br>
{assign var=extra value=$p->getExtra()}

{*

<input value="{$p->carNumber|escape}" name='data[carNumber]' size='6' type='text' class='form-control input-xs input-inline' placeholder="Номер" />
<input value="{$p->carRegion|escape}" name='data[carRegion]' size='6' type="number" class='form-control input-xs input-inline' placeholder="Регион" />
*}

<div class="form-group"><input required="" {if !$can_edit}readonly{/if} type='text' class='form-control' name='extra[firstName]' value="{$extra->firstName|escape}" placeholder='Фамилия' /></div>
<div class="form-group"><input required="" {if !$can_edit}readonly{/if} type='text' class='form-control' name='extra[secondName]' value="{$extra->secondName|escape}" placeholder='Имя' /></div>
<div class="form-group"><input required="" {if !$can_edit}readonly{/if} type='text' class='form-control' name='extra[thirdName]' value="{$extra->thirdName|escape}" placeholder='Отчество' /></div>
<div class="form-group"><input required="" {if !$can_edit}readonly{/if} type='text' class='form-control' name='extra[docType]' value="{$extra->docType|escape}" placeholder='Тип документа (паспорт, права)' /></div>
<div class="form-group"><input required="" {if !$can_edit}readonly{/if} type='text' class='form-control' name='extra[docNumber]' value="{$extra->docNumber|escape}" placeholder='Серия и номер документа' /></div>
<div class="form-group"><input required="" {if !$can_edit}readonly{/if} type='text' class='form-control' name='extra[docDate]' value="{$extra->docDate|escape}" placeholder='Дата выдачи документа' /></div>
<div class="form-group"><input required="" {if !$can_edit}readonly{/if} type='text' class='form-control' name='extra[visitTarget]' value="{$extra->visitTarget|escape}" placeholder='К кому' /></div>
<div class="form-group"><input required="" {if !$can_edit}readonly{/if} type='text' class='form-control' name='extra[visitHours]' value="{$extra->visitHours|escape}" placeholder='Период визита в часах' /></div>
<br><br>
