<label ><b>Автомобиль</b></label><br>
{*

<input value="{$p->carNumber|escape}" name='data[carNumber]' size='6' type='text' class='form-control input-xs input-inline' placeholder="Номер" />
<input value="{$p->carRegion|escape}" name='data[carRegion]' size='6' type="number" class='form-control input-xs input-inline' placeholder="Регион" />
*}
<div class='carPlate'>
    <div class='carNumber'>
        <input  {if !$can_edit}readonly{/if} placeholder="x999xx" value="{$p->carNumber|escape}" name='data[carNumber]' size='6' type='text' class=''  />
    </div>
    <div class='carRegion'>
        <div class='carRegionNumber'>
            <input {if !$can_edit}readonly{/if} value="{$p->carRegion|escape}" name='data[carRegion]' size='6' type="number" class='' placeholder="99" />
        </div>
        <div class='carRegionDesc'>
            RUS  <img style="position:relative;top:-3px" src="http://www.lsis-ru.com/bitrix/templates/lsis/images/russian-flag.png">
        </div>
    </div>

</div>
<br><br>
<input {if !$can_edit}readonly{/if} value="{$p->carDescription|escape}" name='data[carDescription]' class='form-control' placeholder="Описание авто (владелец/марка)" />