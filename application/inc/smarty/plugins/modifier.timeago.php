<?php

function smarty_modifier_timeago($input)
{
    $input = strtotime($input);
    $now = time();
    $diff = $now-$input;
    if( $diff< 60)
        return $diff.' секунд назад';
    if( $diff<60*60)
        return ceil ($diff/60)." минут назад";
    
    
    if( $diff<60*60*24)
        return ceil ($diff/(60*60))." часов назад";
    if( $diff<60*60*24*7)
        return ceil ($diff/(60*60*24))." дней назад";
    
    return date("d.m.Y h:i", $input);
    
    
    
    
    
}
