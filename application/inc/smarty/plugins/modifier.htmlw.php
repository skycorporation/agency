<?php
function smarty_modifier_htmlw($string)
{
    $html = strip_tags($string, '<b><br><i><u><s>');
    return $html;
}