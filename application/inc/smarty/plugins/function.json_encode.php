<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty json_encode function plugin
 */

    function arr2json($arr){
        foreach($arr as $k=>&$val){
            if( is_int($k))
                $json[] = php2js($val);
            else
                $json[] = "\"$k\":".php2js($val);
        }
        if(count($json) > 0) return "{".implode(",", $json)."}";
        else return '{}';
    }
    function php2js($val){
        if(is_array($val)) return arr2json($val);
        if(is_string($val)) return '"'.addslashes($val).'"';
        if(is_bool($val)) return 'Boolean('.(int) $val.')';
        if(is_null($val)) return '""';
        return $val;
    }

function smarty_function_json_encode($params, &$smarty)
{
    if (!isset($params['var']))
        return json_encode( array( ) );
    //var_dump($params['var']);
    $op = serialize( $params['var'] );
    $deop = unserialize($op);
    //return json_encode( $deop );
    $json = arr2json( $deop );
    $json = str_replace("{{", "[{", $json);
    $json = str_replace("}}", "}]", $json);
    //return '[{'.implode("},{",arr2json ( $deop ) ).'}]';
    return $json;
    
}

/* vim: set expandtab: */

?>
