<?php
function smarty_function_gen_output($params, &$smarty){
    if( isset($params['php']) || isset($params['mode']) && $params['mode']=='view') return '';
    
    $select = $smarty->generate['select'];
    
    
    $string = array();
    if( $select )
    foreach($select as $name=>$default){
        $string[]=array('name'=>$name,"value"=>$default);
        
    }
    $inputs= array();
    if( isset($smarty->generate['input']))
    foreach($smarty->generate['input'] as $name=>$default){
        $inputs[]=array('name'=>$name,"value"=>$default);
        
    }
    
    $desc=array();
    if( isset($smarty->generate['desc']))
    foreach($smarty->generate['desc'] as $name=>$default){
        $desc[]=array('name'=>$name,"text"=>$default['text'], "value"=>$default['value']);
        
    }
    return "<script> 
        gen_select=".json_encode($string)."; 
        gen_input=".json_encode($inputs)."; 
        gen_select_description =".json_encode($desc).";
            </script>";
}