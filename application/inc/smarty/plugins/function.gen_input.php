<?php
function smarty_function_gen_input($params, &$smarty){
    if( isset($params['php']) && isset($params['mode']) && $params['mode']=='view') return $params['php'];
    $v=$params['value'];
    $smarty->generate['input'][$params['name']]=$v;
    if( isset($params['mode']) && $params['mode']=='edit') $v=$params['php'];
    
    return "<span class=gen_input_span id=gen_input_span_{$params['name']} data-name={$params['name']}>{$v}</span>
        <input  class=\"gen_input\" style=\"display:none\" data-name=\"{$params['name']}\" id=gen_input_{$params['name']} name=gen_input[{$params['name']}] value=\"{$v}\">";
    
}
