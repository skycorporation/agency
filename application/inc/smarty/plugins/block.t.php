<?php

function smarty_block_t($params, $content, &$smarty)
{
    
    if (is_null($content)) {
        return;
    }
    $lang = $_SESSION['lang'];
    if(isset($params['lang'])) $lang=$params['lang'];
    
    $db = DB::getInstance();
    $hash = md5($content);
    
    $sql = "select result from `lang`  where orig_hash=? and result_lang=?";
    $stmt= $db->prepare($sql);
    $stmt->execute( array($hash, $lang));
    if( $stmt->rowCount()>0)
    {
        $row = $stmt->fetchObject();
        return $row->result;
    }
    else{
        $sql = 'insert into `lang` set orig=?, orig_hash=?, result=?, result_lang=?';
        $stmt=$db->prepare($sql);
        $stmt->execute(array( $content, $hash, $content, $lang));
    }
    return $content;
    
}

/* vim: set expandtab: */

?>
