<?php

function smarty_modifier_helper_link($str) {
    if( preg_match("/^http/", $str))
            return $str;
    if(trim($str)!='')
        return "http://".$str;
    return "";
}
