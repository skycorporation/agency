<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function smarty_function_success($params, &$smarty)
{
    $text = '';
    $rand = rand(1000,9999);
    if( isset($params['text']))
        $text = $params['text'];
    $pause = 3000;
    if( isset($params['delay']))
        $pause = $params['delay'];
    $speed = 700;
    return "<div class=\"tm_updated\" id=\"tm_update$rand\">$text</div>
        <script>
        setTimeout(\"$('#tm_update$rand').animate({opacity:'0'},$speed).slideUp($speed);\", $pause);
        </script>";

}