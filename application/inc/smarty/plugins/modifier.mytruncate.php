<?php
function smarty_modifier_mytruncate($string, $length = 200, $etc = '...',
                                  $break_words = false, $middle = false, $encoding='UTF-8')
{
    if(mb_strlen($string)>$length)
        return substr ($string, $length).'...';
    else
        return $string;
}