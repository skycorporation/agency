<?php
function smarty_modifier_dateToString($input, $show_date_text=  false)
{
    $month = array(
        'Jan' => 'Января',
        'Feb' => 'Февраля',
        'Mar' => 'Марта',
        'Apr' => 'Апреля',
        'May' => 'Мая',
        'Jun' => 'Июня',
        'Jul' => 'Июля',
        'Aug' => 'Августа',
        'Sep' => 'Сентября',
        'Oct' => 'Октября',
        'Nov' => 'Ноября',
        'Dec' => 'Декабря',
    );
    $dayOfWeeks = array(
        'воскресенье', "понедельник", "вторник", "среда", "четверг", "пятница", "суббота"
    );
    
    $prefix = '';
    
    if($show_date_text) 
        return date("d", strtotime($input)) ." ".$month[date("M", strtotime($input))];
    
    $result = date("d", strtotime($input))
        ." ".$month[date("M", strtotime($input))].", ". $dayOfWeeks[date("w", strtotime($input))];
    
    if( date("Y-m-d") == date("Y-m-d", strtotime($input)))
            $prefix = 'Сегодня, ';
    if( date("Y-m-d", time()+60*60*24) == date("Y-m-d", strtotime($input)))
            $prefix = 'Завтра, ';
    
    if( date("Y-m-d", time()+60*60*48) == date("Y-m-d", strtotime($input)))
            $prefix = 'Послезавтра, ';
    
    return $prefix.$result;
}
