<?php

function smarty_modifier_closetags($html){
  #put all opened tags into an array
  preg_match_all("#<([a-z]+)( .*)?(?!/)>#iU",$html,$result);
  $openedtags=$result[1];
  // tags=>count
  $opened = array();
  $closed = array();
  for($i=0;$i<count($openedtags);$i++)
    if( isset($opened[strtolower($openedtags[$i])]))
        $opened[strtolower($openedtags[$i])]++;
    else
        $opened[strtolower($openedtags[$i])]=1;
    
  preg_match_all("#</([a-z]+)>#iU",$html,$result);
  for($i=0;$i<count($result[1]);$i++)
    if( isset($closed[strtolower($result[1][$i])]))
        $closed[strtolower($result[1][$i])]++;
    else
        $closed[strtolower($result[1][$i])]=1;
    
    $opened = array_reverse( $opened );
    foreach($opened as $tag=>$count){
        $diff=0;
        if(!isset($closed[$tag]))
            $diff = $opened[$tag];
        else
            $diff = $opened[$tag]-$closed[$tag];
        if( $diff < 0 ) $diff = 0;
        
        for($i=0;$i<$diff;$i++) $html .= "</".$tag."> ";
    }
  return $html;
}
