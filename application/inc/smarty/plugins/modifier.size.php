<?php
function smarty_modifier_size($size)
{
    if ($size < 1024)
        return $size . "b";
    if ($size < 1024 * 1024)
        return round($size / (1024), 2) . "Kb";
    if ($size < 1024 * 1024 * 1024)
        return round($size / (1024 * 1024), 2) . "Mb";
    if ($size < 1024 * 1024 * 1024 * 1024)
        return round($size / (1024 * 1024 * 1024), 2) . "Gb";
}