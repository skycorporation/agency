<?php



function smarty_modifier_ltranslit($str='', $encoding='UTF-8') {

    if( $str == '') return 'new';
    $ru = 'й,ф,я,ч,ы,ц,у,в,с,м,а,к,е,п,и,т,р,н,г,о,ь,л,ш,щ,д,з,б,ж,х,ю, ,-';
    $en = 'i,f,ya,ch,i,c,u,v,s,m,a,k,e,p,i,t,r,n,g,o,,l,sh,sh,d,z,b,zh,h,u,-,-';
    $ru_arr = explode(',', $ru);
    $en_arr = explode(',', $en);
    
    $new = mb_strtolower($str, $encoding);
    $new = preg_replace("/[^a-zа-я0-9_-\s]/iu", "", $new);

    for($i=0; $i < count($ru_arr); $i++) {
        $new = str_replace($ru_arr[$i], $en_arr[$i], $new);
    }

    return $new;
}

