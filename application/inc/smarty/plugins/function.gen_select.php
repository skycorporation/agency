<?php

function smarty_function_gen_select($params, &$smarty)
{
    if( isset($params['php']) && isset($params['mode']) && $params['mode']=='view') return $params['php'];
    
    $v=explode("|", $params['value']);
    $smarty->generate['select'][$params['name']]=$v;
    
    $var=$v[0];
    if( isset($params['mode']) && $params['mode']=='edit') $var=$params['php'];
    
    return "<input name=gen_select_input[{$params['name']}] value=\"{$var}\" id=gen_select_input_{$params['name']} style=\"display:none\">
        <span class=gen_select id=gen_select_{$params['name']}>{$var}</span>";
    
}
