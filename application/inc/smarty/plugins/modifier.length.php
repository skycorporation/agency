<?php
function smarty_modifier_length($string)
{
    //var_dump($string);
    if( is_array( $string ) )
        return count($string);
    if( $string instanceof PDOStatement )
        return $string->rowCount();
    return strlen($string);
}