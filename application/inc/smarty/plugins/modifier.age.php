<?php

function smarty_modifier_age($string)
{
    $enum = array(
      1=> 'лет',
      2=> 'год',
      3=> 'года',
    );
    if( $string > 100 ) return $enum[1];
    $last = $string%10;
    switch($last)
    {
        case 1:
            return $enum[2];
        case 2:
            return $enum[3];
        default:
            return $enum[1];
    }
    

}
?>