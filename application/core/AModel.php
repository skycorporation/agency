<?php

class AModel {

    /**
     *
     * @var DB
     */
    protected $db;

    /**
     *
     * @var User
     */
    protected $user;
    protected $is_ajax = false;

    public function isAuth() {        
        if (!User::getId()) {
           System::redirect('/login');
        }
        try {
            $this->user->load(User::getId());
        } catch (Exception $e) {
            System::redirect('/?error=' . $e->getCode());
        }
    }
    
    public function checkAuth() {        
        if (!User::getId()) {
           System::redirect('/login');
        }
    }

    public function __construct() {
        @session_set_cookie_params(60 * 60 * 24, '/');
        @session_start();
        if ((isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') || isset($_SERVER['HTTP_X_CSRF_TOKEN']))
            $this->is_ajax = true;
        $this->db = DB::getInstance();
        
        $this->user = new User;
        $this->user->load(User::getId());
        Tpl::$globals['my'] = $this->user;
    }

}
