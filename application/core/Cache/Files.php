<?php

class Cache_Files implements ICache {

    protected $_dir = '../cache/data/';

    public function get($name, $def = false) {
        return $def;
        $key = md5($name);

        if (file_exists($this->_dir . $key[0] . "/" . $key[1] . "/" . $key)) {

            $data = @json_decode(file_get_contents($this->_dir . $key[0] . "/" . $key[1] . "/" . $key), 1);

            if ($data['exp'] < time()) {
                $this->delete($name);
                return $def;
            }
            return $data['data'];
        }
        return $def;
    }

    public function __construct($settings = array()) {
        if (isset($settigs['dir']))
            $this->_dir = $settings['dir'];
    }

    public function delete($name) {
        $key = md5($name);
        unlink($this->_dir . $key[0] . "/" . $key[1] . "/" . $key);
    }

    public function set($name, $value = true, $expire = 60) {
        $data['data'] = $value;
        $data['exp'] = time() + $expire * 60;
        $key = md5($name);
        @mkdir($this->_dir . $key[0] . "/" . $key[1] . "/", 0755, true);
        fwrite(fopen($this->_dir . $key[0] . "/" . $key[1] . "/" . $key, 'wb'), json_encode($data));
        return $value;
    }

}
