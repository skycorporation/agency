<?php

class Cache_Memcache implements ICache {

    /**
     *
     * @var Memcache
     */
    public $memcache = null;

    public function get($name) {
        return @$content = $this->memcache->get($name);
    }

    public function __construct($settings = array()) {
        $config = Registry::get('config');
        
        if (isset($config['memcache_server']) && $config['memcache_server'] != '') {
            list($ip, $port) = explode(":", $config['memcache_server']);
        }
        $this->memcache = new Memcache;
        @$this->memcache->connect($ip, $port);
    }

    public function delete($name) {
        return @$this->memcache->delete($name);
    }

    public function set($name, $value = true, $expire = 60) {
        @$this->memcache->set($name, $value, false, $expire * 60);
        return $value;
    }

}
