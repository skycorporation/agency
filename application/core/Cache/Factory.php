<?php

class Cache_Factory
{
    /**
     *
     * @param <type> $settings
     * @return ICache
     */
    static public function factory( $settings )
    {
        if( $settings['engine']=='file')
            return new Cache_Files ( $settings );
        if( $settings['engine']=='memcache')
            return new Cache_Memcache ( $settings );

    }
}
