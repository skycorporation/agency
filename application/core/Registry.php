<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Registry
 *
 * @author Антон
 */
class Registry {
    protected static $_instance;
    protected static $_var;
    public static function getInstance()
    {
        if( self::$_instance == null)
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    
    static public function get($name)
    {
        if( isset( self::$_var[$name])) return self::$_var[$name];
        return false;
    }
    static public function set($name, $value)
    {
        self::$_var[$name]=$value;
    }

}
