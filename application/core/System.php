<?php

class System {

    static function Redirect($url, $code=200) {
        if (headers_sent()) {
            echo ('<script> top.location=\'' . $url . '\';</script>');
            exit;
        }
        else{
            if( $code =='404')
                header('HTTP/1.0 404 Not Found');
            header("Location: $url");
        }
        exit;
    }

    static function translit($str) {
        $ru = 'й,,ф,,я,,ч,,ы,,ц,,у,,в,,с,,м,,а,,к,,е,,п,,и,,т,,р,,н,,г,,о,,ь,,л,,ш,,щ,,д,,з,,б,,ж,,х,,ю,, ,,-,,.,,,,,';
        $ru .= 'Й,,Ф,,Я,,Ц,,Ы,,Ч,,У,,В,,С,,К,,А,,М,,Е,,П,,И,,Н,,Р,,Т,,Г,,О,,Ь,,Ш,,Л,,Щ,,Д,,Б,,З,,Х,,Ъ,,';
        
        $en = 'i,,f,,ya,,ch,,i,,c,,u,,v,,s,,m,,a,,k,,e,,p,,i,,t,,r,,n,,g,,o,,_,,l,,sh,,sh,,d,,z,,b,,zh,,h,,u,,-,,-,,_,,_,,';
        $en .= 'I,,F,,YA,,C,,I,,CH,,U,,V,,S,,K,,A,,M,,E,,P,,I,,N,,R,,T,,G,,O,,_,,SCH,,L,,SCH,,D,,B,,Z,,H,,_,,';
        
        $ru_arr = explode(',,', $ru);
        $en_arr = explode(',,', $en);

        $new = strtolower($str);
        $new = preg_replace("/[^a-zа-я0-9_-\s]/iu", "", $new);

        for ($i = 0; $i < count($ru_arr); $i++) {
            $new = str_replace($ru_arr[$i], $en_arr[$i], $new);
        }
        $new = str_replace("--", "-", $new);
        $new = str_replace("_", "_", $new);
        

        return $new;
    }

}