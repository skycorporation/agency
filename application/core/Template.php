<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

/**
 * Description of Template
 *
 * @author Антон
 */
class Template {

    protected $content;
    protected $vars=array();
    protected $dir;
    /**
     *
     * @var Smarty
     */
    protected $smarty;
    public function __construct( $directory = 'template/' ) {
        $this->dir = PATH_START.$directory;
        
        
        $smarty = new Smarty();
        $smarty->template_dir = PATH_START."template/";
        $smarty->compile_dir = PATH_APPLICATION."cache/smarty/";
        $smarty->config_dir = PATH_APPLICATION."cache/smarty/";
        $smarty->cache_dir = PATH_APPLICATION."cache/smarty/";
        $smarty->caching =0 ;
        $this->smarty = $smarty;
        
        $url = substr($_SERVER["REQUEST_URI"],1);
        $pos = strpos($url,'/');
        if( $pos>0)
            $url = substr($url, 0, $pos);
        Tpl::$globals['section']=$url;
        

    }
    
    public function get($filename, $vars=array()) {
        
        $this->vars += $vars;
        
        foreach($this->vars as $name=>$value)
                $this->smarty->assign($name, $value);
        
        $this->smarty->display($filename);
    }
    public function fetch($filename, $vars=array()){
        $this->vars += $vars;
        foreach($this->vars as $name=>$value)
                $this->smarty->assign($name, $value);
        
        return $this->smarty->fetch($filename);
    }

    public function __set($name, $value) {
        $this->vars[$name]=$value;
    }


}
class Tpl {
    static $globals = array();
    static public function get($filename, $vars=array()) {
        $tpl = new Template();
        return $tpl->get($filename, $vars+self::$globals);
    }
    static public function fetch($filename, $vars=array()) {
        $tpl = new Template();
        return $tpl->fetch($filename, $vars+self::$globals);
    }

}