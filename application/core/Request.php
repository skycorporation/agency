<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Request
 *
 * @author Антон
 */
class Request {
    protected $data;
    public function __construct(&$array)
    {
        
        $this->data = $array;
    }
    public function &__get($name)
    {
        $null = null;
        if( !isset($this->data[$name]))
        {
                $str = explode("_", $name,2);
                if( count( $str ) == 2)
                {
                    if(!isset($this->data[$str[0]][$str[1]]))
                        return $null;
                    return $this->data[$str[0]][$str[1]];
                }
                else
                {
                    return $null;
                }
        }
        return $this->data[$name];
    }
    public function  __isset($name) {
        return isset($this->data[$name]);
    }
}