<?php

abstract class Table implements JsonSerializable {

    /**
     *
     * @var DB
     */
    protected $_db;
    protected $_table = '';
    protected $_fields = array();

    public function jsonSerialize() {
        return $this->_fields;
    }

    public function __construct() {
        $this->_db = DB::getInstance();
    }

    public function __set($name, $value) {
        $this->_fields[$name] = $value;
    }

    public function &__get($name) {
        if (isset($this->_fields[$name]))
            return $this->_fields[$name];
        $a = false;
        return $a;
    }

    public function getFields() {
        return $this->_fields;
    }

    public function __isset($name) {
        return isset($this->_fields[$name]);
    }

    public function delete($id = 0) {
        if ($id == 0)
            $id = $this->_fields['id'];
        $sql = "delete from " . $this->_table . " where id=?";
        $stmt = $this->_db->prepare($sql);
        $stmt->execute(array($id));
        if ($stmt->rowCount() == 0)
            return false;
        return $this;
    }

    public function insert() {
        $sql = "insert into " . $this->_table . " set `" . trim(implode("`=?,`", array_keys($this->_fields)), ',`') . "`=?";
        $stmt = $this->_db->prepare($sql);
        try {
            $stmt->execute(array_values($this->_fields));
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }

        if ($stmt->rowCount() == 0)
            return false;
        $this->id = $this->_db->lastInsertId();
        return $this;
    }

    public function update($values) {
        $sql = "update " . $this->_table . " set `" . trim(implode("`=?,`", array_keys($values)), ',`') . "`=? where id=?";

        $stmt = $this->_db->prepare($sql);
        try {
            $stmt->execute(array_merge(array_values($values), array($this->_fields['id'])));
        } catch (Exception $e) {
            throw $e;
            return false;
        }
        foreach ($values as $k => $v) {
            $this->$k = $v;
        }
        if ($stmt->rowCount() == 0)
            return false;

        return $this;
    }

    protected $_limit = false;

    public function setLimit($limit) {
        $this->_limit = $limit;
    }

    public function toKeyValue($input = array()) {
        if (empty($input))
            return array();
        $output = array();
        foreach ($input as $k => $v) {

            if (isset($v->id)) {
                $output[$v->id] = $v;
            }
        }
        return $output;
    }

    public function findAll($orderBy = false, $where = false) {
        $sql = "select * from " . $this->_table;
        if ($where)
            $sql .= " where " . $where;
        if ($orderBy)
            $sql .= " order by " . $orderBy;
        else
            $sql .= " order by id desc";
        if ($this->_limit) {
            $sql .= " limit " . $this->_limit;
            $this->_limit = false;
        }
        $stmt = $this->_db->query($sql);
        if ($stmt->rowCount() == 0)
            return false;
            
        $col = array();
        while ($row = $stmt->fetchObject()) {
            $elem = new static($this->_table);
            $elem->setRow($row);
            $col[] = $elem;
        }
        return $col;
    }

    public function findByValue($field, $value, $sortBy = false, $limit = 0, $offset = 0) {
        $this->_db = DB::getInstance();
        $sql = "select * from " . $this->_table . " where `$field`=? ";
        if ($sortBy)
            $sql .= " order by " . $sortBy;
        if ($offset > 0)
            $sql .= " limit " . $limit . ", " . $offset;
        elseif ($limit > 0) {
            $sql .= " limit " . $limit;
        }

        $stmt = $this->_db->prepare($sql);
        $stmt->execute(array($value));
        if ($stmt->rowCount() == 0) {
            return false;
        }
        $col = array();
        while ($row = $stmt->fetchObject()) {
            $elem = new static($this->_table);
            $elem->setRow($row);
            $col[] = $elem;
        }
        return $col;
    }

    public function findById($id) {
        $sql = "select * from " . $this->_table . " where id=? limit 1";
        $stmt = $this->_db->prepare($sql);
        $stmt->execute(array($id));
        if ($stmt->rowCount() == 0)
            return false;
        return $this->setRow($stmt->fetchObject());
    }

    public function setRow($row) {
        foreach ($row as $k => $v)
            $this->$k = $v;
        return $this;
    }

}
