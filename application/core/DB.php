<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DB
 *
 * @author Антон
 */
class DB {

    /**
     *
     * @var PDO
     */
    protected $db;
    protected static $_instance = array();
    public $_sql = array();

    public function trans_begin() {
        return $this->db->beginTransaction();
    }

    public function trans_commit() {
        return $this->db->commit();
    }

    public function trans_rollback() {
        return $this->db->rollBack();
    }

    public function quote($s) {
        return $this->db->quote($s);
    }

    /**
     *
     * @param <type> $sql
     * @return PDOStatement
     */
    public function prepare($sql) {
        return $this->db->prepare($sql);
    }

    protected function __construct($config) {
        if (empty($config)) {
            $config = parse_ini_file('../config.ini');
        }

        $reg = new Request($config);
        $this->db = new PDO($reg->db_dsn, $reg->db_user, $reg->db_pass, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        //$this->db = new PDO("mysql:host=localhost;dbname=sncasting;", "sncasting", "foomanji", array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        $this->db->query('SET NAMES UTF8');
    }

    public function debug() {
        return implode("<br>", $this->_sql);
    }

    /**
     *
     * @param <type> $sql
     * @return PDOStatement
     */
    public function query($sql) {
        $this->_sql[] = $sql;
        return $this->db->query($sql);
    }

    public function fetchObject(PDOStatement $stmt) {
        return $stmt->fetchObject();
    }

    public function total(PDOStatement $stmt) {
        return $stmt->rowCount();
    }

    /**
     *
     * @param array $config
     * @return DB
     */
    public static function getInstance($config = array()) {
        $key = md5(serialize($config));
        if (!isset(self::$_instance[$key]))
            self::$_instance[$key] = new self($config);
        return self::$_instance[$key];
    }

    public function lastInsertId() {
        return $this->db->lastInsertId();
    }

}
