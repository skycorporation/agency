<?php

class Router {

    const DEFAULT_ZONE = '';

    protected $is_sub = false;
    protected $zone = '';
    protected $rules;
    protected $auto = false;
    public $debug_url;

    public function auto($set = true) {
        $this->auto = $set;
    }

    protected $subdomains = array();
    public $debug_subdomain = false;

    public function subdomain($req, $function) {
        $this->subdomains[$req] = $function;
        $this->zone = $req;
    }

    public function zone($zone) {
        $this->zone = $zone;
    }

    public function add($pattern, $settings) {
        $this->rules[$this->zone][$pattern] = $settings;
    }

    public function run($request = '') {
        if ($request == '')
            $request = $_SERVER['REQUEST_URI'];
        if ($this->debug_url != '') {
            $this->debug_subdomain = parse_url($this->debug_url, PHP_URL_HOST);

            if (count(explode(".", $this->debug_subdomain)) == 3)
                $this->is_sub = true;
            $request = parse_url($this->debug_url, PHP_URL_PATH);
        }
        foreach ($this->subdomains as $pattern => $function) {
            
            if (preg_match_all("#" . $pattern . "\..*?\.#is", !$this->debug_subdomain ? getenv("HTTP_HOST") : $this->debug_subdomain, $result)) {
                
                $function($result);
            }
        }
        
        foreach ($this->rules as $sub => $rules) {


            if (
                    ($this->is_sub == false && $sub == '')
                    || ($sub!='' && preg_match("/" . $sub . "\.*.?\./is", !$this->debug_subdomain ? getenv("HTTP_HOST") : $this->debug_subdomain) )
            ) {

                foreach ($rules as $pattern => $settings) {

                    $result = array();
                    // echo preg_match("#".$pattern."#", $request);

                    if (preg_match_all("#" . $pattern . "#is", $request, $result)) {
                    
                        $parsed_vars = array();
                        for ($i = 1; $i < count($result); $i++) {
                            $parsed_vars[] = $result[$i][0];
                        }
                        $settings['_parsed'] = $parsed_vars;

                        return $settings;
                    }
                }
            }
        }

        return false;
    }

}