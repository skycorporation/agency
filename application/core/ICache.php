<?php
interface ICache
{
    public function get( $name, $def=false );
    public function set( $name, $value=true, $expire=60);
    public function delete($name);
}
?>
