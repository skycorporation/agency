<?php

class Profile98908Model extends Admin {

    public function EditAction() {
        $is_admin = Services::isAvailable(Services::SERVICE_ADMINISTRATOR) || Services::isAvailable(Services::SERVICE_USER_MANAGER);

        $userid = User::getId();

        if (isset($_REQUEST['userid']) && $is_admin) {
            $userid = (int) $_REQUEST['userid'];
        }

        $user = new User;
        //var_dump($_REQUEST);
        try {
            $user->load($userid, true);
        } catch (Exception $e) {
            exit($e->getMessage());
        }
        $post = new Request($_POST);
        if (isset($_FILES['file']) && !empty($_FILES['file']) > 0) {
            move_uploaded_file($_FILES['file']['tmp_name'], $user->getPhotoUrl(true));
            echo json_encode($user->getPhotoUrl());
            exit;
        }
        if (isset($post->action)) {
            if ($post->action == 'save') {
                try {
                    $user->update($post->data);
                    $user->setNames($post->name);

                    $config = Registry::get('config');
                    if ($post->pass['p1'] != '' && $post->pass['p2'] == $post->pass['p1'] && md5($config->salt . $post->pass['old']) == $user->getField('pass')) {
                        $new = md5($config->salt . $post->pass['p1']);
                        $user->update(array(
                            'pass' => $new,
                        ));
                    }
                    if ($is_admin) {

                        if (isset($post->data['is_admin'])) {
                            $sql = "update users set is_admin=0 where companyID=" . $user->getCompanyID();
                            $this->db->query($sql);
                        }
                        $user->update(array(
                            'is_admin' => intval(isset($post->data['is_admin']))
                        ));
                        Services::deactivateAll($user);
                        if (isset($post->services)) {
                            foreach ($post->services as $id) {
                                Services::activate($id, $user);
                            }
                        }
                        System::redirect('/profile/edit?success&userid=' . $userid);
                    }
                } catch (Exception $e) {
                    
                }
            }
        }




        $js[] = 'users/edit.js';
        Tpl::get('users/edit.tpl', array(
            'js' => $js,
            'user' => $user,
            'success' => isset($_GET['success']),
            'is_admin' => $is_admin,
        ));
    }

}
