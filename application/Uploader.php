<?php

class UploaderModel extends AModel {

    public function CropAction() {
        $post = new Request($_POST);
        if (!isset($post->url, $post->x, $post->y, $post->width, $post->height)) {
            throw new Exception("Wrong args");
        }
        $crop = new Cropper;
        $crop->setPath('.' . parse_url($post->url, PHP_URL_PATH))
                ->setCoors([
                    'x' => $post->x,
                    'y' => $post->y,
                    'width' => $post->width,
                    'height' => $post->height,
                ])
                ->crop();
        echo json_encode(array('url' => $post->url));
    }

    public function UploadAction() {
        $res = [];

        $dest = 'images/other/' . basename($_FILES['file_data']['name']) . '_' . rand(1, 99999) . '.jpg';
        move_uploaded_file($_FILES['file_data']['tmp_name'], $dest);
        $res['url'] = 'http://' . getenv("HTTP_HOST") . '/' . $dest;
        echo json_encode($res);
    }

}
