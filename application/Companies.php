<?php

class CompaniesModel extends Admin {

    public function __construct() {
        parent::__construct();
        if (!Services::isAvailable(Services::SERVICE_COMPANIES_MANAGER) && !Services::isAvailable(Services::SERVICE_ADMINISTRATOR)) {
            System::redirect('/dashboard?error=403');
        }
    }

    public function EditAction() {
        $is_admin = Services::isAvailable(Services::SERVICE_ADMINISTRATOR) || Services::isAvailable(Services::SERVICE_USER_MANAGER);
        $is_admin_rekv = Services::isAvailable(Services::SERVICE_ADMINISTRATOR) || Services::isAvailable(Services::SERVICE_INVOICE);

        $cid = 0;
        if (isseT($_REQUEST['companyid']))
            $cid = $_REQUEST['companyid'];
        else {
            System::redirect('/');
        }

        $company = new Company;

        try {
            $company->load($cid, true);
        } catch (Exception $e) {
            exit($e->getMessage());
        }
        $post = new Request($_POST);
        if (isset($_FILES['file']) && !empty($_FILES['file']) > 0) {
            move_uploaded_file($_FILES['file']['tmp_name'], $company->getPhotoUrl(true));
            echo json_encode($company->getPhotoUrl());
            exit;
        }
        if (isset($post->action)) {
            if ($post->action == 'save') {
                try {
                    $company->update($post->data);
                    System::redirect('/company/edit?companyid='.$cid.'&success');
                } catch (Exception $e) {
                    
                }
            }
        }




        $js[] = 'companies/edit.js';
        Tpl::get('companies/edit.tpl', array(
            'js' => $js,
            'company' => $company,
            'success' => isset($_GET['success']),
            'user' => new User,
            'is_admin' => $is_admin_rekv,
        ));
    }

    public function ListAction() {
        $post = new Request($_POST);
        if (isset($post->action)) {
            if ($post->action == 'create' && isset($post->data)) {
                $ret = array();
                $ret['status'] = 'error';
                try {
                    $companyRow = new stdClass();
                    foreach ($post->data As $k => $v) {
                        $companyRow->$k = $v;
                    }

                    $company = new Company;
                    $config = Registry::get('config');

                    if (!$company->addCompany($companyRow)) {
                        throw new Exception("Ошибка при добавлении компании");
                    }
                    $ret['status'] = 'ok';
                } catch (Exception $e) {
                    $ret['error'] = $e->getMessage();
                }
                echo json_encode($ret);
                exit;
            }
            if ($post->action == 'block' && isset($post->companies) && is_array($post->companies) && count($post->companies) > 0) {
                $sql = "update companies set is_blocked=1 where id in (" . implode(',', $post->companies) . ") ";
                $this->db->query($sql);
                exit;
            }
            if ($post->action == 'unblock' && isset($post->companies) && is_array($post->companies) && count($post->companies) > 0) {
                $sql = "update companies set is_blocked=0 where id in (" . implode(',', $post->companies) . ") ";
                $this->db->query($sql);
                exit;
            }
            if ($post->action == 'delete' && isset($post->companies) && is_array($post->companies) && count($post->companies) > 0) {
                $sql = "delete from companies where id in (" . implode(',', $post->companies) . ") ";
                $this->db->query($sql);
                exit;
            }
            if ($post->action == 'getCompanies' && isset($post->tid)) {
                $companies = array();
                if ($post->tid > 0) {
                    // selected
                    $type = new Company_Type($post->tid);
                    $companies = $type->getCompanies();
                } else {
                    $companies = (new Company)->findAll();
                }

                Tpl::get('companies/block.tpl', array(
                    'companies' => $companies,
                    'tmpcompany' => new Company,
                ));
                exit;
            }
        }
        $js[] = 'companies/list.js';
        Tpl::get('companies/list.tpl', array(
            'js' => $js,
        ));
    }

}
