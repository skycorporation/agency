<?php

class Worker_Downloader {

    public function run($vars) {
        if (!$vars['url'])
            return false;
        $content = @file_get_contents($vars['url']);
        $fn = md5($vars['recordID']);
        @mkdir("images/records/" . $fn[0] . "/" . $fn[1] . "/", 0777, 1);
        file_put_contents("images/records/" . $fn[0] . "/" . $fn[1] . "/" . $fn . ".jpg", $content);
        return true;
    }

}
