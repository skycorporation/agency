<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Fine
 *
 * @author Anton
 */
class Worker_Fine {

    public function run($vars) {

        $sql = "insert into fines set "
                . " companyID=?,"
                . " is_payed=0, "
                . " amount=?,"
                . " creationDateTime=?,"
                . " recordID=?,"
                . " description=? ";

        $db = DB::getInstance();
        $stmt = $db->prepare($sql);
        $stmt->execute(array(
            $vars['companyID'],
            $vars['amount'],
            date("Y-m-d H:i:s"),
            $vars['recordID'],
            $vars['desc']
        ));

        $fineId = $db->lastInsertId();

        $db->query("update records set fineID=" . $fineId . " where id=" . $vars['recordID']);

        $company = new Company($vars['companyID']);

        $row = new stdClass();
        $row->companyID = $company->id;
        $row->authorID = $company->getAdmin()->id;
        $row->amount = $vars['amount'];
        $row->creationDateTime = date("Y-m-d H:i:s");
        $row->fineID = $fineId;
        $row->positions = '[]';

        $i = new Invoice;
        $i->add($row);

        //send letter
    }

}
