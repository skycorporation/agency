<?php

class Worker_Invoice {

    //put your code here
    public function run($vars) {

        $inv = new Invoice;
        $user = new User;
        $user->load($vars['authorID']);

        $inv->setUser($user);
        $inv->addPosition($vars['sub'], 1);
        $ids = [];
        $ids[] = $vars['passID'];
        $cart = array();
        $cart[$vars['sub']] = 1;

        $i = new stdClass();
        $i->authorID = $vars['authorID'];
        $i->companyID = $vars['companyID'];
        $i->amount = $inv->total();
        $i->creationDateTime = date("Y-m-d H:i:s");
        $i->is_payed = 0;
        $i->adminID = 0;
        $i->positions = json_encode($cart);
        $i->fineID = 0;
        $inv->add($i);
        $inv->bindPasses($ids);
    }

}
