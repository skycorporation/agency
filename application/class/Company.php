<?php

class Company {

    public $id;
    protected $name;

    /**
     *
     * @var Table
     */
    public $table;

    /**
     *
     * @var Company_Type
     */
    protected $type;

    /**
     *
     * @var Company_Class
     */
    protected $class;
    protected $place;

    /**
     *
     * @var DB
     */
    protected $db;
    protected $is_blocked = false;
    protected $max_users = 0;
    protected $code;
    protected $data = array();
    protected $m2 = 0;

    public function getM2() {
        return $this->m2;
    }

    public function getDetails() {
        return $this->data;
    }

    public function getMaxUsers() {
        return $this->max_users;
    }

    protected $passes = false;

    public function getPasses() {
        if ($this->passes !== false)
            return $this->passes;

        $db = DB::getInstance();
        $sql = "select id from passes where companyID=" . $this->id;
        $stmt = $db->query($sql);

        if ($stmt->rowCount() == 0) {
            return [];
        }

        $col = [];
        while ($row = $stmt->fetchObject()) {
            $col[] = new Pass($row->id);
        }
        $this->passes = $col;

        return $col;
    }

    public function getInvoices($limit = false) {
        $col = [];
        $db = DB::getInstance();
        $sql = "select * from invoices where companyID=" . $this->id . " order by id desc";
        if ($limit) {
            $sql .= " limit " . $limit;
        }
        $stmt = $db->query($sql);
        if ($stmt->rowCount() == 0)
            return $col;
        while ($row = $stmt->fetchObject()) {
            $col[] = new Invoice($row->id);
        }
        return $col;
    }

    public function getCode() {
        return $this->code;
    }

    public function addCompany($user) {
        foreach ($user as $key => $value) {
            $this->table->$key = $value;
        }
        $ret = $this->table->insert();
        if (!$ret)
            return $ret;


        $this->load($this->table->id);
        return $ret;
    }

    public function getType() {
        return $this->type;
    }

    public function getClass() {
        return $this->class;
    }

    public function update($arr) {
        return $this->table->update($arr);
    }

    public function __construct($id = false) {
        $this->table = new SimpleTable('companies');
        $this->db = DB::getInstance();
        $this->type = new Company_Type;
        $this->class = new Company_Class;
        if ($id) {
            $this->load($id);
        }
    }

    public function getPlace() {
        return $this->place;
    }

    public function __toString() {
        if (!$this->class || !$this->name)
            return '[cid removed]';
        return $this->class . " \"" . $this->name . "\", ".$this->place;
    }

    public function isBlocked() {
        return $this->is_blocked;
    }

    public function getUsers() {

        $r = (new User)->findByCompanyID($this->getId());
        if ($r)
            return $r;
        return [];
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        return $this->id = $id;
    }

    public function load($id) {
        if (!$this->table->findById($id)) {
            throw new Exception("Company not found", 100);
        }
        $this->setName($this->table->name);
        $this->is_blocked = $this->table->is_blocked;
        $this->type->load($this->table->typeID);
        $this->class->load($this->table->classID);
        $this->place = $this->table->place;
        $this->setId($id);
        $this->max_users = $this->table->max_users;
        $this->code = $this->table->code;
        $this->m2 = $this->table->m2;
        $data = @json_decode($this->table->details, 1);
        if (!$data || !is_array($data)) {
            $this->data = [];
        } else {
            $this->data = $data;
        }
        //return true;
    }

    public function getPhotoUrl($raw = false) {
        $key = md5("somekeyCompany" . $this->id);
        if ($raw) {
            return "images/companies/" . $key . ".jpg";
        }


        if (file_exists("images/companies/" . $key . ".jpg")) {
            return "/images/companies/" . $key . ".jpg";
        }
        return "/images/companies/nophoto.jpg";
    }

    public function findByTypeID($id) {
        return $this->table->findAll("id desc", "`type`=" . $id);
    }

    public function findByClassID($id) {
        return $this->table->findAll("id desc", "`class`=" . $id);
    }

    public function findAll() {
        return $this->table->findAll();
    }

    public function getData() {
        return $this->table->jsonSerialize();
    }

    public function getAdmin() {
        $db = DB::getInstance();
        $sql = "select id from users where companyID=" . $this->id . " and is_admin=1";
        $stmt = $db->query($sql);
        if ($stmt->rowCount() == 0)
            return new User;
        $user = new User;
        $user->load($stmt->fetchObject()->id);
        return $user;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function canIRegisterByCode($code) {
        $sql = "select * from companies where code=?";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array($code));
        if ($stmt->rowCount() == 0) {
            throw new Exception("Код не найден", 100);
        }
        $row = $stmt->fetchObject();
        if ($row->is_blocked) {
            throw new Exception("Компания заблокирована", 101);
        }
        if ($row->max_users > 0 && $row->max_users <= $row->current_users) {
            throw new Exception("Превышен лимит пользователей", 102);
        }
        return $row->id;
    }

    public static function getList() {
        $db = DB::getInstance();
        $sql = "select * from companies order by id desc";
        $stmt = $db->query($sql);
        $col = array();
        while ($row = $stmt->fetchObject()) {
            $col[] = new Company($row->id);
        }
        return $col;
    }

}
