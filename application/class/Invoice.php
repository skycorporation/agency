<?php

class Invoice {

    /**
     *
     * @var SimpleTable
     */
    protected $table;
    protected $cart=[];

    /**
     *
     * @var User
     */
    protected $user;

    /**
     *
     * @var User
     */
    public $author;

    /**
     *
     * @var User
     */
    public $admin;

    /**
     *
     * @var Company
     */
    public $company;
    public $id, $fineID, $passID, $is_payed, $creationDateTime, $positions, $amount;

    public function setUser(User $user) {
        $this->user = $user;
    }

    protected $was = array();
    public $passes = false;

    public function bindPasses($ar) {

        $sql = "insert into invoices_passes set passID=?, invID=" . $this->id;

        $db = DB::getInstance();
        $stmt = $db->prepare($sql);
        if (!is_array($ar))
            $ar = [$ar];
        foreach ($ar as $id) {
            try {
                $stmt->execute(array($id));
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }
    }

    /**
     * 
     * @return \Pass
     */
    public function getPasses() {
        if ($this->passes !== false)
            return $this->passes;

        $db = DB::getInstance();
        $sql = "select passID from invoices_passes where invID=" . $this->id;
        $stmt = $db->query($sql);

        if ($stmt->rowCount() == 0) {
            return [];
        }

        $col = [];
        while ($row = $stmt->fetchObject()) {
            $col[] = new Pass($row->passID);
        }
        $this->passes = $col;

        return $col;
    }

    public function __construct($id = false) {
        $this->table = new SimpleTable("invoices");
        if ($id)
            $this->load($id);
    }

    public function load($id) {
        if (!$this->table->findById($id)) {
            throw new Exception("invoice not found", 100);
        }


        $this->id = $id;


        try {
            $this->author = new User;
            $this->author->load($this->table->authorID);
        } catch (Exception $e) {

            $this->author = new User;
        }

        try {
            $this->company = (new Company);
            $this->company->load($this->table->companyID);
        } catch (Exception $e) {
            $this->company = new Company;
        }
        $this->fineID = $this->table->fineID;
        $this->is_payed = $this->table->is_payed;
        $this->creationDateTime = $this->table->creationDateTime;
        try {
            $this->admin = new User;
            $this->admin->load($this->table->adminID);
        } catch (Exception $e) {
            $this->admin = new User;
        }
        $this->positions = json_decode($this->table->positions, 1);
        try {
            if ($this->author instanceof User)
                $this->setUser($this->author);
            if (!empty($this->positions)) {
                foreach ($this->positions as $sub => $count) {
                    $this->addPosition($sub, $count);
                }
            } else {
                $this->amount = $this->table->amount;
                $this->cart = [];
            }
        } catch (Exception $e) {
            $this->cart = [];
        }


        //return true;
    }

    public function add($row) {

        foreach ($row as $key => $value) {
            $this->table->$key = $value;
        }
        $ret = $this->table->insert();
        if (!$ret)
            return $ret;


        $this->load($this->table->id);
        return $ret;
    }

    /**
     * 
     * @param type $subId
     * @param type $count
     * @return \Invoice
     */
    public function addPosition($subId, $count) {

        $sub = new Passes_Sub($subId);
        if (in_array($sub->id, $this->was))
            return $this;
        $this->was[] = $sub->id;
        $this->cart[$subId] = $count;
        return $this;
    }

    public function getAmount($subId, $count) {
        $tmp = 0;
        $sub = new Passes_Sub($subId);
        $par = $sub->getPassType();
        $purchased = count($par->getPurchased($this->user));

        $passesPerMeter = 0;
        if ($par->norm > 0) {
            $passesPerMeter = floor($this->user->getCompany()->getM2() / $par->norm);
        }
        if (($count + $purchased) > $passesPerMeter) {
            $out = ($count + $purchased) - $passesPerMeter;
        } else {
            $out = $count - $passesPerMeter;
        }
        if ($out > $count)
            $out = $count;

        if ($out < 0)
            $out = $count;
        if ($out > 0 && $par->norm > 0) {
            $tmp += $out * $sub->outOfNormPrice;
        } else {
            $out = 0;
        }
        $inNorm = $count - $out;
        $tmp += $inNorm * $sub->normPrice;
        return $tmp;
    }

    public function total() {
        $total = 0;
        if (!empty($this->cart)) {
            foreach ($this->cart as $subId => $count) {
                $total+=$this->getAmount($subId, $count);
            }
        } else
            $total = $this->amount;
        return $total;
    }

    public function delete() {
        $passes = $this->getPasses();
        foreach ($passes as $pass) {
            $pass->proxyTable()->delete();
        }
        $this->table->delete();
    }

    public function isCanDelete() {
        $passes = $this->getPasses();
        if ($this->fineID > 0)
            return false;
        if ($this->is_payed == '1') {
            return false;
        }

        //если пропусков нет..., штрафы?
        if (empty($passes))
            return true;

        foreach ($passes as $pass) {
            if (!empty($pass->getRecords()))
                return false;
        }

        return true;
    }

    public function getCart() {
        return $this->cart;
    }

    public static function getByFineId($id){
        $db = DB::getInstance();
        $sql = "select id from invoices where fineID=".$id;
        $stmt = $db->query($sql);
        if ( $stmt->rowCount()==0) return false;
        return new self( $stmt->fetchObject()->id );
    }
    public static function getById($id) {
        return new self($id);
    }

    protected function changePayment($value) {
        $this->table->update(array(
            'is_payed' => $value,
            'adminID' => $this->user->id,
        ));
        $passes = $this->getPasses();
        foreach ($passes as $pass) {
            if ($value == 1)
                $pass->setPayment();
            else
                $pass->setUnPayment();
        }
        if ($this->fineID > 0) {
            $db = DB::getInstance();
            $stmt = $db->query("update fines set is_payed=" . $value . " where id=" . $this->fineID);
        }
    }

    public function setPayment() {

        return $this->changePayment(1);
    }

    public function setUnPayment() {
        return $this->changePayment(0);
    }

    public static function getListByFilter($where) {
        $db = DB::getInstance();
        $sql = "select * from invoices where $where order by id desc";
        $stmt = $db->query($sql);
        $col = array();
        while ($row = $stmt->fetchObject()) {
            $col[] = new self($row->id);
        }
        return $col;
    }

    public static function getList() {
        $db = DB::getInstance();
        $sql = "select * from invoices order by id desc";
        $stmt = $db->query($sql);
        $col = array();
        while ($row = $stmt->fetchObject()) {
            $col[] = new self($row->id);
        }
        return $col;
    }

}
