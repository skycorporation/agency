<?php

class Cropper {

    protected $filepath = false;
    protected $coors = false;

    public function setPath($path) {
        $this->filepath = $path;
        return $this;
    }

    /**
     * Устанавливает координаты для обрезки
     * @param array $array из 4х элементов, x,y,width,height
     * @return \Resizer
     */
    public function setCoors($array) {
        $this->coors = $array;
        return $this;
    }

    public function crop($newPath = false) {
        if (!$this->coors) {
            throw new Exception("No coors", 1);
        }
        if (!$this->filepath) {
            throw new Exception("No source");
        }
        list($width, $height, $type) = getimagesize($this->filepath);
        switch ($type) {
            case IMAGETYPE_JPEG: $imgCreate = 'ImageCreateFromJPEG';
                break;
            case IMAGETYPE_GIF: $imgCreate = 'ImageCreateFromGIF';
                break;
            case IMAGETYPE_PNG: $imgCreate = 'ImageCreateFromPNG';
                break;
            default: return false;
        }
        $image = $imgCreate($this->filepath);
        $cropped = imagecrop($image, $this->coors);
        imageinterlace($cropped, 1);
        $newPathSave = (!$newPath) ? $this->filepath : $newPath;
        imagejpeg($cropped, $newPathSave, 100);
        return $newPathSave;
    }

}
