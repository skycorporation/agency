<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Worker
 *
 * @author Anton
 */
class Worker {

    public static function addWork($workName, $vars = array()) {
        $db = DB::getInstance();
        $sql = "insert into works set creationDateTime=?, is_done=0, `type`=?, vars=?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array(
            date("Y-m-d H:i:s"),
            $workName,
            json_encode($vars))
        );
    }

    /**
     * 
     * @param type $type
     * @return \Worker_Downloader
     * @throws Exception
     */
    public static function get($type) {
        if ($type == 'download_number')
            return new Worker_Downloader;
        if ($type == 'make_fine')
            return new Worker_Fine;
        if( $type == 'make_invoice')
            return new Worker_Invoice();
        throw new Exception("Worker $type not found");
    }

    public function run() {
        $db = DB::getInstance();
        $sql = "select * from works where /*creationDateTime<(NOW()-INTERVAL 2 minute) and */is_done=0 order by id asc";
        $stmt = $db->query($sql);
        while ($task = $stmt->fetchObject()) {
            $vars = json_decode($task->vars, 1);
            $type = $task->type;
            try {
                Worker::get($type)->run($vars);
                $db->query("update works set is_done=1 where id=".$task->id);
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }
        }
    }

}
