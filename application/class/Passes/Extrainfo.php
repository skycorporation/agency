<?php

class Passes_Extrainfo {

    /**
     *
     * @var SimpleTable
     */
    protected $table;

    public function __get($name) {
        if (isset($this->_fields[$name])) {
            return $this->_fields[$name];
        }
    }

    public function __construct($id = false) {
        $this->table = new SimpleTable("passes_extrainfo");
        if ($id) {
            $this->load($id);
        }
    }

    public function update($arr) {
        return $this->table->update($arr);
    }

    public function delete() {
        return $this->table->delete();
    }

    public function add($row) {

        foreach ($row as $key => $value) {
            $this->table->$key = $value;
        }
        $ret = $this->table->insert();
        if (!$ret)
            return $ret;


        $this->load($this->table->id);
        return $ret;
    }

    public function load($id) {
        if (!$this->table->findById($id)) {
            throw new Exception("subpassType not found", 100);
        }

        foreach ($this->table->getFields() as $k => $v) {
            $this->$k = $v;
        }


        return $this;
    }

    public static function getById($id) {
        return new self($id);
    }

    public static function getByPassId($id) {

        $db = DB::getInstance();
        $sql = "select id from passes_extrainfo where passID=" . intval($id);
        $stmt = $db->query($sql);
        if ($stmt->rowCount() > 0) {
            return new static($stmt->fetchObject()->id);
        }
        $sql = "insert into passes_extrainfo set passID=" . intval($id);
        $db->query($sql);
        return static::getByPassId($id);
    }

}
