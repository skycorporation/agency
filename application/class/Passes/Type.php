<?php

class Passes_Type {

    /**
     *
     * @var SimpleTable
     */
    protected $table;
    public $name = '[removed]', $norm, $hourFrom, $hourTo, $is_blocked, $id, $fine, $finePeriod, $prepayment, $count, $type;

    /**
     *
     * @var DB
     */
    protected $db;

    public function __construct($id = false) {
        $this->table = new SimpleTable("passestype");
        $this->db = DB::getInstance();
        if ($id) {
            $this->load($id);
        }
    }

    /**
     * 
     * @return Passes_Sub
     */
    public function getMinPrice() {
        $sql = "select * from subpassestypes where is_blocked=0 and passTypeID=" . $this->id . " order by min(normPrice) asc limit 1";
        $stmt = $this->db->query($sql);
        return new Passes_Sub($stmt->fetchObject()->id);
    }

    /**
     * Обновляет инфо
     * @param type $arr
     * @return bool
     */
    public function update($arr) {
        return $this->table->update($arr);
    }

    public function setBlock() {
        if ($this->table->update(array(
                    'is_blocked' => 1,
                ))) {
            $subs = $this->getSubPasses();
            if (!$subs || empty($subs)) {
                return;
            }
            foreach ($subs as $sub) {
                $sub->setBlock();
            }
        }
    }

    public function setUnBlock() {
        if ($this->table->update(array(
                    'is_blocked' => 0,
                ))) {
            $subs = $this->getSubPasses();
            if (!$subs || empty($subs)) {
                return;
            }
            foreach ($subs as $sub) {
                $sub->setUnBlock();
            }
        }
    }

    public function delete() {
        
    }

    public function isBlocked() {
        return $this->is_blocked;
    }

    public function add($row) {
        foreach ($row as $key => $value) {
            $this->table->$key = $value;
        }
        $ret = $this->table->insert();
        if (!$ret)
            return $ret;


        $this->load($this->table->id);
        return $ret;
    }

    public function load($id) {
        if (!$this->table->findById($id)) {
            throw new Exception("passType not found", 100);
        }

        $this->is_blocked = $this->table->is_blocked;
        $this->name = $this->table->name;
        $this->norm = $this->table->norm;
        $this->hourFrom = $this->table->hourFrom;
        $this->hourTo = $this->table->hourTo;
        $this->count = $this->table->count;
        $this->fine = $this->table->fine;
        $this->finePeriod = $this->table->finePeriod;
        $this->prepayment = $this->table->prepayment;
        $this->type = $this->table->type;
        $this->id = $id;

        //return true;
    }

    /**
     * 
     * @return \Passes_Sub
     */
    public function getSubPasses() {
        return Passes_Sub::getListByTypeID($this->id);
    }

    public function getPurchased($userid = false) {
        $subs = $this->getSubPasses();
        if (empty($subs))
            return [];
        $total = [];
        foreach ($subs as $sub) {
            $list = $sub->getPasses();
            foreach ($list as $el) {
                $total[] = $el;
            }
        }
        return $total;
    }

    public function __toString() {
        return $this->name;
    }

    public static function getList($block = -1, $type = 'auto') {
        $db = DB::getInstance();
        $sql = "select * from passestype ";
        $where = [];
        if ($block !== -1) {
            $where [] = 'is_blocked=0';
        }
        if ($type) {
            $where[] = '`type`="' . $type . '"';
        }
        if (!empty($where)) {
            $sql .= ' where ' . implode(' and ', $where);
        }

        $sql .= "order by id desc";
        $stmt = $db->query($sql);
        $col = array();
        while ($row = $stmt->fetchObject()) {
            $col[] = new self($row->id);
        }
        return $col;
    }

}
