<?php

class Passes_Sub {

    /**
     *
     * @var SimpleTable
     */
    protected $table;
    public $is_blocked, $passTypeID, $name='[removed]', $normPrice, $outOfNormPrice, $stayValue, $stayPeriod, $fine, $finePeriod, $id=false;

    public function __construct($id = false) {
        $this->table = new SimpleTable("subpassestypes");
        if ($id) {
            $this->load($id);
        }
    }

    public function update($arr) {
        return $this->table->update($arr);
    }

    public function setBlock() {
        return $this->update(array('is_blocked' => 1));
    }

    public function setUnBlock() {
        return $this->update(array('is_blocked' => 0));
    }

    public function delete() {
        return $this->table->delete();
    }

    public function isBlocked() {
        return $this->is_blocked;
    }

    public function add($row) {

        foreach ($row as $key => $value) {
            $this->table->$key = $value;
        }
        $ret = $this->table->insert();
        if (!$ret)
            return $ret;


        $this->load($this->table->id);
        return $ret;
    }

    public function getPassType() {
        return new Passes_Type($this->passTypeID);
    }

    public function load($id) {
        if (!$this->table->findById($id)) {
            throw new Exception("subpassType not found", 100);
        }

        $this->is_blocked = $this->table->is_blocked;
        $this->passTypeID = $this->table->passTypeID;
        $this->name = $this->table->name;
        $this->normPrice = $this->table->normPrice;
        $this->outOfNormPrice = $this->table->outOfNormPrice;
        $this->stayValue = $this->table->stayValue;
        $this->stayPeriod = $this->table->stayPeriod;
        $this->fine = $this->table->fine;
        $this->finePeriod = $this->table->finePeriod;
        $this->days = $this->table->days;
        $this->id = $id;

        //return true;
    }

    public function __toString() {
        return $this->name;
    }

    public static function getListByTypeID($id) {
        $db = DB::getInstance();
        $sql = "select * from subpassestypes where passTypeID=$id order by id desc";
        $stmt = $db->query($sql);
        $col = array();
        while ($row = $stmt->fetchObject()) {
            $col[] = new self($row->id);
        }
        return $col;
    }

    public static function getById($id) {
        return new self($id);
    }

    public static function getList() {
        $db = DB::getInstance();
        $sql = "select * from subpassestypes order by id desc";
        $stmt = $db->query($sql);
        $col = array();
        while ($row = $stmt->fetchObject()) {
            $col[] = new self($row->id);
        }
        return $col;
    }

    protected $passes = false;

    /**
     * 
     * @return \Pass
     */
    public function getPasses() {
        if ($this->passes !== false)
            return $this->passes;

        $db = DB::getInstance();
        $sql = "select * from passes where subPassID=" . $this->id;
        $stmt = $db->query($sql);

        if ($stmt->rowCount() == 0) {
            return [];
        }

        $col = [];
        while ($row = $stmt->fetchObject()) {
            $col[] = new Pass($row->id);
        }
        $this->passes = $col;

        return $col;
    }

}
