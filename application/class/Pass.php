<?php

class Pass {

    /**
     *
     * @var SimpleTable
     */
    protected $table;

    /**
     *
     * @var Passes_Sub
     */
    public $subPass;

    /**
     *
     * @var User
     */
    public $author;

    /**
     *
     * @var Company
     */
    public $company;
    public $is_payed, $openDate, $expireDate, $description, $carNumber, $carRegion, $carDescription, $creationDateTime;
    public $id = 0;

    /**
     * 
     * @param type $id
     * @return \Invoice
     */
    public static function getInvoiceForPassId($id) {
        $sql = "select invID from invoices_passes where passID=" . $id;
        $db = DB::getInstance();
        $stmt = $db->query($sql);
        if ($stmt->rowCount() == 0) {
            return null;
        }
        return new Invoice($stmt->fetchObject()->invID);
    }

    public function getRecords() {
        $col = [];
        $db = DB::getInstance();
        $sql = "select * from records where passID=" . $this->id . " order by id desc";
        $stmt = $db->query($sql);
        if ($stmt->rowCount() == 0)
            return $col;
        while ($row = $stmt->fetchObject()) {
            $col[] = new Record($row->id);
        }
        return $col;
    }

    public function __construct($id = false) {
        $this->table = new SimpleTable("passes");
        $this->company = new Company;
        $this->subPass = new Passes_Sub;
        if ($id) {
            $this->load($id);
        }
    }

    /**
     * 
     * @return SimpleTable
     */
    public function proxyTable() {
        return $this->table;
    }

    protected function changePayment($val) {
        $this->table->update(array(
            'is_payed' => $val,
        ));
        if ($val == '1') {
            $this->table->update(array(
                'openDate' => date("Y-m-d"),
                'expireDate' => date("Y-m-d", strtotime("+31 day"))
            ));
        }
    }

    public function delete() {
        $this->table->delete();
        return $this;
    }

    public function setPayment() {
        return $this->changePayment(1);
    }

    public function getExtra() {
        return Passes_Extrainfo::getByPassId($this->id);
    }

    public function setUnPayment() {
        return $this->changePayment(0);
    }

    public function load($id) {
        if (!$this->table->findById($id)) {
            throw new Exception("pass not found", 100);
        }


        $this->id = $id;
        $this->subPass = new Passes_Sub($this->table->subPassID);
        $this->is_payed = $this->table->is_payed;
        $this->openDate = $this->table->openDate;
        $this->expireDate = $this->table->expireDate;
        $this->carNumber = $this->table->carNumber;
        $this->carRegion = $this->table->carRegion;
        $this->carDescription = $this->table->carDescription;
        $this->creationDateTime = $this->table->creationDateTime;
        $this->description = $this->table->description;
        $this->canEdit = $this->table->canEdit;


        try {
            $this->author = new User;
            $this->author->load($this->table->authorID);
        } catch (Exception $e) {

            $this->author = new User;
        }

        try {
            $this->company = (new Company);
            $this->company->load($this->table->companyID);
        } catch (Exception $e) {
            $this->company = new Company;
        }


        //return true;
    }

    public function add($row) {

        foreach ($row as $key => $value) {
            $this->table->$key = $value;
        }
        $ret = $this->table->insert();
        if (!$ret)
            return $ret;


        $this->load($this->table->id);
        return $ret;
    }

    /**
     * 
     * @param type $number
     * @param type $region
     * @return \Pass
     * @throws Exception
     */
    public static function getByCar($number, $region) {
        $db = DB::getInstance();
        $sql = "select * from passes where carRegion=? and carNumber=? and expireDate>NOW() order by expireDate desc limit 1";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($region, $number));
        if ($stmt->rowCount() == 0)
            throw new Exception("No pass found", 1);
        return new Pass($stmt->fetchObject()->id);
    }

    public static function getList($authorID = false, $companyID = false, $is_payed = 'all', $is_used = 'all', $type = 'all') {

        $where = [];
        if ($authorID) {
            $where[] = 'authorID=' . $authorID;
        }
        if ($companyID) {
            $where[] = 'companyID=' . $companyID;
        }
        if ($is_payed != 'all') {
            $where[] = 'is_payed=' . $is_payed;
        }
        if ($type != 'all') {
            $where[] = ' `type`="' . $type . '"';
        }
        if (!empty($where))
            $where = 'where ' . implode(' and ', $where);
        else
            $where = '';


        $db = DB::getInstance();
        $sql = "select p.* from passes p "
                . "left join subpassestypes sp on sp.id=p.subPassID "
                . "left join passestype pt on sp.passTypeID=pt.id "
                . "$where order by id desc";

        $stmt = $db->query($sql);
        $col = array();
        while ($row = $stmt->fetchObject()) {
            $el = new self($row->id);
            if ($is_used == 'all') {
                $col[] = $el;
                continue;
            }

            if (count($el->getRecords()) > 0 == ($is_used == '1')) {
                $col[] = $el;
                continue;
            }
        }

        return $col;
    }

}
