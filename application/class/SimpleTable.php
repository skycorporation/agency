<?php

class SimpleTable extends Table {

    public function getTableName() {
        return $this->table;
    }

    public function __construct($table) {
        $this->_table = $table;
        parent::__construct();
    }

}
