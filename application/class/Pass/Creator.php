<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Creator
 *
 * @author Anton
 */
class Pass_Creator {

    protected $table;
    public $fields;

    public function __construct() {
        $this->table = new SimpleTable("passes");
        $this->fields = [];
        $this->fields['creationDateTime'] = date("Y-m-d H:i:s");
    }

    public function setCustomValue($field, $value) {
        $this->fields[$field] = $value;
    }

    public function create($count = 1) {
        $id = [];

        for ($i = 0; $i < $count; $i++) {
            $this->table = new SimpleTable("passes");
            foreach ($this->fields as $k => $v) {
                $this->table->$k = $v;
            }
            $this->table->insert();
            $id[] = $this->table->id;
        }
        return $id;
    }

    public function setCompany($obj) {
        $id = false;
        if ($obj instanceof Company) {
            $id = $obj->getId();
        }
        if ($obj instanceof User) {
            $id = $obj->getCompanyID();
        }
        if (!$id)
            $id = $obj;

        $this->fields['companyID'] = $id;
        return $this;
    }

    public function setType($type) {
        $type = false;
        $this->fields['type'] = $type;
        return $this;
    }

    public function setAuthor($obj) {
        $id = false;

        if ($obj instanceof User) {
            $id = $obj->id;
        }
        if (!$id)
            $id = $obj;
        $this->fields['authorID'] = $id;
        return $this;
    }

    public function setSubPass($obj) {
        $id = false;
        if ($obj instanceof Passes_Sub) {
            $id = $obj->id;
        }
        if (!$id)
            $id = $obj;
        $this->fields['subPassID'] = $id;
        return $this;
    }

}
