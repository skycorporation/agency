<?php

class ProfileGroup {

    public function update($arr) {
        return $this->table->update($arr);
    }

    public function __construct() {
        $this->table = new SimpleTable('profileGroups');
    }

    public function delete() {
        return $this->table->delete();
    }
    
    public function add($profileGroup) {
    	$profileGroup['profiles'] = json_encode($profileGroup['profiles']);
    	foreach ($profileGroup as $key => $value) {
            $this->table->$key = $value;
        }
        $ret = $this->table->insert();
        
        if (!$ret)
            return $ret;

        $this->load($this->table->id);
        return $ret;
    }

    public function getProfiles($id) {
        $profileGroup = $this->table->findById($id);
        $arr = json_decode($profileGroup->profiles);
        if (count($arr)) {
        	$arr = array_unique($arr);
        } else {
	        $arr = [];
        }
        return $arr;
    }

    public function load($id) {
    	return $this->table->findById($id);
    }
    
    
    public function getFavs($id) {
        $profileGroup = $this->table->findById($id);
        $arr = json_decode($profileGroup->favs);
        if (count($arr)) {
        	$arr = array_unique($arr);
        } else {
	        $arr = [];
        }
        return $arr;
    }
    
    
    public function getHidden($id) {
        $profileGroup = $this->table->findById($id);
        $arr = json_decode($profileGroup->hidden);
        if (count($arr)) {
        	$arr = array_unique($arr);
        } else {
	        $arr = [];
        }
        return $arr;
    }
    
    public function getComments($id) {
        $profileGroup = $this->table->findById($id);
        $arr = json_decode($profileGroup->comments);
        return $arr;
    }

}
