<?php

class Company_Class {

    protected $name;
    public $id;
    protected $table;

    public function getId(){
        return $this->id;
    }
    public function __construct($id = false) {
        $this->table = new SimpleTable("companies_class");
        if ($id) {
            $this->load($id);
        }
    }

    public function __toString() {
        return $this->name;
    }

    public function load($id) {
        if ($this->table->findById($id)) {
            $this->name = $this->table->name;
            $this->id = $this->table->id;
        }
    }

    public static function getList() {
        $db = DB::getInstance();
        $sql = "select * from companies_class order by id desc";
        $stmt = $db->query($sql);
        $col = array();
        while ($row = $stmt->fetchObject()) {
            $col[] = new Company_Class($row->id);
        }
        return $col;
    }

}
