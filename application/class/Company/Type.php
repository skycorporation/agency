<?php

class Company_Type {

    protected $type = '';
    protected $id = 0;
    protected $table;

    public function getCompanies() {
        $db = DB::getInstance();
        $sql = "select * from companies where `typeID`=$this->id order by id desc";
        $stmt = $db->query($sql);
        $col = array();
        while ($row = $stmt->fetchObject()) {
            $col[] = new Company($row->id);
        }
        return $col;
    }
     public function getUsers() {

        return (new Company)->findByTypeID($this->getId());
    }


    public static function getList() {
        $db = DB::getInstance();
        $sql = "select * from companies_type order by id desc";
        $stmt = $db->query($sql);
        $col = array();
        while ($row = $stmt->fetchObject()) {
            $col[] = new Company_Type($row->id);
        }
        return $col;
    }

    public function __construct($type = false) {

        $this->table = new SimpleTable("companies_type");
        if ($type) {
            $this->load($type);
        }
    }

    public function load($id) {
        if ($this->table->findById($id)) {
            $this->type = $this->table->name;
            $this->id = $id;
        }
    }

    public function getId() {
        return $this->id;
    }

    public function __toString() {
        return $this->type;
    }

}
