<?php

class User {

    /**
     *
     * @var Table
     */
    public $table;
    protected $email;
    protected $phone;
    protected $active;
    public $id = false;
    protected $is_blocked;
    protected $name;
    protected $companyID;
    protected $photo;
    protected static $_cache;

    /**
     *
     * @var Company
     */
    protected $company;

    public function isBlocked() {
        return $this->is_blocked;
    }

    public function getCompanyID() {
        return $this->companyID;
    }

    public function getField($name) {
        return $this->table->$name;
    }

    public function getPhotoUrl($raw = false) {
        $key = md5("somekey" . $this->id);
        if ($raw) {
            return "images/user/" . $key . ".jpg";
        }


        if (file_exists("images/user/" . $key . ".jpg")) {
            return "/images/user/" . $key . ".jpg";
        }
        return "/images/user/nophoto.jpg";
    }

    public function getNamePart($key) {
        $names = $this->getName();


        return $names[$key];
    }

    public function getName($id = false) {

        $id = !$id ? $this->id : $id;
        $key = md5('names:' . $id);
        if (isset(self::$_cache[$key])) {

            return self::$_cache[$key];
        }
        $config = parse_ini_file("../config_names.ini");

        $db = DB::getInstance($config);
        $name = array(
            'firstname' => '',
            'middlename' => '',
            'lastname' => ''
        );
        $sql = "select firstname,middlename,lastname from names where userid=?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        if ($stmt->rowCount() == 0) {
            return $name;
        }
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        foreach ($row as $k => $v) {
            $name[$k] = $v;
        }
        $this->name = $name;
        self::$_cache[$key] = $this->name;
        return $name;
    }

    public function isActive() {
        return $this->active;
    }

    public function update($arr) {
        return $this->table->update($arr);
    }

    public function __construct() {
        $this->table = new SimpleTable('users');
    }

    public function delete() {
        $sql = "update companies set current_users=current_users-1 where id=" . $this->getCompanyID();
        $db = DB::getInstance();
        $db->query($sql);
        return $this->table->delete();
    }

    public function login($username, $password) {
        if (!$this->findByUsername($username)) {
            throw new Exception("User not found", 100);
        }
        $config = Registry::get('config');
        if ($this->table->password == md5($config->salt . $password)) {
            $this->setId($this->table->id);
            $this->setAuth();
            $this->update(array(
                'lastLoginDateTime' => date("Y-m-d H:i:s")
            ));
            return true;
        }
        throw new Exception("Wrong password", 101);
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setAuth() {
        $config = Registry::get('config');
        setcookie("id", $this->id, time() + 60 * 60 * 24 * 31, '/');
        setcookie("hash", md5($config->salt . $this->id), time() + 60 * 60 * 24 * 31, '/');
        return $this;
    }

    public static function getById($id) {
        $u = new User;
        $u->load($id);
        return $u;
    }

    public static function getId() {
        $cookie = new Request($_COOKIE);
        if (!isset($cookie->id, $cookie->hash))
            return false;
        $config = Registry::get('config');
        if ($cookie->hash == md5($config->salt . $cookie->id)) {
            $db = DB::getInstance();
            $sql = "select 1 from users where id=".$cookie->id;
            if ($db->query($sql)->rowCount() > 0) {
                return $cookie->id;
            }
            return false;
        }
        return false;
    }

    public function setNames($values) {
        $config = parse_ini_file("../config_names.ini");
        $db = DB::getInstance($config);
        $sql = "insert into names set userid=:userid, firstname=:first, middlename=:middle, lastname=:second"
                . " on duplicate key update firstname=:first, middlename=:middle, lastname=:second";
        $stmt = $db->prepare($sql);
        $stmt->execute(array(
            ':userid' => $this->id,
            ':first' => $values['firstname'],
            ':middle' => $values['middlename'],
            ':second' => $values['lastname'],
        ));
        $key = md5('names:' . $this->id);
        if (isset(self::$_cache[$key])) {
            unset(self::$_cache[$key]);
        }
        $this->getName();
        return $this;
    }

    public function setEmail($email) {
        $this->email = $email;
        return $this;
    }

    public function setPhone($phone) {
        $this->phone = $phone;
        return $this;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getPhone() {
        return $this->phone;
    }

    public function getCompany() {
        return $this->company;
    }

    public function setCompany() {
        
    }

    /**
     * Отсылает восстановление пароля
     */
    public function sendPasswordReminder() {
        
    }

    public function sendRegistrationCompleteLetter() {

        $html = Tpl::fetch('letters/registration_complete.tpl', array(
                    'name' => $this->getName(),
        ));
        $mailer = new Mailer();
        $mailer->setTo($this->email);
        $mailer->setSubject('Успешная регистрация');
        $mailer->setHtml($html);

        return $mailer->send();
    }

    public function sendRegistrationLetter() {


        $html = Tpl::fetch('letters/registration.tpl', array(
                    'email' => $this->email,
        ));
        $mailer = new Mailer();
        $mailer->setTo($this->email);
        $mailer->setSubject('Подтверждение регистрации');
        $mailer->setHtml($html);

        return $mailer->send();
    }

    public function sendForgotLetter() {



        $html = Tpl::fetch('letters/forgot.tpl', array(
                    'email' => $this->email,
        ));
        $mailer = new Mailer();
        $mailer->setTo($this->email);
        $mailer->setSubject('Ссылка восстановления');
        $mailer->setHtml($html);

        return $mailer->send();
    }

    public function setPassword($password) {
        $config = Registry::get('config');
        return $this->update(array(
                    'pass' => md5($config->salt . $password)
        ));
    }

    public function addUser($user) {
        foreach ($user as $key => $value) {
            //echo $key."=".$value.PHP_EOL;

            $this->table->$key = $value;
        }
        $ret = $this->table->insert();
        if (!$ret)
            return $ret;

        // setup user
        $this->load($this->table->id);
        return $ret;
    }

    public function load($id) {
        if (!$this->table->findById($id)) {
            $this->setId(0);
        } else {
            $this->setId($id);
        }

        //return $this;
    }

    public function findByCompanyID($id) {
        $res = $this->table->findByValue("companyID", $id);
        if (!$res)
            return false;
        return $res;
    }

    public function findAll() {
        return $this->table->findAll();
    }

    public function findByUsername($username) {
        $res = $this->table->findByValue("username", $username);
        if (!$res)
            return false;
        $user = $res[0];
        $this->load($user->id);
        return $this;
    }

}
