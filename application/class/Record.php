<?php

class Record {

    /**
     *
     * @var Pass
     */
    public $pass;

    /**
     *
     * @var SimpleTable
     */
    protected $table;
    public $id = 0,
            $carNumber = '',
            $carRegion = '',
            $insideDate = '',
            $insideError = 0,
            $insideErrorText = '',
            $outsideDate = '',
            $outsideError = 0;

    /**
     *
     * @var Fines
     */
    public $fineID;

    public function __construct($id = false) {
        $this->table = new SimpleTable('records');
        if ($id) {
            $this->load($id);
        }
    }

    /**
     * 
     * @return SimpleTable
     */
    public function proxyTable() {
        return $this->table;
    }

    public function getPhotoUrl() {
        $fn = md5($this->id);
        $file = "images/records/" . $fn[0] . "/" . $fn[1] . "/" . $fn . ".jpg";

        if (file_exists($file))
            return "/" . $file;
        return "/images/records/nophoto.jpg";
    }

    /**
     * 
     * @param type $number
     * @param type $region
     * @return \Record
     * @throws Exception
     */
    public static function getLatestRecordByCar($number, $region) {
        $db = DB::getInstance();
        $sql = "select * from records where insideError=0 and carNumber=? /*and carRegion=? */and outsideDate='0000-00-00 00:00:00' order by id desc limit 1";
        $stmt = $db->prepare($sql);
        $stmt->execute(array(
            $number,
//            $region
        ));
        if ($stmt->rowCount() == 0) {
            throw new Exception("No record found");
        }
        return new Record($stmt->fetchObject()->id);
    }

    public static function getLatestRecords($cid = false, $limit = 10) {
        $db = DB::getInstance();
        $sql = "select r.* from records r left join passes p on r.passID=p.id where p.companyID=$cid order by r.id desc limit " . $limit;
        $stmt = $db->prepare($sql);
        $stmt->execute(array(
        ));
        if ($stmt->rowCount() == 0) {
            return [];
        }
        $col = [];
        while ($row = $stmt->fetchObject()) {
            $col[] = new Record($row->id);
        }
        return $col;
    }

    public function setInside($time = false) {
        $this->insideDate = date("Y-m-d H:i:s", ($time) ? $time : time());
    }

    public function setOutside($time = false) {
        $this->outsideDate = date("Y-m-d H:i:s", ($time) ? $time : time());
    }

    public function setInsideError($text) {
        $this->insideError = 1;
        $this->insideErrorText = $text;
    }

    public function setOutsideError($text) {
        $this->outsideError = true;
        // $this->fine->setDescription( $text );
    }

    public function load($id) {
        if (!$this->table->findById($id)) {
            throw new Exception("Record not found");
        }
        $this->id = $this->table->id;
        $this->carNumber = $this->table->carNumber;
        $this->carRegion = $this->table->carRegion;
        $this->insideDate = $this->table->insideDate;
        $this->insideError = $this->table->insideError;
        $this->insideErrorText = $this->table->insideErrorText;
        $this->outsideDate = $this->table->outsideDate;
        $this->pass = new Pass($this->table->passID);
        $this->fineID = $this->table->fineID;
    }

    public function isError() {
        
    }

    public function create() {

        $this->table->carNumber = $this->carNumber;
        $this->table->carRegion = $this->carRegion;
        $this->table->insideDate = $this->insideDate;
        $this->table->insideError = $this->insideError;
        $this->table->insideErrorText = $this->insideErrorText;
        $this->table->outSideDate = $this->outsideDate;
        if (isset($this->pass->id))
            $this->table->passID = $this->pass->id;
        else {
            $this->table->passID = 0;
        }
        $a = $this->table->insert();
        $this->id = $this->table->id;
        return $a;
    }

    public function setCar($number, $region) {
        $this->carNumber = $number;
        $this->carRegion = $region;
        return $this;
    }

    /**
     * 
     * @param Pass $p
     * @return \Record
     */
    public function setPass(Pass $p) {
        $this->pass = $p;
        return $this;
    }

}
