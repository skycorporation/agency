<?php

class Profile {

    public function update($arr) {
        return $this->table->update($arr);
    }

    public function __construct() {
        $this->table = new SimpleTable('profiles');
    }

    public function delete() {
        return $this->table->delete();
    }

    public function addProfile($profile) {
    	foreach ($profile as $key => $value) {
            $this->table->$key = $value;
        }
        $ret = $this->table->insert();
        
        if (!$ret)
            return $ret;

        $this->load($this->table->id);
        return $ret;
    }

    public function load($id) {
    	return $this->table->findById($id);
    }
    
    public function age(){
    	global $fields;
    	$y="";
    	if($this->table->birthday!="0000-00-00"){
		    $from = new DateTime($this->table->birthday);
			$to   = new DateTime('today');
			$y = $from->diff($to)->y;
			if($y>1){
				$y.=" years";
			}else{
				$m = $from->diff($to)->m;
				$ye = $from->diff($to)->y;
				if($ye==1){
					$y = "1 year";
				}else{
					$y = "";
				}
				if($m>1){
					$y .= " ".$m." months";
				}else{
					$y .= " 1 month";
				}
			}
		}
		if($this->table->birthday==""){
			$y = "unknown";
		}
		return $y;
	}
	
    public function printProfile($target){
    	global $fields;
    	$result = "";
    	
   		if($target == "height"){
			$height = explode(",", $this->table->height);
			if(count($height)>1){
				$result = $fields["height"][0][$height[0]]."&#39;".$fields["height"][1][$height[1]]."";
			}else{
				$result = $this->table->height;
			}
			
		}else{
			$arr = $this->table->$target;
			$arr = explode(",", $arr);
			foreach($arr as $value){
				if($value!=""){
					if($result != ""){$result.=", ";}
					$result.=$fields[$target][$value];
				}
			}
		}
	   	
	   	return $result;
	}


}
