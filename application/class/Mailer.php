<?php

class Mailer {

    protected $is_debug = false;

    /**
     *
     * @var PHPMailer
     */
    protected $mailer;
    protected $server;
    protected $port;
    protected $html;
    protected $html_template;
    protected $to;
    protected $to_name = false;
    protected $subject;
    protected $from;
    protected $from_name;

    public function setHtml($html) {
        $this->html = $html;
    }

    public function __construct() {

        include_once(PATH_APPLICATION . "application/inc/phpmailer/PHPMailerAutoload.php");
        $this->mailer = new PHPMailer;
    }

    public function addFile($filename, $name) {

        $this->mailer->addAttachment($filename, $name);
    }

    public function prepareServer() {
        $config = Registry::get('config');
        $servers = $config->smtp_servers;

        $errno = 0;
        $errstr = '';

        for ($i = 0; $i < count($servers); $i++) {
            list( $host, $port ) = explode(":", $servers[$i]);
            $sock = @fsockopen($host, int_val($post), $errno, $errstr, 3);
            if ($sock) {
                $this->server = $host;
                $this->port = intval($port);
                return true;
            }
        }
	return true;
    }

    public function prepareHTML() {
        $html = $this->html;
        $matches = array();
        preg_match_all("/src=\"(http:\/\/.*?)\"/im", $html, $matches);
        if (isset($matches[1])) {
            for ($i = 0; $i < count($matches[1]); $i++) {
                $image_url = $matches[1][$i];
                $ext = explode(".", $image_url);
                $extenstion = strtolower(end($ext));
                $type = 'image/jpeg';
                if ($extenstion == 'png')
                    $type = 'image/png';
                if ($extenstion == 'gif')
                    $type = 'image/gif';
                if ($extenstion == 'bmp')
                    $type = 'image/bmp';
                $cid = 'image_' . md5($image_url);
                $image_content = @file_get_contents($image_url);
                if ($image_content) {
                    $this->mailer->addStringEmbeddedImage($image_content, $cid, $cid, 'base64', $type);
                    $html = str_replace($image_url, "cid:" . $cid, $html);
                }
            }
        }
        $this->html_template = $html;
        $this->html = $html;
        return true;
    }

    public function setSubject($subject) {
        $this->subject = $subject;
    }

    public function setTo($to, $name = false) {
        $this->to = $to;
        if ($name) {
            $this->to_name = $name;
        }
    }

    public function send() {
        $config = Registry::get('config');
        $this->mailer->clearAllRecipients();
        if ($this->to === false) {
            throw new MailerException("TO email not defined", 201);
        }
        try {

	$this->is_debug = true;

            $this->mailer->CharSet = "utf-8";
            if (!$this->is_debug) {
                $this->prepareServer();
                $this->mailer->Host = $this->server;
                $this->mailer->Port = $this->port;
                $this->mailer->isSMTP();
            } else {
                $this->mailer->isSendmail();
            }
            if ($this->from === false || $this->from == '')
                $this->from = $config->smtp_from;

            if ($this->from_name === false || $this->from_name == '')
                $this->from_name = $config->smtp_from_name;

            $this->mailer->setFrom($this->from, $this->from_name);
            $this->mailer->addAddress($this->to, ($this->to_name === false ? '' : $this->to_name));
            $this->mailer->Subject = $this->subject;
            $this->prepareHTML();
            $this->mailer->msgHTML($this->html);


            if (!$this->mailer->send()) {
                $this->to_name = '';
                $this->mailer->clearAllRecipients();
                echo $this->mailer->ErrorInfo;
                //throw new MailerException("Cant send: " . $this->mailer->ErrorInfo, 200);
                return false;
            }
            $this->mailer->clearAddresses();
        } catch (MailerException $e) {
            //echo $e->getMessage() . "/" . $e->getCode();
            return false;
        }
        $this->to_name = '';
        $this->mailer->clearAllRecipients();
        return true;
    }

}
