<?php

class CastingList {

    public function update($arr) {
        return $this->table->update($arr);
    }

    public function __construct() {
        $this->table = new SimpleTable('castingLists');
    }

    public function delete() {
        return $this->table->delete();
    }
    
    public function add($profileGroup) {
    	$profileGroup['profiles'] = json_encode($profileGroup['profiles']);
    	foreach ($profileGroup as $key => $value) {
            $this->table->$key = $value;
        }
        $ret = $this->table->insert();
        
        if (!$ret)
            return $ret;

        $this->load($this->table->id);
        return $ret;
    }

    public function getProfiles($id) {
        $profileGroup = $this->table->findById($id);
        $arr = json_decode($profileGroup->profiles);
        if(count($arr)){
        	$arr = array_unique($arr);
        }else{
	        $arr = [];
        }
        return $arr;
    }

    public function load($id) {
    	return $this->table->findById($id);
    }
    
    public function getFavs($id) {
        $castingList = $this->table->findById($id);
        $arr = json_decode($castingList->favs);
        if (count($arr)) {
        	$arr = array_unique($arr);
        } else {
	        $arr = [];
        }
        return $arr;
    }
    
    public function getHidden($id) {
        $castingList = $this->table->findById($id);
        $arr = json_decode($castingList->hidden);
        if (count($arr)) {
        	$arr = array_unique($arr);
        } else {
	        $arr = [];
        }
        return $arr;
    }
    
    public function getComments($id) {
        $castingList = $this->table->findById($id);
        $arr = json_decode($castingList->comments);
        return $arr;
    }
    
    public function getSelectedFields($id) {
        $castingList = $this->table->findById($id);
        return json_decode($castingList->selectedFields);
    }
    
    public function getHiddenFields($id) {
        $castingList = $this->table->findById($id);        
        $allAdditionalFields=array("basedIn","occupation","agency","book","website","instagram","knownFor","upcomingProjects","press","music","video","rate");
        $selectedFields = json_decode($castingList->selectedFields);
        $hiddenFilelds = array();
        foreach ($allAdditionalFields as $field) {
            if(!in_array($field, $selectedFields)) {
                $hiddenFilelds[] = $field;
            }
        }
        return $hiddenFilelds;
    }

}
