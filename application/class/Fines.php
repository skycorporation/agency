<?php

class Fines {

    /**
     *
     * @var SimpleTable
     */
    protected $table;

    public function __construct($id = false) {
        $this->table = new SimpleTable("fines");
        if ($id) {
            $this->load($id);
        }
    }

    public $id, $company, $is_payed, $amount, $creationDateTime, $record, $desc;

    public function __toString() {
        $str = 'Штраф №' . $this->id . ' от ' . $this->creationDateTime . "<br> Повод: " . nl2br($this->desc);
        return $str;
    }

    public function load($id) {
        if (!$this->table->findById($id)) {
            throw new Exception("Fine not found");
        }
        $this->id = $id;
        $this->company = new Company;
        try {
            $this->company->load($this->table->companyID);
        } catch (Exception $ex) {
            
        }
        $this->is_payed = $this->table->is_payed;
        $this->amount = $this->table->amount;
        $this->creationDateTime = $this->table->creationDateTime;
        $this->record = new Record();
        try {
            $this->record->load($this->table->recordID);
        } catch (Exception $ex) {
            
        }
        $this->desc = $this->table->description;
    }

    public static function getById($id) {
        $db = DB::getInstance();
        $sql = "select * from fines where id=?";
        $stmt = $db->prepare($sql);
        $stmt->Execute(array($id));
        if ($stmt->rowCount() == 0)
            throw new Exception("Fine not found");
        return new self($stmt->fetchObject()->id);
    }

}
