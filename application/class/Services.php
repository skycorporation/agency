<?php

class Services {

    const SERVICE_ADMINISTRATOR = 5;
    const SERVICE_USER_MANAGER = 7;
    const SERVICE_COMPANIES_MANAGER = 8;
    const SERVICE_INVOICE = 9;
    const SERVICE_PASSES_MANAGER = 10;

    public static function getPrivateServices($userid = false) {
        $db = DB::getInstance();
        if ($userid instanceof User) {
            $userid = $userid->id;
        }
        if (!$userid) {
            $userid = User::getId();
        }
        $sql = "select * from services where is_public=0";
        return $db->query($sql);
    }

    protected static function changeActive($value, $id, $userid) {
        $db = DB::getInstance();
        if ($userid instanceof User) {
            $userid = $userid->id;
        }
        if (!$userid) {
            $userid = User::getId();
        }
        $sql = "insert into services_rules set "
                . "serviceID=?, userid=?, authorID=?,creationDateTime=?, is_active=? on duplicate key update is_active=?";
        $stmt = $db->prepare($sql);
        return $stmt->execute(array(
                    $id,
                    $userid,
                    User::getId(),
                    date("Y-m-d H:i:s"),
                    $value,
                    $value,
        ));
    }

    public static function deactivateAll($userid = false) {
        $db = DB::getInstance();
        if ($userid instanceof User) {
            $userid = $userid->id;
        }
        if (!$userid) {
            $userid = User::getId();
        }
        $services = Services::getPrivateServices();
        if (!$services)
            return false;
        while ($row = $services->fetchObject()) {

            Services::deactivate($row->id, $userid);
        }
    }

    public static function deactivate($id, $userid = false) {
        return Services::changeActive(0, $id, $userid);
    }

    public static function activate($id, $userid = false) {
        return Services::changeActive(1, $id, $userid);
    }

    public static function getServices() {
        $db = DB::getInstance();
        $userid = User::getId();
        if (!$userid)
            $userid = 0;
        $sql = "SELECT * FROM `services` s1 
                left join services s2 
                    on s1.id=s2.parentID
                left join services_rules sr 
                    on sr.serviceID=s1.id 
                        or sr.serviceID=s2.id
                where 
                        s1.parentID=0 
                        and s1.is_active=1 
                        and (s2.is_active=1 or s2.is_active is null) 
                        and (s1.is_public=1 
                            or  ( 
                                    (s1.is_public is null or s1.is_public=0) 
                                    and sr.serviceID=s1.id 
                                    and sr.userid=$userid
                                ) 
                            or (
                                    (s2.is_public is null or s2.is_public=0) 
                                    and sr.serviceID=s2.id 
                                    and sr.userid=$userid
                                ) 
                            
                        ) and (sr.is_active is null or sr.is_active=1)"
                . "and "
                . "("
                . "(sr.serviceID=s1.id or sr.serviceID is null)"
                . "or (sr.serviceID=s2.id or sr.serviceID is null)"
                . ") order by s1.position desc,s2.position desc ";

        
        $stmt = $db->query($sql);
        $tree = array();
        while ($row = $stmt->fetch(PDO::FETCH_BOTH)) {

            //build 1st

            $elem = array();
            $elem['link'] = $row[2];
            $elem['name'] = $row[1];
            $elem['id'] = $row[0];
            $elem['child'] = array();
            $tree[$row[0]] = $elem;
        }
        $stmt = $db->query($sql);
        $was = [];
        while ($row = $stmt->fetch(PDO::FETCH_BOTH)) {
            //build 2nd
            if ($row[9] == '' || in_array($row[7], $was))
                continue;
            $was[] = $row[7];
            $elem = array();
            $elem['link'] = $row[9];
            $elem['name'] = $row[8];
            $elem['id'] = $row[7];
            $elem['child'] = array();

            $tree[$row[0]]['child'][] = $elem;
        }
        foreach ($tree as $k => &$v) {

            if (isset($v['child'][0]) && is_null($v['child'][0]['link'])) {
                $v['child'] = [];
            }
        }

        return $tree;
    }

    public static function isAvailable($serviceID, $userid = false) {
        $db = DB::getInstance();
        if (!$userid) {
            $userid = User::getId();
        }
        $sql = "select 1 from services_rules where userid=$userid and serviceID=" . $serviceID . " and is_active=1";
        return $db->query($sql)->rowCount() > 0;
    }

}
