<?php

class Server_Logger {

    protected $is_enabled = false;

    public function enable() {
        $this->is_enabled = true;
    }

    public function disable() {
        $this->is_enabled = false;
    }

    public function write($msg) {
        if (!$this->is_enabled)
            return;
        $message = date("r") . "\r\n-------------------------------\r\n" . $msg . "\r\n\r\n";
        $fp = @fopen("../cache/logs/" . date("Ymd") . ".txt", "a");
        if (!$fp)
            return false;
        fwrite($fp, $message);
        fclose($fp);
    }

}
