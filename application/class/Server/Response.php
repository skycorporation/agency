<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Response
 *
 * @author Anton
 */
class Server_Response {

    const MESSAGE_IDLE = 0;
    const MESSAGE_GATE = 1;

    protected $out = array();

    /**
     *
     * @var Server_Logger
     */
    protected $logger = false;

    public function __construct(Server_Logger $log = null) {
        if (!is_null($log))
            $this->logger = $log;
        $this->out = [
            'status' => 'ok',
        ];
    }

    public function answer($mesage_type, $message = false) {
        $this->out['message_type'] = $mesage_type;
        $this->out['message'] = $message;
    }

    protected function setError($message) {
        $this->out['status'] = 'error';
        $this->out['error'] = $message;
    }

    public function isError() {
        return (isset($this->out['error']) ? $this->out['error'] : false);
    }

    public function error($reason = 'Bad Request') {
        if (!$this->isError())
            $this->setError($reason);
    }

    public function send() {
        if ($this->logger) {
            $this->logger->write("[RESPONSE]\r\n----------\r\n" . var_export($this->out, 1));
        }
        echo json_encode($this->out);
    }

}
