<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Request
 *
 * @author Anton
 */
class Server_Request {

    protected $post = array();
    protected $sign = false;
    protected $is_debug = false;

    /**
     *
     * @var Server_Logger
     */
    protected $logger = false;

    public function __construct(Server_Logger $log = null) {
        if (!is_null($log))
            $this->logger = $log;

        if ($this->logger) {
            $this->logger->write("[REQUEST]\r\n---------\r\n" . var_export($_REQUEST, 1));
        }
        $this->post = new Request($_REQUEST);
        if (isset($_SERVER['HTTP_X_SIGN'])) {
            $this->sign = $_SERVER['HTTP_X_SIGN'];
        }
    }

    public function debug() {
        $this->is_debug = true;
    }

    public function is_valid() {
        if ($this->is_debug)
            return true;
        return $this->sign == md5("someSTR" . json_encode($_POST));
    }

}
