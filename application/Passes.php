<?php

class PassesModel extends Admin {

    public function EditAction($arg) {
        $req = new Request($_REQUEST);

        $id = $arg[0];

        $is_admin = Services::isAvailable(Services::SERVICE_ADMINISTRATOR) || Services::isAvailable(Services::SERVICE_PASSES_MANAGER);
        $pass = new Pass($id);

        $error = false;

        if (isset($req->del, $req->hash) && md5(md5($req->id) == $req->hash)) {
            // delete
            $invoice = Pass::getInvoiceForPassId($pass->id);
            if ($invoice && count($invoice->getPasses()) > 1) {
                $error = 'Невозможно удалить один пропуск т.к уже создан счет более, чем на 1 пропуск. <br>'
                        . 'Вы можете удалить весь счет вместе со всеми пропусками!';
            }


            if (!$error) {
                $pass->delete();
                $invoice->delete();

                header("Location:/passes/list");
                exit;
            }
        }




        if (!$is_admin) {
            if ($this->user->getCompanyID() != $pass->company->id) {
                exit("403/2");
            }
            if ($this->user->id != $pass->author->id && $this->user->is_admin == '0') {
                exit("403/1");
            }
        }
        if (isset($req->action)) {

            if ($req->btn == 'save') {
		if(isset($req->extra)){
	                $extra = $pass->getExtra();
	                $extra->update($req->extra);
		}

                if (count($pass->getRecords()) == 0) {
                    $pass->proxyTable()->update($req->data);
                    System::redirect('/passes/edit/' . $id);
                }
                if ($req->btn == 'save') {
                    $pass->proxyTable()->update(array(
                        'description' => $req->data['description']
                    ));
                }
            }
        }

        $users = [];
        if ($is_admin) {
            $users = $this->user->getCompany()->getUsers();
        }

        Tpl::get('passes/edit.tpl', array(
            'error' => $error,
            'p' => $pass,
            'service_admin' => $is_admin,
            'company_admin' => $this->user->getfield('is_admin') == 1,
            'user' => $this->user,
            'company_users' => $users,
            'tu'=>new User,
            'js'=>[
                'passes/edit.js',
            ]
        ));
    }

    public function ListAction() {
        $req = new Request($_REQUEST);
        $is_admin = Services::isAvailable(Services::SERVICE_ADMINISTRATOR) || Services::isAvailable(Services::SERVICE_PASSES_MANAGER);

        if (isset($req->action)) {
            if ($req->action == 'getList') {
                // 
                $list = [];
                if ($is_admin) {
                    $list = Pass::getList(false, false, $req->is_payed, $req->is_used);
                } else {
                    if ($this->user->getField('is_admin')) {
                        $list = Pass::getList(false, $this->user->getCompanyID(), $req->is_payed, $req->is_used, $req->type);
                    } else {
                        $list = Pass::getList($this->user->id, $this->user->getCompanyID(), $req->is_payed, $req->is_used, $req->type);
                    }
                }
                Tpl::get('passes/list_block.tpl', array(
                    'list' => $list,
                ));
                exit;
            }
        }

        $js = [];
        $js[] = 'passes/list.js';
        Tpl::get('passes/list.tpl', array(
            'js' => $js
        ));
    }

    public function CreateAction($args) {

        $sub = $args[0];
        try {
            $subPass = new Passes_Sub($sub);
        } catch (Exception $e) {
            exit($e->getMessage());
        }


        if ($subPass->getPassType()->prepayment == '0') {
            $passCreator = new Pass_Creator;
            $passCreator->setAuthor($this->user);
            $passCreator->setCompany($this->user->getCompany());
            $passCreator->setSubPass($subPass);
            $passCreator->setCustomValue('openDate', date("Y-m-d"));
            $passCreator->setCustomValue('expireDate', date("Y-m-d", strtotime("+31 day")));
            $ids = $passCreator->create();
            $id = $ids[0];
            System::redirect('/passes/edit/' . $id);
        }
    }

    public function NewAction() {
        $post = new Request($_REQUEST);
        if (isset($post->action)) {

            if ($post->action == 'showSubs' && isset($post->passid)) {

                $subs = [];
                if (isset($post->cart)) {
                    foreach ($post->cart as $id => $count) {
                        $subs = new Passes_Sub($id);
                    }
                }

                Tpl::get('passes/cart.tpl', array(
                    'is_prepayment' => $post->is_prepayment == 'true',
                    'pass' => new Passes_Type($post->passid)
                        )
                );

                exit;
            }
        }
        $css = [];
        $js = [];
        $js[] = 'passes/cart.js';
        $js[] = 'passes/new.js';
        $css[] = "assets/css/pricing.css";
        Tpl::get('passes/new.tpl', array(
            'css' => $css,
            'js' => $js,
        ));
    }

    public function ManagerEditAction($args) {
        $is_admin = Services::isAvailable(Services::SERVICE_ADMINISTRATOR) || Services::isAvailable(Services::SERVICE_PASSES_MANAGER);
        if (!$is_admin) {
            System::redirect('/');
        }
        $id = $args[0];
        $passType = new Passes_Type($id);
        $post = new Request($_REQUEST);
        if (isset($post->action)) {
            if ($post->action == 'update') {
                $sub = new Passes_Sub($post->sid);
                $sub->update(array(
                    $post->name => $post->value
                ));
                exit;
            }
            if ($post->action == 'deleteSub') {
                $subC = new Passes_Sub($post->id);
                $subC->delete();
                exit;
            }
            if ($post->action == 'create') {
                $sub = new stdClass();
                $sub->passTypeID = $id;
                $sub->name = $post->data['name'];
                $subC = new Passes_Sub;
                $subC->add($sub);
                exit("[]");
            }
            if ($post->action == 'getSubs') {
                Tpl::get('passes/manager_subs_list.tpl', array(
                    'passType' => $passType,
                ));
                exit;
            }
            if ($post->action == 'save') {
                $passType->update($post->data);
                System::redirect('/passes/' . $id . '/manager');
                exit;
            }
        }

        $js = [];
        $js[] = 'passes/manager_edit.js';
        Tpl::get('passes/manager_edit.tpl', array(
            'passType' => $passType,
            'js' => $js,
        ));
    }

    public function ManagerAction() {
        $is_admin = Services::isAvailable(Services::SERVICE_ADMINISTRATOR) || Services::isAvailable(Services::SERVICE_PASSES_MANAGER);
        if (!$is_admin) {
            System::redirect('/');
        }
        $post = new Request($_POST);
        if (isset($post->action)) {
            if ($post->action == 'delete' || $post->action == 'block' || $post->action == 'unblock') {
                foreach ($post->passes as $id) {
                    $t = new Passes_Type($id);
                    if ($post->action == 'delete') {
                        $t->delete();
                        continue;
                    }
                    if ($post->action == 'block') {
                        $t->setBlock();
                        continue;
                    }
                    if ($post->action == 'unblock') {
                        $t->setUnBlock();
                        continue;
                    }
                }
                exit;
            }
            if ($post->action == 'create') {
                $ret = [];
                $ret['status'] = 'error';

                $row = new stdClass();
                foreach ($post->data as $k => $v) {
                    $row->$k = $v;
                }
                try {
                    $type = new Passes_Type;
                    if ($type->add($row)) {
                        $ret['status'] = 'ok';
                    } else {
                        throw new Exception("Ошибка добавления");
                    }
                } catch (Exception $ex) {
                    $ret['error'] = $ex->getMessage();
                }
                echo json_encode($ret);
                exit;
            }
        }
        $js[] = 'passes/manager.js';
        Tpl::get('passes/manager.tpl', array(
            'is_admin' => $is_admin,
            'js' => $js,
        ));
    }

}
