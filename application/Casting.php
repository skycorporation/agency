<?php

class CastingModel extends AModel {
    
    public function ListAction() {
        $this->checkAuth();
        
    	global $fields;
    	
    	$userid = User::getId();
    	$user = new User;
        $user->load($userid);
        
        $profileGroupsToShow = [];
        $profileGroups = new CastingList;
        $profileGroups = $profileGroups->table->findAll();
        
        foreach($profileGroups as $profileGroup){
        	$group = new CastingList;
        	$profileGroupsToShow[$profileGroup->id]["id"] = $profileGroup->id;
        	$profileGroupsToShow[$profileGroup->id]["name"] = $profileGroup->name;
        	$profileGroupsToShow[$profileGroup->id]["profiles"] = $profileGroup->profiles;
        	$profileGroupsToShow[$profileGroup->id]["location"] = $profileGroup->location;
        	$profileGroupsToShow[$profileGroup->id]["date"] = $profileGroup->date;
        	$profileGroupsToShow[$profileGroup->id]["url"] = $profileGroup->url;
        	$profiles = $group->getProfiles($profileGroup->id);
        	foreach($profiles as $profileObj){
        		$profile = new Profile;
        		$profile->load($profileObj);
        		
        		if($profile->table->firstName){
	        		$profileGroupsToShow[$profileGroup->id]["count"]++;
        		}
        		if(isset($profile->table->photo) and strlen($profile->table->photo)>0){
        			$profileGroupsToShow[$profileGroup->id]["photos"][]=$profile->table->thumbnail;
        		}
        	}
        }
    	
        Tpl::get('castings.tpl', array(
            'js' => $js,
            'userName' => $user->table->name,
            'profileGroups' => $profileGroupsToShow,
            'fields' => $fields
        ));
    }
    
    public function AddAction() {
    	$this->checkAuth();
        
        global $fields;
    	
    	$userid = User::getId();
    	$user = new User;
        $user->load($userid);
        $get = new Request($_GET);
        
        $availableFields = array('ethnicity', 'basedIn', 'occupation', 'agency', 'book', 'website', 'instagram', 'knownFor', 'upcomingProjects', 'press', 'music', 'video', 'rate', 'photoUpload');
        
        $selectedFields = array();
        foreach ($availableFields as $field) {
            if ($get->$field) {
                $selectedFields[] = $field;
            }
        }

        $selectedVisibleFields = array();
        foreach ($availableFields as $field) {
            $fieldVis = $field . 'ForClient';
            if ($get->$fieldVis) {
                $selectedVisibleFields[] = $field; // Сохраняем поле в базе без постфикса ForClient
            }
        }

        $customUrlFields = array();
        $customUrlFieldCount = 8;
        for ($i = 1; $i <= $customUrlFieldCount; $i++) {
            $field = 'customUrl' . $i;
            $fieldName = $field . 'name';
            $fieldVis = $field . 'ForClient';

            if ($get->$field && $get->$fieldName) {
                $customUrlFields[$field] = $get->$fieldName;
                if ($get->$fieldVis) {
                    $selectedVisibleFields[] = $field;
                }
            }
        }

        $profileGroup = new CastingList;
        $addCL['name'] = $get->name;
        $addCL['location'] = $get->location;
        $addCL['date'] = $get->date;
        $addCL['url'] = $get->url;
        $addCL['favs'] = '[]';
        $addCL['selectedFields'] = json_encode($selectedFields);
        $addCL['customUrlFields'] = json_encode($customUrlFields);
        $addCL['visibleFields'] = json_encode($selectedVisibleFields);
        $profileGroup->add($addCL);
        
        System::redirect('/castings/view?id='.$profileGroup->table->id);
        
    }    
    
    public function AddFormAction() {
        $this->checkAuth();
        
    	global $fields;
    	
    	$userid = User::getId();
    	$user = new User;
        $user->load($userid);
        $get = new Request($_GET);
        
        Tpl::get('castingAddForm.tpl', array('userName' => $user->table->name));
        
    }
    
    public function AddToAction() {
        $this->checkAuth();
        
    	global $fields;
    	
    	$userid = User::getId();
    	$user = new User;
        $user->load($userid);
        $get = new Request($_GET);
        
        $profileGroup = new CastingList;
        $profileGroup->load($get->casting);
        $profiles = array_unique(array_merge(explode(",", $get->profiles), $profileGroup->getProfiles($get->casting)));
        foreach($profiles as $key => $val){
	        $jsonFinal[]=$val;
        }
        $addPG["profiles"] = json_encode($jsonFinal);
        
        $profileGroup->update($addPG);
        
        System::redirect('/castings/view?id='.$profileGroup->table->id);
        
    }
    
    public function DeleteAction() {
        $this->checkAuth();
        
    	global $fields;
    	
    	$userid = User::getId();
    	$user = new User;
        $user->load($userid);
        $get = new Request($_GET);
        
        $profileGroup = new CastingList;
        $profileGroup->load($get->id);
        $profileGroup->delete();
        
        System::redirect('/castings');
        
    }

    
    public function EditAction() {
        $this->checkAuth();
        
    	global $fields;
    	
    	$userid = User::getId();
    	$user = new User;
        $user->load($userid);
        $get = new Request($_GET);
        
        $profileGroup = new CastingList;
        $profileGroup->load($get->id);
        $addCL['name'] = $get->name;
        $addCL['location'] = $get->location;
        $addCL['date'] = $get->date;
        $addCL['url'] = urlencode($get->url);
        $profileGroup->update($addCL);
        
        System::redirect('/castings');
        
    }
    
    public function DeleteFromAction() {  
        $this->checkAuth();
        
        $get = new Request($_GET);
        
        $casting = new CastingList;
        $casting->load($get->id);
        $profilesOld = $casting->getProfiles($get->id);
        $profilesNew = [];
        
        foreach($profilesOld as $val){
            if($val != $get->profile){
                $profilesNew[] = $val;
            }
        }        
        
        $addPG['profiles'] = json_encode($profilesNew);
        $casting->update($addPG);
        
        System::redirect('/castings/view?id='.$casting->table->id);
        
    }

    public function DeleteMultipleFromAction() {
        $this->checkAuth();

        $get = new Request($_GET);

        $removed = explode(',', $get->profiles);
        $casting = new CastingList;
        $casting->load($get->id);
        $profilesOld = $casting->getProfiles($get->id);
        $profilesNew = [];
        
        foreach($profilesOld as $val){
            if(!in_array($val, $removed)){
                $profilesNew[] = $val;
            }
        }        
        $addPG['profiles'] = json_encode($profilesNew);
        $casting->update($addPG);
        
        System::redirect('/castings/view?id='.$casting->table->id);
        
    }
    
    public function ViewAction() {
        
    	global $fields;
    	
    	$userid = User::getId();
    	$user = new User;
        $user->load($userid);
        $get = new Request($_GET);
        
                
        $profileGroups = new ProfileGroup;
        $profileGroups = $profileGroups->table->findAll();
        $castingLists = new CastingList;
        $castingLists = $castingLists->table->findAll();
        
        
        $profileGroup = new CastingList;
        $profilesInGroup = $profileGroup->getProfiles($get->id);
        $favProfilesInGroup = $profileGroup->getFavs($get->id);
        $hiddenProfilesInGroup = $profileGroup->getHidden($get->id);
        $commentsProfilesInGroup = $profileGroup->getComments($get->id);
        
        
        $profiles = new Profile;
        if($get->sort=="alph"){
        	setcookie("sort", "alph");
        	$profiles = $profiles->table->findAll("firstName");
        	$sort = "alph";
        }elseif($get->sort=="date"){
        	setcookie("sort", "date");
        	$profiles = $profiles->table->findAll();
        	$sort = "date";
        }else{
	    	if($_COOKIE["sort"]=="alph"){
		    	$profiles = $profiles->table->findAll("firstName");
				$sort = "alph";
	    	}else{
		    	$profiles = $profiles->table->findAll();
				$sort = "date";
	    	}
        }
        
        foreach ($profiles as $profile){
        	if(in_array($profile->id, $profilesInGroup)){
		        $prof = new Profile;
		        $prof->load($profile->id);
		        $profile->age = $prof->age();
		        $profile->height = $prof->printProfile("height");
                $profile->fav = in_array($profile->id, $favProfilesInGroup);
                $profile->hidden = in_array($profile->id, $hiddenProfilesInGroup);
                $profile->comment = $commentsProfilesInGroup->{$profile->id};
                
		        $profilesFinal[]=$profile;
		        
	        }
        }
        
        $num = count($profilesFinal);
        foreach($profilesFinal as $profile){
        	$profile->num = $num;
        	$num--;
        }
    	
    	$profileGroup = new CastingList;
        $profileGroup->load($get->id);
        
        $url = urlencode( str_replace(' ', '', trim( $profileGroup->table->name ) ) );
        
        Tpl::get('dashboardCasting.tpl', array(
            'userName' => $user->table->name,
            'profiles' => $profilesFinal,
            'profileGroups' => $profileGroups,
            'castingLists' => $castingLists,
            'group' => $profileGroup,
            'groupID' => $get->id,
            'fields' => $fields,
            'url' => $url,
            'page' => $page,
            'sort' => $sort,
            'count' => count($profilesFinal)
        ));
        
    }
    
    
    
    public function PhotosAction() {
        $this->checkAuth();
        
    	global $fields;
    	
    	$userid = User::getId();
    	$user = new User;
        $user->load($userid);
        $get = new Request($_GET);
        
        $profileGroup = new CastingList;
        $profilesInGroup = $profileGroup->getProfiles($get->id);
        
        $profiles = new Profile;
        if($get->sort=="alph"){
        	setcookie("sort", "alph");
        	$profiles = $profiles->table->findAll("firstName");
        	$sort = "alph";
        }elseif($get->sort=="date"){
        	setcookie("sort", "date");
        	$profiles = $profiles->table->findAll();
        	$sort = "date";
        }else{
	    	if($_COOKIE["sort"]=="alph"){
		    	$profiles = $profiles->table->findAll("firstName");
				$sort = "alph";
	    	}else{
		    	$profiles = $profiles->table->findAll();
				$sort = "date";
	    	}
        }
        
        foreach ($profiles as $profile){
        	if(in_array($profile->id, $profilesInGroup)){
		        $prof = new Profile;
		        $prof->load($profile->id);
		        $profile->age = $prof->age();
		        $profile->height = $prof->printProfile("height");
		        $profilesFinal[]=$profile;
	        }
        }
    	
    	$profileGroup = new CastingList;
        $profileGroup->load($get->id);
        
        $url = urlencode( str_replace(' ', '', trim( $profileGroup->table->name ) ) );
        
        Tpl::get('castingPhotos.tpl', array(
            'userName' => $user->table->name,
            'profiles' => $profilesFinal,
            'profileGroups' => $profileGroups,
            'group' => $profileGroup,
            'groupID' => $get->id,
            'fields' => $fields,
            'url' => $url,
            'sort' => $sort,
        ));
        
    }
    
    
    public function ToggleFavAction() {
        global $fields;
    	
    	$userid = User::getId();
    	$user = new User;
        $user->load($userid);
        $get = new Request($_GET);
        
        $status = "";

        $castingList = new CastingList;
        $castingList->load($get->list_id);
        
        $favs = $castingList->getFavs($get->list_id);

        if (($key = array_search($get->profile, $favs)) !== false) {
            unset($favs[$key]);
            $status = "removed";
        } else {
            $favs[] = $get->profile;
            $status = "added";
        }

        $addPG["favs"] = json_encode(array_values($favs));

        $castingList->update($addPG);  
        
        echo $status;
    }
    
    public function ToggleHideAction() {
        global $fields;
    	
    	$userid = User::getId();
    	$user = new User;
        $user->load($userid);
        $get = new Request($_GET);
        
        $status = "";

        $castingList = new CastingList;
        $castingList->load($get->list_id);
        
        $favs = $castingList->getHidden($get->list_id);

        if (($key = array_search($get->profile, $favs)) !== false) {
            unset($favs[$key]);
            $status = "removed";
        } else {
            $favs[] = $get->profile;
            $status = "added";
        }

        $addPG["hidden"] = json_encode(array_values($favs));

        $castingList->update($addPG);  
        
        echo $status;
    }
    
    public function CommentSaveAction() {
        global $fields;
    	
    	$userid = User::getId();
    	$user = new User;
        $user->load($userid);
        $get = new Request($_GET);
        
        $status = "";

        $castingList = new CastingList;
        $castingList->load($get->list_id);
        
        $favs = $castingList->getComments($get->list_id);
        $favs->{$get->profile} = $get->comment;
        $addPG["comments"] = json_encode($favs);

        $castingList->update($addPG);  
        
        echo $status;
    }
    

}
