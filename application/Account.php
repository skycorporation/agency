<?php

class AccountModel extends AModel {

    public function DashboardAction() {
        $this->checkAuth();
        
    	global $fields;
    	
    	$userid = User::getId();
    	$user = new User;
        $user->load($userid);
        $get = new Request($_GET);
        
        $profileGroupsToShow = [];
        $profileGroups = new ProfileGroup;
        $profileGroups = $profileGroups->table->findAll();
        $castingLists = new CastingList;
        $castingLists = $castingLists->table->findAll();
        
        
        $profiles = new Profile;
        if($get->sort=="alph"){
        	setcookie("sort", "alph");
        	$profiles = $profiles->table->findAll("firstName");
        	$sort = "alph";
        }elseif($get->sort=="date"){
        	setcookie("sort", "date");
        	$profiles = $profiles->table->findAll();
        	$sort = "date";
        }else{
	    	if($_COOKIE["sort"]=="alph"){
		    	$profiles = $profiles->table->findAll("firstName");
				$sort = "alph";
	    	}else{
		    	$profiles = $profiles->table->findAll();
				$sort = "date";
	    	}
        }
        
        foreach ($profiles as $profile){
	        $prof = new Profile;
	        $prof->load($profile->id);
	        $profile->age = $prof->age();
	        $profile->height = $prof->printProfile("height");
        }
        
    	$page = $get->page;
    	if($page==""){$page=1;}
        
        foreach (array_slice($profiles, ($page-1)*60, 60) as $profile){
		        $prof = new Profile;
		        $prof->load($profile->id);
                
                if ($profile->photo != '' && $profile->thumbnail == '') {
                    $ds = DIRECTORY_SEPARATOR;
                    $thumbsFolder = $_SERVER['DOCUMENT_ROOT'].'/images/profiles/thumbnails/';
                    $profileThumbFolder = $thumbsFolder.urlencode($profile->id).$ds;
                    mkdir($profileThumbFolder, 0777);
                    $thumbTargetFile = $profileThumbFolder.basename(uniqid().".jpg");
                    $file = $profile->photo;
                    $size = getimagesize($file);
                    $width = 550;
                    $src = imagecreatefromstring(file_get_contents($file));
                    $dst = imagecreatetruecolor($width,$width);
                    $success = imagecopyresampled($dst,$src,0,0,0,0,$width,$width,$size[0],$size[0]);
                    imagedestroy($src);
                    if ($success) imagejpeg($dst,$thumbTargetFile);
                    imagedestroy($dst);
                    if ($success) {
                        $arr["thumbnail"] = $thumbTargetFile;
                        $prof->update($arr);
                    }
                }
        }
        
        Tpl::get('dashboard.tpl', array(
        
            'userName' => $user->table->name,
            'profiles' => array_slice($profiles, ($page-1)*60, 60),
            'profileGroups' => $profileGroups,
            'castingLists' => $castingLists,
            'fields' => $fields,
            'page' => $page,
            'sort' => $sort,
            'count' => count($profiles)
        ));
    }
    
    public function ProfileGroupsAction() {
        $this->checkAuth();
        
    	global $fields;
    	
    	$userid = User::getId();
    	$user = new User;
        $user->load($userid);
        
        $profileGroupsToShow = [];
        $profileGroups = new ProfileGroup;
        $profileGroups = $profileGroups->table->findAll();
        
        foreach($profileGroups as $profileGroup){
        	$group = new ProfileGroup;
        	$profileGroupsToShow[$profileGroup->id]["id"] = $profileGroup->id;
        	$profileGroupsToShow[$profileGroup->id]["name"] = $profileGroup->name;
        	$profileGroupsToShow[$profileGroup->id]["profiles"] = $profileGroup->profiles;
        	$profiles = $group->getProfiles($profileGroup->id);
        	foreach($profiles as $profileObj){
        		$profile = new Profile;
        		$profile->load($profileObj);
        		
        		if($profile->table->firstName){
	        		$profileGroupsToShow[$profileGroup->id]["count"]++;
        		}
        		
        		if(isset($profile->table->photo) and strlen($profile->table->photo)>0){
        			$profileGroupsToShow[$profileGroup->id]["photos"][]=$profile->table->thumbnail;
        		}
        	}
        }
    	
        Tpl::get('groups.tpl', array(
            'js' => $js,
            'userName' => $user->table->name,
            'profileGroups' => $profileGroupsToShow,
            'fields' => $fields
        ));
    }
    
    public function KeywordsAction() {
        $this->checkAuth();
        
        $keywordsArr = [];
        
    	global $fields;
    	
    	$userid = User::getId();
    	$user = new User;
        $user->load($userid);
        
        $db = DB::getInstance();
        
        $sql = "SELECT `keywords` FROM `profiles` WHERE `keywords`!='' GROUP BY `keywords`";
        $stmt = $db->query($sql);
        while ($row = $stmt->fetchOBject()) {
        	$keywords = json_decode($row->keywords);
	        foreach($keywords as $key => $word) {
	            $keywordsArr[$word]++;
	        }
        }
        
        Tpl::get('keywords.tpl', array(
            'js' => $js,
            'userName' => $user->table->name,
            'keywords' => $keywordsArr,
            'fields' => $fields
        ));
    }
    
    public function ProfileGroupsAddAction() {
        $this->checkAuth();
        
    	global $fields;
    	
    	$userid = User::getId();
    	$user = new User;
        $user->load($userid);
        $get = new Request($_GET);
        
        $profileGroup = new ProfileGroup;
        $addPG['name'] = $get->name;
        $addPG['favs'] = "[]";
        $addPG['profiles'] = explode(",", $get->profiles);
        $profileGroup->add($addPG);
        
        System::redirect('/groups/view?id='.$profileGroup->table->id);
        
    }
    
    public function ProfileGroupsAddToAction() {
        $this->checkAuth();
        
    	global $fields;
    	
    	$userid = User::getId();
    	$user = new User;
        $user->load($userid);
        $get = new Request($_GET);
        
        $profileGroup = new ProfileGroup;
        $profileGroup->load($get->id);
        $profiles = array_unique(array_merge(explode(",", $get->profiles), $profileGroup->getProfiles($get->id)));
        foreach($profiles as $key => $val){
	        $jsonFinal[]=$val;
        }
        $addPG["profiles"] = json_encode($jsonFinal);
        $profileGroup->update($addPG);
        
        System::redirect('/groups/view?id='.$profileGroup->table->id);
        
    }
    
    public function ProfileGroupsDeleteFromAction() {
        $this->checkAuth();
        
    	global $fields;
    	
    	$userid = User::getId();
    	$user = new User;
        $user->load($userid);
        $get = new Request($_GET);
        
        $profileGroup = new ProfileGroup;
        $profileGroup->load($get->id);
        $profilesOld = $profileGroup->getProfiles($get->id);
        $profilesNew = [];
        
        foreach($profilesOld as $val){
	        if($val != $get->profile){
		        $profilesNew[] = $val;
	        }
        }        
        
        $addPG['profiles'] = json_encode($profilesNew);
        $profileGroup->update($addPG);
        
        System::redirect('/groups/view?id='.$profileGroup->table->id);
        
    }

    public function ProfileGroupsDeleteMultipleFromAction() {
        $this->checkAuth();    
            
        $get = new Request($_GET);

        $removed = explode(',', $get->profiles);
        $profileGroup = new ProfileGroup;
        $profileGroup->load($get->id);
        $profilesOld = $profileGroup->getProfiles($get->id);
        $profilesNew = [];
        
        foreach($profilesOld as $val){
            if(!in_array($val, $removed)){
                $profilesNew[] = $val;
            }
        }       
        
        $addPG['profiles'] = json_encode($profilesNew);
        $profileGroup->update($addPG);
        
        System::redirect('/groups/view?id='.$profileGroup->table->id);        
    }

    
    public function ProfileGroupsEditAction() {
        $this->checkAuth();
    	global $fields;
    	
    	$userid = User::getId();
    	$user = new User;
        $user->load($userid);
        $get = new Request($_GET);
        
        $profileGroup = new ProfileGroup;
        $addPG['name'] = $get->name;
        $profileGroup->load($get->id);
        $profileGroup->update($addPG);
        
        System::redirect('/groups');
        
    }
    
    public function ProfileGroupsDeleteAction() {
        $this->checkAuth();
    	global $fields;
    	
    	$userid = User::getId();
    	$user = new User;
        $user->load($userid);
        $get = new Request($_GET);
        
        $profileGroup = new ProfileGroup;
        $profileGroup->load($get->id);
        $profileGroup->delete();
        
        System::redirect('/groups');
        
    }
    
    public function ProfileGroupsViewAction() {        
    	global $fields;
        
        $get = new Request($_GET);
    	
    	$userid = User::getId();
    	$user = new User;
        $user->load($userid);
        
        
        $profileGroup = new ProfileGroup;
        $profilesInGroup = $profileGroup->getProfiles($get->id);
        $favProfilesInGroup = $profileGroup->getFavs($get->id);
        $hiddenProfilesInGroup = $profileGroup->getHidden($get->id);
        $commentsProfilesInGroup = $profileGroup->getComments($get->id);
        
        if (!$userid && !count($profilesInGroup)) {
            System::redirect('/login');
        }
        
        $profiles = new Profile;
        if($get->sort=="alph"){
        	setcookie("sort", "alph");
        	$profiles = $profiles->table->findAll("firstName");
        	$sort = "alph";
        }elseif($get->sort=="date"){
        	setcookie("sort", "date");
        	$profiles = $profiles->table->findAll();
        	$sort = "date";
        }else{
	    	if($_COOKIE["sort"]=="alph"){
		    	$profiles = $profiles->table->findAll("firstName");
				$sort = "alph";
	    	}else{
		    	$profiles = $profiles->table->findAll();
				$sort = "date";
	    	}
        }
        
        foreach ($profiles as $profile){
        	if(in_array($profile->id, $profilesInGroup)){
		        $prof = new Profile;
		        $prof->load($profile->id);
		        $profile->age = $prof->age();
		        $profile->height = $prof->printProfile("height");
                $profile->fav = in_array($profile->id, $favProfilesInGroup);
                $profile->hidden = in_array($profile->id, $hiddenProfilesInGroup);
                $profile->comment = $commentsProfilesInGroup->{$profile->id};
                
                if ($profile->photo != '' && $profile->thumbnail == '') {
                    $ds = DIRECTORY_SEPARATOR;
                    $thumbsFolder = $_SERVER['DOCUMENT_ROOT'].'/images/profiles/thumbnails/';
                    $profileThumbFolder = $thumbsFolder.urlencode($profile->id).$ds;
                    mkdir($profileThumbFolder, 0777);
                    $thumbTargetFile = $profileThumbFolder.basename(uniqid().".jpg");
                    $file = $profile->photo;
                    $size = getimagesize($file);
                    $width = 550;
                    $src = imagecreatefromstring(file_get_contents($file));
                    $dst = imagecreatetruecolor($width,$width);
                    $success = imagecopyresampled($dst,$src,0,0,0,0,$width,$width,$size[0],$size[0]);
                    imagedestroy($src);
                    if ($success) imagejpeg($dst,$thumbTargetFile);
                    imagedestroy($dst);
                    if ($success) {
                        $profile2 = new Profile;
                        $profile2->load($profile->id);
                        $arr["thumbnail"] = $thumbTargetFile;
                        $profile2->update($arr);
                    }
                    
                }
                
                
                $profilesFinal[]=$profile;
	        }
        }
    	
    	$profileGroup = new ProfileGroup;
        $profileGroup->load($get->id);
        
        
        $profileGroups = new ProfileGroup;
        $profileGroups = $profileGroups->table->findAll();

        $castingLists = new CastingList;
        $castingLists = $castingLists->table->findAll();
        
        $page = $get->page;
    	if($page==""){$page=1;}
    	
        Tpl::get('dashboardGroup.tpl', array(
            'userName' => $user->table->name,
            'profiles' => array_slice($profilesFinal, ($page-1)*60, 60),
            'profileGroups' => $profileGroups,
            'castingLists' => $castingLists,
            'group' => $profileGroup,
            'groupID' => $get->id,
            'fields' => $fields,
            'page' => $page,
            'sort' => $sort,
            'count' => count($profilesFinal)
        ));
        
    }
    
    public function ProfileGroupsToggleFavAction() {
        global $fields;
    	
    	$userid = User::getId();
    	$user = new User;
        $user->load($userid);
        $get = new Request($_GET);
        
        $status = "";

        $profileGroup = new ProfileGroup;
        $profileGroup->load($get->list_id);
        
        $favs = $profileGroup->getFavs($get->list_id);

        if (($key = array_search($get->profile, $favs)) !== false) {
            unset($favs[$key]);
            $status = "removed";
        } else {
            $favs[] = $get->profile;
            $status = "added";
        }

        $addPG["favs"] = json_encode(array_values($favs));

        $profileGroup->update($addPG);  
        
        echo $status;
    }
    public function ProfileGroupsToggleHideAction() {
        global $fields;
    	
    	$userid = User::getId();
    	$user = new User;
        $user->load($userid);
        $get = new Request($_GET);
        
        $status = "";

        $profileGroup = new ProfileGroup;
        $profileGroup->load($get->list_id);
        
        $favs = $profileGroup->getHidden($get->list_id);

        if (($key = array_search($get->profile, $favs)) !== false) {
            unset($favs[$key]);
            $status = "removed";
        } else {
            $favs[] = $get->profile;
            $status = "added";
        }

        $addPG["hidden"] = json_encode(array_values($favs));

        $profileGroup->update($addPG);  
        
        echo $status;
    }
    
    
    
    public function CommentSaveAction() {
        global $fields;
    	
    	$userid = User::getId();
    	$user = new User;
        $user->load($userid);
        $get = new Request($_GET);
        
        $status = "";

        $profileGroup = new ProfileGroup;
        $profileGroup->load($get->list_id);
        
        $favs = $profileGroup->getComments($get->list_id);
        $favs->{$get->profile} = $get->comment;
        $addPG["comments"] = json_encode($favs);

        $profileGroup->update($addPG);  
        
        echo $status;
    }

}
