<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Records
 *
 * @author Anton
 */
class RecordsModel extends Admin {

    public function ListAction() {
        $is_admin = Services::isAvailable(Services::SERVICE_ADMINISTRATOR);
        $req = new Request($_REQUEST);
        $db = DB::getInstance();

        $passes = [];
        $sql = false;
        $badRecords = false;
        if (isset($req->filters['company']) && $req->filters['company'] && $req->filters['company'] == '-1') {
            $sql = "select r.* from records r where (insideError=1 or (insideError=0 and insideDate='0000-00-00 00:00:00') ) and ";
            $badRecords = true;
        }
        if (!$sql) {
            $sql = "select r.* from records r left join passes p on r.passID=p.id ";
        }
        $filters = [];
        if (isset($req->filters))
            $filters = $req->filters;

        $where = [];
        if (!$badRecords && $is_admin && isset($filters['company']) && $filters['company'] > 0) {
            $where[] = 'p.companyID=' . intval($filters['company']);
        }
        if (!$is_admin) {
            $where[] = 'p.companyID=' . intval($this->user->getCompanyID());
            $passes = Pass::getList(false, $this->user->getCompanyID());
        } else {
            $passes = Pass::getList();
        }
        if (isset($filters['passID']) && $filters['passID'] > 0) {
            $where[] = 'r.passID=' . intval($filters['passID']);
        }
        if (isset($filters['inside']) && $filters['inside'] > 0) {
            $where[] = 'r.outsideDate=\'0000-00-00 00:00:00\'';
        }
        if (isset($filters['dateFrom'], $filters['dateTo'])) {
            $to = date("Y-m-d", strtotime($filters['dateTo']) + 60 * 60 * 24);
            $str = false;
            
            if (!$badRecords) {
                $str = '(r.insideError=0 and r.insideDate between ' . $db->quote($filters['dateFrom']) . " and " . $db->quote($to) . ") ";
            } else {
                $str = '(((r.insideDate between ' . $db->quote($filters['dateFrom']) . " and " . $db->quote($to) . ") or (r.outsideDate between " . $db->quote($filters['dateFrom']) . " and " . $db->quote($to) . " )))";

            }
            $where[] = $str;
        }

        if (!empty($where)) {
            if (!$badRecords) {
                $sql .= " where " . implode(" and ", $where);
            } else {
                $sql .= "  " . implode(" and ", $where);
            }
        }

        if (!empty($req->filters) && $this->is_ajax) {
            $sql .= " order by r.id desc";
            //echo $sql;
            $records = [];
            $stmt = $db->query($sql);
            while ($row = $stmt->fetchOBject()) {
                $records[] = new Record($row->id);
            }

            Tpl::get('records/blocks.tpl', array('records' => $records
                , 'is_admin' => $is_admin,));
            exit;
        }

        $js = [];
        $js[] = 'records/list.js';
        $js[] = '../assets/plugins/isotope/jquery.isotope.min.js';
        $js[] = '../assets/plugins/bootstrap-daterangepicker/moment.js';
        $js[] = '../assets/plugins/bootstrap-daterangepicker/daterangepicker.js';
        $css = [];
        $css[] = 'assets/plugins/isotope/isotope.css';
        $css[] = 'assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css';
        Tpl::get('records/list.tpl', array(
            'css' => $css,
            'js' => $js,
            'passes' => $passes,
            'is_admin' => $is_admin,
        ));
    }

}
