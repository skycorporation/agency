<?php

class CronModel extends AModel {

    public function __construct() {
        parent::__construct();
        if (getenv("REMOTE_ADDR") != '46.182.27.101') {
            exit("Localhost only");
        }
    }

    public function IndexAction() {
        $w = new Worker;
        $w->run();
    }

}
