<?php
function generate_code()
{
    $seed = str_split('123456789');
    shuffle($seed); // probably optional since array_is randomized; this may be redundant
    $rand = '';
    foreach (array_rand($seed, 5) as $k) $rand .= $seed[$k];
    
    return $rand;
}

class IndexModel extends AModel {

    public function IndexAction() {
        if (User::getId() > 0) {
            System::redirect('/dashboard');
        }
        System::redirect('/login');
    }

    public function ExitAction() {
        foreach ($_COOKIE as $k => $v) {
            setcookie($k, '-1', -1, '/');
        }
        System::redirect('/');
    }

    public function ForgotAction() {
        $post = new Request($_POST);
        $get = new Request($_GET);
        $step = 1;
        if (isset($get->email, $get->hash) && $get->hash == md5("hashkey" . $get->email)) {
            $step = 2;
        }
        if (isset($post->action)) {
            $ret = array();
            $ret['status'] = 'error';
            $ret['error'] = 'Ошибка';
            if ($post->action == 'reset' && isset($post->email, $post->hash) && md5("hashkey" . $post->email) == $post->hash) {
                $user = new User;
                try {
                    if ($post->password != $post->password2) {
                        throw new Exception("Пароли не совпадают", 100);
                    }
                    if (strlen($post->password) < 6) {
                        throw new Exception("Пароль должен быть 6 и более символов", 101);
                    }
                    if (!$user->findByEmail($post->email)) {
                        throw new Exception("Пользователь не найден");
                    }
                    $user->setPassword($post->password);
                    $ret['status'] = 'ok';
                } catch (Exception $e) {
                    $ret['error'] = $e->getMessage();
                }
            }
            if ($post->action == 'forgot') {
                $user = new User;
                try {
                    if (!$user->findByEmail($post->email)) {
                        throw new Exception("Пользователь не найден");
                    }
                    $user->sendForgotLetter();
                    $ret['status'] = 'ok';
                } catch (Exception $e) {
                    $ret['error'] = $e->getMessage();
                }
            }
            echo json_encode($ret);
            exit;
        }
        if ($step == 1) {
            $js[] = 'frontend/assets/js/app/forgot.js';
            Tpl::get('frontend/forgot.tpl', array(
                'js' => $js,
            ));
        }
        if ($step == 2) {
            $js[] = 'frontend/assets/js/app/forgot_step2.js';
            Tpl::get('frontend/forgot_step2.tpl', array(
                'js' => $js,
                'email' => $get->email,
            ));
        }
    }

    public function LoginAction() {
        $post = new Request($_POST);
        if (isset($post->action)) {
            $ret = array();
            $ret['status'] = 'error';
            $ret['error'] = 'Ошибка';
            if ($post->action == 'login') {
                $user = new User;
                try {
                    if($user->login($post->username, $post->password)){
	                    $ret['status'] = 'ok';
	                    System::redirect('/dashboard');
                    }else{
	                    System::redirect('/login?err=1');
                    }
                    exit;
                    
					
                } catch (Exception $ex) {
	                System::redirect('/login?err=1');
                    $ret['error'] = $ex->getMessage();
                }
            }
            echo json_encode($ret);
            exit;
        }
        Tpl::get('login.tpl', array(
            
        ));
    }

    public function RegistrationAction() {
        $post = new Request($_POST);
        $get = new Request($_GET);

        $step = 1;
        $company = new Company();
        if (isset($post->action)) {
            $ret = array();
            $ret['status'] = 'error';
            $ret['error'] = "Ошибка";
            if ($post->action == 'registration_confirms' && isset($post->email, $post->hash) && $post->hash == md5("hashkey" . $post->email)) {
                $user = new User;
                $user->findByEmail($post->email);
                $user->setNames(array(
                    'firstname' => $post->firstname,
                    'middlename' => $post->middle,
                    'lastname' => $post->lastname,
                ));
                $user->update(array(
                    'startYear' => (int) $post->startYear,
                    'position' => $post->position,
                    'active' => 1,
                ));
                $user->setAuth();
                $user->sendRegistrationCompleteLetter();
                System::redirect('/dashboard');
            }
            if ($post->action == 'tryRegister' && isset($post->code, $post->email, $post->password, $post->password2)) {
                try {
                    if ($post->password != $post->password2) {
                        throw new Exception("Пароли не совпадают", 100);
                    }
                    if (strlen($post->password) < 5) {
                        throw new Exception("Пароль должен быть 6 и более символов", 101);
                    }
                    $company = new Company;
                    $config = Registry::get('config');
                    $user = new User;
                    $userRow = new stdClass();
                    $userRow->companyID = $company->canIRegisterByCode($post->code);
                    $company->table->findById($userRow->companyID);
                    if ($company->table->current_users == 0) {
                        $userRow->is_admin = 1;
                    }
                    $userRow->email = $post->email;
                    $user->setEmail($post->email);
                    $userRow->pass = md5($config->salt . $post->password);
                    $userRow->regDateTime = date("Y-m-d H:i:s");
                    $userRow->creationDateTime = date("Y-m-d H:i:s");

                    if (!$user->addUser($userRow)) {
                        throw new Exception("E-mail уже существует", 102);
                    }
                    if (!$user->sendRegistrationLetter()) {
                        throw new Exception("Неудалось отправить письмо", 103);
                    }



                    $company->table->update(array(
                        'current_users' => $company->table->current_users + 1
                    ));
                    $ret['status'] = 'ok';
                    // отослать уведомление
                } catch (Exception $ex) {
                    $ret['error'] = $ex->getMessage();
                }
            }
            if ($post->action == 'check_code') {

                $company = new Company;
                try {
                    $company->canIRegisterByCode($post->code);
                    $ret['status'] = 'ok';
                } catch (Exception $ex) {
                    $ret['status'] = 'error';
                    $ret['error'] = $ex->getMessage();
                }
            }
            echo json_encode($ret);
            exit;
        }
        if (isset($get->email, $get->hash) && md5("hashkey" . $get->email) == $get->hash) {
            $step = 2;
            $user = new User;
            if (!$user->findByEmail($get->email)) {
                System::redirect('/?error=1');
            }
            $company->load($user->getCompanyID());
        }
        $js[] = 'assets/plugins/parsley/dist/parsley.js';
        $js[] = 'assets/plugins/parsley/src/i18n/ru.js';
        $js[] = 'frontend/assets/js/app/registration.js';
        $css[] = 'assets/plugins/parsley/src/parsley.css';

        if ($step == 1) {
            Tpl::get('frontend/registration.tpl', array(
                'js' => $js,
                'css' => $css,
            ));
        }
        if ($step == 2) {
            //$js = $css = [];
            $css[] = 'assets/plugins/bootstrap-wizard/css/bwizard.min.css';
            $js[] = 'assets/plugins/bootstrap-wizard/js/bwizard.js';
            $js[] = 'frontend/assets/js/app/registration_step2.js';

            Tpl::get('frontend/registration_step2.tpl', array(
                'js' => $js,
                'css' => $css,
                'company' => $company,
            ));
        }
    }

}
