<?php

class ServerModel extends AModel {

    /**
     *
     * @var Server_Request
     */
    protected $request;

    /**
     *
     * @var Server_Response
     */
    protected $response;
    protected $post = array();

    public function __construct() {
        $logger = new Server_Logger();
        $logger->enable();
        //$logger->disable();

        $this->request = new Server_Request($logger);
        //$this->request->debug(); //@TODO: Убрать из прод

        $this->response = new Server_Response($logger);
    }

    protected function outside() {
        $cache = Registry::get('cache');
        $carRegion = $this->post->carRegion;
        $carNumber = $this->post->carNumber;

        $cache_key = 'request/outside/' . $carNumber . '/' . $carRegion;
        $status = $cache->get($cache_key, false);
        if (preg_match("/\*/", $carNumber . $carRegion))
            $status = 1;

        if ($status) {
            $this->response->answer(Server_Response::MESSAGE_IDLE);
            $this->response->send();
            exit;
        }
        $pass = new Pass;
        $error = false;
        $record = new Record;
        try {
            $record = Record::getLatestRecordByCar($carNumber, $carRegion);
            $pass = $record->pass;
        } catch (Exception $ex) {
            //    $this->response->error($e->getMessage());
        }
        $inside = strtotime(str_replace("-", "/", $record->insideDate));
        $outside = time();
        $diff = $outside - $inside; //seconds
        $stayHours = ceil($diff / (60 * 60));
        $stayDays = ceil($diff / (60 * 60 * 24));
        $stayMonths = ceil($diff / (60 * 60 * 24 * 31));

        if (!isset($record->pass->subPass) || $record->pass->subPass->id === false) {
            $record = new Record;
            //$record->setPass($pass);
            $record->setOutside();
            $record->setCar($carNumber, $carRegion);
            $record->create();
            $this->response->answer(Server_Response::MESSAGE_GATE, 1);
            $this->response->send();
            Worker::addWork("download_number", array(
                'recordID' => $record->id,
                'url' => $this->post->url,
            ));
            exit;
        }
        $subPass = $record->pass->subPass;
        // Возможно пассТайм предоплатнывй, а пропуск не оплачен?
        if ($subPass->getPassType()->prepayment == '0' && $pass->is_payed == '0') {
            Worker::addWork('make_invoice', array(
                'companyID' => $pass->company->id,
                'authorID' => $pass->author->id,
                'sub' => $subPass->id,
                'passID' => $pass->id,
            ));
        }

        if ($subPass->stayValue > 0) {
            //существует лимит по времени
            $finePeriod = 0;
            if ($subPass->finePeriod == 1) {
                $finePeriod = 60 * 60;
            }
            if ($subPass->finePeriod == 2) {
                $finePeriod = 60 * 60 * 24;
            }
            if ($subPass->finePeriod == 3) {
                $finePeriod = 60 * 60 * 24 * 31;
            }
            $fineCount = 0;
            $desc = '';
            if ($subPass->stayPeriod == 1 && $stayHours > $subPass->stayValue) {
                //лимит по часам
                $stayHours-=$subPass->stayValue;

                $fineCount = ceil($stayHours);
                $desc = 'Превышение времени стоянки на ' . $fineCount . " час. \r\nВремя заезда: " . date("d.m.Y H:i:s", $inside) . "\r\nВремя выезда: " . date("d.m.Y H:i:s");
            }
            if ($subPass->stayPeriod == 2 && $stayDays > $subPass->stayValue) {
                $stayDays-=$subPass->stayValue;
                $fineCount = ceil($stayDays);
                $desc = 'Превышение времени стоянки на ' . $fineCount . " день. \r\nВремя заезда: " . date("d.m.Y H:i:s", $inside) . "\r\nВремя выезда: " . date("d.m.Y H:i:s");
            }
            if ($subPass->stayPeriod == 3 && $stayMonths > $subPass->stayValue) {
                $stayMonths-=$subPass->stayValue;
                $fineCount = ceil($stayMonths);
                $desc = 'Превышение времени стоянки на ' . $fineCount . " мес. \r\nВремя заезда: " . date("d.m.Y H:i:s", $inside) . "\r\nВремя выезда: " . date("d.m.Y H:i:s");
            }
            if ($subPass->stayPeriod == 0 && $stayHours > $subPass->stayValue) {
                $fineCount = 1;
                $desc = 'Превышение времени стоянки. ';
            }

            $desc = "Автомобиль: $carNumber ($carRegion). " . $desc;
            $fineAmount = round($fineCount * $subPass->fine, 2);
            // Если >0 сделать штраф
            if ($fineAmount > 0) {
                Worker::addWork("make_fine", array(
                    'amount' => $fineAmount,
                    'companyID' => $pass->company->getId(),
                    'authorID' => $pass->author->id,
                    'passID' => $pass->id,
                    'recordID' => $record->id,
                    'desc' => $desc,
                ));
            }
            $desc = '';
        }
        $passType = $subPass->getPassType();
        $outsideNormal = strtotime(date("d.m.Y", $inside) . " " . $passType->hourTo . ":00:00");

        if ($passType->hourFrom > 0 && time() > $outsideNormal) {
            // существует лимит у группы

            $diff = time() - $outsideNormal;
            //$stayHours = ceil($diff / (60 * 60));
            //$stayDays = ceil($diff / (60 * 60 * 24));
            //$stayMonths = ceil($diff / (60 * 60 * 24 * 31));
            $finePeriod = 0;

            $type = $passType->finePeriod;
            $fine = $passType->fine;
            if ($fine > 0) {

                $finePeriod = 0;
                if ($type == 1) {
                    $finePeriod = 60 * 60;
                }
                if ($type == 2) {
                    $finePeriod = 60 * 60 * 24;
                }
                if ($type == 3) {
                    $finePeriod = 60 * 60 * 24 * 31;
                }
                if ($type == 0) {
                    $finePeriod = 1;
                }
                $desc = "Автомобиль: $carNumber ($carRegion). Превышение времени стоянки. \r\nВремя заезда: " . date("d.m.Y H:i:s", $inside) . "\r\nВремя выезда: " . date("d.m.Y H:i:s") . "\r\nНеобходимо было выехать до " . date("d.m.Y H:i:s", $outsideNormal);
                $fineCount = 1;
                if ($diff > $finePeriod && $finePeriod > 1) {
                    $fineCount = ceil($diff / $finePeriod);
                }
                $fineAmount = $fineCount * $fine;

                Worker::addWork("make_fine", array(
                    'amount' => $fineAmount,
                    'companyID' => $pass->company->getId(),
                    'authorID' => $pass->author->id,
                    'passID' => $pass->id,
                    'recordID' => $record->id,
                    'desc' => $desc,
                ));
            }
        }
        Worker::addWork("download_number", array(
            'recordID' => $record->id,
            'url' => $this->post->url,
        ));
        $cache->set($cache_key, true, 1);
        $record->proxyTable()->update(
                array(
                    'outsideDate' => date("Y-m-d H:i:s"),
                    'outsideError' => $desc != '',
        ));
        $this->response->answer(Server_Response::MESSAGE_GATE, 1);
    }

    protected function inside() {
        $cache = Registry::get('cache');
        $carRegion = $this->post->carRegion;
        $carNumber = $this->post->carNumber;

        $cache_key = 'request/inside/' . $carNumber . '/' . $carRegion;
        $status = $cache->get($cache_key, false);
        if ($status) {
            $this->response->answer(Server_Response::MESSAGE_IDLE);
            $this->response->send();
            exit;
        }

        $pass = new Pass;
        $error = false;
        try {
            $pass = Pass::getByCar($carNumber, $carRegion);
        } catch (Exception $e) {
            $this->response->error($e->getMessage());
        }
        // Проверяем, возможно компания заблокирована?
        if ($pass->company->isBlocked()) {
            $this->response->error("company blocked");
        }
        // Возможно саб пасс заблокирован?
        if ($pass->subPass->isBlocked()) {
            $this->response->error("SubPass is blocked");
        }
        $passType = $pass->subPass->getPassType();
        // Возможно пасс заблокирован?
        if ($passType->isBlocked()) {
            $this->response->error("PassType is blocked");
        }

        // возможно период пропуска просрочен?
        $open = strtotime($pass->openDate);
        $close = strtotime($pass->expireDate) + 60 * 60 * 24;


        if (time() > $close || time() < $open) {
            $this->response->error("Open or Close date expired");
        }

        // Возможно пассТайм предоплатнывй, а пропуск не оплачен?
        if ($passType->prepayment == '1' && $pass->is_payed == '0') {
            $this->response->error('PassType is prepayment, but pass not payed');
        }

        // Возможно пропуск ограничен часами?
        if ($passType->hourFrom != '' && $passType->hourFrom > 0) {
            // Пропуск ограничен по времени...
            if ($passType->hourFrom > date("H") || $passType->hourTo < date("H")) {
                // Пропуск не попал по времени
                $this->response->error('PassType has hourFrom, hourTo limits.');
            }
        }

        if ($passType->count > 0 && count($pass->getRecords()) >= $passType->count) {
            $this->response->error('PassType count limits');
        }
        $error = $this->response->isError();
        $record = new Record;
        $record->setPass($pass);
        $record->setInside();
        $record->setCar($carNumber, $carRegion);
        $cache->set($cache_key, time(), 1);
        if ($error) {
            //записать ероринг
            $record->setInsideError($error);
            $record->create();
            $this->response->answer(Server_Response::MESSAGE_GATE, 0);
            $this->response->send();
            Worker::addWork("download_number", array(
                'recordID' => $record->id,
                'url' => $this->post->url,
            ));

            exit;
        }
        $record->create();
        Worker::addWork("download_number", array(
            'recordID' => $record->id,
            'url' => $this->post->url,
        ));
        $this->response->answer(Server_Response::MESSAGE_GATE, 1);  // 
    }

    public function IndexAction() {

        fwrite(fopen("api_log.txt", "a"), date("r") . "\r\n" . var_export($_REQUEST, 1));
        $this->request->debug();
        if (!$this->request->is_valid()) {
            $this->response->error('Request invalid');
            $this->response->send();
            exit;
        }
        $this->post = new Request($_REQUEST);
        if ($this->post->direction == '1') {
            $this->inside();
        }
        if ($this->post->direction == '2') {
            $this->outside();
        }
        $this->response->send();
        exit;
    }

}
