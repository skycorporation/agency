<?php

class InvoicesModel extends Admin {

    public function ViewAction($args) {
        $id = $args[0];
        $is_admin = Services::isAvailable(Services::SERVICE_ADMINISTRATOR) || Services::isAvailable(Services::SERVICE_INVOICE);


        try {
            $inv = new Invoice((int) $id);
        } catch (Exception $e) {
            exit("Bad id");
        }


        if (!$is_admin && ( $this->user->id != $inv->author->id && $this->user->getCompanyID() != $inv->company->getId() )) {
            exit("403");
        }
        $cache = Registry::get('cache');
        $seller = $cache->get('details');
        $buyer = $inv->company->getDetails();
        $file = 'view';
        if (isset($_GET['mode']) && $_GET['mode'] == 'print') {
            $file = 'print';
        }
        if (isset($_GET['mode']) && $_GET['mode'] == '1с') {
            $file = '1c';
        }
        
        if (isset($_GET['mode']) && $_GET['mode'] == 'pdf') {

            include("inc/mpdf/mpdf.php");
            $pdf = new mPDF();
            $file = 'print';
            $html = Tpl::fetch('invoices/' . $file . '.tpl', array(
                        'i' => $inv,
                        'seller' => $seller,
                        'buyer' => $buyer,
            ));
            error_reporting(0);
            $html = str_replace("<script>window.print()</script>", '', $html);
            //var_dump($html);exit;
            $pdf->writeHTML($html);
            $pdf->Output('Счет #' . $inv->id . '.pdf', 'D');
            exit;
        }
        Tpl::get('invoices/' . $file . '.tpl', array(
            'i' => $inv,
            'seller' => $seller,
            'buyer' => $buyer,
        ));
    }

    public function CreateAction() {
        $req = new Request($_REQUEST);
        if (isset($req->cart)) {
            $cart = json_decode($req->cart, 1);
            if (!$cart) {
                exit("Bad Cart");
            }
            $passCreator = new Pass_Creator;
            $passCreator->setAuthor($this->user);
            $passCreator->setCompany($this->user->getCompany());


            $inv = new Invoice;
            $inv->setUser($this->user);
            $passIds = [];
            foreach ($cart as $subId => $count) {
                $passCreator->setSubPass($subId);
                $inv->addPosition($subId, $count);
                $ids = $passCreator->create($count);
                foreach ($ids as $id) {
                    $passIds[] = $id;
                }
            }

            $i = new stdClass();
            $i->authorID = $this->user->id;
            $i->companyID = $this->user->getCompanyID();
            $i->amount = $inv->total();
            $i->creationDateTime = date("Y-m-d H:i:s");
            $i->is_payed = 0;
            $i->adminID = 0;
            $i->positions = json_encode($cart);
            $i->fineID = 0;
            $inv->add($i);

            $inv->bindPasses($passIds);
            System::redirect('/invoices/view/' . $inv->id . '/?new');
        }
    }

    public function RenderAction() {
        $invoice = new Invoice;
        $user = new User;

        if (isset($_REQUEST['userid'])) {
            try {
                $user->load($id);
            } catch (Exception $e) {
                exit("404");
            }
        } else {
            $user = $this->user;
        }
        $post = new Request($_REQUEST);
        $invoice->setUser($user);
        if (isset($post->cart)) {
            foreach ($post->cart as $subId => $count) {
                $invoice->addPosition($subId, $count);
            }
        }

        Tpl::get('invoices/sidebar.tpl', array(
            'i' => $invoice,
        ));
    }

    public function ListAction() {
        $is_admin = Services::isAvailable(Services::SERVICE_ADMINISTRATOR) || Services::isAvailable(Services::SERVICE_INVOICE);
        $req = new Request($_REQUEST);
        $list = [];

        if (isset($req->action)) {
            if ($req->action == 'changeStatus' && $is_admin) {
                $inv = new Invoice;
                $inv->load($req->id);
                if ($req->val == '1')
                    $inv->setPayment();
                else {
                    $inv->setUnPayment();
                }
            }
            if ($req->action == 'delete') {
                $inv = new Invoice;
                try {
                    $inv->load($req->id);
                } catch (Exception $ex) {
                    exit;
                }
                if ($is_admin || ($this->user->getCompanyID() == $inv->company->getId())) {
                    $inv->delete();
                }
            }
            if ($req->action == 'getList') {

                $inv = new Invoice;
                if ($is_admin) {
                    if ($req->type != 'all') {
                        $list = $inv->getListByFilter("is_payed=" . intval($req->type));
                    } else {
                        $list = $inv->getList();
                    }
                } else {
                    $sub = '';
                    if ($req->type != 'all') {
                        $sub = "is_payed=" . intval($req->type) . " and ";
                    }
                    if ($this->user->getField('is_admin') == '1') {
                        $list = $inv->getListByFilter($sub . "companyID=" . $this->user->getCompanyID());
                    } else {
                        $list = $inv->getListByFilter($sub . 'authorID=' . $this->user->id);
                    }
                }

                Tpl::get('invoices/block_table.tpl', array(
                    'list' => $list,
                    'is_admin' => $is_admin,
                ));
                exit;
            }
            exit;
        }
        $js = [];
        $js[] = 'invoices/list.js';
        Tpl::get('invoices/list.tpl', array(
            'js' => $js,
        ));
    }

    //put your code here
    public function DetailsAction() {
        if (!$this->user->getField('is_admin')) {
            header("HTTP/1.0 403 Forbidden");
            exit("<h3>403 Forbidden</h3>");
        }
        $is_admin = false;
        if (isset($_REQUEST['id']) && $_REQUEST['id'] == 'root') {
            $is_admin = Services::isAvailable(Services::SERVICE_ADMINISTRATOR) || Services::isAvailable(Services::SERVICE_INVOICE);
        }

        $post = new Request($_POST);
        $company = $this->user->getCompany();
        $cache = Registry::get('cache');
        if (isset($post->action)) {
            if ($post->action == 'save' && $post->id != 'root') {
                $company->update(array('details' => json_encode($post->data)));
            }
            if ($post->action == 'save' && $post->id == 'root' && $is_admin) {

                $cache->set('details', $post->data, 99999);
            }
            System::redirect('/invoices/settings?success&id=' . $_REQUEST['id']);
        }
        $data = [];

        if ($is_admin) {
            $data = $cache->get('details', array());
        }
        Tpl::get('invoices/details.tpl', array(
            'success' => isset($_GET['success']),
            'company' => $company,
            'is_admin' => $is_admin,
            'data' => $data,
        ));
    }

}
