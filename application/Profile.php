<?php

class ProfileModel extends AModel {

    public function AddAction() {
    	$this->isAuth();
        
        global $fields;
       	$userid = User::getId();

        $db = DB::getInstance();
        $user = new User;
        $user->load($userid);
        $get = new Request($_GET);
        $profile = new Profile;
        $profileType = 2;
        if(isset($get->profileType)){
	        $profileType = $get->profileType;
        }
        $heightRes = "";
        $post = new Request($_POST);
		$photos = "";

		// Настройки видимости полей
		$availableVisibleFields = array(
			'lastName', 'basedIn', 'height', 'shirtSize', 'waistSize', 'pantSize', 'dressSize', 'bustSize', 'hipSize', 'shoeSize', 'bio', 'knownFor', 'upcomingProjects', 'occupation', 'rate', 'availability', 'ethnicity', 'instagram', 'website', 'book', 'press', 'music', 'video'
		);

		$selectedVisibleFields = array();
		if (isset($post->visible) && is_array($post->visible)) {
			foreach ($availableVisibleFields as $field) {
				if (isset($post->visible[$field])) {
					$selectedVisibleFields[] = $field;
				}
			}
		}


        if (isset($post->action)) {
        	
            if ($post->action == 'save') {
                try {
                
					$uploaddir = $_SERVER['DOCUMENT_ROOT'].'/images/profiles/';
					$uploadfile = $uploaddir . basename(uniqid().".jpg");
					
                	$data = $post->data;
                	if (move_uploaded_file($_FILES['data']['tmp_name']['photo'], $uploadfile)) {
						$data["photo"] = $uploadfile;
					}
					
					$uploadfile = $uploaddir . basename(uniqid().".jpg");
					
					if (move_uploaded_file($_FILES['data']['tmp_name']['photo2'], $uploadfile)) {
						$data["photo2"] = $uploadfile;
					}
					
            		if(strlen($data["height1"])>0){
	            		$data["height"] = $data["height1"].",".$data["height2"];
            		}
                    unset($data["height1"]);
	                unset($data["height2"]);
                    
                    if(strlen($data["birthday"])==''){
                        unset($data["birthday"]);
                    }
            		
            		if($data["gender"]=="on"){$data["gender"]="1";}
            		if($data["gender"]==""){$data["gender"]="0";}
            		
            		$hairColor="";
            		if($data["hairColor8"]=="on"){$hairColor.="8,";}
            		if($data["hairColor1"]=="on"){$hairColor.="1,";}
            		if($data["hairColor2"]=="on"){$hairColor.="2,";}
            		if($data["hairColor3"]=="on"){$hairColor.="3,";}
            		if($data["hairColor4"]=="on"){$hairColor.="4,";}
            		if($data["hairColor5"]=="on"){$hairColor.="5,";}
            		if($data["hairColor6"]=="on"){$hairColor.="6,";}
            		if($data["hairColor7"]=="on"){$hairColor.="7,";}
            		unset($data["hairColor8"]);
            		unset($data["hairColor1"]);
            		unset($data["hairColor2"]);
            		unset($data["hairColor3"]);
            		unset($data["hairColor4"]);
            		unset($data["hairColor5"]);
            		unset($data["hairColor6"]);
            		unset($data["hairColor7"]);
            		$data["hairColor"]=$hairColor;
            		
            		$babyAbilities="";
            		if($data["babyAbilities4"]=="on"){$babyAbilities.="4,";}
            		if($data["babyAbilities1"]=="on"){$babyAbilities.="1,";}
            		if($data["babyAbilities2"]=="on"){$babyAbilities.="2,";}
            		if($data["babyAbilities3"]=="on"){$babyAbilities.="3,";}
            		unset($data["babyAbilities4"]);
            		unset($data["babyAbilities1"]);
            		unset($data["babyAbilities2"]);
            		unset($data["babyAbilities3"]);
            		$data["babyAbilities"]=$babyAbilities;
            		
            		$ethnicity="";
            		if($data["ethnicity8"]=="on"){$ethnicity.="8,";}
            		if($data["ethnicity1"]=="on"){$ethnicity.="1,";}
            		if($data["ethnicity2"]=="on"){$ethnicity.="2,";}
            		if($data["ethnicity3"]=="on"){$ethnicity.="3,";}
            		if($data["ethnicity4"]=="on"){$ethnicity.="4,";}
            		if($data["ethnicity5"]=="on"){$ethnicity.="5,";}
            		if($data["ethnicity6"]=="on"){$ethnicity.="6,";}
            		if($data["ethnicity7"]=="on"){$ethnicity.="7,";}
            		unset($data["ethnicity8"]);
					unset($data["ethnicity1"]);
					unset($data["ethnicity2"]);
					unset($data["ethnicity3"]);
					unset($data["ethnicity4"]);
					unset($data["ethnicity5"]);
					unset($data["ethnicity6"]);
					unset($data["ethnicity7"]);
					$data["ethnicity"]=$ethnicity;
                    
                    $data["bio"] = nl2br($data["bio"]);
                    $data["knownFor"] = nl2br($data["knownFor"]);
					$data["privateNotes"] = nl2br($data["privateNotes"]);
					
					$data["visibleFields"] = json_encode($selectedVisibleFields);
            		
            		if($get->id>0){
            			$profile = new Profile;
            			$profile->load($get->id);
						$photos = glob($_SERVER['DOCUMENT_ROOT'].'/images/photosets/'.urlencode($get->id).'/*.{jpg,png,gif}', GLOB_BRACE);
            			$profile->update($data);
            		}else{
                        //var_dump($data);
	            		$profile = new Profile;
	            		$profile->addProfile($data);
            		}
            		System::redirect('/profile/view?id=' . $profile->table->id);
                    
                } catch (Exception $e) {
                    
                }
            }
        }else{
        	try {
	        	if($get->id>0){
	        		$profile = new Profile;
					$profile->load($get->id);
					$photos = glob($_SERVER['DOCUMENT_ROOT'].'/images/photosets/'.urlencode($get->id).'/*.{jpg,png,gif}', GLOB_BRACE);
					$profileType = $profile->table->profileType;
					
					if($data["gender"]=="on"){$data["gender"]="1";}
            		if($data["gender"]==""){$data["gender"]="0";}
					
					if(isset($get->profileType)){
						$data["profileType"] = $get->profileType;
						$profile->update($data);
						System::redirect('/profile/edit?id=' . $profile->table->id);
					}
					
					if($profileType==2){
						$heightRes = $profile->table->height;
						$heightRes = explode(",", $heightRes);
					}
					
					$hairColorRes = $profile->table->hairColor;
					$hairColorRes = explode(",", $hairColorRes);
					foreach($fields["hairColor"] as $key => $value){
						$hairColor[$key] = in_array($key, $hairColorRes);
					}
					
					$babyAbilitiesRes = $profile->table->babyAbilities;
					$babyAbilitiesRes = explode(",", $babyAbilitiesRes);
					foreach($fields["babyAbilities"] as $key => $value){
						$babyAbilities[$key] = in_array($key, $babyAbilitiesRes);
					}
					
					$ethnicityRes = $profile->table->ethnicity;
					$ethnicityRes = explode(",", $ethnicityRes);
					foreach($fields["ethnicity"] as $key => $value){
						$ethnicity[$key] = in_array($key, $ethnicityRes);
					}
 
                    $profile->table->bio = preg_replace('/<br\s?\/?>/i', "", $profile->table->bio); 
                    $profile->table->knownFor = preg_replace('/<br\s?\/?>/i', "", $profile->table->knownFor); 
                    $profile->table->privateNotes = preg_replace('/<br\s?\/?>/i', "", $profile->table->privateNotes);
                    
                    $keywords = json_decode($profile->table->keywords);
                    $press = json_decode($profile->table->press);
                    $instagram = json_decode($profile->table->instagram);
                    $video = json_decode($profile->table->video);
					$music = json_decode($profile->table->audio);
					
					// Настройки видимости полей
					$visibleFields = json_decode($profile->table->visibleFields);
					$selectedVisibleFields = array();
					foreach ($availableVisibleFields as $field) {
						$selectedVisibleFields[$field] = (array_search($field, $visibleFields) !== false);
					}
				}
	        }catch (Exception $e) {
                    
            }
        }

		$sql = "SELECT `basedin` FROM `profiles` WHERE `basedin`!='' GROUP BY `basedin`";
        $stmt = $db->query($sql);
        while ($row = $stmt->fetchOBject()) {
        	$basedInArr[]=$row->basedin;
        }
        
        $js[] = '/template/app-assets/js/scripts/extensions/dropzone.js';
        $js[] = '/template/js/modelform.js';
        Tpl::get('modelform.tpl', array(
            'js' => $js,
            'userName' => $user->table->name,
            'profileType' => $profileType,
            'hairColor' => $hairColor,
            'babyAbilities' => $babyAbilities,
            'ethnicity' => $ethnicity,
            'profile' => $profile->table,
            'fields' => $fields,
            'height' => $heightRes,
            'photos' => $photos,
            'press' => $press,
            'instagram' => $instagram,
            'video' => $video,
            'basedIn' => $basedInArr,
            'music' => $music,
			'keywords' => implode(', ', $keywords),
			'visibleFields' => $selectedVisibleFields,
         ));
    }
    
    public function ViewAction() {
    	global $fields;
       	$userid = User::getId();

        $user = new User;
        $user->load($userid);
        $get = new Request($_GET);
        $profile = new Profile;
        $profile->load($get->id);
		
		$where = "`id`!=".$get->id;
		//$familyProfiles = new Profile;
		//$familyProfiles = $familyProfiles->table->findAll(false, $where);
        
        $photos = array();
        if($profile->table->photo2!=""){
        	$photos[]=$profile->table->photo2;
        }
        $photos = array_merge ($photos, glob($_SERVER['DOCUMENT_ROOT'].'/images/photosets/'.urlencode($get->id).'/*.{jpg,png,gif,avi,mp4,mkv}', GLOB_BRACE));

        $js[] = '/template/app-assets/js/scripts/gallery/photo-swipe/photoswipe-script.js';
        $js[] = '/template/app-assets/js/scripts/charts/echarts/bar-column/stacked-column.js';
        
        $keywords = json_decode($profile->table->keywords);
        foreach($keywords as $key => $word) {
            $keywords[$key] = "<span>&#35;".$word."</span>";
        } 
        $pressval = json_decode($profile->table->press);
        foreach($pressval as $key => $word) {
            $press[$key] = "<a href='$word' target='_blank'>External Link</a>";
        } 
        $instagramval = json_decode($profile->table->instagram);
        foreach($instagramval as $key => $word) {
            $instagram[$key] = "<a href='https://www.instagram.com/".$word."' target='_blank'>@".$word."</a>";
        } 
        $musicval = json_decode($profile->table->audio);
        foreach($musicval as $key => $word) {
            $music[$key] = "<a href='$word' target='_blank'>External Link</a>";
        } 
        $videoval = json_decode($profile->table->video);
        foreach($videoval as $key => $word) {
            $video[$key] = "<a href='$word' target='_blank'>External Link</a>";
		}
		
		// Настройки видимости полей
		$availableVisibleFields = array(
			'lastName', 'basedIn', 'height', 'shirtSize', 'waistSize', 'pantSize', 'dressSize', 'bustSize', 'hipSize', 'shoeSize', 'bio', 'knownFor', 'upcomingProjects', 'occupation', 'rate', 'availability', 'ethnicity', 'instagram', 'website', 'book', 'press', 'music', 'video'
		);
		$selectedVisibleFields = json_decode($profile->table->visibleFields, true);

		$visibleFields = array();
		foreach ($availableVisibleFields as $field) {
			$visibleFields[$field] = (array_search($field, $selectedVisibleFields) !== false);
		}
        
        $profileGroups = new ProfileGroup;
        $profileGroups = $profileGroups->table->findAll();

        $castingLists = new CastingList;
        $castingLists = $castingLists->table->findAll();
        
        Tpl::get('profile.tpl', array(
            'js' => $js,
            'userName' => $user->table->name,
            'profileType' => $profile->table->profileType,
            'hairColor' => $hairColor,
            'babyAbilities' => $babyAbilities,
            'ethnicity' => $ethnicity,
            //'family' => $familyProfiles,
            'profile' => $profile->table,
            'fields' => $fields,
            'height' => $heightRes,
            'age' => $profile->age(),
            'photos' => $photos,
            'ethnicityPrint' => $profile->printProfile("ethnicity"),
            'keywordsPrint' => implode('', $keywords),
            'instagramPrint' => implode(', ', $instagram),
            'musicPrint' => implode('<br>', $music),
            'videoPrint' => implode('<br>', $video),
            'pressPrint' => implode('<br>', $press),
            'hairColorPrint' => $profile->printProfile("hairColor"),
            'babyAbilitiesPrint' => $profile->printProfile("babyAbilities"),
            'heightPrint' => $profile->printProfile("height"),
            'profileGroups' => $profileGroups,
			'castingLists' => $castingLists,
			'visibleFields' => $visibleFields,
         ));
    }
    
    public function SearchAction() {
        
        $this->isAuth();
        
    	global $fields;
    	
        $db = DB::getInstance();
       	$userid = User::getId();
       	$user = new User;
        $user->load($userid);
        $get = new Request($_GET);
        $profileType = 2;
        if(isset($get->data["profileType"])){
	        $profileType = $get->data["profileType"];
        }
        if(isset($get->profileType)){
	        $profileType = $get->profileType;
        }
        $variables = array();
        $profiles = new Profile;
        $where = "";
        
        $profileGroups = new ProfileGroup;
        $profileGroups = $profileGroups->table->findAll();

        $castingLists = new CastingList;
        $castingLists = $castingLists->table->findAll();
        
		$variables['action'] = 0;
		
        if(isset($get->action)){
        
        	$variables['action'] = 1;
            
            //var_dump($get->data);
        	
            if(strlen($get->data["keywords"])>0){
                
                $keywords = json_decode($get->data["keywords"]);    
                
                foreach ($keywords as $keyword) {
                    if($where != ""){$where.= " AND ";}
        		    $where.="`keywords` LIKE '%".$keyword."%'";
                }
                
        		
        	}
            
        	//names
        	
        	if(strlen($get->data["firstName"])>0){
        		if($where != ""){$where.= " AND ";}
        		$where.="`firstName` LIKE '%".$get->data["firstName"]."%'";
        	}
        	
        	if(strlen($get->data["secondName"])>0){
        		if($where != ""){$where.= " AND ";}
        		$where.="`lastName` LIKE '%".$get->data["secondName"]."%'";
        	}
        	
        
        	//hairColor
        	$whereAdd = "";
	        foreach($fields["hairColor"] as $key => $value){
	        	if($get->data[hairColor.$key] == "on"){
	        		if($whereAdd != ""){$whereAdd.= " OR ";}
		        	$whereAdd.="`hairColor` LIKE '%".$key."%'";
		        }
	        }
	        if($whereAdd!=""){
	        	if($where!=""){$where.=" AND ";}
		        $where.="(".$whereAdd.")";
	        }
	        
	        
        	//profileType
        	$whereAdd = "";
	        $whereAdd.="`profileType`='".$profileType."'";
        	if($where!=""){$where.=" AND ";}
	        $where.="(".$whereAdd.")";
	        
        	//gender
        	$whereAdd = "";
	        foreach($fields["gender"] as $key => $value){
	        	if($get->data[gender.$key] == "on"){
	        		if($whereAdd != ""){$whereAdd.= " OR ";}
		        	$whereAdd.="`gender`='".$key."'";
		        }
	        }
	        if($whereAdd!=""){
	        	if($where!=""){$where.=" AND ";}
		        $where.="(".$whereAdd.")";
	        }
	        
        	//ethnicity
        	$whereAdd = "";
	        foreach($fields["ethnicity"] as $key => $value){
	        	if($get->data[ethnicity.$key] == "on"){
	        		if($whereAdd != ""){$whereAdd.= " OR ";}
		        	$whereAdd.="`ethnicity` LIKE '%".$key."%'";
		        }
	        }
	        if($whereAdd!=""){
	        	if($where!=""){$where.=" AND ";}
		        $where.="(".$whereAdd.")";
	        }
	        
        	//babyAbilities
        	$whereAdd = "";
	        foreach($fields["babyAbilities"] as $key => $value){
	        	if($get->data[babyAbilities.$key] == "on"){
	        		if($whereAdd != ""){$whereAdd.= " OR ";}
		        	$whereAdd.="(`profileType`=0 AND `babyAbilities` LIKE '%".$key."%')";
		        }
	        }
	        if($whereAdd!=""){
	        	if($where!=""){$where.=" AND ";}
		        $where.="(`profileType`!=0 OR ".$whereAdd.")";
	        }
	        
        	//age
        	$whereAdd = "";
        	if($get->data["age"]){
	        	$arr = explode(",", $get->data["age"]);
	        	$variables['ageMin'] = round($arr[0]);
	        	$variables['ageMax'] = round($arr[1]);
	        	$whereAdd.= "DATEDIFF(CURRENT_DATE, `birthday`)/365 >= ".round($arr[0])." AND DATEDIFF(CURRENT_DATE, `birthday`)/365 <= ".round($arr[1]);
	        	if($where!=""){$where.=" AND ";}
		        $where.=$whereAdd;
        	}
        	
        	//weight
        	$whereAdd = "";
        	if($get->data["weight"]){
	        	$arr = explode(",", $get->data["weight"]);
	        	$variables['weightMin'] = round($arr[0]);
	        	$variables['weightMax'] = round($arr[1]);
	        	$whereAdd.= "`weight` >= ".round($arr[0])." AND `weight` <= ".round($arr[1]);
	        	if($where!=""){$where.=" AND ";}
		        $where.=$whereAdd;
        	}
        	
        	//weight
        	$whereAdd = "";
        	if($get->data["height"]){
	        	$arr = explode(",", $get->data["height"]);
	        	$variables['heightMin'] = round($arr[0]);
	        	$variables['heightMax'] = round($arr[1]);
	        	$whereAdd.= "`height` >= ".round($arr[0])." AND `height` <= ".round($arr[1]);
	        	if($where!=""){$where.=" AND ";}
		        $where.=$whereAdd;
        	}
        	
        	//waist
        	$whereAdd = "";
        	if($get->data["waist"]){
	        	$arr = explode(",", $get->data["waist"]);
	        	$variables['waistMin'] = round($arr[0]);
	        	$variables['waistMax'] = round($arr[1]);
	        	$whereAdd.= "(`profileType`!=0 OR (`profileType`=2 AND `waist` >= ".round($arr[0])." AND `waist` <= ".round($arr[1])."))";
	        	if($where!=""){$where.=" AND ";}
		        $where.=$whereAdd;
        	}
        	
        	//inseam
        	$whereAdd = "";
        	if($get->data["inseam"]){
	        	$arr = explode(",", $get->data["inseam"]);
	        	$variables['inseamMin'] = round($arr[0]);
	        	$variables['inseamMax'] = round($arr[1]);
	        	$whereAdd.= "(`profileType`!=0 OR (`profileType`=2 AND `inseam` >= ".round($arr[0])." AND `inseam` <= ".round($arr[1])."))";
	        	if($where!=""){$where.=" AND ";}
		        $where.=$whereAdd;
        	}
        	
        	//shoeSize
        	$whereAdd = "";
        	if($get->data["shoeSize"]){
	        	$arr = explode(",", $get->data["shoeSize"]);
	        	$variables['shoeSizeMin'] = round($arr[0]);
	        	$variables['shoeSizeMax'] = round($arr[1]);
	        	$whereAdd.= "`shoeSize` >= ".((round($arr[0])-1)*2)." AND `shoeSize` <= ".((round($arr[1])-1)*2);
	        	if($where!=""){$where.=" AND ";}
		        $where.=$whereAdd;
        	}
        	
        	//pregnant
        	$whereAdd = "";
        	if($get->data["pregnant"]){
	        	$whereAdd.= "`dueDate` >= CURRENT_DATE";
	        	if($where!=""){$where.=" AND ";}
		        $where.=$whereAdd;
        	}
        	
        	//status
        	$whereAdd = "";
        	if($get->data["status"]){
	        	$whereAdd.= "`status` = 0";
	        	if($where!=""){$where.=" AND ";}
		        $where.=$whereAdd;
        	}
        	
        	
        	//shirtSize
        	$whereAdd = "";
	        foreach($fields["shirtSize"] as $key1 => $value1){
	        	foreach($value1 as $key => $value){
		        	if($get->data["shirtSize".$key1."-".$key] == "on"){
		        		if($whereAdd != ""){$whereAdd.= " OR ";}
			        	$whereAdd.="(`profileType`='".$key1."' AND `shirtSize`='".$key."')";
			        }
		        }
	        }
	        if($whereAdd!=""){
	        	if($where!=""){$where.=" AND ";}
		        $where.="(".$whereAdd.")";
	        }
            //var_dump($where);
		    if($get->sort=="alph"){
	        	setcookie("sort", "alph");
	        	$profiles = $profiles->table->findAll("lastName, firstName", $where);
	        	$sort = "alph";
	        }elseif($get->sort=="date"){
	        	setcookie("sort", "date");
	        	$profiles = $profiles->table->findAll(false, $where);
	        	$sort = "date";
	        }else{
		    	if($_COOKIE["sort"]=="alph"){
			    	$profiles = $profiles->table->findAll("lastName, firstName", $where);
					$sort = "alph";
		    	}else{
			    	$profiles = $profiles->table->findAll(false, $where);
					$sort = "date";
		    	}
	        }
	        
	        $variables['noResults'] = 1;
	        foreach ($profiles as $profile){
		        $prof = new Profile;
		        $prof->load($profile->id);
		        $profile->age = $prof->age();
		        $profile->printheight=$prof->printProfile("height");
		        if($profile->id){
			        $variables['noResults'] = 0;
		        }
	        }
			$variables['profiles'] = $profiles;
        }
        
		$sql = "SELECT `basedin` FROM `profiles` WHERE `basedin`!='' GROUP BY `basedin`";
        $stmt = $db->query($sql);
        while ($row = $stmt->fetchOBject()) {
        	$basedInArr[]=$row->basedin;
        }

    	$variables['userName'] = $user->table->name;
        $variables['fields'] = $fields;
        $variables['keywords'] = implode(', ', $keywords);
        $variables['profileGroups'] = $profileGroups;
        $variables['castingLists'] = $castingLists;
        $variables['allget'] = $get->data;
        $variables['profileType'] = $profileType;
        $variables['sort'] = $sort;
        $variables['count'] = count($profiles);
        $variables['basedIn'] = $basedInArr;
        $variables['js'][]='/template/app-assets/js/scripts/extensions/noui-slider.js';
        Tpl::get('search.tpl', $variables);
    }
    
    
    
    public function QuickSearchAction() {
    	$userid = User::getId();
       	$user = new User;
        $user->load($userid);
        $query = new Request($_GET);
        $query = $query->query;
        
        if ($query == '') {
            echo false;
        } else {
        
            $profiles = new Profile;
            $where = "`firstName` LIKE '".$query."%' 
                    OR `lastName` LIKE '".$query."%' 
                    OR CONCAT(`firstName`, ' ', `lastName`) LIKE '".$query."%'
                    OR CONCAT(SUBSTR(`firstName`,1,1), SUBSTR(`lastName`,1,1), LPAD(  `id` , 5, 0 )) LIKE '".$query."%' ";

            $profiles = $profiles->table->findAll(false, $where);

            foreach ($profiles as $profile){
                $prof = new Profile;
                $prof->load($profile->id);
                $profile->age = $prof->age();
            }

            echo json_encode($profiles);
        }
    }
    
    public function UploadAction() {
		$ds = DIRECTORY_SEPARATOR;
		$get = new Request($_GET);
		if($get->type==1){
	    	$imagesFolder = $_SERVER['DOCUMENT_ROOT'].'/images/profiles/';
	    	$thumbsFolder = $_SERVER['DOCUMENT_ROOT'].'/images/profiles/thumbnails/';
	 
			if (!empty($_FILES)) {
				$profileFolder = $imagesFolder.urlencode($get->id).$ds;
				mkdir($profileFolder, 0777);
				$targetFile = $profileFolder.basename(uniqid().".jpg");
			    $tempFile = $_FILES['file']['tmp_name'];              
                
                $profileThumbFolder = $thumbsFolder.urlencode($get->id).$ds;
                mkdir($profileThumbFolder, 0777);
                $thumbTargetFile = $profileThumbFolder.basename(uniqid().".jpg");
                
                $size = getimagesize($tempFile);
                $width = 550;
                $src = imagecreatefromstring(file_get_contents($tempFile));
                $dst = imagecreatetruecolor($width,$width);
                $success = imagecopyresampled($dst,$src,0,0,0,0,$width,$width,$size[0],$size[0]);
                imagedestroy($src);
                if ($success) imagejpeg($dst,$thumbTargetFile);
                imagedestroy($dst);
                
                move_uploaded_file($tempFile, $targetFile);
                if ($success) {
                    $profile = new Profile;
                    $profile->load($get->id);
                    $arr["photo"] = $targetFile;
                    $arr["thumbnail"] = $thumbTargetFile;
                    $profile->update($arr);
                }
			}
		}elseif($get->type==2){
	    	$imagesFolder = $_SERVER['DOCUMENT_ROOT'].'/images/profiles/';
	 
			if (!empty($_FILES)) {
				$profileFolder = $imagesFolder.urlencode($get->id).$ds;
				mkdir($profileFolder, 0777);
				$targetFile = $profileFolder.basename(uniqid().".jpg");
			    $tempFile = $_FILES['file']['tmp_name'];  
			    move_uploaded_file($tempFile, $targetFile);
			    $profile = new Profile;
			    $profile->load($get->id);
			    $arr["photo2"] = $targetFile;
			    $profile->update($arr);
			}
		}else{
			$imagesFolder = $_SERVER['DOCUMENT_ROOT'].'/images/photosets/';
	 
			if (!empty($_FILES)) {
				$profileFolder = $imagesFolder.urlencode($get->id).$ds;
				mkdir($profileFolder, 0777);
				$targetFile = $profileFolder.basename(uniqid().".jpg");
			    $tempFile = $_FILES['file']['tmp_name'];  
			    move_uploaded_file($tempFile, $targetFile);
			}

		}
    }
    
    public function DeletePhotoAction() {
    	$get = new Request($_GET);
		unlink($get->name);
    }
    
    public function DeleteAction() {        
    	$get = new Request($_GET);
        $removed = explode(',', $get->id);
        
        $profile = new Profile;
        foreach ($removed as $id) {
            $profile->table->delete($id);
        
            $image_paths = array(
                '/images/profiles/', '/images/photosets/', '/images/profiles/thumbnails/'
            );
            foreach ($image_paths as $image_path) {
                $path = $_SERVER['DOCUMENT_ROOT'].$image_path.$id.'/*';
                $files = glob($path);
                foreach($files as $file){ 
                  if(is_file($file))
                    unlink($file);
                }
                rmdir($_SERVER['DOCUMENT_ROOT'].$image_path.$id);
            }
        }
        
		System::redirect('/dashboard');
    }
    
    
    public function MobileFormAction() {
    	global $fields;
       	
       	$get = new Request($_GET);
       	$post = new Request($_POST);
        $profile = new Profile;
        
        $castUrl = $_SERVER['REQUEST_URI'];
    	$castUrl = explode("?", $castUrl);
    	$castUrl = $castUrl[0];
    	$castUrl = str_replace("/c/", "", $castUrl);    	
    	$castingList = new CastingList;
    	$castingList = $castingList->table->findByValue("url", $castUrl);
        
        $id = $castingList[0]->id;
        $hiddenFields = new CastingList;
        $hiddenFields = $hiddenFields->getHiddenFields($id);


        $selectedFields = new CastingList;
		$selectedFields = $selectedFields->getSelectedFields($id);
		

		$customUrlFields = new CastingList;
		$customUrlFields = $customUrlFields->table->findById($id);
		$customUrlFields = json_decode($customUrlFields->customUrlFields, true);
    	
        if ($post->action == 'save') {
        	try {                
				$uploaddir = $_SERVER['DOCUMENT_ROOT'].'/images/profiles/';
				$uploadfile = $uploaddir . basename(uniqid().".jpg");
				
            	$data = $post->data;
            	if (move_uploaded_file($_FILES['data']['tmp_name']['photo'], $uploadfile)) {
					$data["photo"] = $uploadfile;
				}
				
				$uploadfile = $uploaddir . basename(uniqid().".jpg");
				
				if (move_uploaded_file($_FILES['data']['tmp_name']['photo2'], $uploadfile)) {
					$data["photo2"] = $uploadfile;
				}
				
        		if(isset($data["height1"])){
            		$data["height"] = $data["height1"].",".$data["height2"];
            		unset($data["height1"]);
            		unset($data["height2"]);
        		}
        		
        		if($data["gender"]=="on"){$data["gender"]="1";}
        		if($data["gender"]==""){$data["gender"]="0";}
        		
        		$hairColor="";
        		if($data["hairColor8"]=="on"){$hairColor.="8,";}
        		if($data["hairColor1"]=="on"){$hairColor.="1,";}
        		if($data["hairColor2"]=="on"){$hairColor.="2,";}
        		if($data["hairColor3"]=="on"){$hairColor.="3,";}
        		if($data["hairColor4"]=="on"){$hairColor.="4,";}
        		if($data["hairColor5"]=="on"){$hairColor.="5,";}
        		if($data["hairColor6"]=="on"){$hairColor.="6,";}
        		if($data["hairColor7"]=="on"){$hairColor.="7,";}
        		unset($data["hairColor8"]);
        		unset($data["hairColor1"]);
        		unset($data["hairColor2"]);
        		unset($data["hairColor3"]);
        		unset($data["hairColor4"]);
        		unset($data["hairColor5"]);
        		unset($data["hairColor6"]);
        		unset($data["hairColor7"]);
        		$data["hairColor"]=$hairColor;
        		
        		$babyAbilities="";
        		if($data["babyAbilities4"]=="on"){$babyAbilities.="4,";}
        		if($data["babyAbilities1"]=="on"){$babyAbilities.="1,";}
        		if($data["babyAbilities2"]=="on"){$babyAbilities.="2,";}
        		if($data["babyAbilities3"]=="on"){$babyAbilities.="3,";}
        		unset($data["babyAbilities4"]);
        		unset($data["babyAbilities1"]);
        		unset($data["babyAbilities2"]);
        		unset($data["babyAbilities3"]);
        		$data["babyAbilities"]=$babyAbilities;
        		
        		$ethnicity="";
        		if($data["ethnicity8"]=="on"){$ethnicity.="8,";}
        		if($data["ethnicity1"]=="on"){$ethnicity.="1,";}
        		if($data["ethnicity2"]=="on"){$ethnicity.="2,";}
        		if($data["ethnicity3"]=="on"){$ethnicity.="3,";}
        		if($data["ethnicity4"]=="on"){$ethnicity.="4,";}
        		if($data["ethnicity5"]=="on"){$ethnicity.="5,";}
        		if($data["ethnicity6"]=="on"){$ethnicity.="6,";}
        		if($data["ethnicity7"]=="on"){$ethnicity.="7,";}
        		unset($data["ethnicity8"]);
				unset($data["ethnicity1"]);
				unset($data["ethnicity2"]);
				unset($data["ethnicity3"]);
				unset($data["ethnicity4"]);
				unset($data["ethnicity5"]);
				unset($data["ethnicity6"]);
				unset($data["ethnicity7"]);
				$data["ethnicity"]=$ethnicity;
        		
        		$profile = new Profile;
        		
        		
        		$data["firstName"]=mb_convert_case($data["firstName"], MB_CASE_TITLE, "UTF-8");
        		$data["lastName"]=mb_convert_case($data["lastName"], MB_CASE_TITLE, "UTF-8");
        		
                //var_dump($profile>table);
        		echo "<pre>";
        		print_r($data);
        		$profile->addProfile($data);
        		echo "!!!".$profile->table->id;
        		echo "</pre>";
        		
        		$profileGroup = new CastingList;
				$profileGroup->load($castingList[0]->id);
				$pID[] = $profile->table->id;
				$profiles = array_unique(array_merge($pID, $profileGroup->getProfiles($castingList[0]->id)));
		        foreach($profiles as $key => $val){
			        $jsonFinal[]=$val;
		        }
		        $addPG["profiles"] = json_encode($jsonFinal);
		        
		        $profileGroup->update($addPG);
        
        		System::redirect('/done?casting='.$castUrl.'&id='.$profile->table->id);
                
            } catch (Exception $e) {
                
            }

        }else{
	        if(isset($get->profileType)){
	        	
				$db = DB::getInstance();
				$sql = "SELECT `basedin` FROM `profiles` WHERE `basedin`!='' GROUP BY `basedin`";
		        $stmt = $db->query($sql);
		        while ($row = $stmt->fetchOBject()) {
		        	$basedInArr[]=$row->basedin;
		        }


				$js[] = '/template/js/modelform.js';
		        $profileType = $get->profileType;
		        Tpl::get('mobileForm.tpl', array(
	            	'userName' => $user->table->name,
					'profileType' => $profileType,
					'js' => $js,
					'fields' => $fields,
					'castingList' => $castingList[0],
                    'hiddenFields' => $hiddenFields,
                    'basedIn' => $basedInArr,
					'selectedFields' => $selectedFields,
					'customUrlFields' => $customUrlFields,
				));
	        }else{
		        Tpl::get('mobileFormStart.tpl', array(
	            	'userName' => $user->table->name,
					'fields' => $fields,
					'castingList' => $castingList[0],
                    'hiddenFields' => $hiddenFields,
					'selectedFields' => $selectedFields,
					'customUrlFields' => $customUrlFields,
				));
	        }
        }
    }
    
    public function FamilyAction() {
    	global $fields;
    	
    	$userid = User::getId();
    	$user = new User;
        $user->load($userid);
        $get = new Request($_GET);
        $profiles = explode(",", $get->profiles);
        
        $profile = new Profile;
        $profile->load($profiles[0]);
        $updateArr["familyId"]=$profile->table->familyID;
        
        foreach($profiles as $pID){
	        $profile->load($pID);
	        $profile->update($updateArr);
        }
       	
       	echo "Ok";
       	exit;
    }
    
    public function DoneAction() {
    	global $fields;
        $get = new Request($_GET);
        $profile = new Profile;
        
        if(isset($get->id)){
	        $profile->load($get->id);
        }
        
        Tpl::get('done.tpl', array(
            'js' => $js,
            'userName' => $user->table->name,
            'profile' => $profile->table,
            'url' => $get->casting,
         ));

    }
    
    public function ExportAction() {
    	global $fields;
        $get = new Request($_GET);
        $profile = new Profile;
        
        if(isset($get->id)){
	        $profile->load($get->id);
        }
        
        if($profile->table->photo2!=""){
        	$photos[]=$profile->table->photo2;
        }
        $photos = array_merge ($photos, glob($_SERVER['DOCUMENT_ROOT'].'/images/photosets/'.urlencode($get->id).'/*.{jpg,png,gif}', GLOB_BRACE));

        
        $html = Tpl::fetch('pdf.tpl', array(
			'profile' => $profile->table,
			'fields' => $fields,
			'age' => $profile->age(),
            'photos' => $photos,
            'heightPrint' => $profile->printProfile("height")
        ));
        
        echo $html;
        //$html2pdf->writeHTML($html);



    }

}
