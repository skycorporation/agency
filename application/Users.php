<?php

class UsersModel extends Admin {

    public function __construct() {
        parent::__construct();
        
    }

    public function ListAction() {
        $post = new Request($_POST);
        if (isset($post->action)) {
            if ($post->action == 'create' && isset($post->data, $post->name)) {
                $ret = array();
                $ret['status'] = 'error';
                try {
                    $userRow = new stdClass();
                    foreach ($post->data As $k => $v) {
                        $userRow->$k = $v;
                    }
                    $user = new User;
                    $company = new Company($post->data['companyID']);
                    $config = Registry::get('config');



                    $company->table->findById($userRow->companyID);
                    if ($company->table->current_users == 0) {
                        $userRow->is_admin = 1;
                    }
                    if ($company->table->max_users > 0 && $company->table->current_users >= $company->table->max_users) {
                        throw new Exception("Невозможно добавить нового пользователя.");
                    }
                    $userRow->pass = md5($config->salt . $post->data['pass']);
                    $userRow->regDateTime = date("Y-m-d H:i:s");
                    $userRow->creationDateTime = date("Y-m-d H:i:s");
                    $userRow->active = 1;
                    if (!$user->addUser($userRow)) {
                        throw new Exception("Ошибка при добавлении пользователя");
                    }
                    $user->setNames($post->name);
                    $ret['status'] = 'ok';
                } catch (Exception $e) {
                    $ret['error'] = $e->getMessage();
                }
                echo json_encode($ret);
                exit;
            }
            if ($post->action == 'block' && isset($post->users) && is_array($post->users) && count($post->users) > 0) {
                $sql = "update users set is_blocked=1 where id in (" . implode(',', $post->users) . ") ";
                $this->db->query($sql);
                exit;
            }
            if ($post->action == 'unblock' && isset($post->users) && is_array($post->users) && count($post->users) > 0) {
                $sql = "update users set is_blocked=0 where id in (" . implode(',', $post->users) . ") ";
                $this->db->query($sql);
                exit;
            }
            if ($post->action == 'delete' && isset($post->users) && is_array($post->users) && count($post->users) > 0) {
                foreach ($post->users as $id) {
                    $user = new User;
                    try {
                        $user->load($id);
                        $user->delete();
                    } catch (Exception $e) {
                        
                    }
                }

          
                exit;
            }
            if ($post->action == 'getUsers' && isset($post->cid)) {
                $users = array();
                if ($post->cid > 0) {
                    // selected
                    $company = new Company($post->cid);
                    $users = $company->getUsers();
                } else {
                    $users = (new User)->findAll();
                }
                Tpl::get('users/block.tpl', array(
                    'users' => $users,
                    'tmpuser' => new User,
                ));
                exit;
            }
        }
        $js[] = 'users/list.js';
        Tpl::get('users/list.tpl', array(
            'js' => $js,
        ));
    }

}
