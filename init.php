<?php
include("fields.php");
//date_default_timezone_set('Europe/Kiev');
define('PATH_APPLICATION', dirname(__FILE__) . '/');

error_reporting(E_ALL);
ini_set('display_errors', 0);


spl_autoload_register(function($name) {
    // try to find stand-alone files

    $search_path = array(
        PATH_APPLICATION . "application/core/",
        PATH_APPLICATION . "application/class/",
        PATH_APPLICATION . "application/inc/",
    );

    foreach ($search_path as $path) {
        if (file_exists($path . $name . ".php")) {

            include($path . $name . ".php");
            return;
        }
    }

    $str = str_replace("_", "/", $name) . ".php";
    $f = false;

    foreach ($search_path as $path) {
        if (file_exists($path . $str)) {
            $f = true;
            include($path . $str);
        }
    }
});
define('SMARTY_DIR', PATH_APPLICATION . 'application/inc/smarty/');
include_once PATH_APPLICATION . 'application/inc/smarty/Smarty.class.php';
//include_once PATH_APPLICATION . 'application/inc/vendor/autoload.php';
$config = parse_ini_file('config.ini');
$configRegisty = new Request( $config );

Registry::set('config', $configRegisty);
Registry::set('cache', Cache_Factory::factory(array('engine' => 'file')));

