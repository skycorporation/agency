<?php /* Smarty version Smarty-3.1.19, created on 2018-10-11 18:34:29
         compiled from "/home/aibdh80ehx97/public_html/sncasting.com/template/modals/newGroup.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5880452135bbf97b54bcb42-36727406%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'abbffd9e0a819f5e2cd9b45c07f5dc751c28acbc' => 
    array (
      0 => '/home/aibdh80ehx97/public_html/sncasting.com/template/modals/newGroup.tpl',
      1 => 1537190464,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5880452135bbf97b54bcb42-36727406',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'profileGroups' => 0,
    't' => 0,
    'castingLists' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bbf97b54cd738_83068916',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bbf97b54cd738_83068916')) {function content_5bbf97b54cd738_83068916($_smarty_tpl) {?><div class="modal fade text-xs-left pr-0" id="createGroup" tabindex="-1" role="dialog" aria-labelledby="newGroupModalTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-group white">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
        <h4 class="modal-title" id="newGroupModalTitle">Add selected profile(s) to group or casting</h4>
      </div>
      <div class="modal-body">

		  <ul class="nav groupOrCastPills nav-pills nav-pill-toolbar nav-justified">
							<li class="nav-item">
								<a class="nav-link active" id="active2-pill1" data-toggle="pill" href="#addToGroupTab" aria-expanded="true">Add to group</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="link2-pill1" data-toggle="pill" href="#addToCastingTab" aria-expanded="false">Add to casting</a>
							</li>
						</ul>
		  <div class="tab-content px-1 pt-1">
							<div role="tabpanel" class="tab-pane active" id="addToGroupTab" aria-labelledby="active2-pill1" aria-expanded="true">
								<h5 class="text-center mt-1 mb-1">Create a new group:</h5>
			  <div class="form">
			  <div class="form-group row">
        		 <div class="col-xs-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2"> 
			 
            <input type="text" placeholder="New group name" id="newProfileGroupName" class="form-control newGroupInput capThis checkThis" onkeyup="clearExistinGroup()">
            <h5 class="text-center mt-1 mb-1">or use existing group:</h5>
            <select id="existingGroup" onchange="clearNewGroup()" class="form-control checkThis mb-1">
              <option value="" disabled selected>Select existing group:</option>
              
					<?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['profileGroups']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?>
					
              <option value="<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['t']->value->name;?>
</option>
              
					<?php } ?>
				
            </select>
				  </div></div></div>
            
            <div class="modal-footer pr-0 pt-2">
              <button type="button" class="mybtn2 mybtn-grey" data-dismiss="modal">Cancel</button>
              &nbsp; <a onclick="createGroup()" class="mybtn2 mybtn-group">Add to group</a> </div>
							</div>
							<div class="tab-pane" id="addToCastingTab" role="tabpanel" aria-labelledby="link2-pill1" aria-expanded="false">
								<h5 class="text-center mt-1 mb-1">Add to casting:</h5>
            <div class="form">
			  <div class="form-group row">
        		 <div class="col-xs-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2">
            <select id="existingCasting" class="form-control checkThis">
              <option disabled selected>Select casting:</option>
              
					<?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['castingLists']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?>
					
              <option value="<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['t']->value->name;?>
</option>
              
					<?php } ?>
				
            </select>
				  </div></div></div>
            <br>
            <div class="modal-footer pr-0 pt-2">
              <button type="button" class="mybtn2 mybtn-grey" data-dismiss="modal">Cancel</button>
              &nbsp; <a onclick="addToCasting($('#existingCasting').val())" class="mybtn2 mybtn-group">Add to casting</a> </div>
							</div>
							
						</div>
		  
		  
		  
      </div>
    </div>
  </div>
</div><?php }} ?>
