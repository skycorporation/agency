<?php /* Smarty version Smarty-3.1.19, created on 2018-10-11 18:34:29
         compiled from "/home/aibdh80ehx97/public_html/sncasting.com/template/modals/deleteMultipleProfiles.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6533031265bbf97b54d4a66-72337096%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c438cc3f56937df9cc31aa3f6de09cbaaf762052' => 
    array (
      0 => '/home/aibdh80ehx97/public_html/sncasting.com/template/modals/deleteMultipleProfiles.tpl',
      1 => 1537190464,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6533031265bbf97b54d4a66-72337096',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bbf97b54d5633_31425574',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bbf97b54d5633_31425574')) {function content_5bbf97b54d5633_31425574($_smarty_tpl) {?><div class="modal fade text-xs-left pr-0" id="removeMultiple" tabindex="-1" role="dialog" aria-labelledby="myModalLabel10" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-danger white">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel10">Delete profile</h4>
      </div>
      <div class="modal-body">
        <h5>Are you sure you want to delete selected profile(s)?<br><br>This action cannot be undone.</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="mybtn2 mybtn-grey mr-1" data-dismiss="modal">Cancel</button>
        <a onclick="removeMultiple('profiles')" class="mybtn2 mybtn-red">Delete</a></div>
    </div>
  </div>
</div>
<?php }} ?>
