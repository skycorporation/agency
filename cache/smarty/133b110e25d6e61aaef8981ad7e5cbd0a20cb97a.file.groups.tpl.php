<?php /* Smarty version Smarty-3.1.19, created on 2019-02-28 22:29:57
         compiled from "/home/aibdh80ehx97/public_html/sncasting.com/template/groups.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5435643275bc0b3aac6e928-55094875%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '133b110e25d6e61aaef8981ad7e5cbd0a20cb97a' => 
    array (
      0 => '/home/aibdh80ehx97/public_html/sncasting.com/template/groups.tpl',
      1 => 1551392983,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5435643275bc0b3aac6e928-55094875',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bc0b3aacf5a75_30189411',
  'variables' => 
  array (
    'profileGroups' => 0,
    't' => 0,
    'photo' => 0,
    'c' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bc0b3aacf5a75_30189411')) {function content_5bc0b3aacf5a75_30189411($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/home/aibdh80ehx97/public_html/application/inc/smarty/plugins/modifier.replace.php';
?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
 
<script>
    var profileToDelete;
</script> 
<?php echo $_smarty_tpl->getSubTemplate ("modals/deleteGroup.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("modals/editGroup.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<h1 class="text-center mt-3 mb-2">Profile groups</h1>
<section id="hover-effects">
  <div class="row">
    <div class="grid-hover"> <?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['profileGroups']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
?>
      <div class="col-md-4 col-xs-12 mb-2">
        <div class="thumbnail-container group">
          <div class="thumb-photo-place">
            <div class="imagesGroup imagesGroup<?php if ($_smarty_tpl->tpl_vars['t']->value["count"]>4) {?>4<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['t']->value["count"];?>
<?php }?>">
              <?php $_smarty_tpl->tpl_vars["c"] = new Smarty_variable(0, null, 0);?>
              <?php  $_smarty_tpl->tpl_vars['photo'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['photo']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['t']->value["photos"]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['photo']->key => $_smarty_tpl->tpl_vars['photo']->value) {
$_smarty_tpl->tpl_vars['photo']->_loop = true;
?>
              <div class="imagesGroupImg" style="background: url('<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['photo']->value,'/home/aibdh80ehx97/public_html/sncasting.com','');?>
');"/>
            </div>
            <?php $_smarty_tpl->tpl_vars['c'] = new Smarty_variable($_smarty_tpl->tpl_vars['c']->value+1, null, 0);?>
            <?php if ($_smarty_tpl->tpl_vars['c']->value>=4) {?> <?php break 1?> <?php }?>
            
            <?php } ?>
            <div style="clear: both;"></div>
            <div class="thumb-overlay"></div>
          </div>
          <div class="thumb-content">
            <div class="editPanel desktop-only">
              <button class="editButton" data-toggle="modal" data-target="#editGroup" onclick="profileToEdit=<?php echo $_smarty_tpl->tpl_vars['t']->value['id'];?>
; $('#editProfileGroupName').val('<?php echo $_smarty_tpl->tpl_vars['t']->value['name'];?>
');"><i class="ft-edit-2"></i></button>
              <button class="deleteProfileButton" data-toggle="modal" data-target="#deleteGroup" onclick="profileToDelete=<?php echo $_smarty_tpl->tpl_vars['t']->value['id'];?>
"><i class="icon-trash4"></i></button>
            </div>
            <a class="thumb-info" href="/groups/view?id=<?php echo $_smarty_tpl->tpl_vars['t']->value['id'];?>
">
            <p class="text-center"><?php if ($_smarty_tpl->tpl_vars['t']->value["count"]==0) {?>No<?php }?><?php echo $_smarty_tpl->tpl_vars['t']->value["count"];?>
 profiles</p>
            </a> </div>
        </div>
        <a class="name-place" href="/groups/view?id=<?php echo $_smarty_tpl->tpl_vars['t']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['t']->value["name"];?>
</a> </div>
    </div>
    <?php } ?> </div>
  </div>
</section>
<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
