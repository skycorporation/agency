<?php /* Smarty version Smarty-3.1.19, created on 2018-10-12 14:46:02
         compiled from "/home/aibdh80ehx97/public_html/sncasting.com/template/modals/deleteGroup.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6074211515bc0b3aad02017-13823921%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4e3071bba1f7c4b7fe147285a8b4fc243ee6d89c' => 
    array (
      0 => '/home/aibdh80ehx97/public_html/sncasting.com/template/modals/deleteGroup.tpl',
      1 => 1537190464,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6074211515bc0b3aad02017-13823921',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bc0b3aad1ca31_62329463',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bc0b3aad1ca31_62329463')) {function content_5bc0b3aad1ca31_62329463($_smarty_tpl) {?><div class="modal fade text-xs-left pr-0" id="deleteGroup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel10" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-danger white">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel10">Delete profile group</h4>
      </div>
      <div class="modal-body">
        <h5>Are you sure you want to delete this group?<br>
          <br>
          All model profiles from this group will remain in the database.</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="mybtn2 mybtn-grey mr-1" data-dismiss="modal">Cancel</button>
        <a onclick="window.location='/groups/delete?id=' + profileToDelete;" class="mybtn2 mybtn-red">Delete group</a></div>
    </div>
  </div>
</div>
<?php }} ?>
