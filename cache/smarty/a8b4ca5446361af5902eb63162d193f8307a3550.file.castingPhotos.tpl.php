<?php /* Smarty version Smarty-3.1.19, created on 2018-10-15 15:44:05
         compiled from "/home/aibdh80ehx97/public_html/sncasting.com/template/castingPhotos.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1294671175bbfdb6cf03e03-65297057%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a8b4ca5446361af5902eb63162d193f8307a3550' => 
    array (
      0 => '/home/aibdh80ehx97/public_html/sncasting.com/template/castingPhotos.tpl',
      1 => 1539618242,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1294671175bbfdb6cf03e03-65297057',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bbfdb6d051625_01917787',
  'variables' => 
  array (
    'group' => 0,
    'profiles' => 0,
    't' => 0,
    'fields' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bbfdb6d051625_01917787')) {function content_5bbfdb6d051625_01917787($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/home/aibdh80ehx97/public_html/application/inc/smarty/plugins/modifier.replace.php';
?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<style>
	.dropzone {
    min-height: 200px;
	}
.dropzone .dz-message {
	left: 200px;
	display: none;
}
</style>

<!-- Hover Effects --> 
<br>
<h2 style="text-align: center">Upload photos to <?php echo $_smarty_tpl->tpl_vars['group']->value->table->name;?>
 casting</h2>
<div class="infoNote"><i class="icon-info2"></i><strong>Drag-and-drop photos into relevant fields for each profile.<br>
  All changes are saved automatically.</strong><br>
  <strong style="color: #000 !important;">Please make sure to use JPGs with sRGB color space.</strong></div>
<?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['profiles']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?>
						<?php $_smarty_tpl->tpl_vars["size"] = new Smarty_variable(getimagesize($_smarty_tpl->tpl_vars['t']->value->photo), null, 0);?>
						<div class="card">
  <div class="card-header text-center"> <a class="card-title h4" href="/profile/view?id=<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
" style="cursor: pointer" target="_blank"><?php echo $_smarty_tpl->tpl_vars['t']->value->firstName;?>
 <?php echo $_smarty_tpl->tpl_vars['t']->value->lastName;?>
</a><br>
      <span class="font-weight-bold">&#35;<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
</span><br>
      <?php echo $_smarty_tpl->tpl_vars['fields']->value['gender'][$_smarty_tpl->tpl_vars['t']->value->gender];?>
 / <?php echo $_smarty_tpl->tpl_vars['t']->value->age;?>

                           
                          </div>
  <div class="card-block">
                            <div class="container-fluid">
      <div class="row flexbox"> <?php if ($_smarty_tpl->tpl_vars['t']->value->photo) {?>
                                <div class="col flexitem text-center mr-1" style="flex-grow: 0;"> <b>Current Profile Photo:</b><br>
                                  <img src="<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['t']->value->photo,'/home/aibdh80ehx97/public_html/sncasting.com','');?>
" alt="Current Profile Photo" title="Current profile photo" style="height: 200px; width: auto;" />
        </div>
                                <?php }?>
                                <div class="col flexitem text-center mr-1"> <b><?php if ($_smarty_tpl->tpl_vars['t']->value->photo) {?>New Profile Photo:<?php } else { ?>Profile Photo:<?php }?></b><br>
                                  <form action="/profile/upload/?id=<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
&type=1" class="dropzone dpz-single-file" id="dpz-single-file">
                                  </form>
        </div>
                                <div class="col flexitem text-center" style="flex-grow: 3;"> <b>Additional Photos:</b><br>
                                  <form action="/profile/upload/?id=<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
" class="dropzone dpz-multiple-files" id="dpz-multiple-files">
                                  </form>
        </div>
                              </div>
    </div>
                          </div>
</div>
<?php } ?>

<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
