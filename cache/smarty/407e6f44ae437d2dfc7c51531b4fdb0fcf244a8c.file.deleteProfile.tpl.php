<?php /* Smarty version Smarty-3.1.19, created on 2018-10-11 18:34:29
         compiled from "/home/aibdh80ehx97/public_html/sncasting.com/template/modals/deleteProfile.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7352875485bbf97b54d1431-08173926%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '407e6f44ae437d2dfc7c51531b4fdb0fcf244a8c' => 
    array (
      0 => '/home/aibdh80ehx97/public_html/sncasting.com/template/modals/deleteProfile.tpl',
      1 => 1537190464,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7352875485bbf97b54d1431-08173926',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bbf97b54d2096_80419214',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bbf97b54d2096_80419214')) {function content_5bbf97b54d2096_80419214($_smarty_tpl) {?><div class="modal fade text-xs-left pr-0" id="deleteProfile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel10" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-danger white">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel10">Delete profile</h4>
      </div>
      <div class="modal-body">
        <h5>Are you sure you want to delete this profile?<br><br>This action cannot be undone.</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="mybtn2 mybtn-grey mr-1" data-dismiss="modal">Cancel</button>
        <a onclick="window.location='/profile/delete?id=' + profileToDelete;" class="mybtn2 mybtn-red">Delete</a></div>
    </div>
  </div>
</div>
<?php }} ?>
