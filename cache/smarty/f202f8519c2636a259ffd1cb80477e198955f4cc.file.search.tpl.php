<?php /* Smarty version Smarty-3.1.19, created on 2018-11-01 19:38:25
         compiled from "/home/aibdh80ehx97/public_html/sncasting.com/template/search.tpl" */ ?>
<?php /*%%SmartyHeaderCode:10139109345bbf98a2aa4cf3-77383655%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f202f8519c2636a259ffd1cb80477e198955f4cc' => 
    array (
      0 => '/home/aibdh80ehx97/public_html/sncasting.com/template/search.tpl',
      1 => 1540949124,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10139109345bbf98a2aa4cf3-77383655',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bbf98a2beda14_72915332',
  'variables' => 
  array (
    'fields' => 0,
    'k' => 0,
    'allget' => 0,
    'action' => 0,
    'profileType' => 0,
    't' => 0,
    'ageMin' => 0,
    'ageMax' => 0,
    'keywords' => 0,
    'weightMin' => 0,
    'weightMax' => 0,
    'heightMin' => 0,
    'heightMax' => 0,
    'waistMin' => 0,
    'waistMax' => 0,
    'inseamMin' => 0,
    'inseamMax' => 0,
    'shoeSizeMin' => 0,
    'shoeSizeMax' => 0,
    'noResults' => 0,
    'profiles' => 0,
    'size' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bbf98a2beda14_72915332')) {function content_5bbf98a2beda14_72915332($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/home/aibdh80ehx97/public_html/application/inc/smarty/plugins/modifier.replace.php';
?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("modals/deleteProfile.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("modals/newGroup.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("modals/deleteMultipleProfiles.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<style>
    	.grid-hover{
	    	padding: 0;
    	}
    	.grid-hover .col-md-4{
	    	padding: 0;
    	}
    	.effect-roxy{
	    	margin: 0 !important;
    	}
    	figure.effect-roxy h2{
	    	padding: 22% 0 10px 0 !important;
    	}
		<?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value["profileType"]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?>
			<?php if ($_smarty_tpl->tpl_vars['allget']->value[("profileType").($_smarty_tpl->tpl_vars['k']->value)]) {?>
		.pt<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
{
				display: block !important;
		}
			<?php }?>
		<?php } ?>
    </style>
<form class="form form-horizontal" method="get" action="#" enctype="multipart/form-data">
  <input type="hidden" value="search" name="action">
  <section class="card">
    <div class="card-header">
      <h4 class="card-title">Search Parameters</h4>
      <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
      <div class="heading-elements">
        <ul class="list-inline mb-0">
          <?php if ($_smarty_tpl->tpl_vars['action']->value==0) {?>
          <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
        </ul>
      </div>
    </div>
    <div class="card-body collapse in" aria-expanded="true" style=""> <?php } else { ?>
      <li><a data-action="collapse"><i class="icon-plus4"></i></a></li>
      </ul>
    </div>
    </div>
    <div class="card-body collapse out" aria-expanded="true" style=""> <?php }?>
      <div class="card-block">
        <div class="form-body"> <BR>
          <div class="form-group row">
            <label class="col-md-3 label-control" for="projectinput1">Profile Type</label>
            <div class="col-md-9">
              <div class="btn-group" role="group" aria-label="Basic example"> <?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value["profileType"]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?> <a type="button" href="/search?profileType=<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
" class="mybtn2 mybtn-<?php if ($_smarty_tpl->tpl_vars['k']->value==$_smarty_tpl->tpl_vars['profileType']->value) {?>green<?php } else { ?>grey<?php }?>"><?php echo $_smarty_tpl->tpl_vars['t']->value;?>
</a> <?php } ?>
                <input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['profileType']->value;?>
" name="data[profileType]">
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 label-control" for="projectinput1">First name</label>
            <div class="col-md-6">
              <input autocomplete="dontsuggest" type="text" name="data[firstName]" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['allget']->value["firstName"];?>
">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 label-control" for="projectinput1">Last name</label>
            <div class="col-md-6">
              <input autocomplete="dontsuggest" type="text" name="data[secondName]" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['allget']->value["secondName"];?>
">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 label-control" for="projectinput1">Gender</label>
            <div class="col-md-6"> <?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value["gender"]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?>
              <fieldset>
                <label class="custom-control custom-checkbox">
                  <input type="checkbox" <?php if ($_smarty_tpl->tpl_vars['allget']->value[("gender").($_smarty_tpl->tpl_vars['k']->value)]) {?>checked<?php }?> class="custom-control-input" name="data[gender<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
]">
                  <span class="custom-control-indicator"></span> <span class="custom-control-description"><?php echo $_smarty_tpl->tpl_vars['t']->value;?>
</span> </label>
              </fieldset>
              <?php } ?> </div>
          </div>
          <br>
          <div class="form-group row">
            <label class="col-md-3 label-control label-push" for="projectinput1">Age</label>
            <div class="col-md-6">
              <div id="slider-age" class="ml-3 mr-2 slider-info" data-min="<?php echo $_smarty_tpl->tpl_vars['ageMin']->value;?>
" data-max="<?php echo $_smarty_tpl->tpl_vars['ageMax']->value;?>
"></div>
              <input type="hidden" id="slider-age-val" name="data[age]" value="<?php if ($_smarty_tpl->tpl_vars['ageMin']->value) {?><?php echo $_smarty_tpl->tpl_vars['ageMin']->value;?>
,<?php echo $_smarty_tpl->tpl_vars['ageMax']->value;?>
<?php }?>">
            </div>
          </div>
          <br>
          <div class="form-group row">
              <label class="col-md-3 label-control" for="projectinput7">Keywords</label>
              <div class="col-md-6">
                <fieldset>
                  <div class="form-group">
                    <div class="form-control" id="keywords" data-tags-input-name="edit-on-delete" style="min-height: 80px"><?php echo $_smarty_tpl->tpl_vars['keywords']->value;?>
</div>
                    <small class="text-muted">Press Enter, Comma or Spacebar to add a new tag, Backspace or Delete to remove.</small> </div>
                </fieldset>
                <input type="hidden" name="data[keywords]" value='<?php echo $_smarty_tpl->tpl_vars['allget']->value["keywords"];?>
'>
              </div>
          </div>
          <!--<div class="form-group row">
            <label class="col-md-3 label-control" for="projectinput1">Hair color</label>
            <div class="col-md-9"> <?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value["hairColor"]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?>
              <fieldset>
                <label class="custom-control custom-checkbox">
                  <input type="checkbox" <?php if ($_smarty_tpl->tpl_vars['allget']->value[("hairColor").($_smarty_tpl->tpl_vars['k']->value)]) {?>checked<?php }?> class="custom-control-input" name="data[hairColor<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
]">
                  <span class="custom-control-indicator"></span> <span class="custom-control-description"><?php echo $_smarty_tpl->tpl_vars['t']->value;?>
</span> </label>
              </fieldset>
              <?php } ?> </div>
          </div>-->
          <div class="form-group row">
            <label class="col-md-3 label-control" for="projectinput1">Ethnicity</label>
            <div class="col-md-6"> <?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value["ethnicity"]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?>
              <fieldset>
                <label class="custom-control custom-checkbox">
                  <input type="checkbox" <?php if ($_smarty_tpl->tpl_vars['allget']->value[("ethnicity").($_smarty_tpl->tpl_vars['k']->value)]) {?>checked<?php }?> class="custom-control-input" name="data[ethnicity<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
]">
                  <span class="custom-control-indicator"></span> <span class="custom-control-description"><?php echo $_smarty_tpl->tpl_vars['t']->value;?>
</span> </label>
              </fieldset>
              <?php } ?> </div>
          </div>
          <br>
          <div class="form-group row">
            <label class="col-md-3 label-control label-push" for="projectinput1">Weight</label>
            <div class="col-md-6">
              <div id="slider-weight" class="ml-3 mr-2 slider-info" data-min="<?php echo $_smarty_tpl->tpl_vars['weightMin']->value;?>
" data-max="<?php echo $_smarty_tpl->tpl_vars['weightMax']->value;?>
"></div>
              <input type="hidden" id="slider-weight-val" name="data[weight]" value="<?php if ($_smarty_tpl->tpl_vars['weightMin']->value) {?><?php echo $_smarty_tpl->tpl_vars['weightMin']->value;?>
,<?php echo $_smarty_tpl->tpl_vars['weightMax']->value;?>
<?php }?>">
            </div>
          </div>
          <br>
          <br>
          <div class="form-group row">
            <label class="col-md-3 label-control label-push" for="projectinput1">Height</label>
            <div class="col-md-6">
              <div id="slider-height" class="ml-3 mr-2 slider-info" data-min="<?php echo $_smarty_tpl->tpl_vars['heightMin']->value;?>
" data-max="<?php echo $_smarty_tpl->tpl_vars['heightMax']->value;?>
"></div>
              <input type="hidden" id="slider-height-val" name="data[height]" value="<?php if ($_smarty_tpl->tpl_vars['heightMin']->value) {?><?php echo $_smarty_tpl->tpl_vars['heightMin']->value;?>
,<?php echo $_smarty_tpl->tpl_vars['heightMax']->value;?>
<?php }?>">
            </div>
          </div>
          <br>
          <div class="form-group row ">
            <label class="col-md-3 label-control" for="projectinput1">Shirt size (adults)</label>
            <div class="col-md-6"> <?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value["shirtSize"][2]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?>
              <fieldset>
                <label class="custom-control custom-checkbox">
                  <input type="checkbox" <?php if ($_smarty_tpl->tpl_vars['allget']->value[("shirtSize2-").($_smarty_tpl->tpl_vars['k']->value)]) {?>checked<?php }?> class="custom-control-input" name="data[shirtSize2-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
]">
                  <span class="custom-control-indicator"></span> <span class="custom-control-description"><?php echo $_smarty_tpl->tpl_vars['t']->value;?>
</span> </label>
              </fieldset>
              <?php } ?> </div>
          </div>
          <?php if ($_smarty_tpl->tpl_vars['profileType']->value==1) {?>
          <div class="form-group row ">
            <label class="col-md-3 label-control" for="projectinput1">Shirt size (childs)</label>
            <div class="col-md-6"> <?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value["shirtSize"][1]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?>
              <fieldset>
                <label class="custom-control custom-checkbox">
                  <input type="checkbox" <?php if ($_smarty_tpl->tpl_vars['allget']->value[("shirtSize1-").($_smarty_tpl->tpl_vars['k']->value)]) {?>checked<?php }?> class="custom-control-input" name="data[shirtSize1-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
]">
                  <span class="custom-control-indicator"></span> <span class="custom-control-description"><?php echo $_smarty_tpl->tpl_vars['t']->value;?>
</span> </label>
              </fieldset>
              <?php } ?> </div>
          </div>
          <?php }?>
          
          <?php if ($_smarty_tpl->tpl_vars['profileType']->value==0) {?>
          <div class="form-group row ">
            <label class="col-md-3 label-control" for="projectinput1">Shirt size (babies)</label>
            <div class="col-md-6"> <?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value["shirtSize"][0]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?>
              <fieldset>
                <label class="custom-control custom-checkbox">
                  <input type="checkbox" <?php if ($_smarty_tpl->tpl_vars['allget']->value[("shirtSize0-").($_smarty_tpl->tpl_vars['k']->value)]) {?>checked<?php }?> class="custom-control-input" name="data[shirtSize0-<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
]">
                  <span class="custom-control-indicator"></span> <span class="custom-control-description"><?php echo $_smarty_tpl->tpl_vars['t']->value;?>
</span> </label>
              </fieldset>
              <?php } ?> </div>
          </div>
          <?php }?>
          
          
          <?php if ($_smarty_tpl->tpl_vars['profileType']->value==2) {?> <br>
          <div class="form-group row ">
            <label class="col-md-3 label-control label-push" for="projectinput1">Pant size - waist (adults)</label>
            <div class="col-md-6">
              <div id="slider-waist" class="ml-3 mr-2 slider-info" data-min="<?php echo $_smarty_tpl->tpl_vars['waistMin']->value;?>
" data-max="<?php echo $_smarty_tpl->tpl_vars['waistMax']->value;?>
"></div>
              <input type="hidden" id="slider-waist-val" name="data[waist]" value="<?php if ($_smarty_tpl->tpl_vars['waistMin']->value) {?><?php echo $_smarty_tpl->tpl_vars['waistMin']->value;?>
,<?php echo $_smarty_tpl->tpl_vars['waistMax']->value;?>
<?php }?>">
            </div>
          </div>
          <div class="form-group row "> <br>
            <br>
            <label class="col-md-3 label-control label-push" for="projectinput1">Pant size - inseam (adults)</label>
            <div class="col-md-6">
              <div id="slider-inseam" class="ml-3 mr-2 slider-info" data-min="<?php echo $_smarty_tpl->tpl_vars['inseamMin']->value;?>
" data-max="<?php echo $_smarty_tpl->tpl_vars['inseamMax']->value;?>
"></div>
              <input type="hidden" id="slider-inseam-val" name="data[inseam]" value="<?php if ($_smarty_tpl->tpl_vars['inseamMin']->value) {?><?php echo $_smarty_tpl->tpl_vars['inseamMin']->value;?>
,<?php echo $_smarty_tpl->tpl_vars['inseamMax']->value;?>
<?php }?>">
            </div>
          </div>
          <?php }?>
          <div class="form-group row"> <br>
            <br>
            <label class="col-md-3 label-control label-push" for="projectinput1">Shoe size</label>
            <div class="col-md-6">
              <div id="slider-shoes" class="ml-3 mr-2 slider-info" data-min="<?php echo $_smarty_tpl->tpl_vars['shoeSizeMin']->value;?>
" data-max="<?php echo $_smarty_tpl->tpl_vars['shoeSizeMax']->value;?>
"></div>
              <input type="hidden" id="slider-shoeSize-val" name="data[shoeSize]" value="<?php if ($_smarty_tpl->tpl_vars['shoeSizeMin']->value) {?><?php echo $_smarty_tpl->tpl_vars['shoeSizeMin']->value;?>
,<?php echo $_smarty_tpl->tpl_vars['shoeSizeMax']->value;?>
<?php }?>">
            </div>
          </div>
     
          <?php if ($_smarty_tpl->tpl_vars['profileType']->value==0) {?>
          <div class="form-group row">
            <label class="col-md-3 label-control" for="projectinput1">Baby abilities</label>
            <div class="col-md-6"> <?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value["babyAbilities"]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?>
              <fieldset>
                <label class="custom-control custom-checkbox">
                  <input type="checkbox" <?php if ($_smarty_tpl->tpl_vars['allget']->value[("babyAbilities").($_smarty_tpl->tpl_vars['k']->value)]) {?>checked<?php }?> class="custom-control-input" name="data[babyAbilities<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
]">
                  <span class="custom-control-indicator"></span> <span class="custom-control-description"><?php echo $_smarty_tpl->tpl_vars['t']->value;?>
</span> </label>
              </fieldset>
              <?php } ?> </div>
          </div>
          <?php }?> </div>
        <div class="form-actions">
          <div class="col-md-12">
            <button type="submit" class="mybtn2 mybtn-green" style="float: right;"> <i class="icon-search"></i> Search </button>
            <br>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
  </section>
</form>
<?php if ($_smarty_tpl->tpl_vars['noResults']->value) {?> <br>
<center>
  <h2>Nothing found</h2>
</center>
<br>
<br>
<?php } else { ?> 
<!-- Hover Effects -->
<section id="hover-effects" class="card search" style="margin-top: 20px !important;">
  <div class="card-block my-gallery" itemscope itemtype="http://schema.org/ImageGallery">
    <div class="row" style="margin: 0;">
      <div class="grid-hover"> <?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['profiles']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?>
        <?php $_smarty_tpl->tpl_vars["size"] = new Smarty_variable(getimagesize($_smarty_tpl->tpl_vars['t']->value->thumbnail), null, 0);?>
        <div class="col-md-4 col-xs-12" >
          <div class="thumbnail-container" data-id="<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
">
            <figure class="effect-roxy"> <img src="<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['t']->value->thumbnail,'/home/aibdh80ehx97/public_html/sncasting.com','');?>
" alt="img15" <?php if ($_smarty_tpl->tpl_vars['size']->value[0]>$_smarty_tpl->tpl_vars['size']->value[1]) {?>style="height: 380px; width: auto;"<?php }?>onerror="this.src='/template/app-assets/images/noimage.png'"/>
              <figcaption>
                <div class="editPanel">
                  <button onclick="location='/profile/edit?id=<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
'" class="editButton"><i class="ft-edit-2"></i></button>
                  <button class="deleteProfileButton" data-toggle="modal" data-target="#deleteProfile" onclick="profileToDelete=<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
" title="Delete profile"><i class="icon-trash4"></i></button>
                </div>
                <div class="thumbInfo" onclick="window.open('/profile/view?id=<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
')">
                  <div class="tableCell align-middle">
                    <p>
									Age: <b><?php echo $_smarty_tpl->tpl_vars['t']->value->age;?>
</b><br>
                                    <?php if ($_smarty_tpl->tpl_vars['t']->value->height) {?>Height: <b><?php echo $_smarty_tpl->tpl_vars['t']->value->height;?>
</b><br><?php }?>

										<?php if ($_smarty_tpl->tpl_vars['t']->value->profileType!=0) {?>
										<!--For adults and teens only-->
												<?php if ($_smarty_tpl->tpl_vars['t']->value->gender==0) {?>
												<!--For males only-->
													<?php if ($_smarty_tpl->tpl_vars['fields']->value['shirtSize'][$_smarty_tpl->tpl_vars['t']->value->profileType][$_smarty_tpl->tpl_vars['t']->value->shirtSize]) {?>Shirt: <b><?php echo $_smarty_tpl->tpl_vars['fields']->value['shirtSize'][$_smarty_tpl->tpl_vars['t']->value->profileType][$_smarty_tpl->tpl_vars['t']->value->shirtSize];?>
</b><br><?php }?>
													<?php if ($_smarty_tpl->tpl_vars['t']->value->waist) {?>Pants: <b><?php echo $_smarty_tpl->tpl_vars['t']->value->waist;?>
x<?php echo $_smarty_tpl->tpl_vars['t']->value->inseam;?>
</b><br><?php }?>
												<!--End of for males only-->
												<?php } else { ?>
												<!--For females only-->
													<?php if ($_smarty_tpl->tpl_vars['t']->value->dressSize) {?>Dress: <b><?php echo $_smarty_tpl->tpl_vars['t']->value->dressSize;?>
</b><br><?php }?>
													<?php if ($_smarty_tpl->tpl_vars['t']->value->bust) {?>Bust: <b><?php echo $_smarty_tpl->tpl_vars['t']->value->bust;?>
</b><br><?php }?>
													<?php if ($_smarty_tpl->tpl_vars['t']->value->waist) {?>Waist: <b><?php echo $_smarty_tpl->tpl_vars['t']->value->waist;?>
</b><br><?php }?>
													<?php if ($_smarty_tpl->tpl_vars['t']->value->hip) {?>Hip: <b><?php echo $_smarty_tpl->tpl_vars['t']->value->hip;?>
</b><br><?php }?>
													
												<!--End of for females only-->
												<?php }?>
										<!--End of for adults and teens only-->
                                		<?php } else { ?>
										<!--For children only-->
                                    		<?php if ($_smarty_tpl->tpl_vars['fields']->value['shirtSize'][$_smarty_tpl->tpl_vars['t']->value->profileType][$_smarty_tpl->tpl_vars['t']->value->shirtSize]) {?>Clothes: <b><?php echo $_smarty_tpl->tpl_vars['fields']->value['shirtSize'][$_smarty_tpl->tpl_vars['t']->value->profileType][$_smarty_tpl->tpl_vars['t']->value->shirtSize];?>
</b><br><?php }?>
										<!--End of for children only-->
										<?php }?>
										
                            
										
									<?php if ($_smarty_tpl->tpl_vars['fields']->value['shoeSize'][$_smarty_tpl->tpl_vars['t']->value->shoeSize]) {?>Shoe: <b><?php echo $_smarty_tpl->tpl_vars['fields']->value['shoeSize'][$_smarty_tpl->tpl_vars['t']->value->shoeSize];?>
</b><?php }?>
                                    </p>
                  </div>
                </div>
                <button class="selectButton" onclick="selectProfile(this)">Select</button>
              </figcaption>
            </figure>
            <a class="name-place" href="/profile/view?id=<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['t']->value->firstName;?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['t']->value->lastName;?>
</a>
          </div>
        </div>
        <?php } ?> </div>
    </div>
  </div>
  <!--/ Image grid --> 
</section>
<?php }?> 
<!--/ Hover Effects --> 

<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
