<?php /* Smarty version Smarty-3.1.19, created on 2019-02-28 17:46:30
         compiled from "/home/aibdh80ehx97/public_html/sncasting.com/template/profile.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6525213915bbf97bac533c6-47475393%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b70a3d88e3d80ac9b34a48959dabd9627fe8e7d1' => 
    array (
      0 => '/home/aibdh80ehx97/public_html/sncasting.com/template/profile.tpl',
      1 => 1551375972,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6525213915bbf97bac533c6-47475393',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bbf97bad6c360_45970531',
  'variables' => 
  array (
    'profile' => 0,
    'size' => 0,
    'my' => 0,
    'age' => 0,
    'fields' => 0,
    'ethnicityPrint' => 0,
    'profileType' => 0,
    'babyAbilitiesPrint' => 0,
    'heightPrint' => 0,
    'instagramPrint' => 0,
    'pressPrint' => 0,
    'musicPrint' => 0,
    'videoPrint' => 0,
    'keywordsPrint' => 0,
    'photos' => 0,
    'p' => 0,
    'family' => 0,
    't' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bbf97bad6c360_45970531')) {function content_5bbf97bad6c360_45970531($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/home/aibdh80ehx97/public_html/application/inc/smarty/plugins/modifier.replace.php';
if (!is_callable('smarty_modifier_date_format')) include '/home/aibdh80ehx97/public_html/application/inc/smarty/plugins/modifier.date_format.php';
?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("modals/deleteProfile.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("modals/newGroup.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<div class="content-body">
  <div class="selectedProfile" data-id="<?php echo $_smarty_tpl->tpl_vars['profile']->value->id;?>
" style="display: none"></div>
  <div id="user-profile">
    <div class="row">
      <div class="col-xl-6 col-md-6 col-xs-12">
        <div class="card profile-with-cover">
          <div class="media profil-cover-details" style="margin-top: 15px;">
            <div class="media-body media-middle row">
              <div class="col-xl-6 col-md-6 col-xs-12"> 
                <!--<div class="btn-group" role="group" aria-label="Basic example">
                                <a class="btn mybtn-red" data-toggle="modal" data-target="#danger" style="color: #FFF">Delete</a>
                                <a class="btn mybtn-yellow" href="/profile/edit?id=<?php echo $_smarty_tpl->tpl_vars['profile']->value->id;?>
">Edit</a>
                                <button type="button" class="btn mybtn-green">Print</button>
                            </div>--> 
              </div>
              <div class="col-xs-6"> </div>
            </div>
          </div>
          <?php $_smarty_tpl->tpl_vars["size"] = new Smarty_variable(getimagesize($_smarty_tpl->tpl_vars['profile']->value->photo), null, 0);?>
          <figure itemprop="associatedMedia" id="firstPhoto" itemscope itemtype="http://schema.org/ImageObject" class="item card-img-top img-fluid"> <a href="<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['profile']->value->photo,'/home/aibdh80ehx97/public_html/sncasting.com','');?>
" itemprop="contentUrl" data-size="<?php echo $_smarty_tpl->tpl_vars['size']->value[0];?>
x<?php echo $_smarty_tpl->tpl_vars['size']->value[1];?>
"> <img src="<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['profile']->value->photo,'/home/aibdh80ehx97/public_html/sncasting.com','');?>
" itemprop="thumbnail" alt="Image description" style="width: 100%; cursor: pointer;" <?php if ($_smarty_tpl->tpl_vars['size']->value[0]>$_smarty_tpl->tpl_vars['size']->value[1]) {?>style="height: 380px; width: auto;"<?php }?>onerror="this.src='/template/app-assets/images/noimage.png'"/> </a> </figure>
        </div>
      </div>
      <?php if ($_smarty_tpl->tpl_vars['my']->value->id>0) {?>
      <div class="col-xl-6 col-md-6 col-xs-12">
        <div class="profile-toolset"> <span class="profile-id" title="Profile number"><i class="ft-user"></i>&nbsp;<?php echo $_smarty_tpl->tpl_vars['profile']->value->id;?>
</span>
          <div style="float: right"> <a class="mybtn mybtn-purple button--moema" data-toggle="modal" data-target="#createGroup" onclick="profileToAddToCast=<?php echo $_smarty_tpl->tpl_vars['profile']->value->id;?>
" id="addToBtn"><i class="ft-copy" style="font-size: 24px; line-height: 1;" title="Add to group or casting"></i></a> <a class="mybtn mybtn-yellow button--moema" href="/profile/edit?id=<?php echo $_smarty_tpl->tpl_vars['profile']->value->id;?>
" title="Edit profile"><i class="ft-edit-2" style="font-size: 24px; line-height: 1;"></i></a> <a class="mybtn mybtn-red button--moema" data-toggle="modal" onclick="profileToDelete=<?php echo $_smarty_tpl->tpl_vars['profile']->value->id;?>
" data-target="#deleteProfile" title="Delete profile"><i class="icon-trash4" style="font-size: 24px; line-height: 1;"></i></a> </div>
        </div>
      </div>
      <?php }?>
      <div class="col-xl-6 col-md-6 col-xs-12">
        <div class="card">
          <div class="card-body ">
            <div class="card-block">
              <div class="card-text">
                <h3 class="card-title" style="text-align: center"><?php echo $_smarty_tpl->tpl_vars['profile']->value->firstName;?>
 <?php echo $_smarty_tpl->tpl_vars['profile']->value->lastName;?>
</h3>
                <br>
				<!--Admin view only-->
				<?php if ($_smarty_tpl->tpl_vars['my']->value->id>0) {?>
                <?php if ($_smarty_tpl->tpl_vars['profile']->value->birthday) {?>Age: <b><?php echo $_smarty_tpl->tpl_vars['age']->value;?>
</b><?php if ($_smarty_tpl->tpl_vars['profile']->value->birthday!="0000-00-00") {?> (<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['profile']->value->birthday);?>
)<?php }?><?php }?><br>
                Gender: <b><?php echo $_smarty_tpl->tpl_vars['fields']->value["gender"][$_smarty_tpl->tpl_vars['profile']->value->gender];?>
</b><br>
                <?php if ($_smarty_tpl->tpl_vars['ethnicityPrint']->value!='') {?>Ethnicity: <b><?php echo $_smarty_tpl->tpl_vars['ethnicityPrint']->value;?>
</b><br><?php }?>
                <?php if ($_smarty_tpl->tpl_vars['profile']->value->basedIn) {?>Based in: <b><?php echo $_smarty_tpl->tpl_vars['profile']->value->basedIn;?>
</b><br><?php }?>
				<?php if ($_smarty_tpl->tpl_vars['profile']->value->occupation) {?>Occupation: <b><?php echo $_smarty_tpl->tpl_vars['profile']->value->occupation;?>
</b><br><?php }?>
				<?php if ($_smarty_tpl->tpl_vars['profile']->value->rateAmount!='') {?>Rate: <b>$<?php echo $_smarty_tpl->tpl_vars['profile']->value->rateAmount;?>
</b> per <b><?php if ($_smarty_tpl->tpl_vars['profile']->value->rateType==0) {?>hour<?php }?><?php if ($_smarty_tpl->tpl_vars['profile']->value->rateType==1) {?>day<?php }?><?php if ($_smarty_tpl->tpl_vars['profile']->value->rateType==2) {?>project<?php }?></b><br><?php }?>
                <?php if ($_smarty_tpl->tpl_vars['profile']->value->availability) {?>Availibility: <b><?php echo $_smarty_tpl->tpl_vars['profile']->value->availability;?>
</b><br><?php }?>
                
                <?php if ($_smarty_tpl->tpl_vars['profile']->value->knownFor) {?>Known for: <b><?php echo $_smarty_tpl->tpl_vars['profile']->value->knownFor;?>
</b><br><?php }?>
                <?php if ($_smarty_tpl->tpl_vars['profile']->value->upcomingProjects) {?>Upcoming projects: <b><?php echo $_smarty_tpl->tpl_vars['profile']->value->upcomingProjects;?>
</b><br><?php }?>
				  
				<?php if ($_smarty_tpl->tpl_vars['profileType']->value==0&&$_smarty_tpl->tpl_vars['babyAbilitiesPrint']->value) {?>
                Baby Availability: <b><?php echo $_smarty_tpl->tpl_vars['babyAbilitiesPrint']->value;?>
</b><br><?php }?>
                
                <?php if ($_smarty_tpl->tpl_vars['heightPrint']->value) {?><br>Height: <b><?php echo $_smarty_tpl->tpl_vars['heightPrint']->value;?>
</b><br><?php }?>
                <?php if ($_smarty_tpl->tpl_vars['profile']->value->weight) {?>Weight: <b><?php echo $_smarty_tpl->tpl_vars['profile']->value->weight;?>
 lb</b><br><?php }?>
                
                
				<!--For adults and teens only-->
				<?php if ($_smarty_tpl->tpl_vars['profileType']->value!=0) {?>
				
					<!--For females only-->
					<?php if ($_smarty_tpl->tpl_vars['profile']->value->gender==1) {?>
				  		<?php if ($_smarty_tpl->tpl_vars['profile']->value->dressSize) {?>Dress size: <b><?php echo $_smarty_tpl->tpl_vars['profile']->value->dressSize;?>
</b><br><?php }?>
						<?php if ($_smarty_tpl->tpl_vars['profile']->value->bust) {?>Bust size: <b><?php echo $_smarty_tpl->tpl_vars['profile']->value->bust;?>
</b><br><?php }?>
				  		<?php if ($_smarty_tpl->tpl_vars['profile']->value->waist) {?>Waist size: <b><?php echo $_smarty_tpl->tpl_vars['profile']->value->waist;?>
</b><br><?php }?>
						<?php if ($_smarty_tpl->tpl_vars['profile']->value->hip) {?>Hip size: <b><?php echo $_smarty_tpl->tpl_vars['profile']->value->hip;?>
</b><br><?php }?>
						
						
					<!--End of for females only-->
					
				  	<!--For males only-->
					<?php } else { ?>
				  		<?php if ($_smarty_tpl->tpl_vars['fields']->value["shirtSize"][$_smarty_tpl->tpl_vars['profileType']->value][$_smarty_tpl->tpl_vars['profile']->value->shirtSize]) {?>Shirt size: <b><?php echo $_smarty_tpl->tpl_vars['fields']->value["shirtSize"][$_smarty_tpl->tpl_vars['profileType']->value][$_smarty_tpl->tpl_vars['profile']->value->shirtSize];?>
</b><br><?php }?>
						<?php if ($_smarty_tpl->tpl_vars['profile']->value->waist) {?>Pant size: <b><?php echo $_smarty_tpl->tpl_vars['profile']->value->waist;?>
<?php if ($_smarty_tpl->tpl_vars['profile']->value->inseam) {?>x<?php echo $_smarty_tpl->tpl_vars['profile']->value->inseam;?>
<?php }?></b><br><?php }?>
					<?php }?>
                	<!--End of for males only-->
				  
                	
				<!--End of for adults and teens only-->
				
				<!--For children only-->
                <?php } else { ?>
                    <?php if ($_smarty_tpl->tpl_vars['fields']->value["shirtSize"][$_smarty_tpl->tpl_vars['profileType']->value][$_smarty_tpl->tpl_vars['profile']->value->shirtSize]) {?>Clothing size: <b><?php echo $_smarty_tpl->tpl_vars['fields']->value["shirtSize"][$_smarty_tpl->tpl_vars['profileType']->value][$_smarty_tpl->tpl_vars['profile']->value->shirtSize];?>
</b><br><?php }?>
                <?php }?>
				<!--End of for children only-->
				  
                <?php if ($_smarty_tpl->tpl_vars['fields']->value["shoeSize"][$_smarty_tpl->tpl_vars['profile']->value->shoeSize]) {?>Shoe size: <b><?php echo $_smarty_tpl->tpl_vars['fields']->value["shoeSize"][$_smarty_tpl->tpl_vars['profile']->value->shoeSize];?>
</b><br><?php }?>
                
                <?php if ($_smarty_tpl->tpl_vars['instagramPrint']->value) {?><br>Instagram: <?php echo $_smarty_tpl->tpl_vars['instagramPrint']->value;?>
<br><br><?php }?>
				<?php if ($_smarty_tpl->tpl_vars['profile']->value->website) {?>Website: <a href="<?php echo $_smarty_tpl->tpl_vars['profile']->value->website;?>
" target="_blank">External Link</a><br><?php }?>
                <?php if ($_smarty_tpl->tpl_vars['profile']->value->book) {?>Book: <a href="<?php echo $_smarty_tpl->tpl_vars['profile']->value->book;?>
" target="_blank">External Link</a><br><?php }?>
                <?php if ($_smarty_tpl->tpl_vars['pressPrint']->value) {?><br>Press:<br><?php echo $_smarty_tpl->tpl_vars['pressPrint']->value;?>
<br><?php }?>
                <?php if ($_smarty_tpl->tpl_vars['musicPrint']->value) {?><br>Music:<br><?php echo $_smarty_tpl->tpl_vars['musicPrint']->value;?>
<br><?php }?>
                <?php if ($_smarty_tpl->tpl_vars['videoPrint']->value) {?><br>Video:<br><?php echo $_smarty_tpl->tpl_vars['videoPrint']->value;?>
<br><?php }?>
                
                
                <?php if ($_smarty_tpl->tpl_vars['profile']->value->bio) {?><br><b>Bio:</b><br><?php echo $_smarty_tpl->tpl_vars['profile']->value->bio;?>
<?php }?>
                <?php if ($_smarty_tpl->tpl_vars['profile']->value->privateNotes) {?><br><br><b>Private notes:</b><br><?php echo $_smarty_tpl->tpl_vars['profile']->value->privateNotes;?>
<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['keywordsPrint']->value!='') {?><br><br>Keywords: <div class="profile-keywords"><?php echo $_smarty_tpl->tpl_vars['keywordsPrint']->value;?>
</div><?php }?>
				<?php }?>
				<!--End of admin view only-->
				  
				
				<!--Client view only-->
				<?php if ($_smarty_tpl->tpl_vars['my']->value->id==0) {?>
				  <?php if ($_smarty_tpl->tpl_vars['profile']->value->basedIn) {?>Based in: <b><?php echo $_smarty_tpl->tpl_vars['profile']->value->basedIn;?>
</b><br><?php }?>
				  <?php if ($_smarty_tpl->tpl_vars['profile']->value->birthday) {?>Age: <b><?php echo $_smarty_tpl->tpl_vars['age']->value;?>
</b><br><?php }?>
				  <?php if ($_smarty_tpl->tpl_vars['profile']->value->occupation) {?>Occupation: <b><?php echo $_smarty_tpl->tpl_vars['profile']->value->occupation;?>
</b><br><?php }?>
				  <?php if ($_smarty_tpl->tpl_vars['profile']->value->knownFor) {?>Known for: <b><?php echo $_smarty_tpl->tpl_vars['profile']->value->knownFor;?>
</b><br><?php }?>
				  <?php if ($_smarty_tpl->tpl_vars['profile']->value->upcomingProjects) {?>Upcoming projects: <b><?php echo $_smarty_tpl->tpl_vars['profile']->value->upcomingProjects;?>
</b><br><?php }?>
				  <?php if ($_smarty_tpl->tpl_vars['instagramPrint']->value) {?><br>Instagram: <?php echo $_smarty_tpl->tpl_vars['instagramPrint']->value;?>
<br><br><?php }?>
				  <?php if ($_smarty_tpl->tpl_vars['profile']->value->website) {?>Website: <a href="<?php echo $_smarty_tpl->tpl_vars['profile']->value->website;?>
" target="_blank">External Link</a><br><?php }?>
				  <?php if ($_smarty_tpl->tpl_vars['profile']->value->book) {?>Book: <a href="<?php echo $_smarty_tpl->tpl_vars['profile']->value->book;?>
" target="_blank">External Link</a><br><?php }?>
				  <?php if ($_smarty_tpl->tpl_vars['pressPrint']->value) {?><br>Press:<br><?php echo $_smarty_tpl->tpl_vars['pressPrint']->value;?>
<br><?php }?>
                  <?php if ($_smarty_tpl->tpl_vars['musicPrint']->value) {?><br>Music:<br><?php echo $_smarty_tpl->tpl_vars['musicPrint']->value;?>
<br><?php }?>
                  <?php if ($_smarty_tpl->tpl_vars['videoPrint']->value) {?><br>Video:<br><?php echo $_smarty_tpl->tpl_vars['videoPrint']->value;?>
<br><?php }?>
				  
				  
				  <?php if ($_smarty_tpl->tpl_vars['heightPrint']->value) {?><br>Height: <b><?php echo $_smarty_tpl->tpl_vars['heightPrint']->value;?>
</b><br><?php }?>
				  <!--For adults and teens only-->
				<?php if ($_smarty_tpl->tpl_vars['profileType']->value!=0) {?>
				
					<!--For females only-->
					<?php if ($_smarty_tpl->tpl_vars['profile']->value->gender==1) {?>
				  		<?php if ($_smarty_tpl->tpl_vars['profile']->value->dressSize) {?>Dress size: <b><?php echo $_smarty_tpl->tpl_vars['profile']->value->dressSize;?>
</b><br><?php }?>
						<?php if ($_smarty_tpl->tpl_vars['profile']->value->bust) {?>Bust size: <b><?php echo $_smarty_tpl->tpl_vars['profile']->value->bust;?>
</b><br><?php }?>
				  		<?php if ($_smarty_tpl->tpl_vars['profile']->value->waist) {?>Waist size: <b><?php echo $_smarty_tpl->tpl_vars['profile']->value->waist;?>
</b><br><?php }?>
						<?php if ($_smarty_tpl->tpl_vars['profile']->value->hip) {?>Hip size: <b><?php echo $_smarty_tpl->tpl_vars['profile']->value->hip;?>
</b><br><?php }?>
						
						
					<!--End of for females only-->
					
				  	<!--For males only-->
					<?php } else { ?>
				  		<?php if ($_smarty_tpl->tpl_vars['fields']->value["shirtSize"][$_smarty_tpl->tpl_vars['profileType']->value][$_smarty_tpl->tpl_vars['profile']->value->shirtSize]) {?>Shirt size: <b><?php echo $_smarty_tpl->tpl_vars['fields']->value["shirtSize"][$_smarty_tpl->tpl_vars['profileType']->value][$_smarty_tpl->tpl_vars['profile']->value->shirtSize];?>
</b><br><?php }?>
						<?php if ($_smarty_tpl->tpl_vars['profile']->value->waist) {?>Pant size: <b><?php echo $_smarty_tpl->tpl_vars['profile']->value->waist;?>
<?php if ($_smarty_tpl->tpl_vars['profile']->value->inseam) {?>x<?php echo $_smarty_tpl->tpl_vars['profile']->value->inseam;?>
<?php }?></b><br><?php }?>
					<?php }?>
                	<!--End of for males only-->
				  
                	
				<!--End of for adults and teens only-->
				
				<!--For children only-->
                <?php } else { ?>
                    <?php if ($_smarty_tpl->tpl_vars['fields']->value["shirtSize"][$_smarty_tpl->tpl_vars['profileType']->value][$_smarty_tpl->tpl_vars['profile']->value->shirtSize]) {?>Clothing size: <b><?php echo $_smarty_tpl->tpl_vars['fields']->value["shirtSize"][$_smarty_tpl->tpl_vars['profileType']->value][$_smarty_tpl->tpl_vars['profile']->value->shirtSize];?>
</b><br><?php }?>
                <?php }?>
				<!--End of for children only-->
				  
                <?php if ($_smarty_tpl->tpl_vars['fields']->value["shoeSize"][$_smarty_tpl->tpl_vars['profile']->value->shoeSize]) {?>Shoe size: <b><?php echo $_smarty_tpl->tpl_vars['fields']->value["shoeSize"][$_smarty_tpl->tpl_vars['profile']->value->shoeSize];?>
</b><br><?php }?>
				<?php if ($_smarty_tpl->tpl_vars['profile']->value->bio) {?><br><b>Bio:</b><br><?php echo $_smarty_tpl->tpl_vars['profile']->value->bio;?>
<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['profile']->value->rateAmount!='') {?><br><br>Rate: <b>$<?php echo $_smarty_tpl->tpl_vars['profile']->value->rateAmount;?>
</b> per <b><?php if ($_smarty_tpl->tpl_vars['profile']->value->rateType==0) {?>hour<?php }?><?php if ($_smarty_tpl->tpl_vars['profile']->value->rateType==1) {?>day<?php }?><?php if ($_smarty_tpl->tpl_vars['profile']->value->rateType==2) {?>project<?php }?></b><br><?php }?>
				<?php if ($_smarty_tpl->tpl_vars['profile']->value->availability) {?><br>Availability: <b><?php echo $_smarty_tpl->tpl_vars['profile']->value->availability;?>
</b><br><?php }?>
				  
				<?php }?>
				<!--End of client view only-->
				</div>
            </div>
          </div>
        </div>
      </div>
      <?php if ($_smarty_tpl->tpl_vars['profile']->value->agentName||$_smarty_tpl->tpl_vars['profile']->value->agency||$_smarty_tpl->tpl_vars['profile']->value->agentPhone||$_smarty_tpl->tpl_vars['profile']->value->agentEmail) {?>
      <div class="col-xl-6 col-md-6 col-xs-12">
        <div class="section-title">Agency</div>
        <div class="card">
          <div class="card-block" style="padding-bottom: 21px;">
            <?php if ($_smarty_tpl->tpl_vars['profile']->value->agentName) {?><?php echo $_smarty_tpl->tpl_vars['profile']->value->agentName;?>
<?php }?>
			  <?php if ($_smarty_tpl->tpl_vars['my']->value->id>0) {?>
            	<?php if ($_smarty_tpl->tpl_vars['profile']->value->agentPhone) {?><br><a href="tel:<?php echo $_smarty_tpl->tpl_vars['profile']->value->agentPhone;?>
"><?php echo $_smarty_tpl->tpl_vars['profile']->value->agentPhone;?>
</a><?php }?>
            	<?php if ($_smarty_tpl->tpl_vars['profile']->value->agentEmail) {?><br><a href="mailto:<?php echo $_smarty_tpl->tpl_vars['profile']->value->agentEmail;?>
"><?php echo $_smarty_tpl->tpl_vars['profile']->value->agentEmail;?>
</a><?php }?>
			  <?php }?>
		  </div>
        </div>
      </div>
      <?php }?>
      
      <?php if ($_smarty_tpl->tpl_vars['my']->value->id>0) {?>
      <div class="col-xl-6 col-md-6 col-xs-12">
        <div class="section-title">Contact</div>
        <div class="card">
            <div class="card-block" style="padding-bottom: 21px;">
			  <?php if ($_smarty_tpl->tpl_vars['profile']->value->address) {?><?php echo $_smarty_tpl->tpl_vars['profile']->value->address;?>
<?php }?>
              <?php if ($_smarty_tpl->tpl_vars['profile']->value->address2) {?><br><?php echo $_smarty_tpl->tpl_vars['profile']->value->address2;?>
<?php }?>
              <?php if ($_smarty_tpl->tpl_vars['profile']->value->city) {?><br><?php echo $_smarty_tpl->tpl_vars['profile']->value->city;?>
,<?php }?>
              <?php if ($_smarty_tpl->tpl_vars['profile']->value->state) {?><?php echo $_smarty_tpl->tpl_vars['profile']->value->state;?>
 <?php }?>
              <?php if ($_smarty_tpl->tpl_vars['profile']->value->ZIPcode) {?><?php echo $_smarty_tpl->tpl_vars['profile']->value->ZIPcode;?>
<br><br><?php }?>
				
              <?php if ($_smarty_tpl->tpl_vars['profile']->value->email) {?>E-mail: <a href="mailto:<?php echo $_smarty_tpl->tpl_vars['profile']->value->email;?>
"><?php echo $_smarty_tpl->tpl_vars['profile']->value->email;?>
</a><?php }?>
              <?php if ($_smarty_tpl->tpl_vars['profile']->value->homePhone) {?><br>Home phone: <a href="tel:<?php echo $_smarty_tpl->tpl_vars['profile']->value->homePhone;?>
"><?php echo $_smarty_tpl->tpl_vars['profile']->value->homePhone;?>
</a><?php }?>
              <?php if ($_smarty_tpl->tpl_vars['profile']->value->cellPhone) {?><br>Cell phone: <a href="tel:<?php echo $_smarty_tpl->tpl_vars['profile']->value->cellPhone;?>
"><?php echo $_smarty_tpl->tpl_vars['profile']->value->cellPhone;?>
</a><?php }?>
			</div>
        </div>
      </div>
      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['profileType']->value!=2&&$_smarty_tpl->tpl_vars['my']->value->id>0&&($_smarty_tpl->tpl_vars['profile']->value->parentName||$_smarty_tpl->tpl_vars['profile']->value->parentPhone)) {?>
      <div class="col-xl-6 col-md-6 col-xs-12">
        <div class="section-title">Parent / Guardian</div>
        <div class="card">
          <div class="text-xs-left">
            <div class="card-block" style="padding-bottom: 21px;">
              <h4 class="card-title"><?php echo $_smarty_tpl->tpl_vars['profile']->value->parentName;?>
</h4>
              <?php if ($_smarty_tpl->tpl_vars['profile']->value->parentPhone) {?>Phone: <a href="tel:<?php echo $_smarty_tpl->tpl_vars['profile']->value->parentPhone;?>
"><?php echo $_smarty_tpl->tpl_vars['profile']->value->parentPhone;?>
</a><br>
              <?php }?>
              E-mail: <a href="mailto:<?php echo $_smarty_tpl->tpl_vars['profile']->value->parentEmail;?>
"><?php echo $_smarty_tpl->tpl_vars['profile']->value->parentEmail;?>
</a> </div>
          </div>
        </div>
      </div>
      <?php }?> </div>
    <section class="card">
      <div class="section-title">Gallery</div>
      <div class="card-block p-0">
        <div class="card-body collapse in">
          <div class="card-block my-gallery">
			  <?php if ($_smarty_tpl->tpl_vars['photos']->value) {?><?php } else { ?><div class="text-muted">No photos here yet...</div><?php }?>
			  <!-- Image grid -->  
              <div id="grid"><?php  $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['p']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['photos']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['p']->key => $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->_loop = true;
?>
              <?php $_smarty_tpl->tpl_vars["size"] = new Smarty_variable(getimagesize($_smarty_tpl->tpl_vars['p']->value), null, 0);?>
              <figure class="item"><a href="<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['p']->value,'/home/aibdh80ehx97/public_html/sncasting.com','');?>
" data-size="<?php echo $_smarty_tpl->tpl_vars['size']->value[0];?>
x<?php echo $_smarty_tpl->tpl_vars['size']->value[1];?>
"><img src="<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['p']->value,'/home/aibdh80ehx97/public_html/sncasting.com','');?>
" alt="" /></a></figure>
              <?php } ?> </div>
			  <!--/ Image grid -->  
          </div>
          
          <!--/ Image grid --> 
          <!-- Root element of PhotoSwipe. Must have class pswp. -->
          <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true"> 
            <!-- Background of PhotoSwipe. It's a separate element as animating opacity is faster than rgba(). -->
            <div class="pswp__bg"></div>
            <!-- Slides wrapper with overflow:hidden. -->
            <div class="pswp__scroll-wrap"> 
              <!-- Container that holds slides. PhotoSwipe keeps only 3 of them in the DOM to save memory. Don't modify these 3 pswp__item elements, data is added later on. -->
              <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
              </div>
              <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
              <div class="pswp__ui pswp__ui--hidden">
                <div class="pswp__top-bar"> 
                  <!--  Controls are self-explanatory. Order can be changed. -->
                  <div class="pswp__counter"></div>
                  <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                  <button class="pswp__button pswp__button--share" title="Share"></button>
                  <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                  <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                  <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR --> 
                  <!-- element will get class pswp__preloader-active when preloader is running -->
                  <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                  <div class="pswp__share-tooltip"></div>
                </div>
                <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"> </button>
                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"> </button>
                <div class="pswp__caption">
                  <div class="pswp__caption__center"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--/ PhotoSwipe --> 
      </div>
    </section>
    <?php if ($_smarty_tpl->tpl_vars['family']->value) {?> 
    <!-- Hover Effects -->
    <section id="hover-effects" class="card dash" style="margin-top: 20px !important; box-shadow: none; background-color: transparent;">
      <h3 class="mb-2" style="text-align: center">Related profiles:</h3>
      <div class="card-block my-gallery" itemscope itemtype="http://schema.org/ImageGallery">
        <div class="row" style="margin: 0;">
          <div class="grid-hover"> <?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['family']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?>
            <?php $_smarty_tpl->tpl_vars["size"] = new Smarty_variable(getimagesize($_smarty_tpl->tpl_vars['t']->value->photo), null, 0);?>
            <div class="col-md-4 col-xs-12" >
              <div class="thumbnail-container" data-id="<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
">
                <figure class="effect-roxy"> <img src="<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['t']->value->photo,'/home/aibdh80ehx97/public_html/sncasting.com','');?>
" alt="img15" <?php if ($_smarty_tpl->tpl_vars['size']->value[0]>$_smarty_tpl->tpl_vars['size']->value[1]) {?>style="height: 380px; width: auto;"<?php }?>onerror="this.src='/template/app-assets/images/noimage.png'"/>
                  <figcaption>
                    <div class="thumbInfo" onclick="location='/profile/view?id=<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
'">
                      <div class="tableCell align-middle">
                        <p><br>
                          <br>
                          <br>
                          <b><?php echo $_smarty_tpl->tpl_vars['fields']->value['profileType'][$_smarty_tpl->tpl_vars['t']->value->profileType];?>
</b><br>
                          Height: <b><?php echo $_smarty_tpl->tpl_vars['t']->value->height;?>
</b><br>
                          Weight: <b><?php echo $_smarty_tpl->tpl_vars['t']->value->weight;?>
lb</b><br>
                          <?php if ($_smarty_tpl->tpl_vars['t']->value->profileType==2) {?>
                          Shirt: <b><?php echo $_smarty_tpl->tpl_vars['fields']->value['shirtSize'][$_smarty_tpl->tpl_vars['t']->value->profileType][$_smarty_tpl->tpl_vars['t']->value->shirtSize];?>
</b><br>
                          Pants: <b><?php echo $_smarty_tpl->tpl_vars['t']->value->waist;?>
/<?php echo $_smarty_tpl->tpl_vars['t']->value->inseam;?>
</b><br>
                          <?php } else { ?>
                          Clothes: <b><?php echo $_smarty_tpl->tpl_vars['fields']->value['shirtSize'][$_smarty_tpl->tpl_vars['t']->value->profileType][$_smarty_tpl->tpl_vars['t']->value->shirtSize];?>
</b><br>
                          <?php }?>
                          Shoe: <b><?php echo $_smarty_tpl->tpl_vars['fields']->value['shoeSize'][$_smarty_tpl->tpl_vars['t']->value->shoeSize];?>
</b></p>
                      </div>
                    </div>
                  </figcaption>
                </figure>
                <a class="name-place" href="/profile/view?id=<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
"><span class="pnum"><?php echo $_smarty_tpl->tpl_vars['t']->value->num;?>
</span><?php echo $_smarty_tpl->tpl_vars['t']->value->firstName;?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['t']->value->lastName;?>
</a>
                <button class="mobSelectButton" onclick="mobSelectProfile(this)"><i id="mobSelectStat" class="icon-android-radio-button-off"></i></button>
              </div>
            </div>
            <?php } ?> </div>
          <div class="row">
            <div class="col-12 hidden-sm-up viewSwitcher">
              <button type="button" id="scrollToTop" class="mt-2"><i class="icon-arrow-up4"></i></button>
            </div>
          </div>
        </div>
      </div>
      <!--/ Image grid --> 
    </section>
    <!--/ Hover Effects --> 
    <?php }?> </div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
