<?php /* Smarty version Smarty-3.1.19, created on 2018-10-11 23:23:12
         compiled from "/home/aibdh80ehx97/public_html/sncasting.com/template/modals/removeFromGroup.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14766346055bbfdb60ec74d0-58846344%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0dad090f7f5a89f80606b93f967fd840cd519985' => 
    array (
      0 => '/home/aibdh80ehx97/public_html/sncasting.com/template/modals/removeFromGroup.tpl',
      1 => 1537190464,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14766346055bbfdb60ec74d0-58846344',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'groupID' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bbfdb60ef14b9_54872631',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bbfdb60ef14b9_54872631')) {function content_5bbfdb60ef14b9_54872631($_smarty_tpl) {?><div class="modal fade text-xs-left pr-0" id="removeFromGroup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel10" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-danger white">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel10">Remove profile from group</h4>
      </div>
      <div class="modal-body">
        <h5>Are you sure you want to remove this profile from this group?<br>
		  <br>
		  The profile will remain in the database.</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="mybtn2 mybtn-grey mr-1" data-dismiss="modal">Cancel</button>
        <a onclick="window.location='/groups/deleteFrom?id=<?php echo $_smarty_tpl->tpl_vars['groupID']->value;?>
&profile=' + profileToDelete;" class="mybtn2 mybtn-red">Remove from group</a></div>
    </div>
  </div>
</div>
<?php }} ?>
