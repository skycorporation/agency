<?php /* Smarty version Smarty-3.1.19, created on 2018-10-11 23:23:12
         compiled from "/home/aibdh80ehx97/public_html/sncasting.com/template/modals/removeMultipleFromGroup.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19039221435bbfdb60ef38f6-16179334%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3038a642abaf284a05d91c80b7f541fa78d1a43b' => 
    array (
      0 => '/home/aibdh80ehx97/public_html/sncasting.com/template/modals/removeMultipleFromGroup.tpl',
      1 => 1537190464,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19039221435bbfdb60ef38f6-16179334',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'groupID' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bbfdb60f080c0_82548959',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bbfdb60f080c0_82548959')) {function content_5bbfdb60f080c0_82548959($_smarty_tpl) {?><div class="modal fade text-xs-left pr-0" id="removeMultiple" tabindex="-1" role="dialog" aria-labelledby="myModalLabel10" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-danger white">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel10">Remove profiles from group</h4>
      </div>
      <div class="modal-body">
        <h5>Are you sure you want to remove these profiles from this group?<br>
		  <br>
		  The profile will remain in the database.</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="mybtn2 mybtn-grey mr-1" data-dismiss="modal">Cancel</button>
        <a onclick="removeMultiple('groups', <?php echo $_smarty_tpl->tpl_vars['groupID']->value;?>
)" class="mybtn2 mybtn-red">Remove from group</a></div>
    </div>
  </div>
</div>
<?php }} ?>
