<?php /* Smarty version Smarty-3.1.19, created on 2018-10-11 18:37:29
         compiled from "/home/aibdh80ehx97/public_html/sncasting.com/template/modals/editCasting.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12490897245bbf986946b906-25598500%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '309225ed0d66a09e9879709ed7757891ea42087f' => 
    array (
      0 => '/home/aibdh80ehx97/public_html/sncasting.com/template/modals/editCasting.tpl',
      1 => 1537190464,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12490897245bbf986946b906-25598500',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bbf9869486fa2_95574633',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bbf9869486fa2_95574633')) {function content_5bbf9869486fa2_95574633($_smarty_tpl) {?><div class="modal fade text-xs-left pr-0" id="castingEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel10" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-warning white">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel10">Edit casting</h4>
      </div>
      <div class="modal-body">
        <form action="/castings/edit/" method="get" id="editCastingForm">
          <input type="hidden" name="id">
          <small class="text-muted">Casting name:</small>
          <input type="text" autocomplete="dontsuggest" placeholder="Casting name" name="name" class="form-control mb-1 w-100 capThis" onkeyup="var newUrl = $(this).val(); newUrl = newUrl.toLowerCase(); newUrl = newUrl.replace(/ /g,''); newUrl = encodeURI(newUrl); $('#newUrl').val(newUrl);" required>
          <small class="text-muted">Casting location:</small>
          <input type="text" autocomplete="dontsuggest" placeholder="Casting location" name="location" class="form-control mb-1 w-100 checkThis">
          <div style="width: 100%; text-align: center" class="mt-1 mb-1 clearfix"><small class="text-muted">Casting sheet URL for models:</small><br>
            <br>
            <div style="width: 45%; float: left; text-align: right; line-height: 2">www.sncasting.com/c/</div>
            <div style="width: 50%; float: left">
              <input autocomplete="dontsuggest" type="text" id="newUrl" placeholder="Casting sheet URL" name="url" style="width: 80%; float: left; padding: 5px; border-width: 0; border-bottom: 1px solid #959595 !important">
            </div>
          </div>
          <small class="text-muted">Casting date:</small>
          <input type='text' placeholder="Casting date" class="form-control pickadate-dropdown checkThis" style="width: 50%" name="date"/>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="mybtn2 mybtn-grey mr-1" data-dismiss="modal">Cancel</button>
        <a onclick="$('#editCastingForm').submit();" class="mybtn2 mybtn-yellow">Save changes</a></div>
    </div>
  </div>
</div>
<?php }} ?>
