<?php /* Smarty version Smarty-3.1.19, created on 2019-02-28 22:09:40
         compiled from "/home/aibdh80ehx97/public_html/sncasting.com/template/castings.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12572897505bbf98693b61e1-18255116%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a07cea2610f022995aaefd6233870822aae44582' => 
    array (
      0 => '/home/aibdh80ehx97/public_html/sncasting.com/template/castings.tpl',
      1 => 1551391759,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12572897505bbf98693b61e1-18255116',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bbf98694636d0_79968779',
  'variables' => 
  array (
    'profileGroups' => 0,
    't' => 0,
    'photo' => 0,
    'c' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bbf98694636d0_79968779')) {function content_5bbf98694636d0_79968779($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/home/aibdh80ehx97/public_html/application/inc/smarty/plugins/modifier.replace.php';
?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
 
<script>
    var profileToDelete;
</script> 
<?php echo $_smarty_tpl->getSubTemplate ("modals/editCasting.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("modals/deleteCasting.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<section id="hover-effects">
  <div class="row">
    <div class="col-12">
      <h1 class="mt-3 mb-2" style="text-align: center">Castings</h1>
    </div>
  </div>
  <div class="row">
    <div class="grid-hover"> <?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['profileGroups']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
?>
      <div class="col-md-4 col-xs-12 mb-2">
        <div class="thumbnail-container casting">
          <div class="thumb-photo-place">
            <div class="imagesGroup imagesGroup<?php if ($_smarty_tpl->tpl_vars['t']->value["count"]>4) {?>4<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['t']->value["count"];?>
<?php }?>">
              <?php $_smarty_tpl->tpl_vars["c"] = new Smarty_variable(0, null, 0);?>
              <?php  $_smarty_tpl->tpl_vars['photo'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['photo']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['t']->value["photos"]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['photo']->key => $_smarty_tpl->tpl_vars['photo']->value) {
$_smarty_tpl->tpl_vars['photo']->_loop = true;
?>
              <div class="imagesGroupImg" style="background: url('<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['photo']->value,'/home/aibdh80ehx97/public_html/sncasting.com','');?>
');"/>
            </div>
            <?php $_smarty_tpl->tpl_vars['c'] = new Smarty_variable($_smarty_tpl->tpl_vars['c']->value+1, null, 0);?>
            <?php if ($_smarty_tpl->tpl_vars['c']->value>=4) {?> <?php break 1?> <?php }?>
            
            <?php } ?>
            <div style="clear: both;"></div>
            <div class="thumb-overlay"></div>
          </div>
          <div class="thumb-content">
            <div class="editPanel desktop-only">
              <button class="editButton" data-toggle="modal" data-target="#castingEdit" onclick="$('#editCastingForm').find('[name=name]').val($(this).data('name'));
              $('#editCastingForm').find('[name=location]').val($(this).data('location'));
              $('#editCastingForm').find('[name=date]').val($(this).data('date'));
              $('#editCastingForm').find('[name=id]').val($(this).data('id'));
              $('#editCastingForm').find('[name=url]').val($(this).data('url'));" data-name="<?php echo $_smarty_tpl->tpl_vars['t']->value["name"];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['t']->value["id"];?>
" data-location="<?php echo $_smarty_tpl->tpl_vars['t']->value["location"];?>
" data-date="<?php echo $_smarty_tpl->tpl_vars['t']->value["date"];?>
" data-url="<?php echo $_smarty_tpl->tpl_vars['t']->value["url"];?>
"><i class="ft-edit-2"></i></button>
              <button class="deleteProfileButton" data-toggle="modal" data-target="#deleteCasting" onclick="profileToDelete=<?php echo $_smarty_tpl->tpl_vars['t']->value['id'];?>
"><i class="icon-trash4"></i></button>
            </div>
            <a class="thumb-info" href="/castings/view?id=<?php echo $_smarty_tpl->tpl_vars['t']->value['id'];?>
">
            <p class="text-center"> <?php echo $_smarty_tpl->tpl_vars['t']->value["location"];?>
<br>
              <?php echo $_smarty_tpl->tpl_vars['t']->value["date"];?>
<br>
              <?php if ($_smarty_tpl->tpl_vars['t']->value["count"]==0) {?>No<?php }?><?php echo $_smarty_tpl->tpl_vars['t']->value["count"];?>
 profiles</p>
            </a> 
            <!--<button class="selectButton" onclick="activateCasting(this)">Deactivate</button>--> 
          </div>
        </div>
        <a class="name-place" href="/castings/view?id=<?php echo $_smarty_tpl->tpl_vars['t']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['t']->value["name"];?>
</a><!--<span class="castingStatus"><i class="fa fa-circle"></i></span>--> 
      </div>
    </div>
    <?php } ?> </div>
  </div>
</section>
<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
