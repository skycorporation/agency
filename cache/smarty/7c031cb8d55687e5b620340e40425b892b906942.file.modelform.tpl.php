<?php /* Smarty version Smarty-3.1.19, created on 2019-02-28 18:24:40
         compiled from "/home/aibdh80ehx97/public_html/sncasting.com/template/modelform.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6577923595bc0b3989ca143-47818846%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7c031cb8d55687e5b620340e40425b892b906942' => 
    array (
      0 => '/home/aibdh80ehx97/public_html/sncasting.com/template/modelform.tpl',
      1 => 1551376556,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6577923595bc0b3989ca143-47818846',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bc0b398cdf440_11664320',
  'variables' => 
  array (
    'profile' => 0,
    'fields' => 0,
    'k' => 0,
    'profileType' => 0,
    't' => 0,
    'd' => 0,
    'keywords' => 0,
    'height' => 0,
    'ethnicity' => 0,
    'hairColor' => 0,
    'instagram' => 0,
    'press' => 0,
    'music' => 0,
    'video' => 0,
    'photos' => 0,
    'p' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bc0b398cdf440_11664320')) {function content_5bc0b398cdf440_11664320($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/aibdh80ehx97/public_html/application/inc/smarty/plugins/modifier.date_format.php';
if (!is_callable('smarty_modifier_replace')) include '/home/aibdh80ehx97/public_html/application/inc/smarty/plugins/modifier.replace.php';
?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<style>
	.dropzone {
    min-height: 200px;
	}
	.dropzone .dz-message {
		font-size: 1.5rem;
		height: auto;
		margin-top: 18px;
	}
	.dropzone .dz-default.dz-message:before {
		font-size: 40px;
	}
</style>
<div class="w-100 text-center mt-3 mb-2">
  <h1><?php if ($_smarty_tpl->tpl_vars['profile']->value->id) {?>Edit<?php } else { ?>Create new<?php }?> profile</h1>
</div>
<br>
<form class="form form-horizontal" method="post" action="#" enctype="multipart/form-data">
  <div class="form-body">
    <section class="card">
      <div class="card-block">
        <button type="submit" class="btn btn-primary mybtn-green" style="float: right;"> <i class="icon-check2"></i> Save </button>
        <h4 class="form-section"><i class="icon-head"></i>General Info</h4>
        <div class="form-group row">
          <label class="col-md-3 label-control">Profile Type</label>
          <div class="col-md-9">
            <div class="btn-group" role="group" aria-label="Profile Type"> <?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value["profileType"]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?> <a type="button" href="/profile/<?php if ($_smarty_tpl->tpl_vars['profile']->value->id) {?>edit?id=<?php echo $_smarty_tpl->tpl_vars['profile']->value->id;?>
&profileType=<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
<?php } else { ?>add?profileType=<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
<?php }?>" class="mybtn2 mybtn-<?php if ($_smarty_tpl->tpl_vars['k']->value==$_smarty_tpl->tpl_vars['profileType']->value) {?>green<?php } else { ?>grey<?php }?>"><?php echo $_smarty_tpl->tpl_vars['t']->value;?>
</a> <?php } ?>
              <input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['profileType']->value;?>
" name="data[profileType]">
            </div>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputFirstName">First Name</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="<?php echo $_smarty_tpl->tpl_vars['profile']->value->firstName;?>
" type="text" id="projectinputFirstName" class="form-control checkThis capThis trimit" name="data[firstName]" required>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputLastName">Last Name</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="<?php echo $_smarty_tpl->tpl_vars['profile']->value->lastName;?>
" type="text" id="projectinputLastName" class="form-control checkThis capThis trimit" name="data[lastName]" required>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 col-sm-12 col-xs-12 label-control">Gender</label>
          <div class="col-md-9 float-xs-left">
            <fieldset>
              <div class="float-xs-left">
                <input type="checkbox" id="genderSwitch" class="switch" data-icon-cls="fa" data-off-icon-cls="icon-male2" data-on-icon-cls="icon-female2" data-off-label="Male" data-on-label="Female" <?php if ($_smarty_tpl->tpl_vars['profile']->value->gender==1) {?>checked="checked"<?php }?> onchange="if(this.checked){ $('.maleField').hide(); $('.femaleField').show(); }else{ $('.femaleField').hide(); $('.maleField').show();}" name="data[gender]" /></div>
            </fieldset>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-xs-12 col-sm-12 col-md-3 label-control">Birthday</label>
          <div class="col-sm-6 col-md-4 col-lg-3 col-xl-3">
            <div class="input-group">
              <select class="form-control checkThis mr-5px" id="bdMonth" onchange="bdChange()">
                <option value="" disabled selected>month</option>
                <option value="01" <?php if (smarty_modifier_date_format($_smarty_tpl->tpl_vars['profile']->value->birthday,"%m")==1) {?>selected<?php }?>>Jan</option>
                <option value="02" <?php if (smarty_modifier_date_format($_smarty_tpl->tpl_vars['profile']->value->birthday,"%m")==2) {?>selected<?php }?>>Feb</option>
                <option value="03" <?php if (smarty_modifier_date_format($_smarty_tpl->tpl_vars['profile']->value->birthday,"%m")==3) {?>selected<?php }?>>Mar</option>
                <option value="04" <?php if (smarty_modifier_date_format($_smarty_tpl->tpl_vars['profile']->value->birthday,"%m")==4) {?>selected<?php }?>>Apr</option>
                <option value="05" <?php if (smarty_modifier_date_format($_smarty_tpl->tpl_vars['profile']->value->birthday,"%m")==5) {?>selected<?php }?>>May</option>
                <option value="06" <?php if (smarty_modifier_date_format($_smarty_tpl->tpl_vars['profile']->value->birthday,"%m")==6) {?>selected<?php }?>>Jun</option>
                <option value="07" <?php if (smarty_modifier_date_format($_smarty_tpl->tpl_vars['profile']->value->birthday,"%m")==7) {?>selected<?php }?>>Jul</option>
                <option value="08" <?php if (smarty_modifier_date_format($_smarty_tpl->tpl_vars['profile']->value->birthday,"%m")==8) {?>selected<?php }?>>Aug</option>
                <option value="09" <?php if (smarty_modifier_date_format($_smarty_tpl->tpl_vars['profile']->value->birthday,"%m")==9) {?>selected<?php }?>>Sep</option>
                <option value="10" <?php if (smarty_modifier_date_format($_smarty_tpl->tpl_vars['profile']->value->birthday,"%m")==10) {?>selected<?php }?>>Oct</option>
                <option value="11" <?php if (smarty_modifier_date_format($_smarty_tpl->tpl_vars['profile']->value->birthday,"%m")==11) {?>selected<?php }?>>Nov</option>
                <option value="12" <?php if (smarty_modifier_date_format($_smarty_tpl->tpl_vars['profile']->value->birthday,"%m")==12) {?>selected<?php }?>>Dec</option>
              </select>
              <select class="form-control checkThis mr-5px" id="bdDay" onchange="bdChange()">
                <option value="" disabled selected>day</option>
                
		                    <?php $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['k']->step = 1;$_smarty_tpl->tpl_vars['k']->total = (int) ceil(($_smarty_tpl->tpl_vars['k']->step > 0 ? 31+1 - (1) : 1-(31)+1)/abs($_smarty_tpl->tpl_vars['k']->step));
if ($_smarty_tpl->tpl_vars['k']->total > 0) {
for ($_smarty_tpl->tpl_vars['k']->value = 1, $_smarty_tpl->tpl_vars['k']->iteration = 1;$_smarty_tpl->tpl_vars['k']->iteration <= $_smarty_tpl->tpl_vars['k']->total;$_smarty_tpl->tpl_vars['k']->value += $_smarty_tpl->tpl_vars['k']->step, $_smarty_tpl->tpl_vars['k']->iteration++) {
$_smarty_tpl->tpl_vars['k']->first = $_smarty_tpl->tpl_vars['k']->iteration == 1;$_smarty_tpl->tpl_vars['k']->last = $_smarty_tpl->tpl_vars['k']->iteration == $_smarty_tpl->tpl_vars['k']->total;?>
                
                <option value="<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
" <?php if (smarty_modifier_date_format($_smarty_tpl->tpl_vars['profile']->value->birthday,"%e")==$_smarty_tpl->tpl_vars['k']->value) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['k']->value;?>
</option>
                

		                    <?php }} ?>
              
              </select>
              <select class="form-control checkThis" id="bdYear" onchange="bdChange()">
                <option value="" disabled selected>year</option>
                
                
		                    <?php if ($_smarty_tpl->tpl_vars['profileType']->value==2) {?><?php $_smarty_tpl->tpl_vars["d"] = new Smarty_variable(70, null, 0);?><?php }?>
		                    <?php if ($_smarty_tpl->tpl_vars['profileType']->value==1) {?><?php $_smarty_tpl->tpl_vars["d"] = new Smarty_variable(18, null, 0);?><?php }?>
		                    <?php if ($_smarty_tpl->tpl_vars['profileType']->value==0) {?><?php $_smarty_tpl->tpl_vars["d"] = new Smarty_variable(3, null, 0);?><?php }?>
		                    <?php $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['k']->step = 1;$_smarty_tpl->tpl_vars['k']->total = (int) ceil(($_smarty_tpl->tpl_vars['k']->step > 0 ? date('Y')+1 - (date('Y')-$_smarty_tpl->tpl_vars['d']->value) : date('Y')-$_smarty_tpl->tpl_vars['d']->value-(date('Y'))+1)/abs($_smarty_tpl->tpl_vars['k']->step));
if ($_smarty_tpl->tpl_vars['k']->total > 0) {
for ($_smarty_tpl->tpl_vars['k']->value = date('Y')-$_smarty_tpl->tpl_vars['d']->value, $_smarty_tpl->tpl_vars['k']->iteration = 1;$_smarty_tpl->tpl_vars['k']->iteration <= $_smarty_tpl->tpl_vars['k']->total;$_smarty_tpl->tpl_vars['k']->value += $_smarty_tpl->tpl_vars['k']->step, $_smarty_tpl->tpl_vars['k']->iteration++) {
$_smarty_tpl->tpl_vars['k']->first = $_smarty_tpl->tpl_vars['k']->iteration == 1;$_smarty_tpl->tpl_vars['k']->last = $_smarty_tpl->tpl_vars['k']->iteration == $_smarty_tpl->tpl_vars['k']->total;?>

                
                <option value="<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
" <?php if (smarty_modifier_date_format($_smarty_tpl->tpl_vars['profile']->value->birthday,"%Y")==$_smarty_tpl->tpl_vars['k']->value) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['k']->value;?>
</option>
                
		                    <?php }} ?>
              
              </select>
            </div>
            <input value="<?php echo $_smarty_tpl->tpl_vars['profile']->value->birthday;?>
" type='hidden' name="data[birthday]" id="dbFinal"/>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-xs-12 col-sm-12 col-md-3 label-control" for="projectinputBased">Based in</label>
          <div class="col-sm-6 col-md-4 col-lg-3 col-xl-3">
            <input autocomplete="dontsuggest" value="<?php echo $_smarty_tpl->tpl_vars['profile']->value->basedIn;?>
" type="text" id="projectinputBased" class="form-control checkThis trimit" name="data[basedin]">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control">Keywords</label>
          <div class="col-md-6">
            <fieldset>
              <div class="form-group">
                <div class="form-control" id="keywords" data-tags-input-name="edit-on-delete" style="min-height: 80px"><?php echo $_smarty_tpl->tpl_vars['keywords']->value;?>
</div>
                <small class="text-muted">Press Enter, Comma or Spacebar to add a new tag, Backspace or Delete to remove.</small></div>
            </fieldset>
            <input type="hidden" name="data[keywords]" value='<?php echo $_smarty_tpl->tpl_vars['profile']->value->keywords;?>
'>
          </div>
        </div>
          <hr style="margin-bottom: 2.5rem;margin-top: 2.5rem;">
      <div class="form-group row">
          <label class="col-md-3 label-control" for="profilePrivateNotes">Private notes</label>
          <div class="col-md-6">
              <textarea id="profilePrivateNotes" class="form-control checkThis" rows="6" name="data[privateNotes]"><?php echo $_smarty_tpl->tpl_vars['profile']->value->privateNotes;?>
</textarea>
          </div>
        </div>
      </div>
    </section>
    <section class="card">
      <div class="card-block">
        <h4 class="form-section"><i class="icon-clipboard4"></i>Sizes</h4>
        <div class="form-group row">
          <label class="col-xs-12 col-sm-12 col-md-3 label-control">Height</label>
          <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="input-group"> 
              <!--For children only--> 
              <?php if ($_smarty_tpl->tpl_vars['profileType']->value==0) {?>
              <input autocomplete="dontsuggest" value="<?php echo $_smarty_tpl->tpl_vars['profile']->value->height;?>
" type="number" id="projectinputHeight" placeholder="inches" class="form-control numOnly checkThis" name="data[height]">
              <!--End of for children only--> 
              
              <!--For teens and adults only--> 
              <?php } else { ?>
              <select id="projectinputHeight1" name="data[height1]" class="form-control checkThis mr-5px">
                <option value="" hidden>feet</option>
                
					  <?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value["height"][0]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?>
					  
                <option value="<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['k']->value==$_smarty_tpl->tpl_vars['height']->value[0]&&$_smarty_tpl->tpl_vars['height']->value[0]!='') {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['t']->value;?>
</option>
                
					  <?php } ?>
				  
              </select>
              <select id="projectinputHeight2" name="data[height2]" class="form-control checkThis">
                <option value="" hidden>inches</option>
                
					  <?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value["height"][1]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?>
					  
                <option value="<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['k']->value==$_smarty_tpl->tpl_vars['height']->value[1]&&$_smarty_tpl->tpl_vars['height']->value[1]!='') {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['t']->value;?>
</option>
                
					  <?php } ?>
				  
              </select>
              <?php }?> 
              <!--End of for teens and adults only--> 
            </div>
          </div>
        </div>
        <!--<div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputWeight">Weight</label>
          <div class="col-md-3">
            <input autocomplete="dontsuggest" value="<?php echo $_smarty_tpl->tpl_vars['profile']->value->weight;?>
" type="text" id="projectinputWeight" placeholder="pounds" class="form-control checkThis numOnly" name="data[weight]">
          </div>
        </div>--> 
        
        <!--For children only--> 
        <?php if ($_smarty_tpl->tpl_vars['profileType']->value==0) {?>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputClothes">Clothing size</label>
          <div class="col-md-3">
            <select id="projectinputClothes" name="data[shirtSize]" class="form-control checkThis">
              <option value="" hidden class="placeholder">select one...</option>
              
				<?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value["shirtSize"][$_smarty_tpl->tpl_vars['profileType']->value]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?>
				
              <option value="<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['k']->value==$_smarty_tpl->tpl_vars['profile']->value->shirtSize&&$_smarty_tpl->tpl_vars['profile']->value->shirtSize!='') {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['t']->value;?>
</option>
              
				<?php } ?>
			  
            </select>
          </div>
        </div>
        <?php }?> 
        <!--End of for children only--> 
        
        <!--For teens and adults only--> 
        <?php if ($_smarty_tpl->tpl_vars['profileType']->value!=0) {?> 
        
        <!--For males only-->
        <div class="form-group row maleField"<?php if ($_smarty_tpl->tpl_vars['profile']->value->gender==0) {?> style="display: block;"<?php }?>>
          <label class="col-xs-12 col-sm-12 col-md-3 label-control">Shirt size</label>
          <div class="col-sm-6 col-md-3">
            <select id="projectinput7" name="data[shirtSize]" class="form-control checkThis">
              <option value="" hidden>select one...</option>
              

		      <?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value["shirtSize"][2]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?>

              
              <option value="<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['k']->value==$_smarty_tpl->tpl_vars['profile']->value->shirtSize&&$_smarty_tpl->tpl_vars['profile']->value->shirtSize!='') {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['t']->value;?>
</option>
              

		      <?php } ?>

            
            </select>
          </div>
        </div>
		 <!--End of for males only-->
		  
		  
        <div class="form-group row">
          <label class="col-xs-12 col-sm-12 col-md-3 label-control maleField" <?php if ($_smarty_tpl->tpl_vars['profile']->value->gender==0) {?> style="display: block;"<?php }?>>Pant size</label>
          <label class="col-xs-12 col-sm-12 col-md-3 label-control femaleField" <?php if ($_smarty_tpl->tpl_vars['profile']->value->gender==1) {?> style="display: block;"<?php }?>>Waist size</label>
          <div class="col-sm-6 col-md-3">
            <div class="input-group">
              <input autocomplete="dontsuggest" value="<?php echo $_smarty_tpl->tpl_vars['profile']->value->waist;?>
" type="number" placeholder="waist" class="form-control checkThis numOnly mr-5px" name="data[waist]" <?php if ($_smarty_tpl->tpl_vars['profile']->value->gender==1) {?> style="margin-right: 0 !important; border-bottom-right-radius:5px; border-top-right-radius:5px;"<?php }?>>
              <input autocomplete="dontsuggest" value="<?php echo $_smarty_tpl->tpl_vars['profile']->value->inseam;?>
" type="number" placeholder="inseam" class="form-control checkThis numOnly maleField" name="data[inseam]" <?php if ($_smarty_tpl->tpl_vars['profile']->value->gender==1) {?> style="display: none;"<?php }?>>
            </div>
          </div>
        </div>
         
        
        <!--For females only-->
        <div class="form-group row femaleField"<?php if ($_smarty_tpl->tpl_vars['profile']->value->gender==1) {?> style="display: block;"<?php }?>>
          <label class="col-xs-12 col-sm-12 col-md-3 label-control" for="projectinputDress">Dress size</label>
          <div class="col-sm-6 col-md-3">
            <select id="projectinputDress" name="data[dressSize]" class="form-control checkThis">
              <option value="" hidden>select one...</option>
              

		      <?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value["dressSize"]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?>

              
              <option value="<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['k']->value==$_smarty_tpl->tpl_vars['profile']->value->dressSize&&$_smarty_tpl->tpl_vars['profile']->value->dressSize!='') {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['t']->value;?>
</option>
              

		      <?php } ?>

            
            </select>
          </div>
        </div>
        <div class="form-group row femaleField"<?php if ($_smarty_tpl->tpl_vars['profile']->value->gender==1) {?> style="display: block;"<?php }?>>
          <label class="col-xs-12 col-sm-12 col-md-3 label-control" for="projectinputBust">Bust size</label>
          <div class="col-sm-6 col-md-3">
            <input id="projectinputBust" autocomplete="dontsuggest" value="<?php echo $_smarty_tpl->tpl_vars['profile']->value->bust;?>
" type="number" placeholder="bust" class="form-control checkThis mr-5px" name="data[bust]">
          </div>
        </div>
        
        <div class="form-group row femaleField"<?php if ($_smarty_tpl->tpl_vars['profile']->value->gender==1) {?> style="display: block;"<?php }?>>
          <label class="col-xs-12 col-sm-12 col-md-3 label-control" for="projectinputHip">Hip size</label>
          <div class="col-sm-6 col-md-3">
            <input id="projectinputHip" autocomplete="dontsuggest" value="<?php echo $_smarty_tpl->tpl_vars['profile']->value->hip;?>
" type="number" placeholder="hip" class="form-control checkThis mr-5px" name="data[hip]">
          </div>
        </div>
        
        <!--End of for females only--> 
        
        <?php }?> 
        <!--End of for teens and adults only-->
        
        <div class="form-group row">
          <label class="col-xs-12 col-sm-12 col-md-3 label-control" for="projectinputShoe">Shoe size</label>
          <div class="col-sm-6 col-md-3">
            <select id="projectinputShoe" name="data[shoeSize]" class="form-control checkThis">
              <option value="" hidden>select one...</option>
              
				<?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value["shoeSize"]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?>
				
              <option value="<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['k']->value==$_smarty_tpl->tpl_vars['profile']->value->shoeSize&&$_smarty_tpl->tpl_vars['profile']->value->shoeSize!='') {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['t']->value;?>
</option>
              
				<?php } ?>
			  
            </select>
          </div>
        </div>
      </div>
    </section>
    <section class="card">
      <div class="card-block">
        <h4 class="form-section"><i class="icon-clipboard4"></i>Details</h4>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="profileBio">Bio</label>
          <div class="col-md-6">
            <textarea id="profileBio" class="form-control checkThis" rows="5" name="data[bio]"><?php echo $_smarty_tpl->tpl_vars['profile']->value->bio;?>
</textarea>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputKnown">Known for</label>
          <div class="col-md-6">
            <textarea id="projectinputKnown" class="form-control checkThis" rows="3" name="data[knownFor]"><?php echo $_smarty_tpl->tpl_vars['profile']->value->knownFor;?>
</textarea>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputProjects">Upcoming projects</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="<?php echo $_smarty_tpl->tpl_vars['profile']->value->upcomingProjects;?>
" type="text" id="projectinputProjects" class="form-control checkThis trimit" name="data[upcomingProjects]">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputOccupation">Occupation/Hobbies</label>
          <div class="col-md-4">
            <input autocomplete="dontsuggest" value="<?php echo $_smarty_tpl->tpl_vars['profile']->value->occupation;?>
" type="text" id="projectinputOccupation" class="form-control checkThis trimit" name="data[occupation]">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-xs-12 col-sm-12 col-md-3 label-control" for="projectinputRate">Rate</label>
          <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="input-group">
              <input autocomplete="dontsuggest" value="<?php echo $_smarty_tpl->tpl_vars['profile']->value->rateAmount;?>
" type="number" id="projectinputRate" placeholder="$" class="form-control numOnly checkThis trimit mr-10px" name="data[rateAmount]">
              <span style="float: left; margin-top: 8px;">per</span>
              <select id="projectinput9" name="data[rateType]" class="form-control checkThis ml-10px">
                <option value="" hidden>select...</option>
                
						<?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value["rateType"]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?>
						
                <option value="<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['k']->value==$_smarty_tpl->tpl_vars['profile']->value->rateType&&$_smarty_tpl->tpl_vars['profile']->value->rateType!='') {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['t']->value;?>
</option>
                
						<?php } ?>
					
              </select>
            </div>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputAvailability">Availability</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="<?php echo $_smarty_tpl->tpl_vars['profile']->value->availability;?>
" type="text" id="projectinputAvailability" class="form-control checkThis trimit" name="data[availability]">
          </div>
        </div>
        <br>
        <br>
        <div class="form-group row">
          <label class="col-md-3 label-control">Ethnicity</label>
          <div class="col-md-9"> <?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value["ethnicity"]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?>
            <fieldset>
              <label class="custom-control custom-checkbox">
                <input <?php if ($_smarty_tpl->tpl_vars['ethnicity']->value[$_smarty_tpl->tpl_vars['k']->value]==1) {?>checked<?php }?> type="checkbox" class="custom-control-input" name="data[ethnicity<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
]">
                <span class="custom-control-indicator"></span> <span class="custom-control-description"><?php echo $_smarty_tpl->tpl_vars['t']->value;?>
</span> </label>
            </fieldset>
            <?php } ?> </div>
        </div>
        <!--<div class="form-group row">
          <label class="col-md-3 label-control" for="projectinput7">Hair color</label>
          <div class="col-md-9"> <?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value["hairColor"]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?>
            <fieldset>
              <label class="custom-control custom-checkbox">
                <input <?php if ($_smarty_tpl->tpl_vars['hairColor']->value[$_smarty_tpl->tpl_vars['k']->value]==1) {?>checked<?php }?> type="checkbox" class="custom-control-input" name="data[hairColor<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
]">
                <span class="custom-control-indicator"></span> <span class="custom-control-description"><?php echo $_smarty_tpl->tpl_vars['t']->value;?>
</span> </label>
            </fieldset>
            <?php } ?> </div>
        </div>--> 
      </div>
    </section>
    <section class="card">
      <div class="card-block">
        <h4 class="form-section"><i class="icon-clipboard4"></i>URLs</h4>
        <div class="form-group row">
          <label class="col-md-3 label-control">Instagram</label>
          <div class="col-md-6">
            <div class="form-group instagram-repeater">
              <div data-repeater-list="repeater-group"> <?php if (isset($_smarty_tpl->tpl_vars['instagram']->value)) {?>
                <?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['instagram']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?>
                <div class="input-group mb-1" data-repeater-item>
                  <div class="input-group-prepend"><span class="input-group-text">@</span></div>
                  <input autocomplete="dontsuggest" type="text" placeholder="username" class="input-instagram form-control instafield checkThis trimit" name="data[instagram][<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['t']->value;?>
">
                  <span class="input-group-append" id="button-addon2">
                  <button  type="button" data-repeater-delete class="mybtnSide mybtn-clear instagram-delete"><i class="ft-x"></i></button>
                  </span> <small class="errormsg w-100 ml-3 red" style="display: none"></small> </div>
                <?php } ?>
                <?php } else { ?>
                <div class="input-group mb-1" data-repeater-item>
                  <div class="input-group-prepend"><span class="input-group-text">@</span></div>
                  <input autocomplete="dontsuggest" type="text" placeholder="username" class="input-instagram form-control instafield checkThis trimit" name="data[instagram]">
                  <span class="input-group-append" id="button-addon2">
                  <button  type="button" data-repeater-delete class="mybtnSide mybtn-clear instagrm-delete"><i class="ft-x"></i></button>
                  </span> <small class="errormsg w-100 ml-3 red" style="display: none"></small> </div>
                <?php }?> </div>
              <button type="button" data-repeater-create class="btn mybtn-transp btn-sm" style="float: right; margin-top: -10px;">Add another link</button>
              <input type="hidden" value='<?php echo $_smarty_tpl->tpl_vars['profile']->value->instagram;?>
' name="data[instagram]" id="instagram">
            </div>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputWebsite">Website</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="<?php echo $_smarty_tpl->tpl_vars['profile']->value->website;?>
" type="text" id="projectinputWebsite" placeholder="www..." class="form-control urlfield" name="data[website]">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputBook">Book</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="<?php echo $_smarty_tpl->tpl_vars['profile']->value->book;?>
" type="text" id="projectinputBook" placeholder="www..." class="form-control urlfield" name="data[book]">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control">Press</label>
          <div class="col-md-6">
            <div class="form-group press-repeater">
              <div data-repeater-list="press"> <?php if (isset($_smarty_tpl->tpl_vars['press']->value)) {?>
                <?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['press']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?>
                <div class="input-group mb-1" data-repeater-item>
                  <input autocomplete="dontsuggest" type="text" placeholder="www..." class="input-press form-control urlfield" name="data[press][<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['t']->value;?>
">
                  <span class="input-group-append" id="button-addon2">
                  <button  type="button" data-repeater-delete class="mybtnSide mybtn-clear press-delete"><i class="ft-x"></i></button>
                  </span> </div>
                <?php } ?>
                <?php } else { ?>
                <div class="input-group mb-1" data-repeater-item>
                  <input autocomplete="dontsuggest" type="text" placeholder="www..." class="input-press form-control urlfield" name="data[press]">
                  <span class="input-group-append" id="button-addon2">
                  <button  type="button" data-repeater-delete class="mybtnSide mybtn-clear press-delete"><i class="ft-x"></i></button>
                  </span> </div>
                <?php }?> </div>
              <button type="button" data-repeater-create class="btn mybtn-transp btn-sm" style="float: right; margin-top: -10px;">Add another link</button>
              <input type="hidden" value='<?php echo $_smarty_tpl->tpl_vars['profile']->value->press;?>
' name="data[press]" id="press">
            </div>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control">Music</label>
          <div class="col-md-6">
            <div class="form-group music-repeater">
              <div data-repeater-list="music"> <?php if (isset($_smarty_tpl->tpl_vars['music']->value)) {?>
                <?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['music']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?>
                <div class="input-group mb-1" data-repeater-item>
                  <input autocomplete="dontsuggest" type="text" placeholder="www..." class="input-music form-control urlfield" name="data[music][<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['t']->value;?>
">
                  <span class="input-group-append" id="button-addon2">
                  <button  type="button" data-repeater-delete class="mybtnSide mybtn-clear music-delete"><i class="ft-x"></i></button>
                  </span> </div>
                <?php } ?>
                <?php } else { ?>
                <div class="input-group mb-1" data-repeater-item>
                  <input autocomplete="dontsuggest" type="text" placeholder="www..." class="input-music form-control urlfield" name="data[music]">
                  <span class="input-group-append" id="button-addon2">
                  <button  type="button" data-repeater-delete class="mybtnSide mybtn-clear music-delete"><i class="ft-x"></i></button>
                  </span> </div>
                <?php }?> </div>
              <button type="button" data-repeater-create class="btn mybtn-transp btn-sm" style="float: right; margin-top: -10px;">Add another link</button>
              <input type="hidden" value='<?php echo $_smarty_tpl->tpl_vars['profile']->value->music;?>
' name="data[audio]" id="music">
            </div>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control">Video</label>
          <div class="col-md-6">
            <div class="form-group video-repeater">
              <div data-repeater-list="video"> <?php if (isset($_smarty_tpl->tpl_vars['video']->value)) {?>
                <?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['video']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?>
                <div class="input-group mb-1" data-repeater-item>
                  <input autocomplete="dontsuggest" type="text" placeholder="www..." class="input-video form-control urlfield" name="data[video][<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['t']->value;?>
">
                  <span class="input-group-append" id="button-addon2">
                  <button  type="button" data-repeater-delete class="mybtnSide mybtn-clear video-delete"><i class="ft-x"></i></button>
                  </span> </div>
                <?php } ?>
                <?php } else { ?>
                <div class="input-group mb-1" data-repeater-item>
                  <input autocomplete="dontsuggest" type="text" placeholder="www..." class="input-video form-control urlfield" name="data[video]">
                  <span class="input-group-append" id="button-addon2">
                  <button  type="button" data-repeater-delete class="mybtnSide mybtn-clear video-delete"><i class="ft-x"></i></button>
                  </span> </div>
                <?php }?> </div>
              <button type="button" data-repeater-create class="btn mybtn-transp btn-sm" style="float: right; margin-top: -10px;">Add another link</button>
              <input type="hidden" value='<?php echo $_smarty_tpl->tpl_vars['profile']->value->video;?>
' name="data[video]" id="video">
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="card">
      <div class="card-block">
        <h4 class="form-section"><i class="icon-clipboard4"></i>Contact</h4>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputAddress1">Address</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="<?php echo $_smarty_tpl->tpl_vars['profile']->value->address;?>
" type="text" id="projectinputAddress1" placeholder="house number, street" class="form-control checkThis trimit" name="data[address]">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputAddress2">Address</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="<?php echo $_smarty_tpl->tpl_vars['profile']->value->address2;?>
" type="text" id="projectinputAddress2" placeholder="apartment" class="form-control checkThis trimit" name="data[address2]">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputCity">City</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="<?php echo $_smarty_tpl->tpl_vars['profile']->value->city;?>
" type="text" id="projectinputCity" placeholder="city" class="form-control capThis checkThis trimit" name="data[city]">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputState">State</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="<?php echo $_smarty_tpl->tpl_vars['profile']->value->state;?>
" type="text" id="projectinputState" placeholder="NY, CA, etc." class="form-control statefield checkThis checkThis trimit" name="data[state]">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputZip">Zip code</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="<?php echo $_smarty_tpl->tpl_vars['profile']->value->ZIPcode;?>
" type="tel" id="projectinputZip" placeholder="12345" class="form-control zipfield checkThis checkThis trimit" name="data[ZIPcode]">
          </div>
        </div>
        <br>
        <br>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputCellPhone">Cell phone</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="<?php echo $_smarty_tpl->tpl_vars['profile']->value->cellPhone;?>
" type="tel" id="projectinputCellPhone" class="form-control phonefield numOnly checkThis trimit" name="data[cellPhone]">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputPersEmail">E-mail</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="<?php echo $_smarty_tpl->tpl_vars['profile']->value->email;?>
" type="text" id="projectinputPersEmail" class="form-control emailfield checkThis trimit" name="data[email]">
          </div>
        </div>
      </div>
    </section>
    <section class="card">
      <div class="card-block">
        <h4 class="form-section"><i class="icon-clipboard4"></i>Agency</h4>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputAgentName">Name</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="<?php echo $_smarty_tpl->tpl_vars['profile']->value->agentName;?>
" type="text" id="projectinputAgentName" class="form-control capThis checkThis trimit" name="data[agentName]">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputAgentPhone">Phone</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="<?php echo $_smarty_tpl->tpl_vars['profile']->value->agentPhone;?>
" type="tel" id="projectinputAgentPhone" class="form-control phonefield numOnly checkThis trimit" name="data[agentPhone]">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputAgentEmail">E-mail</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="<?php echo $_smarty_tpl->tpl_vars['profile']->value->agentEmail;?>
" type="text" id="projectinputAgentEmail" class="form-control emailfield checkThis trimit" name="data[agentEmail]">
          </div>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['profileType']->value==0) {?> </div>
    </section>
    <section class="card">
      <div class="card-block">
        <h4 class="form-section"><i class="icon-clipboard4"></i>Parent/Guardian</h4>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputParent">Name</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="<?php echo $_smarty_tpl->tpl_vars['profile']->value->parentName;?>
" type="text" id="projectinputParent" class="form-control checkThis capThis checkThis trimit" name="data[parentName]">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputParentPhone">Phone</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="<?php echo $_smarty_tpl->tpl_vars['profile']->value->parentPhone;?>
" type="tel" id="projectinputParentPhone" class="form-control phonefield numOnly checkThis trimit" name="data[parentPhone]">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-md-3 label-control" for="projectinputParentEmail">E-mail</label>
          <div class="col-md-6">
            <input autocomplete="dontsuggest" value="<?php echo $_smarty_tpl->tpl_vars['profile']->value->parentEmail;?>
" type="text" id="projectinputParentEmail" class="form-control emailfield checkThis trimit" name="data[parentEmail]">
          </div>
        </div>
        <?php }?> </div>
    </section>
    <section class="card">
      <div class="card-block">
        <h4 class="form-section"><i class="icon-clipboard4"></i>Photo</h4>
        <div class="form-group row">
          <label class="col-md-3 label-control">Profile photo</label>
          <div class="col-md-6"> <?php if ($_smarty_tpl->tpl_vars['profile']->value->photo) {?> <img src="<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['profile']->value->photo,'/home/aibdh80ehx97/public_html/sncasting.com','');?>
" height="150" style="margin-top: 10px;"> <?php } else { ?>
            <input type="file" accept="image/jpeg" id="projectinput832" class="form-control" name="data[photo]" style="height: 150px;">
            <?php }?> </div>
        </div>
        <div class="form-actions">
          <button type="submit" class="btn btn-primary mybtn-green" style="float: right;"> <i class="icon-check2"></i> Save</button>
        </div>
        <input type="hidden" name="action" value="save">
      </div>
    </section>
  </div>
</form>
<?php if ($_smarty_tpl->tpl_vars['profile']->value->id) {?>
<div class="card">
  <div class="card-header">
    <h4 class="card-title">Edit gallery</h4>
  </div>
  <div class="card-block"> <?php if ($_smarty_tpl->tpl_vars['photos']->value) {?><?php } else { ?>
    <div class="text-muted">No photos here yet...</div>
    <?php }?>
    <?php  $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['p']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['photos']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['p']->key => $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->_loop = true;
?>
    <div class="div-thumbnail"> <img src="<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['p']->value,'/home/aibdh80ehx97/public_html/sncasting.com','');?>
" alt="" class="img-fluid" />
      <button class="deleteThumb" onclick="deletePhoto('<?php echo $_smarty_tpl->tpl_vars['p']->value;?>
', this)"><i class="icon-close"></i></button>
    </div>
    <?php } ?> </div>
</div>
<div class="card">
  <div class="card-header">
    <h4 class="card-title">Upload more photos</h4>
  </div>
  <div class="card-block">
    <div class="bs-callout-warning callout-border-left mb-1 p-1">
      <p>Please make sure to use <strong>JPGs</strong> with <strong>sRGB</strong> color space.</p>
    </div>
    <form action="/profile/upload/?id=<?php echo $_smarty_tpl->tpl_vars['profile']->value->id;?>
" class="dropzone dpz-multiple-files" id="dpz-multiple-files">
    </form>
  </div>
</div>
<?php }?> 
<script>
function bdChange(){
	var bd = $("#bdYear").val() + "-" + $("#bdMonth").val() + "-" + $("#bdDay").val();
	$("#dbFinal").val(bd);
};
</script> 
<script src="/template/app-assets/vendors/js/forms/tags/tagging.min.js" type="text/javascript"></script> 
<script src="/template/app-assets/js/scripts/forms/tags/tagging.js" type="text/javascript"></script> 
<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
