<?php /* Smarty version Smarty-3.1.19, created on 2018-10-11 18:37:31
         compiled from "/home/aibdh80ehx97/public_html/sncasting.com/template/modals/removeMultipleFromCasting.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13981484785bbf986bd0e076-13670546%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '65003a458ebca40a85f01f7c456fd7b4409466a5' => 
    array (
      0 => '/home/aibdh80ehx97/public_html/sncasting.com/template/modals/removeMultipleFromCasting.tpl',
      1 => 1537190464,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13981484785bbf986bd0e076-13670546',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'group' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bbf986bd1e777_41078608',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bbf986bd1e777_41078608')) {function content_5bbf986bd1e777_41078608($_smarty_tpl) {?><div class="modal fade text-xs-left pr-0" id="removeMultiple" tabindex="-1" role="dialog" aria-labelledby="myModalLabel10" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-danger white">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel10">Remove profiles from casting</h4>
      </div>
      <div class="modal-body">
        <h5>Are you sure you want to remove these profiles from this casting?<br>
		  <br>
		  The profiles will remain in the database.</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="mybtn2 mybtn-grey mr-1" data-dismiss="modal">Cancel</button>
        <a onclick="removeMultiple('castings', <?php echo $_smarty_tpl->tpl_vars['group']->value->table->id;?>
)" class="mybtn2 mybtn-red">Remove from casting</a></div>
    </div>
  </div>
</div>
<?php }} ?>
