<?php /* Smarty version Smarty-3.1.19, created on 2019-02-28 23:35:28
         compiled from "/home/aibdh80ehx97/public_html/sncasting.com/template/dashboardCasting.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12288483235bbf986bc0b940-72168987%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9cc423e2bf383a236782c4abae19c894005b2938' => 
    array (
      0 => '/home/aibdh80ehx97/public_html/sncasting.com/template/dashboardCasting.tpl',
      1 => 1551392917,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12288483235bbf986bc0b940-72168987',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bbf986bccd4a9_09431041',
  'variables' => 
  array (
    'my' => 0,
    'group' => 0,
    'profiles' => 0,
    't' => 0,
    'size' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bbf986bccd4a9_09431041')) {function content_5bbf986bccd4a9_09431041($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/home/aibdh80ehx97/public_html/application/inc/smarty/plugins/modifier.replace.php';
?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<?php if ($_smarty_tpl->tpl_vars['my']->value->id>0) {?>
<?php echo $_smarty_tpl->getSubTemplate ("modals/removeFromCasting.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("modals/removeMultipleFromCasting.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("modals/newGroup.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("modals/editCasting.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php }?>
<section id="hover-effects">
  <div class="row">
    <div class="col-12 viewSwitcher mobile-only">
      <button type="button" id="scrollToBottom"><i class="icon-arrow-down4"></i></button>
    </div>
    <div class="col-12">
      <div class="breadcrumbs">
        <div class="breadcrumb-wrapper text-center">
          <ol class="breadcrumb d-inline-block p-0">
            <li class="breadcrumb-item"><a href="/castings">Castings</a></li>
            <li class="breadcrumb-item active"><?php echo $_smarty_tpl->tpl_vars['group']->value->table->name;?>
</li>
          </ol>
        </div>
      </div>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['my']->value->id>0) {?>
    <div class="col-12 text-center">
      <div class="btn-group">
        <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn mybtn-green dropdown-toggle">Actions</button>
        <div class="dropdown-menu arrow">
          <button class="dropdown-item" data-toggle="modal" data-target="#castingEdit" onclick="$('#editCastingForm').find('[name=name]').val($(this).data('name'));
              $('#editCastingForm').find('[name=location]').val($(this).data('location'));
              $('#editCastingForm').find('[name=date]').val($(this).data('date'));
              $('#editCastingForm').find('[name=id]').val($(this).data('id'));
              $('#editCastingForm').find('[name=url]').val($(this).data('url'));" data-name="<?php echo $_smarty_tpl->tpl_vars['group']->value->table->name;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['group']->value->table->id;?>
" data-location="<?php echo $_smarty_tpl->tpl_vars['group']->value->table->location;?>
" data-date="<?php echo $_smarty_tpl->tpl_vars['group']->value->table->date;?>
" data-url="<?php echo $_smarty_tpl->tpl_vars['group']->value->table->url;?>
"><i class="ft-edit-2 mr-1"></i>Edit casting</button>
          <div class="dropdown-divider mobile-only"></div>
          <button class="dropdown-item mobile-only" id="viewAsThumbs"><i class="icon-grid2 mr-1"></i>View as grid</button>
          <button class="dropdown-item mobile-only" id="viewAsList"><i class="icon-align-justify2 mr-1"></i>View as List</button>
          <div class="dropdown-divider"></div>
          <button onclick="selectAll(); return false;" class="dropdown-item"><i class="ft-plus-circle mr-1"></i>Select all</button>
          <button onclick="selectFavs(); return false;" class="dropdown-item"><i class="ft-heart mr-1"></i>Select favorites</button>
          <!--<button class="dropdown-item btn-reveal-comments"><i class="ft-message-circle mr-1"></i>Reveal comments</button>--> 
          <a href="/casting/photos?id=<?php echo $_smarty_tpl->tpl_vars['group']->value->table->id;?>
" class="dropdown-item"><i class="ft-upload-cloud mr-1"></i>Upload photos</a> </div>
      </div>
    </div>
    <?php }?>
    <div class="col-12 text-center <?php if ($_smarty_tpl->tpl_vars['my']->value->id>0) {?>mt-2<?php }?>">
      <div class="d-block"><?php if ($_smarty_tpl->tpl_vars['group']->value->table->location) {?><?php echo $_smarty_tpl->tpl_vars['group']->value->table->location;?>
, <?php }?><?php echo $_smarty_tpl->tpl_vars['group']->value->table->date;?>
</div>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['my']->value->id>0) {?>
    <div class="castingURL"><i class="icon-clipboard4 icon"></i><br>
      <a href="/c/<?php echo $_smarty_tpl->tpl_vars['group']->value->table->url;?>
" class="castingsheeturl" target="_blank" title="Casting sheet URL">www.sncasting.com/c/<?php echo $_smarty_tpl->tpl_vars['group']->value->table->url;?>
</a> </div>
    <?php }?> </div>
  <div class="row">
    <div class="grid-hover"> <?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['profiles']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?>
      <?php $_smarty_tpl->tpl_vars["size"] = new Smarty_variable(getimagesize($_smarty_tpl->tpl_vars['t']->value->thumbnail), null, 0);?>
      <div class="col-md-4 col-xs-12 mb-2">
        <div class="thumbnail-container<?php if ($_smarty_tpl->tpl_vars['t']->value->fav) {?> likedProfile<?php }?>" data-id="<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
">
          <div class="thumb-photo-place"><img class="thumbnail-photo" src="<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['t']->value->thumbnail,'/home/aibdh80ehx97/public_html/sncasting.com','');?>
" alt="Thumbnail" <?php if ($_smarty_tpl->tpl_vars['size']->value[0]>$_smarty_tpl->tpl_vars['size']->value[1]) {?>style="height: 380px; width: auto;"<?php }?>onerror="this.src='/template/app-assets/images/noimage.png'"/>
            <div class="thumb-overlay"></div>
            <div class="thumb-content"> <?php if ($_smarty_tpl->tpl_vars['my']->value->id>0) {?>
              <div class="editPanel desktop-only">
                <button onclick="location='/profile/edit?id=<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
'" class="editButton" title="Edit profile"><i class="ft-edit-2"></i></button>
                <button class="deleteProfileButton" data-toggle="modal" data-target="#removeFromCasting" onclick="profileToDelete=<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
" title="Remove from casting"><i class="ft-user-minus"></i></button>
              </div>
              <?php }?> <a class="thumb-info" href="/profile/view?id=<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
" style="<?php if ($_smarty_tpl->tpl_vars['my']->value->id==0) {?>top: 0;<?php }?>">
              <p> <?php if ($_smarty_tpl->tpl_vars['t']->value->basedIn) {?><i class="ft-map-pin"></i><b><?php echo $_smarty_tpl->tpl_vars['t']->value->basedIn;?>
</b><br>
                <br>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['t']->value->occupation) {?><i class="ft-award"></i><b><?php echo $_smarty_tpl->tpl_vars['t']->value->occupation;?>
</b><br>
                <br>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['t']->value->height) {?><i class="ft-arrow-up"></i><b><?php echo $_smarty_tpl->tpl_vars['t']->value->height;?>
</b><?php }?> </p>
              </a> <?php if ($_smarty_tpl->tpl_vars['my']->value->id>0) {?>
              <button class="selectButton desktop-only" onclick="selectProfile(this)">Select</button>
              <?php }?> </div>
            <!--<div class="thumb-comments">
              <p class="text-center">Type comments here:</p>
              <textarea class="comment-input" spellcheck="false"></textarea>
              <button class="btn-clear-comments">CLEAR COMMENTS</button>
              <button class="btn-clear-comments">SAVE COMMENTS</button>
            </div>--> 
          </div>
          <a class="name-place" href="/profile/view?id=<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['t']->value->firstName;?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['t']->value->lastName;?>
</a>
          <div class="thumbnail-toolbar"> <?php if ($_smarty_tpl->tpl_vars['my']->value->id>0) {?> <span class="pnum">&#35;<?php echo $_smarty_tpl->tpl_vars['t']->value->num;?>
</span> <?php }?>
            <div class="thumb-tools"> 
              <!--<button class="btn-comments"><i class="fa fa-comment" title="Profile comments"></i></button>-->
              <button class="btn-like" onclick="loveProfile(this, 'castings', <?php echo $_smarty_tpl->tpl_vars['group']->value->table->id;?>
, <?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
)" title="Mark as favorite"><i class="fa fa-heart"></i></button>
              <?php if ($_smarty_tpl->tpl_vars['my']->value->id>0) {?> 
              <!--<button class="btn-hide"><i class="ft-eye"></i></button>--> 
              <?php }?> </div>
            <?php if ($_smarty_tpl->tpl_vars['my']->value->id>0) {?>
            <div class="thumb-mob-actions">
              <div class="mob-actions-inner">
                <button class="btn-mob-edit" onclick="location='/profile/edit?id=<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
'"><i class="ft-edit-2" title="Edit profile"></i></button>
                <button class="btn-mob-remove" data-toggle="modal" data-target="#removeFromCasting" onclick="profileToDelete=<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
" title="Remove from casting"><i class="ft-x"></i></button>
                <button class="btn-mob-select" onclick="mobSelectProfile(this)"><i class="ft-plus-circle mob-select-icon" title="Select profile"></i></button>
              </div>
            </div>
            <button class="btn-toggle-mob-actions mobile-only"><i class="ft-more-horizontal"></i></button>
            <?php }?> </div>
        </div>
      </div>
      <?php } ?> </div>
  </div>
  <div class="row">
    <div class="col-12 viewSwitcher mobile-only">
      <button type="button" id="scrollToTop" class="mt-2 mb-2"><i class="icon-arrow-up4"></i></button>
    </div>
  </div>
</section>
<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
