<?php /* Smarty version Smarty-3.1.19, created on 2019-02-28 23:33:22
         compiled from "/home/aibdh80ehx97/public_html/sncasting.com/template/dashboardGroup.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11771721125bbfdb60d8e1f2-29871432%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fed7b73ada0c1816c3cc10fb84baeac5faa4422a' => 
    array (
      0 => '/home/aibdh80ehx97/public_html/sncasting.com/template/dashboardGroup.tpl',
      1 => 1551396794,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11771721125bbfdb60d8e1f2-29871432',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bbfdb60ebf435_84952992',
  'variables' => 
  array (
    'my' => 0,
    'group' => 0,
    'count' => 0,
    'page' => 0,
    'foo' => 0,
    'profiles' => 0,
    't' => 0,
    'size' => 0,
    'groupID' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bbfdb60ebf435_84952992')) {function content_5bbfdb60ebf435_84952992($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/home/aibdh80ehx97/public_html/application/inc/smarty/plugins/modifier.replace.php';
?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<?php if ($_smarty_tpl->tpl_vars['my']->value->id>0) {?>
<?php echo $_smarty_tpl->getSubTemplate ("modals/removeFromGroup.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("modals/removeMultipleFromGroup.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("modals/editGroup.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php }?>
<section id="hover-effects">
  <div class="row">
    <div class="col-12 viewSwitcher mobile-only pb-1">
      <button type="button" id="scrollToBottom"><i class="icon-arrow-down4"></i></button>
    </div>
    <div class="col-12">
      <div class="breadcrumbs">
        <div class="breadcrumb-wrapper text-center">
          <ol class="breadcrumb d-inline-block p-0">
            <li class="breadcrumb-item"><a href="/groups">Groups</a></li>
            <li class="breadcrumb-item active"><?php echo $_smarty_tpl->tpl_vars['group']->value->table->name;?>
</li>
          </ol>
        </div>
      </div>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['my']->value->id>0) {?>
    <div class="col-12 text-center">
      <div class="btn-group">
        <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn mybtn-green dropdown-toggle">Actions</button>
        <div class="dropdown-menu arrow">
          <button class="dropdown-item" data-toggle="modal" data-target="#editGroup" onclick="profileToEdit=$(this).data('id'); $('#editProfileGroupName').val($(this).data('name'));" data-id="<?php echo $_smarty_tpl->tpl_vars['group']->value->table->id;?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['group']->value->table->name;?>
"><i class="ft-edit-2 mr-1"></i>Edit group</button>
          <div class="dropdown-divider mobile-only"></div>
          <button class="dropdown-item mobile-only active" id="viewAsThumbs"><i class="icon-grid2 mr-1"></i>View as grid</button>
          <button class="dropdown-item mobile-only" id="viewAsList"><i class="icon-align-justify2 mr-1"></i>View as List</button>
          <div class="dropdown-divider"></div>
          <button onclick="selectAll(); return false;" class="dropdown-item"><i class="ft-plus-circle mr-1"></i>Select all</button>
          <button onclick="selectFavs(); return false;" class="dropdown-item"><i class="ft-heart mr-1"></i>Select favorites</button>
          <!--<button class="dropdown-item btn-reveal-comments"><i class="ft-message-circle mr-1"></i>Reveal comments</button>--> 
        </div>
      </div>
    </div>
    <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['count']->value>60) {?>
    <div class="col-12">
      <div class="px-1">
        <ul class="pagination pagination-flat d-flex justify-content-center">
          <?php if ($_smarty_tpl->tpl_vars['page']->value>1) {?>
          <li class="page-item<?php if ($_smarty_tpl->tpl_vars['page']->value==1) {?> active<?php }?>"> <a class="page-link" href="?page=<?php echo $_smarty_tpl->tpl_vars['page']->value-1;?>
" aria-label="Previous"> <span aria-hidden="true">«</span> <span class="sr-only">Previous</span> </a> </li>
          <?php }?>
          
          <?php $_smarty_tpl->tpl_vars['foo'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['foo']->step = 1;$_smarty_tpl->tpl_vars['foo']->total = (int) ceil(($_smarty_tpl->tpl_vars['foo']->step > 0 ? $_smarty_tpl->tpl_vars['count']->value/60+1 - (1) : 1-($_smarty_tpl->tpl_vars['count']->value/60)+1)/abs($_smarty_tpl->tpl_vars['foo']->step));
if ($_smarty_tpl->tpl_vars['foo']->total > 0) {
for ($_smarty_tpl->tpl_vars['foo']->value = 1, $_smarty_tpl->tpl_vars['foo']->iteration = 1;$_smarty_tpl->tpl_vars['foo']->iteration <= $_smarty_tpl->tpl_vars['foo']->total;$_smarty_tpl->tpl_vars['foo']->value += $_smarty_tpl->tpl_vars['foo']->step, $_smarty_tpl->tpl_vars['foo']->iteration++) {
$_smarty_tpl->tpl_vars['foo']->first = $_smarty_tpl->tpl_vars['foo']->iteration == 1;$_smarty_tpl->tpl_vars['foo']->last = $_smarty_tpl->tpl_vars['foo']->iteration == $_smarty_tpl->tpl_vars['foo']->total;?>
          <li class="page-item<?php if ($_smarty_tpl->tpl_vars['foo']->value==$_smarty_tpl->tpl_vars['page']->value) {?> active<?php }?>"><a class="page-link" href="?page=<?php echo $_smarty_tpl->tpl_vars['foo']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['foo']->value;?>
</a></li>
          <?php }} ?>
          <?php if ($_smarty_tpl->tpl_vars['page']->value==$_smarty_tpl->tpl_vars['foo']->value) {?>
          <li class="page-item"> <a class="page-link" href="?page=<?php echo $_smarty_tpl->tpl_vars['page']->value+1;?>
" aria-label="Next"> <span aria-hidden="true">»</span> <span class="sr-only">Next</span> </a> </li>
          <?php }?>
        </ul>
      </div>
      <hr class="mb-2">
    </div>
      <?php }?>
  </div>
  <div class="row">
    <div class="grid-hover"> <?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['profiles']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?>
      <?php $_smarty_tpl->tpl_vars["size"] = new Smarty_variable(getimagesize($_smarty_tpl->tpl_vars['t']->value->photo), null, 0);?>
      <div class="col-md-4 col-xs-12 mb-2">
        <div class="thumbnail-container<?php if ($_smarty_tpl->tpl_vars['t']->value->fav) {?> likedProfile<?php }?>" data-id="<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
">
          <div class="thumb-photo-place"><img class="thumbnail-photo" src="<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['t']->value->thumbnail,'/home/aibdh80ehx97/public_html/sncasting.com','');?>
" alt="Thumbnail" <?php if ($_smarty_tpl->tpl_vars['size']->value[0]>$_smarty_tpl->tpl_vars['size']->value[1]) {?>style="height: 380px; width: auto;"<?php }?>onerror="this.src='/template/app-assets/images/noimage.png'"/>
            <div class="thumb-overlay"></div>
            <div class="thumb-content"> <?php if ($_smarty_tpl->tpl_vars['my']->value->id>0) {?>
              <div class="editPanel desktop-only">
                <button onclick="location='/profile/edit?id=<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
'" class="editButton" title="Edit profile"><i class="ft-edit-2"></i></button>
                <button class="deleteProfileButton" data-toggle="modal" data-target="#removeFromGroup" onclick="profileToDelete=<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
" title="Remove from group"><i class="ft-user-minus"></i></button>
              </div>
              <?php }?> <a class="thumb-info" href="/profile/view?id=<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
" style="<?php if ($_smarty_tpl->tpl_vars['my']->value->id==0) {?>top: 0;<?php }?>">
              <p> <?php if ($_smarty_tpl->tpl_vars['t']->value->basedIn) {?><i class="ft-map-pin"></i><b><?php echo $_smarty_tpl->tpl_vars['t']->value->basedIn;?>
</b><br>
                <br>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['t']->value->occupation) {?><i class="ft-award"></i><b><?php echo $_smarty_tpl->tpl_vars['t']->value->occupation;?>
</b><br>
                <br>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['t']->value->height) {?><i class="ft-arrow-up"></i><b><?php echo $_smarty_tpl->tpl_vars['t']->value->height;?>
</b><?php }?></p>
              </a> <?php if ($_smarty_tpl->tpl_vars['my']->value->id>0) {?>
              <button class="selectButton desktop-only" onclick="selectProfile(this)" title="Select profile">Select</button>
              <?php }?> </div>
            <!--<div class="thumb-comments">
              <p class="text-center">Type comments here:</p>
              <textarea class="comment-input" spellcheck="false"></textarea>
              <button class="btn-clear-comments">CLEAR COMMENTS</button>
              <button class="btn-clear-comments">SAVE COMMENTS</button>
            </div>-->
          </div>
          <a class="name-place" href="/profile/view?id=<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['t']->value->firstName;?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['t']->value->lastName;?>
</a>
          <div class="thumbnail-toolbar">
            <div class="thumb-tools">
              <!--<button class="btn-comments" title="Profile comments"><i class="fa fa-comment"></i></button>-->
              <button class="btn-like" onclick="loveProfile(this, 'groups', <?php echo $_smarty_tpl->tpl_vars['groupID']->value;?>
, <?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
)" title="Mark as favorite"><i class="fa fa-heart"></i></button>
              <?php if ($_smarty_tpl->tpl_vars['my']->value->id>0) {?>
              <!--<button class="btn-hide"><i class="ft-eye"></i></button>-->
              <?php }?> </div>
            <?php if ($_smarty_tpl->tpl_vars['my']->value->id>0) {?>
            <div class="thumb-mob-actions">
              <div class="mob-actions-inner">
                <button class="btn-mob-edit" onclick="location='/profile/edit?id=<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
'" title="Edit profile"><i class="ft-edit-2"></i></button>
                <button class="btn-mob-remove" data-toggle="modal" data-target="#removeFromGroup" onclick="profileToDelete=<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
" title="Remove from group"><i class="ft-x"></i></button>
                <button class="btn-mob-select" onclick="mobSelectProfile(this)"><i class="ft-plus-circle mob-select-icon" title="Select profile"></i></button>
              </div>
            </div>
            <button class="btn-toggle-mob-actions mobile-only"><i class="ft-more-horizontal"></i></button>
            <?php }?> </div>
        </div>
      </div>
      <?php } ?> </div>
  </div>
  <div class="row"> <?php if ($_smarty_tpl->tpl_vars['count']->value>60) {?>
    <center style="width: 100%; display: inline-block;">
      <ul class="pagination pagination-flat d-flex justify-content-center">
        <?php if ($_smarty_tpl->tpl_vars['page']->value>1) {?>
        <li class="page-item<?php if ($_smarty_tpl->tpl_vars['page']->value==1) {?> active<?php }?>"> <a class="page-link" href="?page=<?php echo $_smarty_tpl->tpl_vars['page']->value-1;?>
" aria-label="Previous"> <span aria-hidden="true">«</span> <span class="sr-only">Previous</span> </a> </li>
        <?php }?>
        
        <?php $_smarty_tpl->tpl_vars['foo'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['foo']->step = 1;$_smarty_tpl->tpl_vars['foo']->total = (int) ceil(($_smarty_tpl->tpl_vars['foo']->step > 0 ? $_smarty_tpl->tpl_vars['count']->value/60+1 - (1) : 1-($_smarty_tpl->tpl_vars['count']->value/60)+1)/abs($_smarty_tpl->tpl_vars['foo']->step));
if ($_smarty_tpl->tpl_vars['foo']->total > 0) {
for ($_smarty_tpl->tpl_vars['foo']->value = 1, $_smarty_tpl->tpl_vars['foo']->iteration = 1;$_smarty_tpl->tpl_vars['foo']->iteration <= $_smarty_tpl->tpl_vars['foo']->total;$_smarty_tpl->tpl_vars['foo']->value += $_smarty_tpl->tpl_vars['foo']->step, $_smarty_tpl->tpl_vars['foo']->iteration++) {
$_smarty_tpl->tpl_vars['foo']->first = $_smarty_tpl->tpl_vars['foo']->iteration == 1;$_smarty_tpl->tpl_vars['foo']->last = $_smarty_tpl->tpl_vars['foo']->iteration == $_smarty_tpl->tpl_vars['foo']->total;?>
        <li class="page-item<?php if ($_smarty_tpl->tpl_vars['foo']->value==$_smarty_tpl->tpl_vars['page']->value) {?> active<?php }?>"><a class="page-link" href="?page=<?php echo $_smarty_tpl->tpl_vars['foo']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['foo']->value;?>
</a></li>
        <?php }} ?>
        <?php if ($_smarty_tpl->tpl_vars['page']->value==$_smarty_tpl->tpl_vars['foo']->value) {?>
        <li class="page-item"> <a class="page-link" href="?page=<?php echo $_smarty_tpl->tpl_vars['page']->value+1;?>
" aria-label="Next"> <span aria-hidden="true">»</span> <span class="sr-only">Next</span> </a> </li>
        <?php }?>
      </ul>
    </center>
    <?php }?> </div>
  <div class="row">
    <div class="col-12 viewSwitcher mobile-only">
      <button type="button" id="scrollToTop" class="mt-2 mb-2"><i class="icon-arrow-up4"></i></button>
    </div>
  </div>
</section>
<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
