<?php /* Smarty version Smarty-3.1.19, created on 2019-02-28 22:09:29
         compiled from "/home/aibdh80ehx97/public_html/sncasting.com/template/dashboard.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5654773895bbf97b536e173-29179641%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0f5fd3cbc86031bc02273143e006b43f3e3f0fda' => 
    array (
      0 => '/home/aibdh80ehx97/public_html/sncasting.com/template/dashboard.tpl',
      1 => 1551391760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5654773895bbf97b536e173-29179641',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bbf97b5451877_29290256',
  'variables' => 
  array (
    'count' => 0,
    'page' => 0,
    'foo' => 0,
    'profiles' => 0,
    't' => 0,
    'size' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bbf97b5451877_29290256')) {function content_5bbf97b5451877_29290256($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/home/aibdh80ehx97/public_html/application/inc/smarty/plugins/modifier.replace.php';
?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("modals/deleteProfile.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("modals/deleteMultipleProfiles.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("modals/newGroup.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<section id="hover-effects">
  <div class="row">
    <div class="col-12 viewSwitcher mobile-only pb-1">
      <button type="button" id="scrollToBottom"><i class="icon-arrow-down4"></i></button>
    </div>
    <div class="col-12">
      <h1 class="mt-3 mb-2" style="text-align: center">Browse profiles &#40;<?php echo $_smarty_tpl->tpl_vars['count']->value;?>
&#41;</h1>
    </div>
    <div class="col-12">
      <div class="px-1">
        <ul class="pagination pagination-flat d-flex justify-content-center">
          <?php if ($_smarty_tpl->tpl_vars['page']->value>1) {?>
          <li class="page-item<?php if ($_smarty_tpl->tpl_vars['page']->value==1) {?> active<?php }?>"> <a class="page-link" href="?page=<?php echo $_smarty_tpl->tpl_vars['page']->value-1;?>
" aria-label="Previous"> <span aria-hidden="true">«</span> <span class="sr-only">Previous</span> </a> </li>
          <?php }?>
          
          <?php $_smarty_tpl->tpl_vars['foo'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['foo']->step = 1;$_smarty_tpl->tpl_vars['foo']->total = (int) ceil(($_smarty_tpl->tpl_vars['foo']->step > 0 ? $_smarty_tpl->tpl_vars['count']->value/60+1 - (1) : 1-($_smarty_tpl->tpl_vars['count']->value/60)+1)/abs($_smarty_tpl->tpl_vars['foo']->step));
if ($_smarty_tpl->tpl_vars['foo']->total > 0) {
for ($_smarty_tpl->tpl_vars['foo']->value = 1, $_smarty_tpl->tpl_vars['foo']->iteration = 1;$_smarty_tpl->tpl_vars['foo']->iteration <= $_smarty_tpl->tpl_vars['foo']->total;$_smarty_tpl->tpl_vars['foo']->value += $_smarty_tpl->tpl_vars['foo']->step, $_smarty_tpl->tpl_vars['foo']->iteration++) {
$_smarty_tpl->tpl_vars['foo']->first = $_smarty_tpl->tpl_vars['foo']->iteration == 1;$_smarty_tpl->tpl_vars['foo']->last = $_smarty_tpl->tpl_vars['foo']->iteration == $_smarty_tpl->tpl_vars['foo']->total;?>
          <li class="page-item<?php if ($_smarty_tpl->tpl_vars['foo']->value==$_smarty_tpl->tpl_vars['page']->value) {?> active<?php }?>"><a class="page-link" href="?page=<?php echo $_smarty_tpl->tpl_vars['foo']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['foo']->value;?>
</a></li>
          <?php }} ?>
          <?php if ($_smarty_tpl->tpl_vars['page']->value==$_smarty_tpl->tpl_vars['foo']->value) {?>
          <li class="page-item"> <a class="page-link" href="?page=<?php echo $_smarty_tpl->tpl_vars['page']->value+1;?>
" aria-label="Next"> <span aria-hidden="true">»</span> <span class="sr-only">Next</span> </a> </li>
          <?php }?>
        </ul>
      </div>
      <hr class="mb-2">
    </div>
  </div>
  <div class="row"> <?php  $_smarty_tpl->tpl_vars['t'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['t']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['profiles']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['t']->key => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['t']->key;
?>
    <?php $_smarty_tpl->tpl_vars["size"] = new Smarty_variable(getimagesize($_smarty_tpl->tpl_vars['t']->value->thumbnail), null, 0);?>
    <div class="col-md-4 mb-2">
      <div class="thumbnail-container" data-id="<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
">
        <div class="thumb-photo-place"><img class="thumbnail-photo" src="<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['t']->value->thumbnail,'/home/aibdh80ehx97/public_html/sncasting.com','');?>
" alt="Thumbnail" <?php if ($_smarty_tpl->tpl_vars['size']->value[0]>$_smarty_tpl->tpl_vars['size']->value[1]) {?>style="height: 380px; width: auto;"<?php }?>onerror="this.src='/template/app-assets/images/noimage.png'"/>
          <div class="thumb-overlay"></div>
          <div class="thumb-content">
            <div class="editPanel desktop-only">
              <button onclick="location='/profile/edit?id=<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
'" class="editButton" title="Edit profile"><i class="ft-edit-2"></i></button>
              <button class="deleteProfileButton" data-toggle="modal" data-target="#deleteProfile" onclick="profileToDelete=<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
" title="Delete profile"><i class="icon-trash4"></i></button>
            </div>
            <a class="thumb-info" href="/profile/view?id=<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
">
            <p> <?php if ($_smarty_tpl->tpl_vars['t']->value->basedIn) {?><i class="ft-map-pin"></i><b><?php echo $_smarty_tpl->tpl_vars['t']->value->basedIn;?>
</b><br>
              <br>
              <?php }?>
              <?php if ($_smarty_tpl->tpl_vars['t']->value->occupation) {?><i class="ft-award"></i><b><?php echo $_smarty_tpl->tpl_vars['t']->value->occupation;?>
</b><br>
              <br>
              <?php }?>
              <?php if ($_smarty_tpl->tpl_vars['t']->value->height) {?><i class="ft-arrow-up"></i><b><?php echo $_smarty_tpl->tpl_vars['t']->value->height;?>
</b><?php }?> </p>
            </a>
            <button class="selectButton desktop-only" onclick="selectProfile(this)"  title="Select profile">Select</button>
          </div>
        </div>
        <a class="name-place" href="/profile/view?id=<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['t']->value->firstName;?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['t']->value->lastName;?>
</a>
        <div class="thumbnail-toolbar mobile-only">
          <div class="thumb-tools"></div>
          <div class="thumb-mob-actions">
            <div class="mob-actions-inner">
              <button class="btn-mob-edit" onclick="location='/profile/edit?id=<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
'"><i class="ft-edit-2" title="Edit profile"></i></button>
              <button class="btn-mob-delete" data-toggle="modal" data-target="#deleteProfile" onclick="profileToDelete=<?php echo $_smarty_tpl->tpl_vars['t']->value->id;?>
" title="Delete profile"><i class="icon-trash4"></i></button>
              <button class="btn-mob-select" onclick="mobSelectProfile(this)"><i class="ft-plus-circle mob-select-icon" title="Select profile"></i></button>
            </div>
          </div>
          <button class="btn-toggle-mob-actions"><i class="ft-more-horizontal"></i></button>
        </div>
      </div>
    </div>
    <?php } ?> </div>
  <div class="row mb-2">
    <div class="col-12">
      <hr>
      <div class="px-1">
        <ul class="pagination pagination-flat d-flex justify-content-center">
          <?php if ($_smarty_tpl->tpl_vars['page']->value>1) {?>
          <li class="page-item<?php if ($_smarty_tpl->tpl_vars['page']->value==1) {?> active<?php }?>"> <a class="page-link" href="?page=<?php echo $_smarty_tpl->tpl_vars['page']->value-1;?>
" aria-label="Previous"> <span aria-hidden="true">«</span> <span class="sr-only">Previous</span> </a> </li>
          <?php }?>
          
          <?php $_smarty_tpl->tpl_vars['foo'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['foo']->step = 1;$_smarty_tpl->tpl_vars['foo']->total = (int) ceil(($_smarty_tpl->tpl_vars['foo']->step > 0 ? $_smarty_tpl->tpl_vars['count']->value/60+1 - (1) : 1-($_smarty_tpl->tpl_vars['count']->value/60)+1)/abs($_smarty_tpl->tpl_vars['foo']->step));
if ($_smarty_tpl->tpl_vars['foo']->total > 0) {
for ($_smarty_tpl->tpl_vars['foo']->value = 1, $_smarty_tpl->tpl_vars['foo']->iteration = 1;$_smarty_tpl->tpl_vars['foo']->iteration <= $_smarty_tpl->tpl_vars['foo']->total;$_smarty_tpl->tpl_vars['foo']->value += $_smarty_tpl->tpl_vars['foo']->step, $_smarty_tpl->tpl_vars['foo']->iteration++) {
$_smarty_tpl->tpl_vars['foo']->first = $_smarty_tpl->tpl_vars['foo']->iteration == 1;$_smarty_tpl->tpl_vars['foo']->last = $_smarty_tpl->tpl_vars['foo']->iteration == $_smarty_tpl->tpl_vars['foo']->total;?>
          <li class="page-item<?php if ($_smarty_tpl->tpl_vars['foo']->value==$_smarty_tpl->tpl_vars['page']->value) {?> active<?php }?>"><a class="page-link" href="?page=<?php echo $_smarty_tpl->tpl_vars['foo']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['foo']->value;?>
</a></li>
          <?php }} ?>
          <?php if ($_smarty_tpl->tpl_vars['page']->value==$_smarty_tpl->tpl_vars['foo']->value) {?>
          <li class="page-item"> <a class="page-link" href="?page=<?php echo $_smarty_tpl->tpl_vars['page']->value+1;?>
" aria-label="Next"> <span aria-hidden="true">»</span> <span class="sr-only">Next</span> </a> </li>
          <?php }?>
        </ul>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-12 viewSwitcher mobile-only">
      <button type="button" id="scrollToTop" class="mt-2 mb-2"><i class="icon-arrow-up4"></i></button>
    </div>
  </div>
</section>
<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
