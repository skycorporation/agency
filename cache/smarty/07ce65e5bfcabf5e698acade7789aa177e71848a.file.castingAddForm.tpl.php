<?php /* Smarty version Smarty-3.1.19, created on 2019-01-24 01:16:49
         compiled from "/home/aibdh80ehx97/public_html/sncasting.com/template/castingAddForm.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1394044085bc4be3f335740-55478992%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '07ce65e5bfcabf5e698acade7789aa177e71848a' => 
    array (
      0 => '/home/aibdh80ehx97/public_html/sncasting.com/template/castingAddForm.tpl',
      1 => 1548292602,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1394044085bc4be3f335740-55478992',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bc4be3f3b4962_44956823',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bc4be3f3b4962_44956823')) {function content_5bc4be3f3b4962_44956823($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<h1 class="mt-3 mb-2" style="text-align: center">Create new casting</h1>
    <section class="card">
      <div class="card-block">
        <form class="form form-horizontal" method="get" action="/castings/add/" enctype="multipart/form-data">
          <div class="form-body">
			  <h4 class="form-section"><i class="icon-head"></i>Casting info</h4>
			  <div class="form-group row">
          <label class="col-xs-12 col-sm-12 col-md-3 col-lg-4 label-control" for="projectinput1">Casting name &#42;</label>
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
          <input autocomplete="dontsuggest" type="text" name="name" class="form-control checkThis capThis trimit" onkeyup="var newUrl = $(this).val(); newUrl = newUrl.toLowerCase(); newUrl = newUrl.replace(/ /g,''); newUrl = encodeURI(newUrl); $('#newUrl1').val(newUrl); $('#urlPreview').html('www.sncasting.com/c/' + newUrl);" required autofocus>
				  </div></div>
			  <div class="form-group row">
          <label class="col-xs-12 col-sm-12 col-md-3 col-lg-4 label-control" for="projectinput1">Casting location</label>
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
          <input autocomplete="dontsuggest" type="text" name="location" class="form-control checkThis capThis trimit">
				  </div></div>
			  <div class="form-group row">
          <label class="col-xs-12 col-sm-12 col-md-3 col-lg-4 label-control" for="projectinput1">Casting date &#42;</label>
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
          <div style="position: relative; padding-bottom: 3px;">
              <input type='text' placeholder="select date..." class="form-control pickadate-dropdown-all" name="date" required>
          </div>
				  </div></div>
			  <div class="form-group row">
          <label class="col-xs-12 col-sm-12 col-md-3 col-lg-4 label-control" for="projectinput1">Casting sheet URL</label>
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
			 <div class="input-group">
                  <div class="input-group-prepend"><span class="input-group-text">www.sncasting.com/c/</span></div>
			  <input autocomplete="dontsuggest" type="text" id="newUrl1" placeholder="url" class="form-control" name="url">
			  </div>
				  </div></div>
         
          <h4 class="form-section"><i class="icon-head"></i>Optional fields</h4>
           
			  <div class="container-fluid d-flex justify-content-center">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
          <div class="row">
          
              <ul class="list-group list-group-flush">
                <li class="list-group-item" style="border-top: none">
					<div class="float-right">
                  <input type="checkbox" id="basedIn" name="basedIn" class="switchery basedInVal" />
					</div>
                  <label for="basedIn" class="font-small-4 mb-0">Based in</label>
                </li>
                <li class="list-group-item">
					<div class="float-right">
                  <input type="checkbox" name="occupation" id="occupation" class="switchery" />
					</div>
                  <label for="occupation" class="font-small-4 mb-0">Occupation</label>
                </li>
                <li class="list-group-item">
					<div class="float-right">
                  <input type="checkbox" id="agency" name="agency" class="switchery" />
					</div>
                  <label for="switchery3" class="font-small-4 mb-0">Agency</label>
                </li>
				  <li class="list-group-item">
					<div class="float-right">
                  <input type="checkbox" name="instagram" id="instagram" class="switchery" />
					</div>
                  <label for="instagram" class="font-small-4 mb-0">Instagram</label>
                </li>
                <li class="list-group-item">
					<div class="float-right">
                  <input type="checkbox" id="book" name="book" class="switchery" />
					</div>
                  <label for="book" class="font-small-4 mb-0">Book</label>
                </li>
                <li class="list-group-item">
					<div class="float-right">
                  <input type="checkbox" id="website" name="website" class="switchery" />
					</div>
                  <label for="website" class="font-small-4 mb-0">Website</label>
                </li>
				  <!--<li class="list-group-item">
					<div class="float-right">
                  <input type="checkbox" id="rate" name="rate" class="switchery" />
					</div>
                  <label for="rate" class="font-small-4 mb-0">Rate</label>
                </li>-->
                <li class="list-group-item">
					<div class="float-right">
                  <input type="checkbox" id="knownFor" name="knownFor" class="switchery" />
					</div>
                  <label for="knownFor" class="font-small-4 mb-0">Known for</label>
                </li>
                <li class="list-group-item">
					<div class="float-right">
                  <input type="checkbox" id="press" name="press" class="switchery" />
					</div>
                  <label for="press" class="font-small-4 mb-0">Press links</label>
                </li>
                <li class="list-group-item">
					<div class="float-right">
                  <input type="checkbox" id="music" name="music" class="switchery" />
					</div>
                  <label for="music" class="font-small-4 mb-0">Music links</label>
                </li>
                <li class="list-group-item">
					<div class="float-right">
                  <input type="checkbox" id="video" name="video" class="switchery" />
					</div>
                  <label for="video" class="font-small-4 mb-0">Video links</label>
                </li>
				  <li class="list-group-item" style="border-bottom: none">
					<div class="float-right">
                  <input type="checkbox" id="upcomingProjects" name="upcomingProjects" class="switchery" />
					</div>
                  <label for="upcomingProjects" class="font-small-4 mb-0">Upcoming projects</label>
                </li>
              </ul>
          
			</div></div></div>
          <div class="form-actions right">
            <button type="submit" class="mybtn2 mybtn-green"><i class="ft-plus"></i> Create casting</button>
          </div>
        </form>
      </div>
    </section>

<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
 <?php }} ?>
