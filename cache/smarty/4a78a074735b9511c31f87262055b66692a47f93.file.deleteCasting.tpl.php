<?php /* Smarty version Smarty-3.1.19, created on 2018-10-11 18:37:29
         compiled from "/home/aibdh80ehx97/public_html/sncasting.com/template/modals/deleteCasting.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7506814425bbf9869488745-98126997%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4a78a074735b9511c31f87262055b66692a47f93' => 
    array (
      0 => '/home/aibdh80ehx97/public_html/sncasting.com/template/modals/deleteCasting.tpl',
      1 => 1537190464,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7506814425bbf9869488745-98126997',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bbf9869489804_88823075',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bbf9869489804_88823075')) {function content_5bbf9869489804_88823075($_smarty_tpl) {?><div class="modal fade text-xs-left pr-0" id="deleteCasting" tabindex="-1" role="dialog" aria-labelledby="myModalLabel10" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-danger white">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel10">Delete casting</h4>
      </div>
      <div class="modal-body">
        <h5>Are you sure you want to delete this casting?<br>
          <br>
          All model profiles from this casting will remain in the database.</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="mybtn2 mybtn-grey mr-1" data-dismiss="modal">Cancel</button>
        <a onclick="window.location='/castings/delete?id=' + profileToDelete;" class="mybtn2 mybtn-red">Delete casting</a></div>
    </div>
  </div>
</div>
<?php }} ?>
