<?php
//exit( dirname(__FILE__));
//define("PATH_START", dirname(__FILE__)."/www/");
ob_start ("ob_gzhandler");

include("init.php");


$server = new Request($_SERVER);
$request_uri = $server->REQUEST_URI;
include('router.php');

###########################################
$Model = 'Index';
$Action = 'Index';
$result = array();
$result['_parsed'] = '';
if ($result = $router->run($request_uri)) {
    if (isset($result['model']))
        $Model = ucfirst($result['model']);
    if (isset($result['action']))
        $Action = ucfirst($result['action']);
}

if (file_exists(PATH_APPLICATION . 'application/' . $Model . '.php')) {
    include(PATH_APPLICATION . 'application/' . $Model . '.php');
}
$Model .= 'Model';
$Action .= 'Action';
try {
    $object = new $Model();
    $object->$Action($result['_parsed']);
} catch (Exception $e) {
    echo "Exception: " . $e->getMessage() . "<br>";
    echo nl2br($e->getTraceAsString());
}
